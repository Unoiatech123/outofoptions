package com.unoiatech.outofoptions.model;

import com.unoiatech.outofoptions.model.User;

/**
 * Created by Unoiatech on 5/9/2017.
 */

public class Notification {
    private String jobId;
    private String senderId;
    private Long id;
    private String message;
    private String receiverId;
    private Long proposalId;
    private String timeSent;
    private String activity;
    private String isRead;
    private String jobTitle;
    private Long companyProposalId;
    private Long companyId;

    //Getter Setters

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public Long getCompanyProposalId() {
        return companyProposalId;
    }

    public void setCompanyProposalId(Long companyProposalId) {
        this.companyProposalId = companyProposalId;
    }

    public String getJobId ()
    {
        return jobId;
    }

    public void setJobId (String jobId)
    {
        this.jobId = jobId;
    }

    public String getSenderId ()
    {
        return senderId;
    }

    public void setSenderId (String senderId)
    {
        this.senderId = senderId;
    }

    public Long getId ()
    {
        return id;
    }

    public void setId (Long id)
    {
        this.id = id;
    }

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public String getReceiverId ()
    {
        return receiverId;
    }

    public void setReceiverId (String receiverId)
    {
        this.receiverId = receiverId;
    }

    public Long getProposalId ()
    {
        return proposalId;
    }

    public void setProposalId (Long proposalId)
    {
        this.proposalId = proposalId;
    }

    public String getTimeSent ()
    {
        return timeSent;
    }

    public void setTimeSent (String timeSent)
    {
        this.timeSent = timeSent;
    }

    public String getActivity ()
    {
        return activity;
    }

    public void setActivity (String activity)
    {
        this.activity = activity;
    }

    public String getIsRead ()
    {
        return isRead;
    }

    public void setIsRead (String isRead)
    {
        this.isRead = isRead;
    }
}
