package com.unoiatech.outofoptions.model;

import java.util.Date;

/**
 * Created by unoiaAndroid on 4/24/2017.
 */

public class BuyerReview {
    private Long id;
    private Long revieweeId;
    private String descriptin;
    private Float stars;
    private Integer qualityOfWork;
    private Integer skills;
    private Integer punctuality;
    private Integer communication;
    private Long reviewTime;
    private String isJobCompleted;
    private String hireAgain;
    Job job;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getRevieweeId() {
        return revieweeId;
    }

    public void setRevieweeId(Long revieweeId) {
        this.revieweeId = revieweeId;
    }

    public String getDescriptin() {
        return descriptin;
    }

    public void setDescriptin(String descriptin) {
        this.descriptin = descriptin;
    }

    public Float getStars() {
        return stars;
    }

    public void setStars(Float stars) {
        this.stars = stars;
    }

    public Integer getQualityOfWork() {
        return qualityOfWork;
    }

    public void setQualityOfWork(Integer qualityOfWork) {
        this.qualityOfWork = qualityOfWork;
    }

    public Integer getSkills() {
        return skills;
    }

    public void setSkills(Integer skills) {
        this.skills = skills;
    }

    public Integer getPunctuality() {
        return punctuality;
    }

    public void setPunctuality(Integer punctuality) {
        this.punctuality = punctuality;
    }

    public Integer getCommunication() {
        return communication;
    }

    public void setCommunication(Integer communication) {
        this.communication = communication;
    }

    public Long getReviewTime() {
        return reviewTime;
    }

    public void setReviewTime(Long reviewTime) {
        this.reviewTime = reviewTime;
    }

    public String getIsJobCompleted() {
        return isJobCompleted;
    }

    public void setIsJobCompleted(String isJobCompleted) {
        this.isJobCompleted = isJobCompleted;
    }

    public String getHireAgain() {
        return hireAgain;
    }

    public void setHireAgain(String hireAgain) {
        this.hireAgain = hireAgain;
    }

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }
}
