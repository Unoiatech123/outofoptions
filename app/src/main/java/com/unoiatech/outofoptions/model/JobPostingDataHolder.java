package com.unoiatech.outofoptions.model;

import android.net.Uri;

/**
 * Created by unoiatech on 4/21/2016.
 */
public class JobPostingDataHolder {
    private static JobPostingDataHolder dataObject = null;
    private String title;
    private String location;
    private String description;
    private int budget;
    private String startingDate;
    private String endingDate;
    private Double lat;
    private Double lng;
    private String date;
    private String[] imagePaths;
    private Uri[] imageUri;
    private String category;
    private Long timeline;
    private Long postingTime;
    private String jobStatus;
    private String cancellation_hours;
    private String individual;
    private Long userId;

    public JobPostingDataHolder() {
        // left blank intentionally
    }
    public static JobPostingDataHolder getInstance() {
        if (dataObject == null)
            dataObject = new JobPostingDataHolder();
        return dataObject;
    }

    public String getCancellation_hours() {
        return cancellation_hours;
    }

    public void setCancellation_hours(String cancellation_hours) {
        this.cancellation_hours = cancellation_hours;
    }

    public String getIndividual() {
        return individual;
    }

    public void setIndividual(String individual) {
        this.individual = individual;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String[] getImagePaths() {
        return imagePaths;
    }

    public void setImagePaths(String[] imagePaths) {
        this.imagePaths = imagePaths;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getBudget() {
        return budget;
    }

    public void setBudget(int budget) {
        this.budget = budget;
    }

    public String getStartingDate() {
        return startingDate;
    }

    public void setStartingDate(String startingDate) {
        this.startingDate = startingDate;
    }

    public String getEndingDate() {
        return endingDate;
    }

    public void setEndingDate(String endingDate) {
        this.endingDate = endingDate;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public Long getTimeline() {
        return timeline;
    }

    public void setTimeline(Long timeline) {
        this.timeline = timeline;
    }

    public Long getPostingTime() {
        return postingTime;
    }

    public void setPostingTime(Long postingTime) {
        this.postingTime = postingTime;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getJobStatus() {
        return jobStatus;
    }

    public void setJobStatus(String jobStatus) {
        this.jobStatus = jobStatus;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Uri[] getImageUri() {
        return imageUri;
    }

    public void setImageUri(Uri[] imageUri) {
        this.imageUri = imageUri;
    }
}
