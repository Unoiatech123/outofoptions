package com.unoiatech.outofoptions.model;

import java.util.ArrayList;

/**
 * Created by Unoiatech on 6/12/2017.
 */

public class BlockUser
{
    private Long blockedUserId;
    private String createdId;
    private Long userId;
    private ArrayList<User> blockedUsers;

    public ArrayList<User> getBlockedUsers() {
        return blockedUsers;
    }

    public void setBlockedUsers(ArrayList<User> blockedUsers) {
        this.blockedUsers = blockedUsers;
    }

    public Long getBlockedUserId() {
        return blockedUserId;
    }

    public void setBlockedUserId(Long blockedUserId) {
        this.blockedUserId = blockedUserId;
    }

    public String getCreatedId() {
        return createdId;
    }

    public void setCreatedId(String createdId) {
        this.createdId = createdId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
