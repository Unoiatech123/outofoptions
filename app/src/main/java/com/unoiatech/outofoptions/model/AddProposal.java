package com.unoiatech.outofoptions.model;

/**
 * Created by Unoiatech on 6/22/2017.
 */

public class AddProposal {
    private String completionMinutes;
    private String completionHours;
    private int offeredQuote;
    private String proposalDescription;
    private String postingTime;
    private String startingTime;
    private Long userID;

    public String getCompletionHours() {
        return completionHours;
    }

    public void setCompletionHours(String completionHours) {
        this.completionHours = completionHours;
    }

    public String getCompletionMinutes() {
        return completionMinutes;
    }

    public void setCompletionMinutes(String completionMinutes) {
        this.completionMinutes = completionMinutes;
    }

    public int getOfferedQuote() {
        return offeredQuote;
    }

    public void setOfferedQuote(int offeredQuote) {
        this.offeredQuote = offeredQuote;
    }

    public String getProposalDescription() {
        return proposalDescription;
    }

    public void setProposalDescription(String proposalDescription) {
        this.proposalDescription = proposalDescription;
    }

    public String getPostingTime() {
        return postingTime;
    }

    public void setPostingTime(String postingTime) {
        this.postingTime = postingTime;
    }

    public String getStartingTime() {
        return startingTime;
    }

    public void setStartingTime(String startingTime) {
        this.startingTime = startingTime;
    }

    public Long getUserID() {
        return userID;
    }

    public void setUserID(Long userID) {
        this.userID = userID;
    }
}
