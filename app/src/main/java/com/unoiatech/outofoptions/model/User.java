package com.unoiatech.outofoptions.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.List;

/**
 * Created by unoiaAndroid on 3/14/2017.
 */

public class User implements Parcelable{
    private Long id;
    private String email;
    private String phoneNo;
    private String userPassword;
    private String firstName;
    private String lastName;
    private Double lat;
    private Double lng;
    private String about;
    private String tagline;
    private String imagePath;
    private Long createdAt;
    private String deviceId;
    private String deviceType;
    private String name;
    private String addressType;
    private transient Integer quickbloxId;
    private String userType;
    private Company company;
    private Company companyUser;


    /****Reviews***/
    String average_review;
    private List<Reviews> seller_reviews;
    private List<Reviews> buyer_reviews;
    private String totalReviews;

    /**getJob***/
    private List<User> content;
    private Job job;
    private User user;
    private NotificationStatus notificationStatus;

    /***Account Verification***/
    private String verificationType;
    private int isVerified;


    //Company Proposal parameter
    private Long assignee;

    public User(){
    }

    public User(Parcel in) {
        firstName = in.readString();
        lastName = in.readString();
        imagePath = in.readString();
        assignee=in.readLong();

       /* email = in.readString();
        userPassword = in.readString();
        about = in.readString();
        tagline = in.readString();
        deviceId = in.readString();
        deviceType = in.readString();
        name = in.readString();
        addressType = in.readString();
        userType = in.readString();
        average_review = in.readString();
        totalReviews = in.readString();
        content = in.createTypedArrayList(User.CREATOR);
        job = in.readParcelable(Job.class.getClassLoader());
        user = in.readParcelable(User.class.getClassLoader());
        verificationType = in.readString();*/
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(firstName);
        parcel.writeString(lastName);
        parcel.writeString(imagePath);
        parcel.writeLong(assignee);

        /*parcel.writeString(email);
        parcel.writeString(userPassword);
        parcel.writeString(about);
        parcel.writeString(tagline);
        parcel.writeString(deviceId);
        parcel.writeString(deviceType);
        parcel.writeString(name);
        parcel.writeString(addressType);
        parcel.writeString(userType);
        parcel.writeString(average_review);
        parcel.writeString(totalReviews);
        parcel.writeTypedList(content);
        parcel.writeParcelable(job, i);
        parcel.writeParcelable(user, i);
        parcel.writeString(verificationType);*/
    }

    //getter Setter
    public Long getAssignee() {
        return assignee;
    }

    public void setAssignee(Long assignee) {
        this.assignee = assignee;
    }

    public Company getCompanyUser() {
        return companyUser;
    }

    public void setCompanyUser(Company companyUser) {
        this.companyUser = companyUser;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getVerificationType() {
        return verificationType;
    }

    public void setVerificationType(String verificationType) {
        this.verificationType = verificationType;
    }
    public int getIsVerified() {
        return isVerified;
    }

    public void setIsVerified(int isVerified) {
        this.isVerified = isVerified;
    }

    public NotificationStatus getNotificationStatus() {
        return notificationStatus;
    }

    public void setNotificationStatus(NotificationStatus notificationStatus) {
        this.notificationStatus = notificationStatus;
    }

    public String getAddressType() {
        return addressType;
    }

    public void setAddressType(String addressType) {
        this.addressType = addressType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getTagline() {
        return tagline;
    }

    public void setTagline(String tagline) {
        this.tagline = tagline;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo= phoneNo;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public Integer getQuickbloxId() {
        return quickbloxId;
    }

    public void setQuickbloxId(Integer quickbloxId) {
        this.quickbloxId = quickbloxId;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    public String getAverage_review() {
        return average_review;
    }

    public void setAverage_review(String average_review) {
        this.average_review = average_review;
    }

    public List<Reviews> getSeller_reviews() {
        return seller_reviews;
    }

    public void setSeller_reviews(List<Reviews> seller_reviews) {
        this.seller_reviews = seller_reviews;
    }

    public List<Reviews> getBuyer_reviews() {
        return buyer_reviews;
    }

    public void setBuyer_reviews(List<Reviews> buyer_reviews) {
        this.buyer_reviews = buyer_reviews;
    }

    public String getTotalReviews() {
        return totalReviews;
    }

    public void setTotalReviews(String totalReviews) {
        this.totalReviews = totalReviews;
    }

    public List<User> getContent() {
        return content;
    }

    public void setContent(List<User> content) {
        this.content = content;
    }

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
