package com.unoiatech.outofoptions.model;

import com.unoiatech.outofoptions.companymodule.CompanyProfile;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Unoiatech on 8/12/2017.
 */

public class CompanyProposal {
    private Long companyId;
    private String proposalDescription;
    private String isSenderAssigned;
    private int noOfAssignees;
    private String offeredTimeline;
    private String postingTime;
    private String startingTime;
    private int offeredQuote;
    private String completionHours;
    private String completionMinutes;
    private Long sentBy;
    private ArrayList<User> companyProposalAssignees;
    private CompanyProposal companyProposal;
    private Job job;


    //Getter Setter

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    public CompanyProposal getCompanyProposal() {
        return companyProposal;
    }

    public void setCompanyProposal(CompanyProposal companyProposal) {
        this.companyProposal = companyProposal;
    }

    public ArrayList<User> getCompanyProposalAssignees() {
        return companyProposalAssignees;
    }

    public void setCompanyProposalAssignees(ArrayList<User> companyProposalAssignees) {
        this.companyProposalAssignees = companyProposalAssignees;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public String getProposalDescription() {
        return proposalDescription;
    }

    public void setProposalDescription(String proposalDescription) {
        this.proposalDescription = proposalDescription;
    }

    public String getIsSenderAssigned() {
        return isSenderAssigned;
    }

    public void setIsSenderAssigned(String isSenderAssigned) {
        this.isSenderAssigned = isSenderAssigned;
    }

    public String getOfferedTimeline() {
        return offeredTimeline;
    }

    public void setOfferedTimeline(String offeredTimeline) {
        this.offeredTimeline = offeredTimeline;
    }

    public int getNoOfAssignees() {
        return noOfAssignees;
    }

    public void setNoOfAssignees(int noOfAssignees) {
        this.noOfAssignees = noOfAssignees;
    }

    public String getPostingTime() {
        return postingTime;
    }

    public void setPostingTime(String postingTime) {
        this.postingTime = postingTime;
    }

    public String getStartingTime() {
        return startingTime;
    }

    public void setStartingTime(String startingTime) {
        this.startingTime = startingTime;
    }

    public int getOfferedQuote() {
        return offeredQuote;
    }

    public void setOfferedQuote(int offeredQuote) {
        this.offeredQuote = offeredQuote;
    }

    public String getCompletionHours() {
        return completionHours;
    }

    public void setCompletionHours(String completionHours) {
        this.completionHours = completionHours;
    }

    public String getCompletionMinutes() {
        return completionMinutes;
    }

    public void setCompletionMinutes(String completionMinutes) {
        this.completionMinutes = completionMinutes;
    }

    public Long getSentBy() {
        return sentBy;
    }

    public void setSentBy(Long sentBy) {
        this.sentBy = sentBy;
    }

}
