package com.unoiatech.outofoptions.model;

/**
 * Created by unoiaAndroid on 5/26/2017.
 */

public class BuyerReviewWithUserInfo {
        private User user;
        private BuyerReview buyerReview;

        public User getUser() {
            return user;
        }

        public void setUser(User user) {
            this.user = user;
        }

        public BuyerReview getReview() {
            return buyerReview;
        }

        public void setReview(BuyerReview review) {
            this.buyerReview = buyerReview;
        }

}
