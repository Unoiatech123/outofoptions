package com.unoiatech.outofoptions.model;

import android.widget.LinearLayout;

import java.util.List;

/**
 * Created by Unoiatech on 7/24/2017.
 */

public class CompanyData {
    private Company company;
    private TransactionRequest transactionRequest;
    private List<User> members;
    private String jobs_completed;
    private String reviews_count;
    private User coordinator;
    private User owner;
    private User user;
    private TransactionRequest transaction;

    //Getter Setter
    public TransactionRequest getTransaction() {
        return transaction;
    }

    public void setTransaction(TransactionRequest transaction) {
        this.transaction = transaction;
    }

    public TransactionRequest getTransactionRequest() {
        return transactionRequest;
    }

    public void setTransactionRequest(TransactionRequest transactionRequest) {
        this.transactionRequest = transactionRequest;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<User> getMembers() {
        return members;
    }

    public void setMembers(List<User> members) {
        this.members = members;
    }

    public String getReviews_count() {
        return reviews_count;
    }

    public void setReviews_count(String reviews_count) {
        this.reviews_count = reviews_count;
    }

    public User getCoordinator() {
        return coordinator;
    }

    public void setCoordinator(User coordinator) {
        this.coordinator = coordinator;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public String getJobs_completed() {
        return jobs_completed;
    }

    public void setJobs_completed(String jobs_completed) {
        this.jobs_completed = jobs_completed;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }


}
