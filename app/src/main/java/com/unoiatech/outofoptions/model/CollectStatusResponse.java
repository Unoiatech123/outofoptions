package com.unoiatech.outofoptions.model;

/**
 * Created by unoiaAndroid on 4/6/2017.
 */

public class CollectStatusResponse {
    private String ocspResponse;
    private String signature;
    private String progressStatus;

    public String getOcspResponse() {
        return ocspResponse;
    }

    public void setOcspResponse(String ocspResponse) {
        this.ocspResponse = ocspResponse;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getProgressStatus() {
        return progressStatus;
    }

    public void setProgressStatus(String progressStatus) {
        this.progressStatus = progressStatus;
    }
}
