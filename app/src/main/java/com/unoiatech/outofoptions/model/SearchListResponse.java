package com.unoiatech.outofoptions.model;


import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;
import com.unoiatech.outofoptions.home.fragment.adapter.JobListResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Unoiatech on 4/4/2017.
 */
public class SearchListResponse
{
    private String average_review;
    private List seller_reviews;
    private User user;
    private List buyer_reviews;
    public String type;
    private ArrayList<SearchListResponse> content;
    private Proposal proposal;
    private Job job;
    private Integer number;
    private Integer numberOfElements=0;
    private Integer size;
    private Boolean last;
    private Integer totalPages;

    //Getter Setter
    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Integer getNumberOfElements() {
        return numberOfElements;
    }

    public void setNumberOfElements(Integer numberOfElements) {
        this.numberOfElements = numberOfElements;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Boolean getLast() {
        return last;
    }

    public void setLast(Boolean last) {
        this.last = last;
    }

    public Integer getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(Integer totalPages) {
        this.totalPages = totalPages;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }
    public Proposal getProposal() {
        return proposal;
    }

    public void setProposal(Proposal proposal) {
        this.proposal = proposal;
    }

    public ArrayList<SearchListResponse> getContent() {
        return content;
    }

    public void setContent(ArrayList<SearchListResponse> content) {
        this.content = content;
    }

    public SearchListResponse(String type)
    {
        this.type= type;
    }

    public List getSeller_reviews() {
        return seller_reviews;
    }

    public void setSeller_reviews(List seller_reviews) {
        this.seller_reviews = seller_reviews;
    }

    public String getTotalReviews() {
        return totalReviews;
    }

    public void setTotalReviews(String totalReviews) {
        this.totalReviews = totalReviews;
    }

    private String totalReviews;


    public String getAverage_review() {
        return average_review;
    }

    public void setAverage_review(String average_review) {
        this.average_review = average_review;
    }

    public List getBuyer_reviews() {
        return buyer_reviews;
    }

    public void setBuyer_reviews(List buyer_reviews) {
        this.buyer_reviews = buyer_reviews;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    class Image
    {
        private Long id;
        private String name;

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}