package com.unoiatech.outofoptions.model;

/**
 * Created by Unoiatech on 9/7/2017.
 */

public class Payment {
    private int amount;
    private Long transactionId;
    private Long jobId;
    private Long userId;
    private Customer customer;
    private Long id;
    private Long balance;
    private String lastMofidied;
    private Long creditBalance;

    //Getter n Setter
    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Long transactionId) {
        this.transactionId = transactionId;
    }

    public Long getJobId() {
        return jobId;
    }

    public void setJobId(Long jobId) {
        this.jobId = jobId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getBalance() {
        return balance;
    }

    public void setBalance(Long balance) {
        this.balance = balance;
    }

    public String getLastMofidied() {
        return lastMofidied;
    }

    public void setLastMofidied(String lastMofidied) {
        this.lastMofidied = lastMofidied;
    }

    public Long getCreditBalance() {
        return creditBalance;
    }

    public void setCreditBalance(Long creditBalance) {
        this.creditBalance = creditBalance;
    }
}
