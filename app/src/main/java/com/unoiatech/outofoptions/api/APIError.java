package com.unoiatech.outofoptions.api;

/**
 * Created by Unoiatech on 1/22/2018.
 */

public class APIError {

    private int statusCode;
    private String message;

    public APIError() {
    }

    public int status() {
        return statusCode;
    }

    public String message() {
        return message;
    }
}
