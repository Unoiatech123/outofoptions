package com.unoiatech.outofoptions.api;

import com.unoiatech.outofoptions.model.AcceptProposal;
import com.unoiatech.outofoptions.model.ActiveJobsResponse;
import com.unoiatech.outofoptions.model.ActiveProposalResponse;
import com.unoiatech.outofoptions.model.AddInstantJob;
import com.unoiatech.outofoptions.model.AddProposal;
import com.unoiatech.outofoptions.model.BlockUser;
import com.unoiatech.outofoptions.model.Company;
import com.unoiatech.outofoptions.model.CompanyData;
import com.unoiatech.outofoptions.model.CompanyProposal;
import com.unoiatech.outofoptions.model.Deals;
import com.unoiatech.outofoptions.model.DraftResponse;
import com.unoiatech.outofoptions.model.Job;
import com.unoiatech.outofoptions.model.Payment;
import com.unoiatech.outofoptions.model.Proposal;
import com.unoiatech.outofoptions.model.RequestJobData;
import com.unoiatech.outofoptions.model.JobWithUser;
import com.unoiatech.outofoptions.model.Login;
import com.unoiatech.outofoptions.model.NotificationResponse;
import com.unoiatech.outofoptions.model.NotificationStatus;
import com.unoiatech.outofoptions.model.PageDetail;
import com.unoiatech.outofoptions.model.PropsalResponse;
import com.unoiatech.outofoptions.model.RequestProposalData;
import com.unoiatech.outofoptions.model.Review;
import com.unoiatech.outofoptions.model.SearchJobsRequest;
import com.unoiatech.outofoptions.model.SearchListResponse;
import com.unoiatech.outofoptions.model.SearchMapResponse;
import com.unoiatech.outofoptions.model.SearchRequest;
import com.unoiatech.outofoptions.model.SellerTransaction;
import com.unoiatech.outofoptions.model.ShopData;
import com.unoiatech.outofoptions.model.SwishCustomer;
import com.unoiatech.outofoptions.model.TransactionRequest;
import com.unoiatech.outofoptions.model.User;
import com.unoiatech.outofoptions.model.UserRating;
import com.unoiatech.outofoptions.model.VerifyBankIdResponse;


import java.util.ArrayList;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by unoiaAndroid on 3/18/2017.
 */

public interface ApiInterface {

    /* user urls*/
    @POST(URL.REGISTER_URL)
    Call<Long> registerUser(@Path("userId") Long userId,@Body User user);

    @POST(URL.LOGIN_URL)
    Call<User> loginUser(@Body User user);

    @GET(URL.GET_USER_RATING)
    Call<UserRating> userRating(@Path("userId") Long userId);

    @Multipart
    @POST(URL.UPLOAD_USER_IMAGE_TO_S3_URL)
    Call<Void> uploadImage(@Part MultipartBody.Part file,@Path("userId") Long userId);


    /******* SIGN IN USING BANKID *****/
    @GET(URL.SIGN_IN_WITH_BANKID)
    Call<VerifyBankIdResponse> bankIdSignIn();

    /* job urls*/
    @GET(URL.FIND_JOB_BY_ID)
    Call<JobWithUser> findJobById(@Path("jobId") Long jobId);


    @POST(URL.SEARCH_JOBS_FOR_USER_ID)
    Call<PageDetail> searchJobForUserId(@Body SearchRequest searchRequest, @Path("userId") Long userId);


   /* verify urls*/
   /* @GET(URL.VERIFY_BANK_ID_URL_WITH_ID_NUMBER)
    Call<VerifyBankIdResponse> verifyBankIdWithIdNumber(@Path("idNumber") String idNumber,@Path("userId") Long userId);

    @POST(URL.VERIFY_BANK_ID_URL_WITHOUT_ID_NUMBER)
    Call<VerifyBankIdResponse> verifyBankIdWithoutIdNumber(@Path("userId") Long userId);*/

    @GET(URL.COLLECT_STATUS_URL)
    Call<User> collectStatus(@Path("orderRef") String orderRef);


    /*******Verify Ids*********/
    @POST(URL.VERIFY_EMAIL_REQUEST_URL)
    Call<Void> verifyEmail(@Body Login login);

    /***********VerifyId via Facebook***********/
    @POST(URL.VERIFY_FACEBOOK_URL)
    Call<ResponseBody> verifyIdViaFacebook(@Body Login login);

    @POST(URL.REGISTER_GCM_ID_URL+"{userId}")
    Call<Void> registerDeviceId(@Path("userId") Long userId, @Body User user);

    /******Job Url
     * @param dataHolder************/
    @POST(URL.ADD_INSTANT_JOB)
    Call<Long> addInstantJob(@Body AddInstantJob dataHolder);

    @Multipart
    @POST(URL.UPLOAD_JOB_IMAGES)
    Call<Void> uploadJobImage(@Part MultipartBody.Part file,@Path("jobId") Long jobId,@Path("userId") Long userId);

    @GET(URL.GET_POSTED_JOB)
    Call<ArrayList<ActiveJobsResponse>>getPostedJobs(@Path("userId") Long userId);

    @GET(URL.GET_EXTRA_JOB_DATA)
    Call<PropsalResponse> getExtraJobData(@Path("jobId") Long job_id);

    @POST(URL.GET_JOB_ON_MAP)
    Call<ArrayList<SearchMapResponse>> getJobsOnMap(@Path("userId") Long userId, @Body SearchJobsRequest jobs);

    @POST(URL.SEARCH_JOBS_FOR_USER_ID)
    Call<SearchListResponse> getJobsOnList(@Path("userId") Long userId, @Body SearchRequest jobs);

    @POST(URL.CANCEL_PENDING_JOB)
    Call<ResponseBody>markPendingJobCancel(@Body RequestJobData job);

    @POST(URL.CANCEL_JOBS)
    Call<ResponseBody> cancelActiveJob(@Path("jobId") Long jobId, @Path("proposalId") Long proposalId, @Body Job job);

    @GET(URL.MARK_JOB_COMPLETED)
    Call<ResponseBody> markJobCompleted(@Path("id") Long jobId, @Path("proposalId") Long proposalId, @Query("time") long time);


    @POST(URL.SAVE_JOB_AS_DRAFT)
    Call<Long> saveAsDraft(@Path("userId")Long userId, @Body AddInstantJob data);

    @POST(URL.DELETE_JOB_PERMANENTLY)
    Call<ResponseBody> deleteJobPermanently(@Path("jobId") Long jobId, @Path("userId") Long userId);

    @POST(URL.REMOVE_DRAFT)
    Call<ResponseBody> removeDraft(@Path("draftId") Long id);


    /**********Users Url********************/
    @POST(URL.LOGOUT_USER_URL)
    Call<Void> logoutUser(@Path("deviceId") String deviceId);

    @POST(URL.ADD_ADDRESS)
    Call<Void> addAddresses(@Path("userId") Long userId, @Body User user);

    @POST(URL.USER_INFO)
    Call<User> getUserProfile(@Path("userId") Long userId);

    @GET(URL.VERIFY_IDS_ON_PROFILE)
    Call<ArrayList<User>> verifyIdsOnProfile(@Path("userId") Long userId);

    @GET(URL.GET_USER_REVIEWS_ON_PROFILE)
    Call<User> getUserReviews(@Path("userId") Long userId);

    @PUT(URL.EDIT_USER_PROFILE)
    Call<Void> editUserProfile(@Path("userId") Long userId, @Body User user);

    @GET(URL.GET_DRAFTED_JOB)
    Call<ArrayList<DraftResponse>> getDraftedJobData(@Path("userId") Long userId);

    @POST(URL.BLOCK_USER)
    Call<ResponseBody> blockUser(@Body BlockUser blockUser);

    @GET(URL.GET_BLOCKED_USER)
    Call<BlockUser> getBlockedUsers(@Path("userId") Long id);

    @POST(URL.UNBLOCK_USER)
    Call<ResponseBody> unBlockUser(@Body BlockUser blockUser);

    @GET(URL.GET_ADDRESS_LIST)
    Call<ArrayList<User>> getAddressList(@Path("userId")Long userId);

    /*****BUYER_REVIEWS_URL**********/
    @POST(URL.ADD_BUYER_REVIEWS_FOR_JOB)
    Call<ResponseBody> addBuyerReviews(@Path("jobId")Long jobId, @Body Review addReviews);


    /********REVIEWS URL*********/
    @POST(URL.ADD_REVIEWS_FOR_JOB)
    Call<ResponseBody> addReviews(@Path("jobId") Long jobId, @Body Review addReviews);

    /**********Add Proposal***************/
    @POST(URL.ADD_PROPOSAL)
    Call<ResponseBody> addProposal(@Path("jobId") Long jobId, @Body AddProposal proposal);

    @POST(URL.ACCEPT_PROPOSAL)
    Call<ResponseBody> acceptProposal(@Body AcceptProposal proposal);

    @GET(URL.SEARCH_PROPOSALS)
    Call<ArrayList<ActiveProposalResponse>> getProposal(@Path("userId") Long userId);

    @GET(URL.WITHDRAW_PROPOSAL)
    Call<ResponseBody> removeProposal(@Path("id") Long proposalId);

    @POST(URL.CONFIRM_PROPOSAL)
    Call<ResponseBody> markProposalConfirmed(@Path("proposalId") Long proposalId, @Body RequestProposalData proposal);

    @POST(URL.FIND_JOB_BY_CATEGORY)
    Call<SearchListResponse> findJobByCategory(@Body Map<String,Object> requestMap, @Path("userId") Long userId);

    @GET(URL.FIND_PROPOSAL)
    Call<Proposal> findProposal(@Path("proposalId") Long proposalId);

    @GET(URL.MARK_PROPOSAL_COMPLETED)
    Call<ResponseBody> markProposalCompleted(@Path("id") Long proposalId, @Path("userId") Long userId, @Query("time") long time);

    /*****NOTIFICATION URL*****/
    @GET(URL.GET_NOTIFICATION_DATA)
    Call<NotificationResponse> getNotificationData(@Path("userId") Long userId, @Path("pageNo")String pageNo);

    @PUT(URL.UPDATE_NOTIFICATION_STATUS)
    Call<ResponseBody> updateNotificationStatus(@Path("userId") Long userId, @Body NotificationStatus notification);

   /* @FormUrlEncoded
    @POST(URL.MARK_NOTIFICATION_AS_READ)
    Call<ResponseBody> markNotificationAsRead(@Field("notificationIds[]") ArrayList<Long> val);*/

    @POST(URL.MARK_NOTIFICATION_AS_READ)
    Call<ResponseBody> markNotificationAsRead(@Body ArrayList<Long> val);

    @GET(URL.GET_CATEGORIZED_NOTIFICATION)
    Call<NotificationResponse> getCategorizedNotification(@Path("receiverId") Long receiverId);


    //******************Company Module Urls ***************************
    @GET(URL.FETCH_INFO_FROM_ALLABOLAG)
    Call<Company> fetchInfoFromAllabolag(@Path("orgNumber")String orgNumber,@Path("userId")Long userId);

    @POST(URL.REGISTER_COMPANY_DATA)
    Call<Company> registerCompanyData(@Body Company companyData, @Path("userId") Long userId);

    @Multipart
    @POST(URL.ADD_COMPANY_LOGO)
    Call<Void> addCompanyLogo(@Part MultipartBody.Part file, @Path("companyId") Long companyId);

    @GET(URL.GET_COMPANY_PROFILE)
    Call<CompanyData> getCompanyProfile(@Path("companyId")Long companyId);

    @PUT(URL.EDIT_COMPANY_PROFILE)
    Call<Void> editCompanyProfile(@Path("companyId") Long companyId, @Body Company company);

    @GET(URL.REMOVE_COMPANY_MEMBER)
    Call<Void> removeCompanyMember(@Path("userId") Long userId,@Path("companyId") Long companyId);

    @POST(URL.INVITE_STAFF_MEMBER)
    Call<Void> inviteStaffMember(@Path("userId") Long userId,@Body Company company);

    @GET(URL.UNASSIGN_COORDINATOR_ROLE)
    Call<Void> unassignedCoordinatorRole(@Path("userId") Long userId,@Path("companyId") Long companyId,@Path("companyUserId") Long ownerId);

    @GET(URL.ASSIGN_COORDINATOR_ROLE)
    Call<Void> assignCoordinatorRole(@Path("userId") Long userId,@Path("companyId")Long companyId,@Path("companyUserId") Long ownerId);

    @GET(URL.INVITE_USER_RESPONSE)
    Call<Void> inviteUserResponse(@Path("response") String response,@Path("companyId") Long companyId,@Path("userId")Long userId);


    /************Admin Module URLS******************/
    @GET(URL.GET_COMPANY_REQUEST)
    Call<ArrayList<CompanyData>> getCompanyRequest();

    @GET(URL.GET_COMPANY_DETAILS)
    Call<CompanyData> getCompanyDetails(@Path("companyId")Long companyId);

    @POST(URL.COMPANY_REQUEST_RESPONSE)
    Call<ResponseBody> companyRequestResponse(@Path("companyId") Long companyId,@Body Company company);

    @GET(URL.GET_COMPANY_RECORDS)
    Call<ArrayList<CompanyData>> getCompanyRecords();

    /************ADMIN MODULE Payments Tab URLS**************************/
    @GET(URL.GET_WITHDRAWAL_PAYMENT_LIST)
    Call<ArrayList<CompanyData>> getWithdrawalPaymentList();

    @POST(URL.WITHDRAWAL_REQUEST_RESPONSE)
    Call<ResponseBody> withdrawalRequestResponse(@Path("response")String response, @Path("sellerTransactionId") Long id, @Body TransactionRequest body);

    @GET(URL.GET_PAYMENT_RECORDS)
    Call<ArrayList<CompanyData>> getPaymentRecords();


    /***********COMPANY MODULE ***********/
    @POST(URL.ADD_COMPANY_PROPOSAL)
    Call<ResponseBody> addCompanyProposal(@Path("jobId")Long jobId, @Body CompanyProposal proposal);

    /**********COMPANY REVIEW************/
    @GET(URL.GET_COMPANY_PROPOSAL_DETAIL)
    Call<CompanyProposal> getCompanyProposalDetail(@Path("companyProposalId") Long proposalId);


    //************* STRIPE URLS*************************/
    @POST(URL.STRIPE_CREATING_CHARGES)
    Call<ResponseBody> creatingCharges(@Body Payment payment);


    /*********PAYMENT URLS**********/
    @POST(URL.SUBMIT_PAYMENT_TO_O3_ESCROW)
    Call<Payment> submitPaymentToO3Escrow(@Body Payment payment);

    @GET(URL.GET_USER_WALLET)
    Call<Payment> getUserWallet(@Path("userId") Long userId);

    @GET(URL.GET_USER_CREDITS)
    Call<Payment> getUserCredit(@Path("userId")Long userId);

    @POST(URL.WITHDRAWAL_FUND_REQUEST)
    Call<ResponseBody> withdrawFundRequest(@Body SellerTransaction sellerTransaction);

    @POST(URL.REDEEM_CREDIT_TO_WALLET)
    Call<ResponseBody> redeemCreditToWallet(@Body Payment payment);


    /****************** SWISH URLS ************************/
    @POST(URL.CONNECT_SWISH)
    Call<Void> connectSwish(@Body SwishCustomer customer);

    @GET(URL.DISCONNECT_SWISH)
    Call<Void> disconnectSwish(@Path("userId") Long userId);

    @GET(URL.GET_SWISH_CUSTOMER)
    Call<SwishCustomer> getSwishCustomer(@Path("userId") Long userId);


    /**********SHOP REGISTRATION ********/
    @POST(URL.REGISTER_SHOP)
    Call<ShopData> registerShop(@Body ShopData data);

    @Multipart
    @POST(URL.UPLOAD_SHOP_IMAGES)
    Call<ResponseBody> uploadShopImages(@Part MultipartBody.Part file,@Path("shopId") Long shopId,@Path("userId") Long userId);

    @GET(URL.GET_SHOP_DETAILS)
    Call<ShopData> getShopDetails(@Path("shopId") Long shopId);

    /******** CREATE DEAL ********/
    @POST(URL.CREATE_DEAL)
    Call<ResponseBody> createDeal(@Body Deals deal);

    @GET(URL.GET_DEALS_LIST)
    Call<ArrayList<Deals>> getDealsList();

    @GET(URL.GET_DEAL_DETAILS)
    Call<Deals> getDealDetails(@Path("id") Long Id);

}
