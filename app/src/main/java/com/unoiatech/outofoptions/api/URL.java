package com.unoiatech.outofoptions.api;

import okhttp3.internal.connection.StreamAllocation;

/**
 * Created by unoiaAndroid on 3/15/2017.
 */

public class URL {

    //online
    // private static String BASE_URL="https:oceal.info/";

    //local
    public static final String BASE_URL="http://192.168.100.28:8080/";


    // *********  USERS URLS ************
    public static final String LOGIN_URL="/users/login";
    //public static final String REGISTER_URL="/users/add";
    public static final String REGISTER_URL="/users/updateEmail/{userId}";

    //SignInwithBankId
    public static final String SIGN_IN_WITH_BANKID="/users/signInWithBankId/";

    // users/registerDeviceToken/forUser/{userId}
    public static final String REGISTER_GCM_ID_URL="/users/registerDeviceToken/forUser/";

    // users/user/{userId}
   // public static final String USER_INFO_URL="/users/user/{userId}";
    public static final String USER_INFO="/users/userWithDetails/{userId}";

   // users/{userId}/S3upload
   public static final String UPLOAD_USER_IMAGE_TO_S3_URL="/users/{userId}/S3upload";

    //users/rating/{userId}
    public static final String GET_USER_RATING_URL="/users/rating/";

    //users/rating/{userId}
    public static final String GET_USER_RATING="/users/rating/{userId}";

   //users/logoutUser/{deviceId}
    public static final String LOGOUT_USER_URL="/users/logoutUser/{deviceId}";

   //users/listDrafts/forUser/{userId}
   public static final String GET_DRAFTED_JOB="/users/listDrafts/forUser/{userId}";

   //users/blockUser
   public static final String BLOCK_USER="/users/blockUser";

   //users/blockedUsers/{userId}
   public static final String GET_BLOCKED_USER="users/blockedUsers/{userId}";

   //users/unblockUser
   public static final String UNBLOCK_USER="users/unblockUser";

    //users/listAddress/forUser/{userId}
    public static final String GET_ADDRESS_LIST="/users/listAddress/forUser/{userId}";


    // ******* JOBS URLS *********
    //jobs/findByCategoryContaining/forUser/{userId}
    public static final String FIND_JOB_BY_CATEGORY="/jobs/findByCategoryContaining/forUser/{userId}";

    //jobs/searchJobs/{userId}
    public static final String SEARCH_JOBS_FOR_USER_ID="/jobs/searchJobsForAndroid/{userId}";

    //jobs/job/{jobId}
    public static final String FIND_JOB_BY_ID="/jobs/job/{jobId}";

   //jobs/deleteJobPermanently/{jobId}/by/{userId}
   public static final String DELETE_JOB_PERMANENTLY="/jobs/deleteJobPermanently/{jobId}/by/{userId}";

   //jobs/removeDraft/{draftId}
   public static final String REMOVE_DRAFT="/jobs/removeDraft/{draftId}";

   public static final String SAVE_JOB_AS_DRAFT="/jobs/saveJobAsDraft/forUser/{userId}";


    // ******** VERIFY ID URLS ***********
    public static final String VERIFY_FACEBOOK_URL=BASE_URL+"/verifiedIds/verifyFacebook";
    public static final String VERIFY_EMAIL_REQUEST_URL= BASE_URL+"/verifiedIds/verifyEmailRequest";
    /*public static final String VERIFY_BANK_ID_URL_WITH_ID_NUMBER="/verifiedIds/verifyBankId/{idNumber}/user/{userId}";
    public static final String VERIFY_BANK_ID_URL_WITHOUT_ID_NUMBER="/verifiedIds/verifyBankId/{userId}";
    public static final String COLLECT_STATUS_URL="/verifiedIds/collectStatus/{orderRef}/by/{verfiedId}";*/

    public static final String COLLECT_STATUS_URL="/verifiedIds/collectStatusForSign/{orderRef}/";
    public static final String VERIFY_IDS_ON_PROFILE="verifiedIds/{userId}/findByUser";


    //users/edit/{userId}
    public static final String EDIT_USER_PROFILE="users/edit/{userId}";


    // ******* JOBS URLS *********
    public static final String ADD_INSTANT_JOB="/jobs/add";

    public static final String UPLOAD_JOB_IMAGES="/jobs/{jobId}/uploadedBy/{userId}";

    public static final String GET_POSTED_JOB="/jobs/{userId}/listJobsPostedForAndroid";

    public static final String GET_EXTRA_JOB_DATA="/jobs/jobWithExtraDataForAndroid/{jobId}";

    //jobs/searchJobsWithoutPaging/{userId}
    public static final String GET_JOB_ON_MAP="/jobs/searchJobsForAndroidWithoutPaging/{userId}";

    //jobs/searchJobs/{userId}
    public static final String GET_JOB_ONSEARCH_LIST="/jobs/searchJobs/{userId}";


    //users/addAddress/forUser/{userId}
    public static final String ADD_ADDRESS="users/addAddress/forUser/{userId}";

    public static final String CANCEL_PENDING_JOB="/jobs/markPendingJobCancelled/";

    //jobs/cancelJob/{jobId}/forProposal/{proposalId}
    public static final String CANCEL_JOBS="/jobs/cancelJob/{jobId}/forProposal/{proposalId}";

    public static final String MARK_JOB_COMPLETED="/jobs/{id}/markJobCompleted/forProposal/{proposalId}";


    // ********* PROPOSALS URLS *******
    //proposals/addProposal/forJobId/{jobId}
    public static final String ADD_PROPOSAL="/proposals/addProposal/forJobId/{jobId}";

    public static final String ACCEPT_PROPOSAL ="/proposals/buyerResponse";

    //proposals/{userId}/listProposals
    public static final String SEARCH_PROPOSALS="/proposals/{userId}/listProposalsForAndroid";

    //proposals/{id}/removeProposal
    public static final String WITHDRAW_PROPOSAL="/proposals/{id}/removeProposal";

    //proposals/markProposalAsConfirmed/{proposalId}
    public static final String CONFIRM_PROPOSAL="/proposals/markProposalAsConfirmed/{proposalId}";

    //proposals/{proposalId}/findProposal
    public static final String FIND_PROPOSAL="/proposals/{proposalId}/findProposal";

    //proposals/{id}/markProposalCompleted/forUser/{userId}
    public static final String MARK_PROPOSAL_COMPLETED="/proposals/{id}/markProposalCompleted/forUser/{userId}";


    /********NOTIFICATION URLS************/
    public static final String GET_NOTIFICATION_DATA="/users/getNotifications/{userId}/pageNo/{pageNo}";

    public static final String UPDATE_NOTIFICATION_STATUS="/users/updateNotificationStatus/{userId}";

    public static final String MARK_NOTIFICATION_AS_READ="/users/markNotificationAsRead/";

    public static final String GET_CATEGORIZED_NOTIFICATION="/users/getCategorizedNotificationsForUser/{receiverId}";

    //********* PAYMENTS URLS *********


    // ********* BUYERS REVIEW URLS ******
    //buyerReviews/add/forJob/{jobId}
    public static final String ADD_BUYER_REVIEWS_FOR_JOB="/buyerReviews/add/forJob/{jobId}";


    // ******** REVIEW URLS ***********
    public static final String GET_USER_REVIEWS_ON_PROFILE="users/rating/{userId}";

    public static final String ADD_REVIEWS_FOR_JOB="/reviews/add/forJob/{jobId}";


    //******COMPANY URLS**************
    public static final String FETCH_INFO_FROM_ALLABOLAG="/business/registerCompany/{orgNumber}/byUser/{userId}";

    public static final String REGISTER_COMPANY_DATA="/business/updateManualFills/{userId}";

    public static final String ADD_COMPANY_LOGO="/business/uploadLogo/{companyId}";

    public static final String GET_COMPANY_PROFILE ="/business/companyProfile/{companyId}";

    public static final String EDIT_COMPANY_PROFILE="/business/edit/{companyId}";

    public static final String INVITE_STAFF_MEMBER="/business/inviteUserRequestBy/{userId}";

    public static final String REMOVE_COMPANY_MEMBER="/business/removeMember/{userId}/fromCompany/{companyId}";

    public static final String ASSIGN_COORDINATOR_ROLE="/business/assignCoordinatorRole/toUser/{userId}/inCompany/{companyId}/byUser/{companyUserId}";

    public static final String UNASSIGN_COORDINATOR_ROLE="/business/unassignCoordinatorRole/toUser/{userId}/inCompany/{companyId}/byUser/{companyUserId}";

    public static final String INVITE_USER_RESPONSE="/business/inviteUserResponse/{response}/forCompany/{companyId}/byUser/{userId}";


    /****************ADMIN MODULE URLS*********************************/
    public static final String GET_COMPANY_DETAILS ="/business/companyDetails/{companyId}";

    public static final String GET_COMPANY_REQUEST="/business/listRegisterCompanyRequests/";

    public static final String COMPANY_REQUEST_RESPONSE="/business/registerCompanyResponse/{companyId}";

    public static final String GET_COMPANY_RECORDS="/business/listAllCompanyRecords/";


    /******************ADMIN MODULE PAYMENT URLS*********************/
    public static final String GET_WITHDRAWAL_PAYMENT_LIST="/payments/listWithdrawFundsRequests";

    public static final String WITHDRAWAL_REQUEST_RESPONSE="/payments/withdrawFundRequestResponse/{response}/forId/{sellerTransactionId}";

    public static final String GET_PAYMENT_RECORDS="/payments/listTransactionRecord";


    /****************COMPANY PROPOSAL***********/
    public static final String ADD_COMPANY_PROPOSAL="/companyProposal/addProposal/for/{jobId}";

    public static final String GET_COMPANY_PROPOSAL_DETAIL="/companyProposal/companyProposalDetails/{companyProposalId}";


    /***********PAYMENT URLS***********/
    public static final String SUBMIT_PAYMENT_TO_O3_ESCROW="/payments/submitPaymentToO3Escrow";

    public static final String GET_USER_WALLET="/payments/userWallet/{userId}";

    public static final String GET_USER_CREDITS="/payments/userCredit/{userId}";

    public static final String WITHDRAWAL_FUND_REQUEST="/payments/withdrawFundsFromO3Escrow";

    public static final String REDEEM_CREDIT_TO_WALLET="/payments/redeemO3CreditToO3Wallet";


    /**************STRIPE URLS***********/
    public static final String STRIPE_CREATING_CHARGES="/stripe/creatingCharges";

    public static final String STRIPE_CUSTOMER="/stripe/customer/{userId}";

    public static final String STRIPE_CUSTOMER_SOURCE="/stripe/customer/{userId}/sources";

    public static final String STRIPE_CUSTOMER_UPDATE_SOURCE="/stripe/customer/{userId}/updateSources";


    /**********SWISH_URLS*****************/
    public static final String CONNECT_SWISH="/swish/connectSwish";

    public static final String DISCONNECT_SWISH="/swish/disconnectSwish/{userId}";

    public static final String GET_SWISH_CUSTOMER="/swish/getSwishCustomer/{userId}";


    /*******REGISTER SHOP URLS**********/

    public static final String REGISTER_SHOP="/shops/registerShop/";

    public static final String UPLOAD_SHOP_IMAGES="/shops/{shopId}/uploadedBy/{userId}";

    public static final String GET_SHOP_DETAILS="/shops/detail/{shopId}";

    public static final String IS_SHOP_REGISTERED="/shops/isShopRegistered/{userId}";


    /********TO CREATE DEAL URLS*********/

    public static final String CREATE_DEAL="/deals/deal/";

    public static final String GET_DEALS_LIST="/deals/deal";

    public static final String GET_DEAL_DETAILS="/deals/deal/details/{id}";

}
