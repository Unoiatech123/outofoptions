package com.unoiatech.outofoptions.api;

import com.unoiatech.outofoptions.util.NullOnEmptyConverterFactory;
import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import static com.unoiatech.outofoptions.api.URL.BASE_URL;

/**
 * Created by unoiaAndroid on 3/18/2017.
 */

public class ApiClient {
    private static Retrofit retrofit = null;

    /*static Gson gson = new GsonBuilder()
            .setLenient()
            .create();*/

    public static Retrofit getClient() {
        if (retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(new NullOnEmptyConverterFactory())
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(ApiClient.okClient())
                    .build();
        }
        return retrofit;
    }

    private static OkHttpClient okClient() {
        return new OkHttpClient.Builder()
                .connectTimeout(2, TimeUnit.MINUTES)
                .writeTimeout(1, TimeUnit.MINUTES)
                .readTimeout(1, TimeUnit.MINUTES)
                .build();
    }
}
