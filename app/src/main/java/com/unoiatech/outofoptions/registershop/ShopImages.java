package com.unoiatech.outofoptions.registershop;

/**
 * Created by Unoiatech on 1/29/2018.
 */

public class ShopImages {

    private String name;
    private String path;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    private Long id;


}
