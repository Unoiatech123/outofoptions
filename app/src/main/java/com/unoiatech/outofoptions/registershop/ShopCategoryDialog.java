package com.unoiatech.outofoptions.registershop;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.unoiatech.outofoptions.R;

/**
 * Created by Unoiatech on 11/27/2017.
 */

public class ShopCategoryDialog extends DialogFragment implements View.OnClickListener {
    private static ShopCategoryDialog category;
    private TextView salon,restaurant,others,cancel;

    public static ShopCategoryDialog newInstance() {
        if(category==null){
            category= new ShopCategoryDialog();
        }
        return category;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.select_shop_category,container,false);

        initView(view);
        return view;
    }

    private void initView(View view) {
        salon=(TextView)view.findViewById(R.id.salon_text);
        restaurant=(TextView)view.findViewById(R.id.restaurant_text);
        others=(TextView)view.findViewById(R.id.others);
        cancel=(TextView)view.findViewById(R.id.cancel);

        //OnClickListeners
        cancel.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.cancel:
                getActivity().getSupportFragmentManager().popBackStack();
                break;
        }
    }
}
