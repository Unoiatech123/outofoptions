package com.unoiatech.outofoptions.registershop;

import com.unoiatech.outofoptions.model.BusinessHour;
import com.unoiatech.outofoptions.model.User;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Unoiatech on 1/29/2018.
 */

public class Shop{
       private String about;
       private ArrayList<BusinessHour> businessHourss;
       private String category;
       private Date createdAt;
       private Long id;
       private String imageUrl;
       private ArrayList<ShopImages> imageses;
       private Boolean isUpdateRequested;
       private Double lat;
       private Double lng;
       private String location;
       private String name;
       private String phoneNumber;
       private String status;
       private String telephoneNumber;
       private User user;
       private String website;

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public ArrayList<BusinessHour> getBusinessHourss() {
        return businessHourss;
    }

    public void setBusinessHourss(ArrayList<BusinessHour> businessHourss) {
        this.businessHourss = businessHourss;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public ArrayList<ShopImages> getImageses() {
        return imageses;
    }

    public void setImageses(ArrayList<ShopImages> imageses) {
        this.imageses = imageses;
    }

    public Boolean getUpdateRequested() {
        return isUpdateRequested;
    }

    public void setUpdateRequested(Boolean updateRequested) {
        isUpdateRequested = updateRequested;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }
}
