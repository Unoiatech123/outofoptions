package com.unoiatech.outofoptions.registershop;

import com.unoiatech.outofoptions.api.ApiClient;
import com.unoiatech.outofoptions.api.URL;
import com.unoiatech.outofoptions.menu.MenuService;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Unoiatech on 1/29/2018.
 */

public class ShopService {


    public interface ShopAPI{

        @GET(URL.REGISTER_SHOP)
        Call<Void> registerShop(@Body Shop shop);


    }

    public ShopService.ShopAPI getAPI(){
        ShopService.ShopAPI apiService = ApiClient.getClient().create(ShopService.ShopAPI.class);
        return apiService;
    }
}
