package com.unoiatech.outofoptions.registershop;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.app.NotificationCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.api.ApiClient;
import com.unoiatech.outofoptions.api.ApiInterface;
import com.unoiatech.outofoptions.fragments.instantjob.AddVideoFragment;
import com.unoiatech.outofoptions.model.BusinessHour;
import com.unoiatech.outofoptions.model.JobPostingDataHolder;
import com.unoiatech.outofoptions.model.ShopData;
import com.unoiatech.outofoptions.util.ApiError;
import com.unoiatech.outofoptions.util.BankId.ErrorUtils;
import com.unoiatech.outofoptions.util.common.activities.MyFileContentProvider;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;

import jp.wasabeef.glide.transformations.CropSquareTransformation;
import jp.wasabeef.glide.transformations.RoundedCornersTransformation;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Unoiatech on 11/9/2017.
 */
public class ShopImagesFragment extends Fragment implements View.OnClickListener {
    private TabSelectorListener mCallBack;
    private FrameLayout submitButton,backButton;
    private ImageButton addImages;
    private CheckBox confirmCheck;
    private ArrayList<Uri> imagesList =new ArrayList();
    public static final int GET_FROM_GALLERY = 3;
    public  static  final  int GET_FROM_CAMERA=2;
    private Uri selectedImage;
    private RecyclerView mRecyclerView;
    private MyAdapter mAdapter;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mCallBack = (TabSelectorListener) getActivity();
        }
        catch (ClassCastException ex){
            ex.printStackTrace();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.shop_images_layout,container,false);
        initView(view);

        //for tab selection
        mCallBack.unselectImagesTab();
        return view;
    }

    private void initView(View view) {
        submitButton=(FrameLayout) view.findViewById(R.id.submit_btn);
        backButton=(FrameLayout)view.findViewById(R.id.back_button);
        confirmCheck=(CheckBox)view.findViewById(R.id.confirm);
        addImages=(ImageButton)view.findViewById(R.id.add_images);
        mRecyclerView=(RecyclerView)view.findViewById(R.id.shop_images);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);
        mAdapter = new MyAdapter(getActivity(),imagesList);
        mRecyclerView.setAdapter(mAdapter);

        //Click Listeners
        submitButton.setOnClickListener(this);
        addImages.setOnClickListener(this);
        backButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.submit_btn:
                if (confirmCheck.isChecked()){
                    submitShopDetails();
                }else {
                    notifyUser(getString(R.string.confirm_detail_text));
                }
                break;
            case R.id.back_button:
                FragmentTabHost mhost = (FragmentTabHost)getActivity().findViewById(R.id.fragment_tabhost);
                mhost.setCurrentTab(1);
                mCallBack.unselectImagesTab();
                break;
            case R.id.add_images:
                addShopImages();
                break;
        }
    }

    private void addShopImages() {
        InputMethodManager inputmanager= (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputmanager.hideSoftInputFromWindow(getView().getWindowToken(),0);
        if(imagesList.size()<=4) {
            dialogToAddImages();
        }
        else {
            notifyUser("Cannot add more than 5 images");
        }
    }

    private void dialogToAddImages() {
        View view = getActivity().getLayoutInflater().inflate (R.layout.add_images_dialog_layout, null);
        TextView fromGallery = (TextView)view.findViewById( R.id.gallery);
        TextView fromCamera = (TextView)view.findViewById( R.id.camera);
        TextView cancel=(TextView)view.findViewById(R.id.cancel);
        final Dialog addImageDialog = new Dialog(getActivity(), R.style.Theme_Dialog);
        addImageDialog.setContentView (view);
        addImageDialog.setCancelable(true);
        addImageDialog.show();

        /************** Listeners on Dialog**********/
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addImageDialog.dismiss();
            }
        });
        fromGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addImageDialog.dismiss();
                startActivityForResult(new Intent(Intent.ACTION_PICK,
                                MediaStore.Images.Media.INTERNAL_CONTENT_URI),
                        GET_FROM_GALLERY);
            }
        });
        fromCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PackageManager pm = getActivity().getPackageManager();
                addImageDialog.dismiss();
                if (pm.hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
                    Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    i.putExtra(MediaStore.EXTRA_OUTPUT, MyFileContentProvider.CONTENT_URI);
                    startActivityForResult(i, GET_FROM_CAMERA);
                }
                else {
                    notifyUser("Camera is not Available");
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==GET_FROM_GALLERY && resultCode == Activity.RESULT_OK) {
            selectedImage = data.getData();
            imagesList.add(selectedImage);
            mAdapter.notifyDataSetChanged();
        }
        if(requestCode==GET_FROM_CAMERA && resultCode == Activity.RESULT_OK) {
            Log.i("DATA", "Receive the camera result"+"  "+getContext().getFilesDir());
            File out = new File(getContext().getFilesDir(), "newImage.jpg");
            if(!out.exists()) {
                notifyUser("Error while capturing image");
                return;
            }
            Bitmap mBitmap = BitmapFactory.decodeFile(out.getAbsolutePath());
            selectedImage=getImageUri(getContext(),mBitmap);
            imagesList.add(selectedImage);
            mAdapter.notifyDataSetChanged();
        }
        ShopData shopImages = ShopData.getInstance();
        Uri[] imagesArray = new Uri[imagesList.size()];
        imagesArray = imagesList.toArray(imagesArray);
        shopImages.setImageUri(imagesArray);
    }
    public Uri getImageUri(Context inContext, Bitmap inImage){
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    private void notifyUser(String message) {
        Snackbar.make(getView(),message,Snackbar.LENGTH_SHORT)
                .setAction(getString(R.string.max_ok_btn), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                    }
                }).setActionTextColor(getResources().getColor(R.color.app_color)).show();
    }

    private void submitShopDetails() {
        mCallBack.selectImagesTab();
        ShopData data= ShopData.getInstance();
        Log.e("Data","Data"+"  "+data.getName()+"  "+data.getAbout()+"  "+data.getCategory()+"  "+data.getPhoneNumber()+"  "+
        data.getWebsite()+"  "+data.getLocation()+"  "+data.getLat()+"  "+data.getLng()+"  "+data.getBusinessHourss().toArray());
        ApiInterface service= ApiClient.getClient().create(ApiInterface.class);
        Call<ShopData> call=service.registerShop(data);
        call.enqueue(new Callback<ShopData>() {
            @Override
            public void onResponse(Call<ShopData> call, Response<ShopData> response) {
//                Log.d("Response","RegisterShop"+response.isSuccessful()+"  "+response.body().getId());
                if(response.isSuccessful()){
                    Long shopId= response.body().getId();
                    Long userId= response.body().getUserId();
                    Uri[] images=ShopData.getInstance().getImageUri();
                    if(images!=null){
                    if(images.length>0) {
                        for (Uri uri : images) {
                            addJobImages(getRealPathFromURI(uri), shopId,userId);
                        }
                    }}
                    else{
                        getActivity().finish();
                    }
                }else{
                    ApiError error= ErrorUtils.parseError(response);
                    notifyUser(error.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ShopData> call, Throwable t) {
                Log.e("Failure"," "+t.toString());
            }
        });
        //getActivity().finish();
    }

    private void addJobImages(String imagePath, Long shopId, Long userId) {
        Log.e("Add_Job_Image","Add_Job_Image"+"  "+imagePath);

        //Create Upload Server Client
        ApiInterface service =   ApiClient.getClient().create(ApiInterface.class);

        //File creating from selected URL
        File file = new File(imagePath);

        // create RequestBody instance from file
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("file", file.getName(), requestFile);
        //User user= SharedPrefsHelper.getInstance().readUserPref();
        Log.e("","UploadJobImage"+body+" "+shopId+" "+userId);

        Call<ResponseBody> resultCall = service.uploadShopImages(body,shopId,userId);
        resultCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d("Shop_IMage","UploadImagesResponse"+response.body()+response);
                if(response.isSuccessful()) {
                    //showJobCompletedDialog();
                    getActivity().finish();
                }
                else {
                    ApiError error = ErrorUtils.parseError(response);
                    notifyUser(error.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("Failure","Error"+t.toString());
            }
        });
    }

    //Adapter
    public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
        private ArrayList<Uri> mDataset;
        Activity activity;

        // Provide a reference to the views for each data item
        // Complex data items may need more than one view per item, and
        // you provide access to all the views for a data item in a view holder
        public  class ViewHolder extends RecyclerView.ViewHolder {
            public ImageView mImageView;
            public ViewHolder(View v) {
                super(v);
                ImageView imageView=(ImageView) v.findViewById(R.id.image_view);
                mImageView = imageView;}
        }

        public MyAdapter(FragmentActivity activity, ArrayList<Uri> myDataset) {
            mDataset = myDataset;
        }
        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            // create a new view
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_list_item, parent, false);
            ViewHolder vh = new MyAdapter.ViewHolder(v);
            return vh;
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            Glide.with(holder.mImageView.getContext())
                    .load(mDataset.get(position))
                    //bitmapTransform(new BlurTransformation(getContext()))
                    .bitmapTransform(new CropSquareTransformation(getContext()),new RoundedCornersTransformation(holder.mImageView.getContext(),30,10))
                    //.centerCrop()
                    .into(holder.mImageView);
           /* User user= SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readUserPref();
            Glide.with(getActivity())
                    .load( S3Utils.getDownloadJobImageURL(
                            user.getId(),"9XFOSE"))
                    //bitmapTransform(new BlurTransformation(getContext()))
                    .bitmapTransform(new CropSquareTransformation(getContext()),new RoundedCornersTransformation(holder.mImageView.getContext(),30,10))
                    //.centerCrop()
                    .into(holder.mImageView);*/
        }
        @Override
        public int getItemCount() {
            return mDataset.size();
        }
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }
}
