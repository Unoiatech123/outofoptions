package com.unoiatech.outofoptions.registershop;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.fragments.instantjob.SelectTimeDialog;
import com.unoiatech.outofoptions.fragments.offerhelp.OfferHelpScreen;
import com.unoiatech.outofoptions.home.fragment.JobsListFragment;
import com.unoiatech.outofoptions.home.fragment.filter.CategoryFragment;
import com.unoiatech.outofoptions.util.RecyclerItemClickListener;
import com.unoiatech.outofoptions.util.SharedPrefsHelper;

import org.w3c.dom.Text;
import android.widget.AdapterView.OnItemClickListener;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by Unoiatech on 11/18/2017.
 */

public class BusinessHourDialog extends DialogFragment implements View.OnClickListener, OnItemClickListener{
    private RecyclerView.LayoutManager mLayoutManager;
    ArrayList<String> businessHours;
    static BusinessHourDialog businessHour;
    public static int flag = 0;
    ListView lv;
    public static Button btn;
    businesshourlistview adapter;
    int mHour,mMinute;
    private TextView title,timingHour1,timingHour2,timingHour3,timingHour4,timingHour5,timingHour6,timingHour7,textviewhours,textviewday;
    ImageView backImage;
    public String day[] = {"Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"};

    public String timerange[] = {"9:00 to 18:00","9:00 to 18:00","9:00 to 18:00","9:00 to 18:00","9:00 to 18:00","9:00 to 18:00","9:00 to 18:00"};

    static final int TIME_DIALOG_REQUEST=999;
    private static final String TAG="BusinessHourDialog";
    public int mPosition;
    private TextView[] textview;
    private TextView[] staticText;

    public static DialogFragment newInstance() {
        if(businessHour==null){
            businessHour= new BusinessHourDialog();
        }
        return businessHour;
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.choose_business_hour_layout,container,false);

        initView(view);
        lv= (ListView) view.findViewById(R.id.business_hour_listview);
       /* btn = (Button)view.findViewById(R.id.closed_button);

       btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flag = 1;
            }
        });*/
        adapter = new businesshourlistview(getActivity(),day,timerange);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(this);


        //Click listeners
        backImage.setOnClickListener(this);
        return view;
    }
    public void onItemClick(AdapterView<?> arg0, View arg1, int position, long id) {
        // TODO Auto-generated method stub

         /*if(flag == 1)
        {
            timerange[position]="closed";
            lv.setAdapter(adapter);
            flag = 0;
        }
       else
            {
            showTimePicker(position);
            textviewday = (TextView) arg1.findViewById(R.id.textviewday);
            textviewday.setTextColor(getResources().getColor(R.color.app_color));
            }*/


        showTimePicker(position);
        textviewday = (TextView) arg1.findViewById(R.id.textviewday);
        textviewday.setTextColor(getResources().getColor(R.color.app_color));
    }

    private void initView(View view) {
        backImage=(ImageView)view.findViewById(R.id.back);
        title=(TextView)view.findViewById(R.id.title);
       /* textview = new TextView[7];
        staticText= new TextView[7];
        for(int i=0; i<textview.length; i++){
                String textId = "text_view" + (i+1);
                int resID = getResources().getIdentifier(textId, "id", getActivity().getPackageName());
                textview[i] = ((TextView)view.findViewById(resID));
                textview[i].setOnClickListener(getOnClickDoSomething(textview[i]));
        }
        for(int j=0;j<staticText.length;j++){
            String textId = "text" + (j+1);
            int resID = getResources().getIdentifier(textId, "id", getActivity().getPackageName());
            staticText[j] = ((TextView)view.findViewById(resID));
        }*/
    }

   /* private View.OnClickListener getOnClickDoSomething(final TextView textView) {
        return new View.OnClickListener() {
            public void onClick(View v) {
                int index = 0;
                for(int j=0;j<staticText.length;j++){
                    if(textviewday.getCurrentTextColor()==getResources().getColor(R.color.app_color)){
                        textviewhours.setTextColor(getResources().getColor(R.color.text_color));
                        textviewday.setText("9:00"+" to "+"18:00");
                    }
                }
                for (int i = 0; i < day.length; i++) {
                    if (textviewday.getId() == v.getId()) {
                        index = i;
                        showTimePicker(index);
                        break;
                    }
                }
            }
        };
    }*/

    private void showTimePicker(int index) {
        DialogFragment newFragment = SelectTimeDialog.newInstance();
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        //Add Data To sent
        Bundle bundle= new Bundle();
        bundle.putString("view","shop");
        bundle.putInt("index",index);
        newFragment.setArguments(bundle);

        // specify a transition animation
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        // To make it fullscreen, use the 'content' root view as the container
        // for the fragment, which is always the root view for the activity
        // To bind data from DialogFragment
        newFragment.setTargetFragment(this,TIME_DIALOG_REQUEST);
        transaction.add(R.id.main_layout, newFragment, "time_picker")
                .addToBackStack(null).commit();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
           case R.id.back:
               getActivity().getSupportFragmentManager().popBackStack();
               ShopContactFragment shopContact=(ShopContactFragment) getActivity().getSupportFragmentManager().findFragmentByTag("contact");
               shopContact.onResume();
               break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e(TAG," "+"  "+"OnActivityResult");
        switch (requestCode) {
            case TIME_DIALOG_REQUEST:
                if (resultCode == Activity.RESULT_OK) {
                    // here the part where I get my selected date from the saved variable in the intent of Select Time Dialog and the displaying it on Detail Fragment.
                    Bundle bundle = data.getExtras();
                    if(bundle.getString("startingtime")!=null&&bundle.getString("endingtime")!=null) {
                        SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).saveBusinessHour("day",timerange[bundle.getInt("index")].toString());
                        SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).saveBusinessHour("starting",bundle.getString("startingtime"));
                        SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).saveBusinessHour("ending",bundle.getString("endingtime"));
                        //staticText[bundle.getInt("index")].setTextColor(getResources().getColor(R.color.app_color));
                        //textview[bundle.getInt("index")].setText(bundle.getString("startingtime") + "  to  " + bundle.getString("endingtime"));
                        timerange[bundle.getInt("index")]= bundle.getString("startingtime") + "  to  "+bundle.getString("endingtime");
                        lv.setAdapter(adapter);
                        textviewday.setTextColor(getResources().getColor(R.color.app_color));
                    }
                    else
                    {
                        //timerange[bundle.getInt("index")]= "9:00"+" to "+"18:00";
                        textviewday.setTextColor(getResources().getColor(R.color.app_color));
                        lv.setAdapter(adapter);
                    }
                }
                break;
            case 112:
                if(resultCode == Activity.RESULT_OK)
                {
                    Bundle bundle = data.getExtras();
                    //staticText[bundle.getInt("index")].setTextColor(getResources().getColor(R.color.app_color));
                    //textview[bundle.getInt("index")].setText(bundle.getString("startingtime") + "  to  " + bundle.getString("endingtime"));
                    timerange[bundle.getInt("index")]= bundle.getString("close");
                    lv.setAdapter(adapter);
                    textviewday.setTextColor(getResources().getColor(R.color.app_color));
                }
        }
    }
}
