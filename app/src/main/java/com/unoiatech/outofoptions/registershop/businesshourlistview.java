package com.unoiatech.outofoptions.registershop;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.util.RecyclerItemClickListener;

/**
 * Created by unoiaAndroid on 1/22/2018.
 */

public class businesshourlistview extends BaseAdapter
{
    Activity context;
    String day[];
    String time[];

    public businesshourlistview(Activity context, String[] day, String[] time) {
        super();
        this.context = context;
        this.day = day;
        this.time = time;
    }

    public int getCount() {
        // TODO Auto-generated method stub
        return day.length;
    }

    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return null;
    }

    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }


    public class ViewHolder {
        TextView txtViewday;
        TextView txtViewtime;
    }


    public View getView(int position, View convertView, ViewGroup parent)
    {
        // TODO Auto-generated method stub
        ViewHolder holder;
        LayoutInflater inflater =  context.getLayoutInflater();

        if (convertView == null)
        {
            convertView = inflater.inflate(R.layout.choose_business_hour_listviewlayout, null);
            holder = new ViewHolder();
            holder.txtViewday = (TextView) convertView.findViewById(R.id.textviewday);
            holder.txtViewtime = (TextView) convertView.findViewById(R.id.textviewhours);
            convertView.setTag(holder);
        }
        else
        {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.txtViewday.setText(day[position]);
        holder.txtViewtime.setText(time[position]);

        return convertView;
    }

}