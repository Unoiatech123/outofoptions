package com.unoiatech.outofoptions.adminmodule.company;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.api.ApiClient;
import com.unoiatech.outofoptions.api.ApiInterface;
import com.unoiatech.outofoptions.model.Company;
import com.unoiatech.outofoptions.model.CompanyData;
import com.unoiatech.outofoptions.util.RecyclerItemClickListener;
import com.unoiatech.outofoptions.util.S3Utils;
import com.unoiatech.outofoptions.api.URL;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Unoiatech on 7/4/2017.
 */
public class CompanyRequestFragment extends Fragment {
    RequestAdapter adapter;
    private ArrayList<Company> companyRequest;
    private RecyclerView recyclerView;
    LinearLayout emptyView;
    TextView staticText;
    private static String TAG="CompanyRequest";

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.my_proposal_list_fragment,container,false);
        recyclerView=(RecyclerView) view.findViewById(R.id.my_jobs_recycler_view);
        emptyView=(LinearLayout)view.findViewById(R.id.empty_view);
        staticText=(TextView)view.findViewById(R.id.static_text);
        companyRequest= new ArrayList<>();

        /******Set RecyclerView*****/
        setRecyclerView(companyRequest);

        //Add ClickListener on RecyclerView Item
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(),new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent intent= new Intent(getActivity(),RegistrationRequestScreen.class);
                Bundle bundle= new Bundle();
                bundle.putLong("company_id",companyRequest.get(position).getId());
                intent.putExtras(bundle);
                getActivity().startActivity(intent);
            }
        }));
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        //get Company requests
        getCompanyRequest();
    }

    private void setRecyclerView(ArrayList<Company> companyRequest) {
        // Set Linear Layout Manager
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        //initialize Adapter
        adapter= new RequestAdapter(getActivity(), this.companyRequest);
        Log.e("Size","Size"+companyRequest.size());
        //setAdapter
        if(this.companyRequest.size()>0){
            emptyView.setVisibility(View.INVISIBLE);
            staticText.setVisibility(View.INVISIBLE);
            recyclerView.setAdapter(adapter);
        }
        else{
            emptyView.setVisibility(View.VISIBLE);
            staticText.setVisibility(View.VISIBLE);
            staticText.setText(R.string.static_txt_on_company_req);
        }
    }

    private void getCompanyRequest() {
        companyRequest.clear();
        final ApiInterface apiService= ApiClient.getClient().create(ApiInterface.class);
        Call<ArrayList<CompanyData>> call=apiService.getCompanyRequest();
        call.enqueue(new Callback<ArrayList<CompanyData>>() {
            @Override
            public void onResponse(Call<ArrayList<CompanyData>> call, Response<ArrayList<CompanyData>> response) {
                Log.d(TAG,"Response"+response.body());
                if(response.isSuccessful()) {
                    for(int i=0;i<response.body().size();i++) {
                        Log.e("For_Loop","For_Loop");
                        companyRequest.add(response.body().get(i).getCompany());
                    }
                    setRecyclerView(companyRequest);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<CompanyData>> call, Throwable t) {
                Log.e(TAG,"Failure"+t.toString());
            }
        });
    }

    class RequestAdapter extends RecyclerView.Adapter<RequestAdapter.ViewHolder>{
        Activity activity;
        ArrayList<Company> requestData;
        public RequestAdapter(FragmentActivity activity, ArrayList<Company> companyRequest) {
            this.activity=activity;
            requestData=companyRequest;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.company_request_layout, parent, false);
            RequestAdapter.ViewHolder vh = new RequestAdapter.ViewHolder(v);
            return vh;
        }

        @Override
        public void onBindViewHolder(RequestAdapter.ViewHolder holder, int position) {
            holder.companyName.setText(requestData.get(position).getBusinessName());
            holder.userName.setText(requestData.get(position).getUser().getFirstName()+" "+requestData.get(position).getUser().getLastName());
            setUserImage(holder.userImage,requestData.get(position).getUser().getImagePath(),requestData.get(position).getUser().getId());
            if(requestData.get(position).getLogo()!=null)
            Glide.with(activity).load(S3Utils.getCompanylogo(requestData.get(position).getId(), requestData.get(position).getLogo())).into(holder.companyLogo);
        }

        private void setUserImage(ImageView memberImage, String imagePath, Long id) {
            if (imagePath != null) {
                if (imagePath.contains("\\")) {
                    String lastIndex = imagePath.replace("\\", " ");
                    String[] Stamp = lastIndex.split("\\s");
                    String timeStamp = Stamp[3];
                    String userImageUrl = URL.BASE_URL + "/users/download/" + id + "/image/" + timeStamp;
                    Glide.with(activity).load(userImageUrl).into(memberImage);
                } else {
                    Glide.with(activity).load(S3Utils.getDownloadUserImageURL(id, imagePath)).into(memberImage);
                }
            }
        }

        @Override
        public int getItemCount() {
            return requestData.size();
        }

        // Provide a reference to the views for each data item
        // Complex data items may need more than one view per item, and
        // you provide access to all the views for a data item in a view holder
        public  class ViewHolder extends RecyclerView.ViewHolder {
            public TextView companyName,userName;
            CircleImageView companyLogo,userImage;

            public ViewHolder(View v) {
                super(v);
                companyName=(TextView) v.findViewById(R.id.company_name);
                userName=(TextView) v.findViewById(R.id.user_name);
                companyLogo=(CircleImageView)v.findViewById(R.id.logo);
                userImage=(CircleImageView)v.findViewById(R.id.user_image);
            }
        }
    }
}
