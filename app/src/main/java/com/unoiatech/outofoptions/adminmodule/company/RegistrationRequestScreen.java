package com.unoiatech.outofoptions.adminmodule.company;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.api.ApiClient;
import com.unoiatech.outofoptions.api.ApiInterface;
import com.unoiatech.outofoptions.model.Company;
import com.unoiatech.outofoptions.model.CompanyData;
import com.unoiatech.outofoptions.util.ApiError;
import com.unoiatech.outofoptions.util.BankId.ErrorUtils;
import com.unoiatech.outofoptions.util.DateTimeConverter;
import com.unoiatech.outofoptions.util.S3Utils;
import com.unoiatech.outofoptions.api.URL;
import com.wang.avi.AVLoadingIndicatorView;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Unoiatech on 8/2/2017.
 */

public class RegistrationRequestScreen extends AppCompatActivity implements View.OnClickListener{
    TextView companyName,
            username,
            businessName,
            ownerName,
            regYear,
            businessSector,
            businessEnterprise,
            status,
            fSkatt,
            bankGiro,
            telNumber,
            emailAddress,
            swishHandle;
    TextView yesBtn;
    CircleImageView userImage,logo;
    private Long companyId;
    private AVLoadingIndicatorView progressBar;
    private static final String TAG="RegistrationRequest";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration_request_layout);

        //Action Bar View
        TextView mTitle=(TextView)findViewById(R.id.title);
        mTitle.setText(R.string.reg_request_title);

        companyName=(TextView)findViewById(R.id.company_name);
        logo=(CircleImageView)findViewById(R.id.logo);

        username=(TextView)findViewById(R.id.user_name);
        userImage=(CircleImageView)findViewById(R.id.user_image);

        businessName=(TextView)findViewById(R.id.business_name);
        ownerName=(TextView)findViewById(R.id.owner_name);
        regYear=(TextView)findViewById(R.id.reg_year);
        businessSector=(TextView)findViewById(R.id.business_sector);
        businessEnterprise=(TextView)findViewById(R.id.business_enterprise);
        status=(TextView)findViewById(R.id.business_status);
        fSkatt=(TextView)findViewById(R.id.skatt);
        bankGiro=(TextView)findViewById(R.id.bankgiro);
        telNumber=(TextView)findViewById(R.id.telephone);
        swishHandle=(TextView)findViewById(R.id.swish_handle);
        emailAddress=(TextView)findViewById(R.id.email);
        progressBar=(AVLoadingIndicatorView)findViewById(R.id.progress_bar1);

        //Response Buttons
        yesBtn=(TextView)findViewById(R.id.yes_btn);
        yesBtn.setText(R.string.yes_text);
        Button noBtn=(Button)findViewById(R.id.no_btn);
        yesBtn.setOnClickListener(this);
        noBtn.setOnClickListener(this);

        //Get Company details
        companyId=getIntent().getExtras().getLong("company_id");
        getCompanyDetails(getIntent().getExtras().getLong("company_id"));
    }

    private void getCompanyDetails(long company_id) {
        ApiInterface apiService= ApiClient.getClient().create(ApiInterface.class);
        Call<CompanyData> call= apiService.getCompanyDetails(company_id);
        call.enqueue(new Callback<CompanyData>() {
            @Override
            public void onResponse(Call<CompanyData> call, Response<CompanyData> response) {
                Log.d("Response","Response"+"  "+response.message()+"  "+response.body());
                if(response.isSuccessful()) {
                    companyName.setText(response.body().getCompany().getBusinessName());
                    username.setText(response.body().getUser().getFirstName()+" "+response.body().getUser().getLastName());
                    businessName.setText(response.body().getCompany().getBusinessName());
                    ownerName.setText(response.body().getCompany().getOwnerName());
                    regYear.setText(DateTimeConverter.getRegYear(Long.valueOf(response.body().getCompany().getRegistrationYear())));
                    businessSector.setText(response.body().getCompany().getBsector());
                    businessEnterprise.setText(response.body().getCompany().getBenterprise());
                    status.setText(response.body().getCompany().getBstatus());
                    fSkatt.setText(response.body().getCompany().getFskatt());
                    bankGiro.setText(response.body().getCompany().getBankgiro());
                    telNumber.setText(response.body().getCompany().getPhoneNumber());
                    swishHandle.setText(response.body().getCompany().getSwishNumber());
                    emailAddress.setText(response.body().getCompany().getEmail());

                    //SetUser Image
                    setMemberImage(userImage,response.body().getUser().getImagePath(),response.body().getUser().getId());

                    //set Company Logo
                    if(response.body().getCompany().getLogo()!=null)
                    Glide.with(RegistrationRequestScreen.this).load(S3Utils.getCompanylogo(response.body().getCompany().getId(), response.body().getCompany().getLogo())).into(logo);
                }
            }

            @Override
            public void onFailure(Call<CompanyData> call, Throwable t) {
                Log.e("failure","Failure"+t.toString());
            }
        });
    }
    private void setMemberImage(ImageView memberImage, String imagePath, Long id) {
        if (imagePath != null) {
            if (imagePath.contains("\\")) {
                String lastIndex = imagePath.replace("\\", " ");
                String[] Stamp = lastIndex.split("\\s");
                String timeStamp = Stamp[3];
                String userImageUrl = URL.BASE_URL + "/users/download/" + id + "/image/" + timeStamp;
                Glide.with(this).load(userImageUrl).into(memberImage);
            }
            else {
                Glide.with(this).load(S3Utils.getDownloadUserImageURL(id, imagePath)).into(memberImage);
            }
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.yes_btn:
                //Accept Company registration Request
                registrationRequestResponse(1,"", progressBar,yesBtn);
                break;
            case R.id.no_btn:
                showCancelRegistrationDialog();
                //registrationRequestResponse("0");
                break;
        }
    }

    private void showCancelRegistrationDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        View view = getLayoutInflater().inflate(R.layout.cancel_reg_dialog_layout, null);
        dialogBuilder.setView(view);
        Button no = (Button) view.findViewById(R.id.no);
        final TextView yes=(TextView) view.findViewById(R.id.yes);
        final AVLoadingIndicatorView progressBar=(AVLoadingIndicatorView)view.findViewById(R.id.progress_bar1);
        final TextView desc=(TextView)view.findViewById(R.id.des_edit);
        TextView mTitle=(TextView)view.findViewById(R.id.title);

        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();

        mTitle.setText(getString(R.string.cancel_reg_title));
        desc.setHint(getString(R.string.cancel_reg_message));

        /****Negative response Button****/
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        /***Positive Response Button*****/
        yes.setText(R.string.yes_text);
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!desc.getText().toString().isEmpty()) {
                    InputMethodManager inputManager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow((null == getCurrentFocus()) ? null :
                            getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

                    //To Cancel company Registration Request
                    registrationRequestResponse(2, desc.getText().toString(),progressBar,yes);
                }
                else{
                    Toast.makeText(getApplicationContext(),getString(R.string.cancel_reg_message),Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void registrationRequestResponse(int response, String detail, final AVLoadingIndicatorView progressBar, TextView yesButton) {
        progressBar.setVisibility(View.VISIBLE);
        yesButton.setText("");
        Company company= new Company();
        company.setResponse(Long.valueOf(response));
        if(!detail.isEmpty())
        company.setDetails(detail);
        ApiInterface apiService= ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call= apiService.companyRequestResponse(companyId,company);
        Log.e("data","Data"+company.getResponse()+"  "+company.getDetails());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d(TAG,"Response"+response.body()+response.isSuccessful());
                if(response.isSuccessful()){
                    progressBar.setVisibility(View.INVISIBLE);
                    finish();
                }else{
                    progressBar.setVisibility(View.INVISIBLE);
                    ApiError error = ErrorUtils.parseError(response);
                    notifyUser(error.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG,"Failure"+t.toString());
            }
        });
    }

    private void notifyUser(String message) {
        Snackbar.make(getWindow().getDecorView().getRootView(),message,Snackbar.LENGTH_SHORT)
                .setAction(R.string.max_ok_btn, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                    }
                }).setActionTextColor(getResources().getColor(R.color.app_color));
    }
}
