package com.unoiatech.outofoptions.adminmodule.payment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.api.ApiClient;
import com.unoiatech.outofoptions.api.ApiInterface;
import com.unoiatech.outofoptions.model.CompanyData;
import com.unoiatech.outofoptions.util.DateTimeConverter;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Unoiatech on 7/4/2017.
 */
public class PaymentRecordFragment extends Fragment {
    RecyclerView mRecyclerView;
    ArrayList<CompanyData> paymentRecords;
    MyAdapter mAdapter;
    static String TAG="PaymentRecordFragment";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.my_proposal_list_fragment,container,false);
        mRecyclerView=(RecyclerView)view.findViewById(R.id.my_jobs_recycler_view);
        LinearLayoutManager layoutManager= new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(layoutManager);

        paymentRecords= new ArrayList<>();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        //get Payment Records
        getPaymentRecords();
    }

    private void getPaymentRecords() {
        paymentRecords.clear();
        ApiInterface apiService= ApiClient.getClient().create(ApiInterface.class);
        Call<ArrayList<CompanyData>> call=apiService.getPaymentRecords();
        call.enqueue(new Callback<ArrayList<CompanyData>>() {
            @Override
            public void onResponse(Call<ArrayList<CompanyData>> call, Response<ArrayList<CompanyData>> response) {
                Log.d(TAG,"Response"+response.body());
                if(response.isSuccessful()) {
                    for(int i= response.body().size()-1;i>=0;i--){
                        paymentRecords.add(response.body().get(i));
                    }
                    setDataOnList(paymentRecords);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<CompanyData>> call, Throwable t) {
                Log.e(TAG,"Failure"+t.toString());
            }
        });
    }

    private void setDataOnList(ArrayList<CompanyData> paymentRecords) {
        Log.e("setData_on_List","Set_Data_on_list"+paymentRecords.size());
        if(paymentRecords.size()>0){
            mAdapter=new MyAdapter(PaymentRecordFragment.this,paymentRecords);
            mRecyclerView.setAdapter(mAdapter);
        }
        else{

        }
    }

    private class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder>{
        ArrayList<CompanyData> data;
        LayoutInflater inflater;

        public MyAdapter(PaymentRecordFragment activity, ArrayList<CompanyData> paymentRecords) {
            data=paymentRecords;
            inflater= getActivity().getLayoutInflater();
        }

        @Override
        public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view= inflater.inflate(R.layout.payment_record_list_items,parent,false);
            MyAdapter.ViewHolder vH= new MyAdapter.ViewHolder(view);
            return vH;
        }

        @Override
        public void onBindViewHolder(MyAdapter.ViewHolder holder, int position) {
            holder.userName.setText(data.get(position).getUser().getFirstName()+" "+data.get(position).getUser().getLastName());
            holder.amount.setText(data.get(position).getTransaction().getAmountRequested()+"SEK");
            if(data.get(position).getTransaction().getSwishNumber()!=null) {
                holder.swishNumber.setText(data.get(position).getTransaction().getSwishNumber());
            }
            else{
                holder.swishNumber.setText(getString(R.string.swish_no_msg));
            }
            //Set ModifiedOn
            holder.timeline.setText(DateTimeConverter.getPaymentModifiedDate(data.get(position).getTransaction().getModifiedOn()));

            //Set transaction Status
            setTransactionStatus(holder.paymentStatus,holder.statusImage,data.get(position).getTransaction().getTransactionStatus());
        }

        private void setTransactionStatus(TextView paymentStatus, ImageView statusImage, String transactionStatus) {
            if(transactionStatus.equals("1")){
                paymentStatus.setTextColor(getResources().getColor(R.color.app_color));
                paymentStatus.setText(getString(R.string.completed_job));
                statusImage.setImageResource(R.drawable.record_completed);
            }
            else{
                paymentStatus.setTextColor(getResources().getColor(android.R.color.holo_red_dark));
                paymentStatus.setText(getString(R.string.archive_cancel_job));
                statusImage.setImageResource(R.drawable.record_cancelled);
            }
        }

        @Override
        public int getItemCount() {
            return data.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder{
            TextView userName,swishNumber,amount,paymentStatus,timeline;
            ImageView statusImage;

            public ViewHolder(View itemView) {
                super(itemView);
                userName=(TextView)itemView.findViewById(R.id.name);
                swishNumber=(TextView)itemView.findViewById(R.id.swish_number);
                amount=(TextView)itemView.findViewById(R.id.budget);
                paymentStatus=(TextView)itemView.findViewById(R.id.payment_status);
                statusImage=(ImageView)itemView.findViewById(R.id.payment_status_image);
                timeline=(TextView)itemView.findViewById(R.id.timeline);
            }
        }
    }
}
