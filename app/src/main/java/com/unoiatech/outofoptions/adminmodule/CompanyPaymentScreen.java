package com.unoiatech.outofoptions.adminmodule;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTabHost;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TabHost;
import android.widget.TextView;

import com.unoiatech.outofoptions.R;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Unoiatech on 7/4/2017.
 */

public class CompanyPaymentScreen extends AppCompatActivity {
   FragmentTabHost mTabHost;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.active_jobs_layout);

        /******Fragment TabHost***********/
        mTabHost = (FragmentTabHost)findViewById(android.R.id.tabhost);
        mTabHost.setup(this, getSupportFragmentManager(), android.R.id.tabcontent);
        mTabHost.getTabWidget().setStripEnabled(false);
        mTabHost.getTabWidget().setDividerDrawable(null);

        /**Adding Fragments in Tab Host**/
        View view1 = customTabView(getLayoutInflater(), getResources().getString(R.string.payment_tab_text));
        TabHost.TabSpec tabSpec1 = mTabHost.newTabSpec("payment").setIndicator(view1);
        mTabHost.addTab(tabSpec1, PaymentFragment.class, null);

        View view2 = customTabView(getLayoutInflater(), getResources().getString(R.string.company_tab_text));
        TabHost.TabSpec tabSpec3 = mTabHost.newTabSpec("company").setIndicator(view2);
        mTabHost.addTab(tabSpec3, CompanyFragment.class, null);

        mTabHost.setCurrentTab(0);
        mTabHost.getTabWidget().getChildAt(0).setBackgroundResource(R.drawable.help_requested_drawable);

        /*******TabClickListener****************/
        mTabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String tabId) {
                int tab = mTabHost.getCurrentTab();
                if(tab==0) {
                    mTabHost.getTabWidget().getChildAt(tab).setBackgroundResource(R.drawable.help_requested_drawable);
                }
                else{
                    mTabHost.getTabWidget().getChildAt(tab).setBackgroundResource(R.drawable.offer_help_drawable);
                }
            }
        });
    }

    private View customTabView(LayoutInflater inflater, String tabText) {
        View view= inflater.inflate(R.layout.tab_layout_for_tab1,null);
        TextView text= (TextView) view.findViewById(R.id.tab_text);
        text.setTextColor(getResources().getColorStateList(R.color.tab_text_color));
        text.setText(tabText);
        text.setBackgroundResource(android.R.color.transparent);
        return view;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
