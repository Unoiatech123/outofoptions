package com.unoiatech.outofoptions.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.unoiatech.outofoptions.*;
import com.unoiatech.outofoptions.api.ApiClient;
import com.unoiatech.outofoptions.api.ApiInterface;
import com.unoiatech.outofoptions.home.HomeScreenActivity;
import com.unoiatech.outofoptions.model.User;
import com.unoiatech.outofoptions.util.ApiError;
import com.unoiatech.outofoptions.util.BankId.ErrorUtils;
import com.unoiatech.outofoptions.util.ConnectionDetection;
import com.unoiatech.outofoptions.util.GPSTracker;
import com.unoiatech.outofoptions.util.SharedPrefsHelper;
import com.wang.avi.AVLoadingIndicatorView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Unoiatech on 4/8/2016.
 */

public class LoginActivity extends Activity implements View.OnClickListener{
    private static String TAG="LoginActivity";
    private String usernameText,passwordText;
    private String latitude,longitude;
    private EditText emailEditText,passwordEditText;
    private static AVLoadingIndicatorView progressBar;
    public static final String EMAIL_PATTERN = "[^\\s@]+@[^\\s@]+\\.[a-zA-z][a-zA-Z][a-zA-Z]*";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
        emailEditText= (EditText) findViewById(R.id.email_edit_text);
        passwordEditText=(EditText) findViewById(R.id.password_edit_text);
        Button loginButton=(Button) findViewById(R.id.login_button);
        TextView registerButton=(TextView) findViewById(R.id.register);
        progressBar= (AVLoadingIndicatorView) findViewById(R.id.progress_bar);

        //Set OnClick Listeners
        loginButton.setOnClickListener(this);
        registerButton.setOnClickListener(this);
    }

    // user login
    public void loginUser(final User user) {
        progressBar.setVisibility(View.VISIBLE);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<User> call = apiService.loginUser(user);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User>call, Response<User> response) {
                progressBar.setVisibility(View.GONE);

                Log.d(TAG+"Response",response.toString());
                //login successful
                if(response.code()==200) {
                   User user= response.body();
                    //save user locally
                    SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).writeUserPref(user);
                    resisterDeviceId(user.getId());

                    // login user into quickblox
                 //   new QuickbloxService().loginUserInQB(getApplicationContext(),user);
                }
                // Handle Error
                else {
                    Log.d(TAG+"Message",response.message());
                    ApiError error = ErrorUtils.parseError(response);
                    Log.d(TAG+"Error Body",error.toString());
                    Snackbar.make(getWindow().getDecorView().getRootView(),
                            error.getMessage(), Snackbar.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<User>call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
            }
        });
    }

    private void notifyUser(String message) {
        Snackbar.make(getWindow().getDecorView().getRootView(), message, Snackbar.LENGTH_LONG)
                .setAction(getString(R.string.max_ok_btn), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                    }
                }).setActionTextColor(getResources().getColor(R.color.app_color)).show();
    }

    private void resisterDeviceId(Long userId) {
        String fcmId=SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).getFcmId();
        User user = new User();
        user.setDeviceId(fcmId);
        user.setDeviceType("android");
        Log.e("DATA","DATa"+user.getDeviceId()+" "+user.getDeviceType());
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<Void> call = apiService.registerDeviceId(userId, user);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful()) {
                    Intent intent = new Intent(LoginActivity.this, HomeScreenActivity.class);
                    LoginActivity.this.finish();
                    startActivity(intent);
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                } else {
                    ApiError error = ErrorUtils.parseError(response);
                    Log.d(TAG + "Code", "" + response);
                    Snackbar.make(getWindow().getDecorView().getRootView(),
                            error.getMessage(), Snackbar.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.d(TAG, "Failure" + t.toString());
            }
        });
    }

    public void getLocation() {
        GPSTracker gps = new GPSTracker(this);
        // check if GPS enabled
        if (gps.canGetLocation()) {
            latitude= String.valueOf(gps.getLatitude());
            longitude=String.valueOf(gps.getLongitude());
            Log.e("latnlongitude","latnlongitude"+latitude+" "+longitude)  ;
        } else {
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            gps.showSettingsAlert();
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.login_button:
                checkLoginData();
                break;
            case R.id.register:
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                finish();
                startActivity(intent);
                break;
        }
    }

    private void checkLoginData() {
        //dismiss keyboard
        InputMethodManager inputManager = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow((null == getCurrentFocus()) ? null : getCurrentFocus().getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);

        usernameText = emailEditText.getText().toString();
        passwordText = passwordEditText.getText().toString();
        if (!usernameText.isEmpty() && !passwordText.isEmpty()) {
            if (!usernameText.matches(EMAIL_PATTERN)) {
                notifyUser(getString(R.string.invalid_email_address));
            } else if (passwordText.length() < 8) {
                notifyUser(getString(R.string.invalid_paswrd));
            } else if (ConnectionDetection.isInternetAvailable(getApplicationContext())) {
                User user = new User();
                user.setEmail(usernameText);
                user.setUserPassword(passwordText);
                loginUser(user);
                Log.e("UserObject", "Response");
            } else {
                notifyUser(getString(R.string.internet_connection_error));
            }
        } else {
            if(emailEditText.getText().toString().isEmpty()) {
                notifyUser(getString(R.string.empty_email_address));
            }
            else if(passwordEditText.getText().toString().isEmpty()){
                notifyUser(getString(R.string.empty_paswrd));
            }
        }
    }
}
