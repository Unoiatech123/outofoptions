package com.unoiatech.outofoptions.companymodule;

/**
 * Created by Unoiatech on 7/5/2017.
 */

public interface SelectedViewCallback {
    void selectedTabView();
    void unselectedTabView();

    //pass data between fragment
    public void allabolagData(String orgNumber,String businessName);
}
