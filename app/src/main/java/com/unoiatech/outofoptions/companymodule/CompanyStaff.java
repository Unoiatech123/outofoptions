package com.unoiatech.outofoptions.companymodule;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.api.ApiClient;
import com.unoiatech.outofoptions.api.ApiInterface;
import com.unoiatech.outofoptions.model.CompanyData;
import com.unoiatech.outofoptions.model.User;
import com.unoiatech.outofoptions.util.ApiError;
import com.unoiatech.outofoptions.util.BankId.ErrorUtils;
import com.unoiatech.outofoptions.util.DateTimeConverter;
import com.unoiatech.outofoptions.util.S3Utils;
import com.unoiatech.outofoptions.util.SharedPrefsHelper;
import com.unoiatech.outofoptions.api.URL;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Unoiatech on 7/24/2017.
 */
public class CompanyStaff extends Activity implements View.OnClickListener,OnDialogFinishListener {
    private static final String TAG="CompanyStaff";
    ArrayList<User> staffMembersList;
    MyAdapter adapter;
    RecyclerView staffData;
    private Long companyId;
    private TextView noteText;
    private EditText searchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.company_staff);

        //Action Bar Components
        TextView addMember=(TextView)findViewById(R.id.add_member);
        searchView=(EditText)findViewById(R.id.search_view);

        addMember.setOnClickListener(this);

        staffData=(RecyclerView)findViewById(R.id.staff_members);
        LinearLayoutManager layoutManager= new LinearLayoutManager(this);
        noteText=(TextView)findViewById(R.id.note);
        staffData.setLayoutManager(layoutManager);

        staffMembersList= new ArrayList<>();
        companyId=getIntent().getExtras().getLong("company_id");
        getCompanyProfile(companyId);

        //Add TextWatcher on SerachView EditText
        searchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void afterTextChanged(Editable editable) {
                Log.e("AfterTextChanged","AfterTextChanged"+editable);
                String charSearch= searchView.getText().toString();
                adapter.filter(charSearch);
            }
        });
    }

    @Override
    public void onFinishEditDialog(String msg) {
        if(msg.equals("Success")) {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
            View view1 = getLayoutInflater().inflate(R.layout.staff_invite_sent, null);
            dialogBuilder.setView(view1);
            TextView ok_btn = (TextView) view1.findViewById(R.id.okay_btn);
            final AlertDialog alertDialog = dialogBuilder.create();
            alertDialog.setCancelable(false);
            alertDialog.show();
            ok_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                }
            });
        }
        else {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
            View view1 = getLayoutInflater().inflate(R.layout.custom_toast_layout, null);
            dialogBuilder.setView(view1);
            TextView ok_btn = (TextView) view1.findViewById(R.id.okay_btn);
            TextView msgText = (TextView) view1.findViewById(R.id.message);
            msgText.setText(msg);
            final AlertDialog alertDialog = dialogBuilder.create();
            alertDialog.setCancelable(false);
            alertDialog.show();
            ok_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                }
            });
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.add_member:
                FragmentTransaction ft= getFragmentManager().beginTransaction();
                ft.addToBackStack(null);
                DialogFragment dialog= InviteStaffDialog.newInstance();
                dialog.show(ft,"");
                break;
        }
    }

    public void getCompanyProfile(long companyId) {
        staffMembersList.clear();
        ApiInterface apiService= ApiClient.getClient().create(ApiInterface.class);
        Call<CompanyData> call= apiService.getCompanyProfile(companyId);
        call.enqueue(new Callback<CompanyData>() {
            @Override
            public void onResponse(retrofit2.Call<CompanyData> call, Response<CompanyData> response) {
                Log.d(TAG,"Response"+response.body()+" "+response.message());
                if(response.code()==200) {
                    showRetroResult(response.body());
                }
                else{
                    ApiError error = ErrorUtils.parseError(response);
                    Snackbar.make(getWindow().getDecorView().getRootView(),error.getMessage(),Snackbar.LENGTH_SHORT).show();
                    Log.d("Code" + "Code", "" + response);
                }
            }

            @Override
            public void onFailure(retrofit2.Call<CompanyData> call, Throwable t) {
                Log.d("Failure","Failure"+t.toString());
            }
        });
    }

    private void showRetroResult(CompanyData body) {
        if (body.getCoordinator() != null) {
            staffMembersList.add(0, body.getCoordinator());
            if (body.getMembers().size() > 0)
                for (int i = 0; i < body.getMembers().size(); i++) {
                    staffMembersList.add(body.getMembers().get(i));
                }
        } else {
            if (body.getMembers().size() > 0) {
                for (int i = 0; i < body.getMembers().size(); i++) {
                    staffMembersList.add(0, body.getMembers().get(i));
                }
            }
        }
        if(staffMembersList.size()>0) {
            setDataOnList(staffMembersList);
        }
        else{
            noteText.setVisibility(View.VISIBLE);
            SpannableStringBuilder staticText = new SpannableStringBuilder(getString(R.string.static_text_on_staff_screen));
            staticText.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.app_color)), 0, 5, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            noteText.setText(staticText);
        }
    }

    private void setDataOnList(ArrayList<User> staffMembers) {
        adapter= new MyAdapter(this,staffMembers);
        staffData.setAdapter(adapter);
    }

    private class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder>{
        ArrayList<User> data;
        Activity activity;
        ArrayList<User> sortedData;

        public MyAdapter(CompanyStaff companyStaff, ArrayList<User> staffMembers) {
            this.activity=companyStaff;
            data=staffMembers;
            this.sortedData= new ArrayList<>();
            this.sortedData.addAll(data);
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater= LayoutInflater.from(activity);
            return new ViewHolder(inflater.inflate(R.layout.staff_list_items,parent,false));
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            String userRole=null;
            holder.userName.setText(data.get(position).getFirstName()+" "+data.get(position).getLastName());
            setMemberImage(holder.userImage,data.get(position).getImagePath(),data.get(position).getId());
            if(data.get(position).getCompanyUser().getUserRole().equals("1")) {
                userRole="Owner";
            }
            else if(data.get(position).getCompanyUser().getUserRole().equals("2")){
                userRole="Coordinator";
            }
            else if(data.get(position).getCompanyUser().getUserRole().equals("3")){
                userRole="Staff member";
            }
            holder.userRole.setText(userRole);
            holder.mDate.setText(DateTimeConverter.getCreatedDate(data.get(position).getCompanyUser().getCreatedAt()));

            holder.assignRole.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showAssignRoleDialog(holder.userRole.getText().toString(),companyId,data.get(position).getId());
                }
            });
        }

        @Override
        public int getItemCount() {
            return data.size();
        }

        public void filter(String charSearch) {
            Log.e("Filter","Filter"+"  "+charSearch);
           // charSearch= charSearch.toLowerCase(Locale.getDefault());
            data.clear();
            if(charSearch.length()==0) {
                data.addAll(sortedData);
            }
            else{
                Log.e("Else","Else"+charSearch);
                for (User user : sortedData) {
                    if (user.getFirstName().contains(charSearch)){
                        data.add(user);
                    }
                }
            }
            notifyDataSetChanged();
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            ImageView userImage;
            LinearLayout assignRole;
            TextView userName,userRole,mDate;

            public ViewHolder(View view) {
                super(view);
                userImage=(ImageView)view.findViewById(R.id.user_image);
                userName=(TextView)view.findViewById(R.id.user_name);
                userRole=(TextView)view.findViewById(R.id.user_role);
                mDate=(TextView)view.findViewById(R.id.date);
                assignRole=(LinearLayout)view.findViewById(R.id.assign_role);
            }
        }
    }
    private void setMemberImage(ImageView memberImage, String imagePath, Long id) {
        if(imagePath!=null){
            if(imagePath.contains("\\")){
                String lastIndex= imagePath.replace("\\", " ");
                String[] Stamp=lastIndex.split("\\s");
                String timeStamp=Stamp[3];
                String userImageUrl = URL.BASE_URL+"/users/download/"+id+"/image/"+timeStamp;
                Glide.with(this).load(userImageUrl).into(memberImage);
            }
            else{
                Glide.with(this).load(S3Utils.getDownloadUserImageURL(id,imagePath)).into(memberImage);
            }
        }
    }
    private void showAssignRoleDialog(final String assignedRole, final Long companyId, final Long companyUserId) {
        View view = getLayoutInflater().inflate(R.layout.assign_role_dialog_layout, null);
        TextView removeMember = (TextView) view.findViewById(R.id.remove_member);
        final TextView assignRole = (TextView) view.findViewById(R.id.user_role);
        TextView cancel = (TextView) view.findViewById(R.id.cancel);
        final Dialog mBottomSheetDialog = new Dialog(CompanyStaff.this, R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.BOTTOM);
        mBottomSheetDialog.show();
        if(assignedRole.equals("Coordinator")){
            assignRole.setText(R.string.unassign_role);
        }
        else {
            assignRole.setText(R.string.coordinator_role_text);
        }
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetDialog.dismiss();
            }
        });
        removeMember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
                removeCompanyMember(companyUserId,companyId);
            }
        });

        assignRole.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
                Log.e("OnClick","OnClick"+" "+assignRole.getText().toString());
                if(assignRole.getText().toString().equals("Assign Coordinator Role")) {
                    unassignCoordinatorRole(companyId,companyUserId,"AssignRole");
                }
                else{
                    unassignCoordinatorRole(companyId,companyUserId,"Unassign");
                }
            }
        });
    }

    private void assignCoordinatorRole(final Long companyId, Long userId) {
        ApiInterface apiService= ApiClient.getClient().create(ApiInterface.class);
        User user= SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readUserPref();
        Call<Void> call= apiService.assignCoordinatorRole(userId,companyId,user.getId());
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.e(TAG,"Response"+response.body());
                getCompanyProfile(companyId);
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.e(TAG,"Failure"+t.toString());
            }
        });
    }

    private void unassignCoordinatorRole(final Long companyId, final Long userId, final String assignRole) {
        ApiInterface apiService= ApiClient.getClient().create(ApiInterface.class);
        User user= SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readUserPref();
        Call<Void> call= apiService.unassignedCoordinatorRole(userId,companyId,user.getId());
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.d(TAG,"Response"+response.isSuccessful()+" "+response.code()+" "+assignRole);
                if(response.isSuccessful())
                    if(assignRole.equals("AssignRole")) {
                        assignCoordinatorRole(companyId, userId);
                    }
                    else {
                        getCompanyProfile(companyId);
                    }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.e(TAG,"Failure"+t.toString());
            }
        });
    }

    private void removeCompanyMember(Long userId, final Long companyId) {
        ApiInterface apiService= ApiClient.getClient().create(ApiInterface.class);
        Call<Void> call= apiService.removeCompanyMember(userId,companyId);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.d(TAG,"Response"+response.isSuccessful()+" "+response.message());
                getCompanyProfile(companyId);
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.e(TAG,"Failure"+t.toString());
            }
        });
    }
}
