package com.unoiatech.outofoptions.companymodule.registration;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.companymodule.SelectedViewCallback;
import com.unoiatech.outofoptions.model.Company;
import com.unoiatech.outofoptions.util.SharedPrefsHelper;
import com.unoiatech.outofoptions.util.common.view.AutoAddTextWatcher;

/**
 * Created by Unoiatech on 7/13/2017.
 */

public class CompanyDetailsFragment extends Fragment implements View.OnClickListener,View.OnTouchListener
{
    RelativeLayout backButton;
    EditText bankGiro,telephoneNumber,swishHandel,enterpriseMail;
    TextView companyName,companyLocation;
    FragmentTabHost mTabhost;
    SelectedViewCallback mCallback;
    Button nextButton;
    boolean isEnable=true;
    public static final String EMAIL_PATTERN="[^\\s@]+@[^\\s@]+\\.[a-zA-z][a-zA-Z][a-zA-Z]*";

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mCallback=(SelectedViewCallback)context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.company_details_layout,container,false);

        mTabhost=(FragmentTabHost)getActivity().findViewById(R.id.fragment_tabhost);
        LinearLayout cancelImage=(LinearLayout)getActivity().findViewById(R.id.cancel_image);

        companyName=(TextView)view.findViewById(R.id.company_name);
        companyName.setText(((CompanyRegistration)getActivity()).getBusinessName());
        companyLocation=(TextView)view.findViewById(R.id.company_location);

        bankGiro=(EditText)view.findViewById(R.id.bankgiro);
        bankGiro.addTextChangedListener(new AutoAddTextWatcher(bankGiro, "-",4));
        bankGiro.setOnTouchListener(this);

        telephoneNumber=(EditText)view.findViewById(R.id.telephone);
        telephoneNumber.addTextChangedListener(new AutoAddTextWatcher(telephoneNumber,"+",0));
        telephoneNumber.addTextChangedListener(new AutoAddTextWatcher(telephoneNumber,"-",3));
        telephoneNumber.setOnTouchListener(this);

        swishHandel=(EditText)view.findViewById(R.id.swish_handel);
        swishHandel.addTextChangedListener(new AutoAddTextWatcher(swishHandel," ",3,6));
        swishHandel.setOnTouchListener(this);

        enterpriseMail=(EditText)view.findViewById(R.id.mail);
        enterpriseMail.setOnTouchListener(this);

        companyLocation=(TextView)view.findViewById(R.id.company_location);
        companyLocation.setOnTouchListener(this);

        // Request focus and show soft keyboard automatically
        bankGiro.requestFocus();
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        //Back and Next Button
        backButton=(RelativeLayout) view.findViewById(R.id.back_button);
        nextButton=(Button) view.findViewById(R.id.next_button);
        backButton.setOnClickListener(this);
        nextButton.setOnClickListener(this);
        cancelImage.setOnClickListener(this);
        companyLocation.setOnClickListener(this);

        /**********onEditorAction on EditText*****************/
        bankGiro.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId== EditorInfo.IME_ACTION_NEXT) {
                    checkEnteredData(bankGiro,getString(R.string.bankgiro_error_msg));
                }
                return false;
            }
        });
        telephoneNumber.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId== EditorInfo.IME_ACTION_NEXT) {
                    checkEnteredData(telephoneNumber,getString(R.string.telephn_error_msg));
                }
                return false;
            }
        });
        swishHandel.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId== EditorInfo.IME_ACTION_NEXT){
                    checkEnteredData(swishHandel,getString(R.string.swish_error_msg));
                }
                return false;
            }
        });

        enterpriseMail.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId== EditorInfo.IME_ACTION_NEXT) {
                    checkEnteredData(enterpriseMail,getString(R.string.invalid_email_address));
                }
                return false;
            }
        });

        return view;
    }

    private void checkEnteredData(EditText editText, String message) {
        if(!editText.getText().toString().isEmpty()) {
            if (editText.equals(bankGiro)) {
                if (editText.getText().toString().length()<9) {
                    notifyUser(message);
                    setFocus(bankGiro);
                }
                else {
                    setFocusonView();
                }
            }
            else if(editText.equals(telephoneNumber)) {
                if (editText.getText().toString().length()< 13){
                    notifyUser(message);
                    setFocus(telephoneNumber);
                }
                else {
                    setFocusonView();
                }
            }
            else if(editText.equals(swishHandel)) {
                if (editText.getText().toString().length()<12) {
                    notifyUser(message);
                    setFocus(swishHandel);
                }
                else {
                    setFocusonView();;
                }
            }
            else if(editText.equals(enterpriseMail)) {
                if (!editText.getText().toString().matches(EMAIL_PATTERN)) {
                    notifyUser(message);
                    setFocus(enterpriseMail);
                }
                else {
                    setFocusonView();
                }
            }
        }
       else {
            setFocusonView();
        }
    }

    private void setFocusonView() {
        telephoneNumber.setFocusableInTouchMode(true);
        telephoneNumber.setFocusable(true);

        swishHandel.setFocusableInTouchMode(true);
        swishHandel.setFocusable(true);

        enterpriseMail.setFocusableInTouchMode(true);
        enterpriseMail.setFocusable(true);

        bankGiro.setFocusableInTouchMode(true);
        bankGiro.setFocusable(true);
    }

    private void setFocus(EditText editText) {
        if(editText.equals(bankGiro)) {
            bankGiro.setFocusableInTouchMode(true);
            bankGiro.setFocusable(true);

            telephoneNumber.setFocusable(false);
            swishHandel.setFocusable(false);
            enterpriseMail.setFocusable(false);
        }
        else if(editText.equals(telephoneNumber)){
            telephoneNumber.setFocusableInTouchMode(true);
            telephoneNumber.setFocusable(true);

            swishHandel.setFocusable(false);
            enterpriseMail.setFocusable(false);
            bankGiro.setFocusable(false);
        }
        else if(editText.equals(swishHandel)){
            swishHandel.setFocusable(true);
            swishHandel.setFocusableInTouchMode(true);

            telephoneNumber.setFocusable(false);
            enterpriseMail.setFocusable(false);
            bankGiro.setFocusable(false);
        }
        else if(editText.equals(enterpriseMail)){
            enterpriseMail.setFocusableInTouchMode(true);
            enterpriseMail.setFocusable(true);

            telephoneNumber.setFocusable(false);
            swishHandel.setFocusable(false);
            bankGiro.setFocusable(false);
        }
    }
    private void notifyUser(String message) {
        Snackbar.make(getView(),message,Snackbar.LENGTH_SHORT)
                .setAction(getString(R.string.max_ok_btn), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                    }
                }).setActionTextColor(getResources().getColor(R.color.app_color)).show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.next_button:
                //Hide Soft Keyboard
                InputMethodManager inputManager = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow((null == getActivity().getCurrentFocus()) ? null :
                        getActivity().getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

                checkEnteredDetails();
                 break;
            case R.id.back_button:
                 mTabhost.setCurrentTab(0);
                 mCallback.selectedTabView();
                 break;
            case R.id.cancel_image:
                 getActivity().finish();
                 break;
            case R.id.company_location:
                 Double lat=((CompanyRegistration)getActivity()).getLatitude();
                 Double lng=((CompanyRegistration)getActivity()).getLongitude();
                 if(lat!=null&&lng!=null) {
                     Intent intent = new Intent(getActivity(), CompanyLocation.class);
                     Bundle bundle= new Bundle();
                     bundle.putDouble("lat",lat);
                     bundle.putDouble("lng",lng);
                     intent.putExtras(bundle);
                     startActivity(intent);
                 }
                else{
                     notifyUser(getString(R.string.location_error));
                 }
                 break;
        }
    }

    private void checkEnteredDetails() {
        if (!bankGiro.getText().toString().isEmpty() ||!telephoneNumber.getText().toString().isEmpty()||!swishHandel.getText().toString().isEmpty()){
            isEnable=true;
            if(!bankGiro.getText().toString().isEmpty()&&bankGiro.getText().toString().length()<9) {
                isEnable=false;
                notifyUser(getString(R.string.bankgiro_error_msg));
            }
            if(!telephoneNumber.getText().toString().isEmpty()&&telephoneNumber.getText().toString().length()<13){
                isEnable=false;
                notifyUser(getString(R.string.telephn_error_msg));
            }
            if(!swishHandel.getText().toString().isEmpty()&&swishHandel.getText().toString().length()<12) {
                isEnable=false;
                notifyUser(getString(R.string.swish_error_msg));
            }
            if (!enterpriseMail.getText().toString().isEmpty()&&!enterpriseMail.getText().toString().matches(EMAIL_PATTERN)) {
                isEnable=false;
                notifyUser(getString(R.string.invalid_email_address));
            }
            else {
                if(isEnable) {
                    Company companyData= Company.getInstance();
                    if(!bankGiro.getText().toString().isEmpty()) {
                        companyData.setBankgiro(bankGiro.getText().toString());
                    }
                    if(!telephoneNumber.getText().toString().isEmpty()) {
                        companyData.setPhoneNumber(telephoneNumber.getText().toString());
                    }
                    if(!swishHandel.getText().toString().isEmpty()) {
                        companyData.setSwishNumber(swishHandel.getText().toString());
                    }
                    if(!enterpriseMail.getText().toString().isEmpty()) {
                        companyData.setEmail(enterpriseMail.getText().toString());
                    }
                    if(!companyLocation.getText().toString().isEmpty()) {
                        companyData.setLocationName(companyLocation.getText().toString());
                        companyData.setLat((Double) SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readJobLocationData("lat"));
                        companyData.setLng((Double) SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readJobLocationData("lng"));
                    }
                    mTabhost.setCurrentTab(2);
                    mCallback.unselectedTabView();
                }
            }
        }
        else{
            mTabhost.setCurrentTab(2);
            mCallback.unselectedTabView();
        }
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (view.getId()){
            case R.id.bankgiro:
                checkEnteredData(bankGiro,getString(R.string.bankgiro_error_msg));
                break;
            case R.id.telephone:
                checkEnteredData(telephoneNumber,getString(R.string.telephn_error_msg));
                break;
            case R.id.swish_handel:
                checkEnteredData(swishHandel,getString(R.string.swish_error_msg));
                break;
            case R.id.mail:
                checkEnteredData(enterpriseMail,getString(R.string.invalid_email_address));
                break;
        }
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
        String place=(String) SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readJobLocationData("place");
        if (!place.isEmpty()) {
            companyLocation.setText(place);
        }
    }
}
