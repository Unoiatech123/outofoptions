package com.unoiatech.outofoptions.companymodule;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;
import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.api.ApiClient;
import com.unoiatech.outofoptions.api.ApiInterface;
import com.unoiatech.outofoptions.home.HomeScreenActivity;
import com.unoiatech.outofoptions.model.CompanyData;
import com.unoiatech.outofoptions.model.User;
import com.unoiatech.outofoptions.util.ApiError;
import com.unoiatech.outofoptions.util.BankId.ErrorUtils;
import com.unoiatech.outofoptions.util.S3Utils;
import com.unoiatech.outofoptions.api.URL;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Unoiatech on 7/24/2017.
 */

public class CompanyProfile extends Fragment implements View.OnClickListener {
    private ImageView profile_Image,memberImage1,memberImage2;
    private TextView name,memberName1,memberName2;
    private TextView title;
    private TextView edit_profile;
    private TextView companyLocation;
    private TextView reviews;
    private TextView about,tagline,completedJobs,lastWorked,inviteMember;
    private String companyLogo;
    private Long companyId;
    LinearLayout memberLayout;
    private SimpleRatingBar ratingBar;
    private RelativeLayout seeMore;
    private static CompanyProfile companyProfile;
    private static String TAG="CompanyProfile";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View view= inflater.inflate(R.layout.company_profile,container,false);

        /****Initialise Views ********/
        initViews(view);

        /*******Click Listeners************/
        seeMore.setOnClickListener(this);
        inviteMember.setOnClickListener(this);
        return view;
    }

    private void initViews(View view) {
        title=(TextView)view.findViewById(R.id.title);
        ratingBar=(SimpleRatingBar)view.findViewById(R.id.rating_bar);
        reviews=(TextView)view.findViewById(R.id.reviews);
        profile_Image=(ImageView)view.findViewById(R.id.company_image);
        name=(TextView)view.findViewById(R.id.name);
        tagline=(TextView)view.findViewById(R.id.tagline);
        about=(TextView)view.findViewById(R.id.about);
        companyLocation=(TextView)view.findViewById(R.id.company_location);
        edit_profile=(TextView)view.findViewById(R.id.edit);
        seeMore=(RelativeLayout)view.findViewById(R.id.see_more_layout);
        completedJobs=(TextView)view.findViewById(R.id.completed_jobs);
        lastWorked=(TextView)view.findViewById(R.id.last_worked);
        memberImage1=(ImageView)view.findViewById(R.id.member_image1);
        memberImage2=(ImageView)view.findViewById(R.id.member_image2);
        memberName1=(TextView)view.findViewById(R.id.member_name1);
        memberName2=(TextView)view.findViewById(R.id.member_name2);
        memberLayout=(LinearLayout)view.findViewById(R.id.member_layout);
        inviteMember=(TextView)view.findViewById(R.id.invite_member);
    }

    @Override
    public void onResume() {
        super.onResume();

        /****GetUserProfile***********/
        HomeScreenActivity.showTabHost();
        edit_profile.setVisibility(View.VISIBLE);
        title.setText(getString(R.string.company_profile_title));
        edit_profile.setText(getString(R.string.edit_title));
        edit_profile.setOnClickListener(CompanyProfile.this);
        Long companyId= getArguments().getLong("companyId");
        getCompanyProfile(companyId);
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.edit:
                Intent intent = new Intent(getActivity(), EditCompanyProfile.class);
                Bundle bundle= new Bundle();
                bundle.putLong("company_id",companyId);
                bundle.putString("tagline",tagline.getText().toString());
                bundle.putString("about",about.getText().toString());
                bundle.putString("logo",companyLogo);
                intent.putExtras(bundle);
                startActivity(intent);
                //getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                break;

            case R.id.see_more_layout:
                Intent intent1= new Intent(getActivity(),CompanyStaff.class);
                Bundle bundle1= new Bundle();
                bundle1.putLong("company_id",companyId);
                intent1.putExtras(bundle1);
                getActivity().startActivity(intent1);
                break;

            case R.id.invite_member:
                Intent intent2= new Intent(getActivity(),CompanyStaff.class);
                Bundle bundle2= new Bundle();
                bundle2.putLong("company_id",companyId);
                intent2.putExtras(bundle2);
                getActivity().startActivity(intent2);
                break;
        }
    }

    public void getCompanyProfile(long userId) {
        ApiInterface apiService= ApiClient.getClient().create(ApiInterface.class);
        Call<CompanyData> call= apiService.getCompanyProfile(userId);
        call.enqueue(new Callback<CompanyData>() {
            @Override
            public void onResponse(Call<CompanyData> call, Response<CompanyData> response) {
                Log.d(TAG,"Response"+response.body()+" "+response.message());
                if(response.code()==200) {
                    showRetroResult(response.body());
                }else{
                    ApiError error = ErrorUtils.parseError(response);
                    Snackbar.make(getView(),error.getMessage(),Snackbar.LENGTH_SHORT).show();
                    Log.d("Code" + "Code", "" + response);
                }
            }

            @Override
            public void onFailure(Call<CompanyData> call, Throwable t) {
                Log.d("Failure","Failure"+t.toString());
            }
        });
    }

   public  void showRetroResult(CompanyData body) {
       ArrayList<User> memberList= new ArrayList<>();
       try {
            String companyName = body.getCompany().getBusinessName();
            companyId=body.getCompany().getId();
            companyLogo=body.getCompany().getLogo();

            //Set Company Data
            if(companyLogo!=null)
            Glide.with(getActivity()).load(S3Utils.getCompanylogo(companyId,companyLogo)).into(profile_Image);
            name.setText(companyName);
            if(body.getCompany().getAbout()!=null && !body.getCompany().getAbout().isEmpty()){
                about.setText(body.getCompany().getAbout());}
            else{
                about.setText(R.string.add_company_about);}
            if(body.getCompany().getTagline()!=null && !body.getCompany().getTagline().isEmpty()){
                tagline.setText(body.getCompany().getTagline());}
            else{
                tagline.setText(R.string.add_company_tagline);}

            //Set Job Completed Text
            String completedJobText=getString(R.string.complete)+"\n"+body.getJobs_completed()+" "+getString(R.string.jobs);
            SpannableStringBuilder completedJobsText = new SpannableStringBuilder(completedJobText);
            completedJobsText.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.unselected_menu)), 0, 9, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            completedJobsText.setSpan(new RelativeSizeSpan(0.8f), 0, 9,0);
            completedJobs.setText(completedJobsText);

            //Set Text to Last Worked
            String lastWorkedText=getString(R.string.last_work_text)+"\n"+"20th Feb";
            SpannableStringBuilder lastWork = new SpannableStringBuilder(lastWorkedText);
            lastWork.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.unselected_menu)), 0, 11, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            lastWork.setSpan(new RelativeSizeSpan(0.8f),0,11,0);
            lastWorked.setText(lastWork);

            //set staff member Images and Name
            if (body.getCoordinator() != null) {
                memberLayout.setVisibility(View.VISIBLE);
                inviteMember.setVisibility(View.GONE);
                memberList.add(0, body.getCoordinator());
                if (body.getMembers().size() > 0)
                    for (int i = 0; i < body.getMembers().size(); i++) {
                        memberList.add(1,body.getMembers().get(i));
                    }
                }
                else {
                if (body.getMembers().size() > 0) {
                    memberLayout.setVisibility(View.VISIBLE);
                    inviteMember.setVisibility(View.GONE);
                    for (int i = 0; i < body.getMembers().size(); i++) {
                            memberList.add(body.getMembers().get(i));
                        }
                    }
                }
            if(memberList.size()>0){
                memberLayout.setVisibility(View.VISIBLE);
                inviteMember.setVisibility(View.GONE);
                for(int i=0;i<memberList.size();i++){
                    if(i==0) {
                        setMemberImage(memberImage1,memberList.get(i).getImagePath(),memberList.get(i).getId());
                        memberName1.setText(memberList.get(i).getFirstName() + " " +memberList.get(i).getLastName());
                    }
                    else if(i==1){
                        setMemberImage(memberImage2,memberList.get(i).getImagePath(),memberList.get(i).getId());
                        memberName2.setText(memberList.get(i).getFirstName() + " " +memberList.get(i).getLastName());
                    }
                }
            }
            companyLocation.setText(body.getCompany().getLocationName());
            reviews.setText("( "+body.getReviews_count()+" )");
        }
        catch (NullPointerException ex) {
            ex.printStackTrace();
        }
    }

    private void setMemberImage(ImageView memberImage, String imagePath, Long id) {
        if(imagePath!=null){
            if(imagePath.contains("\\")){
                String lastIndex= imagePath.replace("\\", " ");
                String[] Stamp=lastIndex.split("\\s");
                String timeStamp=Stamp[3];
                String userImageUrl = URL.BASE_URL+"/users/download/"+id+"/image/"+timeStamp;
                Glide.with(getActivity()).load(userImageUrl).into(memberImage);
            }
            else{
                Glide.with(getActivity()).load(S3Utils.getDownloadUserImageURL(id,imagePath)).into(memberImage);
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        HomeScreenActivity.removeTabHost();
    }

    public static CompanyProfile getCompanyProfile() {
        if(companyProfile==null) {
            companyProfile=new CompanyProfile();
        }
        return companyProfile;
    }
}

