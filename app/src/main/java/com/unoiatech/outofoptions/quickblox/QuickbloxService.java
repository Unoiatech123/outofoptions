package com.unoiatech.outofoptions.quickblox;

import android.content.Context;

import com.unoiatech.outofoptions.model.User;

import java.io.File;

/**
 * Created by unoiaAndroid on 5/5/2017.
 */

public class QuickbloxService {
    public void registerUserOnQB(Context context,User user,File avatar) {
        QuickbloxUser.getInstance().initializeSession(context);
        QuickbloxUser.getInstance().createSession();
        QuickbloxUser.getInstance().signup(user);
        QuickbloxUser.getInstance().signIn(user);
        QuickbloxUser.getInstance().uploadProfilePic(avatar,user.getQuickbloxId());
    }

    public void loginUserInQB(Context context,User user){
        QuickbloxUser.getInstance().initializeSession(context);
        QuickbloxUser.getInstance().createSession();
        QuickbloxUser.getInstance().signIn(user);
    }
}
