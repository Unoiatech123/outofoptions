package com.unoiatech.outofoptions.home.fragment.filter;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.util.SharedPrefsHelper;
import java.util.ArrayList;

/**
 * Created by unoiaAndroid on 3/29/2017.
 */

public class CategoryFragment extends DialogFragment {
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private String TAG="Category Fragment";
    private static ArrayList<Dataset> mydataset = new ArrayList<>();
    private static CategoryFragment categoryFragment;


    //create single instance of Category Fragment
    public static CategoryFragment getInstance(){
        if(categoryFragment==null) {
            categoryFragment = new CategoryFragment();
        }
        return categoryFragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.category_fragment_layout, container, false);

        Button reset=(Button) view.findViewById(R.id.reset_button);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.category_recycler_view);
        ImageView cancel_image=(ImageView)view.findViewById(R.id.cancel_image);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 mydataset.clear();
                 setupDefaultDataset(true);
                 mAdapter.notifyDataSetChanged();
            }
        });

        cancel_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        //adding default dataset
        if(((String)SharedPrefsHelper.getInstance(SharedPrefsHelper.FILTER_OPTIONS_PREF).
                readSearchRequestByKey("category_text")).equalsIgnoreCase("all")){
            setupDefaultDataset(true);
        }
        else{
         // contains specific categories
           mydataset.clear();

           // get data from shared Preference
           ArrayList<String> existingCategories=(ArrayList<String>) SharedPrefsHelper.getInstance(SharedPrefsHelper.FILTER_OPTIONS_PREF).
                            readSearchRequestByKey("category");

            //add data to Dataset
            String[] category = getResources().getStringArray(R.array.categories_array);
            for (int i = 0; i < category.length; i++) {
                Dataset dataset = new Dataset();
                if(existingCategories.contains(category[i])) {
                    dataset.setCategory(category[i]);
                    dataset.setValue(1);
                    Log.e("category else",existingCategories.size()+"");
                }
                else {
                    dataset.setCategory(category[i]);
                    dataset.setValue(0);
                    Log.e("category else",existingCategories.size()+"");
                }
                mydataset.add(dataset);
            }
        }
        mAdapter = new CategoryAdapter(mydataset);
        mRecyclerView.setAdapter(mAdapter);
    }

    public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {
        // Provide a reference to the views for each data item
        // Complex data items may need more than one view per item, and
        // you provide access to all the views for a data item in a view holder
        public  class ViewHolder extends RecyclerView.ViewHolder {
            public TextView mTextView;
            public CheckBox mCheckBox;
            public ViewHolder(View v) {
                super(v);
                mTextView = (TextView) v.findViewById(R.id.category_name);
                mCheckBox=(CheckBox) v.findViewById(R.id.radio_button);
            }
        }

        // Provide a suitable constructor
        public CategoryAdapter(ArrayList<Dataset> datasets) {
            mydataset=datasets;
          }

        // Create new views (invoked by the layout manager)
        @Override
        public CategoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,int viewType) {
            // create a new view
            View v =  LayoutInflater.from(parent.getContext()).inflate(R.layout.category_list_item_layout, parent, false);
            ViewHolder vh = new ViewHolder(v);
            return vh;
        }

        // Replace the contents of a view (invoked by the layout manager)
        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
             final int FIRST_ITEM=0;
             final int LAST_ITEM= mydataset.size()-1;

            // - get element from your dataset at this position
            // - replace the contents of the view with that element
            holder.mTextView.setText(mydataset.get(position).getCategory());

            //set the state of checkbox accorrding to value
            if(mydataset.get(position).getValue()==0) {
                holder.mCheckBox.setChecked(false);
            }
            if(mydataset.get(position).getValue()==1){
                holder.mCheckBox.setChecked(true);
            }

            //click listerner for checkbox
            holder.mCheckBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

            //toggel checkbox status inside value list
             if(mydataset.get(position).getValue()==0)
             mydataset.get(position).setValue(1);
                 else
               mydataset.get(position).setValue(0);

             // if selected other postion than first , uncheck it
             if(position!=0&&position!=mydataset.size()-1) {
                 mydataset.get(0).setValue(0);
                 mydataset.get(mydataset.size()-1).setValue(0);
                 mAdapter.notifyItemChanged(0);
                 Log.d(TAG,"position !0");
             }
             // if selected other postion than last , uncheck it
                if(position!=mydataset.size()-1){
                    mydataset.get(mydataset.size()-1).setValue(0);
                    mAdapter.notifyItemChanged(mydataset.size()-1);
                    Log.d(TAG,"position !0");
                }
            //if first item uncheck rest
            if(position==FIRST_ITEM) {
                Log.d(TAG,"position0");

                mydataset.clear();
                setupDefaultDataset(true);
                mAdapter.notifyDataSetChanged();
            }
                    //if first item uncheck rest
                    if(position==LAST_ITEM) {
                        Log.d(TAG,"position0");

                        mydataset.clear();
                        setupDefaultDataset(false);
                        mAdapter.notifyDataSetChanged();
                    }

                    //if nothing is selected
                  if(!containsSelectedValue()) {
                       mydataset.get(FIRST_ITEM).setValue(1);
                       mAdapter.notifyItemChanged(0);
                       Log.d(TAG,"not contains 1");
                   }else
                      Log.d(TAG," contains 1");
               }
            });
      }

        // Return the size of your dataset (invoked by the layout manager)
        @Override
        public int getItemCount() {
            return mydataset.size();
        }
    }

    class Dataset{
        private String category;
        private Integer value;
        public String getCategory() {
            return category;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public Integer getValue() {
            return value;
        }

        public void setValue(Integer value) {
            this.value = value;
        }
    }

    void setupDefaultDataset(boolean isFirst) {
        ArrayList<Integer> value = new ArrayList<>();

        //set the default state
        mydataset.clear();
        if (isFirst){
        value.add(1);
        value.add(0);
        value.add(0);
        value.add(0);
        value.add(0);
        value.add(0);
        value.add(0);
        value.add(0);
        value.add(0);
        value.add(0);
     }
        else
        {
            value.add(0);
            value.add(0);
            value.add(0);
            value.add(0);
            value.add(0);
            value.add(0);
            value.add(0);
            value.add(0);
            value.add(0);
            value.add(1);
        }

            String[] category = getResources().getStringArray(R.array.categories_array);
            for (int i = 0; i < category.length; i++) {
                Dataset dataset = new Dataset();
                dataset.setCategory(category[i]);
                dataset.setValue(value.get(i));
                mydataset.add(dataset);
            }
}

    public boolean containsSelectedValue( ) {
        for(Dataset o : mydataset) {
            if(o.getValue().intValue()==1) {
                Log.d(TAG,"true");
                return true;
            }
        }
        Log.d(TAG,"false");
        return false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
            }

    @Override
    public void onPause() {
        super.onPause();
        ArrayList<String> selectedCategories= new ArrayList<>();

        //add selected categories to arraylist
        for(Dataset dataset:mydataset)
         {
            if(dataset.getValue()==1)
                selectedCategories.add(dataset.getCategory());
        }

        //save categories as string
        SharedPrefsHelper.getInstance(SharedPrefsHelper.FILTER_OPTIONS_PREF).
                writeSearchRequest("category_text",android.text.TextUtils.join(" , ", selectedCategories));

        //remove all from array
        if(selectedCategories.contains("All"))
            selectedCategories.remove("All");

        //save selected categories
        SharedPrefsHelper.getInstance(SharedPrefsHelper.FILTER_OPTIONS_PREF).
                writeSearchRequest("category",selectedCategories);
    }
}
