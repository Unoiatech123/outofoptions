package com.unoiatech.outofoptions.home.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.fragments.instantjob.InstantJobActivity;
import com.unoiatech.outofoptions.home.HomeScreenActivity;
import com.unoiatech.outofoptions.model.JobPostingDataHolder;

/**
 * Created by unoiatech on 5/12/2016.
 */
public class CategoryDialogFragment extends Fragment implements View.OnClickListener{
    Button[] button;
    private static CategoryDialogFragment categoryFrag;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view= inflater.inflate(R.layout.category_layout, container, false);
        //LinearLayout backImage=(LinearLayout)view.findViewById(R.id.back_image);
        //fetch Radio Button View By Ids
        button=new Button[9];
        for(int i=0;i<button.length;i++){
            String resId="r"+(i+1);

            int resID = getResources().getIdentifier(resId, "id", getActivity().getPackageName());
            button[i] = ((Button) view.findViewById(resID));
            button[i].setOnClickListener(this);
        }


        /*final int images[]={R.drawable.home_n_gardening, R.drawable.vehicle, R.drawable.health_and_beauty, R.drawable.photographer_green, R.drawable.animal_passing
                , R.drawable.carring_and_assemble, R.drawable.minor_tech_fixes, R.drawable.ground_n_construction, R.drawable.all};
        GridView gridview = (GridView) view.findViewById(R.id.gridview);
        gridview.setAdapter(new CategoryAdapter(this,getResources().getStringArray(R.array.categories),images));*/
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        HomeScreenActivity.removeTabHost();
    }

    @Override
    public void onPause() {
        super.onPause();
        //HomeScreenActivity.showTabHost();
    }

    @Override
    public void onClick(View view) {
        getActivity().getSupportFragmentManager().popBackStack();
        String selectedCategory=null;
        if(view.getId()==R.id.r1) {
            selectedCategory="Home & Gardening";
        }
        else if(view.getId()==R.id.r2){
            selectedCategory="Car & Vehicle";
        }
        else if(view.getId()==R.id.r3){
            selectedCategory="Health & Beauty";
        }
        else if(view.getId()==R.id.r4){
            selectedCategory="Photoshoot";
        }
        else if(view.getId()==R.id.r5)
        {
            selectedCategory="Animal sitting & Pet care";
        }
        else if(view.getId()==R.id.r6) {
            selectedCategory="Carrying & Assemble";
        }
        else if(view.getId()==R.id.r7) {
            selectedCategory="Computer & Tech fixes";
        }
        else if(view.getId()==R.id.r8) {
            selectedCategory="Ground & Construction";
        }
        else if(view.getId()==R.id.r9) {
            selectedCategory="Others";
        }
        JobPostingDataHolder dataHolderClass = JobPostingDataHolder.getInstance();
        dataHolderClass.setCategory(selectedCategory);
        Intent intent = new Intent(getActivity(), InstantJobActivity.class);
        try
        {
            if(getArguments().getString("View_id").equals("Repost")){
                intent.putExtra("job_info",getArguments().getParcelable("job_info"));
                intent.putExtra("View_id","Repost");
            }
        }
        catch (NullPointerException ex){
            ex.printStackTrace();
        }
        getActivity().startActivity(intent);
    }

    public static CategoryDialogFragment getCategoryFrag() {
        if(categoryFrag==null) {
            categoryFrag=new CategoryDialogFragment();
        }
        return categoryFrag;
    }
}

