package com.unoiatech.outofoptions.home;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TabHost;

import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.activity.RegisterActivity;
import com.unoiatech.outofoptions.api.ApiClient;
import com.unoiatech.outofoptions.api.ApiInterface;
import com.unoiatech.outofoptions.fragments.menufragments.notificationscreen.NotificationScreen;
import com.unoiatech.outofoptions.home.fragment.CategoryDialogFragment;
import com.unoiatech.outofoptions.menu.MenuFragment;
import com.unoiatech.outofoptions.home.fragment.filter.FilterDialogFragment;
import com.unoiatech.outofoptions.jobdetail.chat.ChatFragment;
import com.unoiatech.outofoptions.model.User;
import com.unoiatech.outofoptions.util.ConnectionDetection;
import com.unoiatech.outofoptions.util.SharedPrefsHelper;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by unoiaAndroid on 3/20/2017.
 */

public class HomeScreenActivity extends AppCompatActivity  {
    private static String TAG="HomeScreenActivity";
    private static FragmentTabHost mTabHost;
    private static String emailVerified="completeProfile";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e("OnCreate","OnCreate");
        setContentView(R.layout.home_screen_layout);

        // initialize tool bar
        // initToolbar();
        // initialize tabs
        initTabHost();

        /*implement addOnBackStackChangedListener to handle the on backpress event
        of the fragments inside homescreen activity*/
        getSupportFragmentManager().addOnBackStackChangedListener(getListener());
        mTabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String tabId) {
                if(tabId.equals("instant_job_post")) {
                    if(emailVerified.equals("verified")){
                    }
                    else{
                        mTabHost.setCurrentTab(0);
                        if(emailVerified.equals("completeProfile")) {
                            Snackbar.make(getWindow().getDecorView(), getString(R.string.complete_profile_check), Snackbar.LENGTH_LONG).setAction(getString(R.string.max_ok_btn), new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent1 = new Intent(HomeScreenActivity.this, RegisterActivity.class);
                                    startActivity(intent1);
                                }
                            }).setActionTextColor(getResources().getColor(R.color.app_color)).show();
                        }
                        else{
                            Snackbar.make(getWindow().getDecorView(), getString(R.string.complete_profile_check), Snackbar.LENGTH_LONG).setAction(getString(R.string.max_ok_btn), new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent1 = new Intent(HomeScreenActivity.this, RegisterActivity.class);
                                    startActivity(intent1);
                                }
                            }).setActionTextColor(getResources().getColor(R.color.app_color)).show();
                            //Snackbar.make(getWindow().getDecorView(),getString(R.string.verify_email_check),Snackbar.LENGTH_LONG).show();
                        }
                    }
                }
            }
        });
    }

    private void initToolbar() {
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
    }

    private void initTabHost() {
        mTabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);

        mTabHost.setup(this, getSupportFragmentManager(), android.R.id.tabcontent);
        mTabHost.getTabWidget().setStripEnabled(false);
        mTabHost.getTabWidget().setDividerDrawable(null);

        mTabHost.addTab(mTabHost.newTabSpec("jobs_list").setIndicator(prepareTabView(R.drawable.home_selector)),
                JobDealScreen.class, null);
        mTabHost.addTab(mTabHost.newTabSpec("chat").setIndicator(prepareTabView(R.drawable.chat_selector)),
                ChatFragment.class, null);
        mTabHost.addTab(mTabHost.newTabSpec("instant_job_post").setIndicator(prepareTabViewForInstantJob(R.drawable.instant_job_icon)),
                CategoryDialogFragment.class, null);
        mTabHost.addTab(mTabHost.newTabSpec("notification").setIndicator(prepareTabViewForMenu(R.drawable.main_menu_selector)),
                NotificationScreen.class, null);
        mTabHost.addTab(mTabHost.newTabSpec("menu").setIndicator(prepareTabViewForMenu(R.drawable.main_menu)),
                MenuFragment.class, null);

        mTabHost.getTabWidget().getChildAt(4).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mTabHost.setCurrentTab(4);
                LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(getApplicationContext());
                Intent i = new Intent("TAG_REFRESH");
                lbm.sendBroadcast(i);
                Log.e(TAG, "inside" + mTabHost.getCurrentTab());
            }
        });
    }

    private View prepareTabView(int Image) {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.tab_icon_layout, null);
        ImageView image = (ImageView) view.findViewById(R.id.tab_image);
        image.setBackgroundResource(Image);
        return view;
    }

    private View prepareTabViewForInstantJob(int Image) {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.instant_tab_icon_layout, null);
        ImageView image = (ImageView) view.findViewById(R.id.tab_image);
        image.setBackgroundResource(Image);
        return view;
    }

    private View prepareTabViewForMenu(int Image) {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.menu_tab_icon_layout, null);
        ImageView image = (ImageView) view.findViewById(R.id.tab_image);
        image.setBackgroundResource(Image);
        return view;
    }

    // for custom font across the activity
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private FragmentManager.OnBackStackChangedListener getListener(){
        FragmentManager.OnBackStackChangedListener result = new FragmentManager.OnBackStackChangedListener()
        {
            public void onBackStackChanged()
            {
                FragmentManager manager = getSupportFragmentManager();
                if (manager != null){
                    Log.e("Manager_is_not_Null","Manager_is_not_null"+"  "+manager.getBackStackEntryCount()+"  "+manager.findFragmentByTag("filter"));

                    /*if( Constants.returnFromFilterFragment)
                    {
                     JobsListFragment jobsListFragment = (JobsListFragment) manager.findFragmentByTag("jobs_list");
                     jobsListFragment.onResume();
                        Log.e(TAG,"onResume called from OnBackStackChangedListener");
                    }*/

                    FilterDialogFragment filterDialogFragment = (FilterDialogFragment) manager.findFragmentByTag("filter");
                    if (filterDialogFragment != null)
                        filterDialogFragment.onResume();
                }
            }
        };
        return result;
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e(TAG,"onResume");

        //Check User Account is Verified
        User user= SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readUserPref();
        if(ConnectionDetection.isInternetAvailable(this)) {
            getVerifiedIds(user.getId());
        }else{
            Snackbar.make(getWindow().getDecorView(),getString(R.string.internet_connection_error),Snackbar.LENGTH_SHORT).show();
        }
    }

    public static void showTabHost()
    {
        mTabHost.getTabWidget().setVisibility(View.VISIBLE);
    }

    public static void hideTabHost()
    {
        mTabHost.getTabWidget().setVisibility(View.INVISIBLE);
    }

    public static void removeTabHost()
    {
        mTabHost.getTabWidget().setVisibility(View.GONE);
    }

    @Override
    public void onBackPressed() {
        Log.e("Back_press","Back_press"+mTabHost.getCurrentTab()+"  "+getSupportFragmentManager().getBackStackEntryCount()+"  "+getSupportFragmentManager().findFragmentByTag("filter"));
        int index = mTabHost.getCurrentTab();

        //To handle childFragments of Menu Screen Fragment using getChildFragmentManager()
        MenuFragment menuFrag=(MenuFragment)getSupportFragmentManager().findFragmentByTag("menu");
        if(index==4) {
            if (menuFrag.getChildFragmentManager().getBackStackEntryCount()>0) {
                menuFrag.getChildFragmentManager().popBackStack();
            } else {
                mTabHost.setCurrentTab(0);
            }
        }
        else if(index==3) {
            mTabHost.setCurrentTab(0);
        }
        else if(index==2) {
            mTabHost.setCurrentTab(0);
        }
        else if(index==1) {
            mTabHost.setCurrentTab(0);
        }
        else if(getSupportFragmentManager().findFragmentByTag("filter")!=null&&getSupportFragmentManager().getBackStackEntryCount()==1) {
        }
        else{
            super.onBackPressed();
        }
    }

    private void getVerifiedIds(Long id) {
        ApiInterface apiService= ApiClient.getClient().create(ApiInterface.class);
        Call<ArrayList<User>> call= apiService.verifyIdsOnProfile(id);
        call.enqueue(new Callback<ArrayList<User>>() {
            @Override
            public void onResponse(Call<ArrayList<User>> call, Response<ArrayList<User>> response) {
                Log.d("response","response"+" body" +response.body());
                showAccountVerification(response.body());
            }

            @Override
            public void onFailure(Call<ArrayList<User>> call, Throwable t) {
                Log.d("Failure","Failure"+t.toString());
            }
        });
    }
    public  void showAccountVerification(ArrayList<User> body) {
        for (User item : body) {
            if (item.getVerificationType().equals("email")) {
                emailVerified="profileCompleted";
                if(item.getIsVerified()==1){
                    emailVerified="verified";
                }
            }
        }
    }
}
