package com.unoiatech.outofoptions.home.fragment.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.activity.RegisterActivity;
import com.unoiatech.outofoptions.api.ApiClient;
import com.unoiatech.outofoptions.api.ApiInterface;
import com.unoiatech.outofoptions.home.HomeScreenActivity;
import com.unoiatech.outofoptions.home.fragment.JobsListFragment;
import com.unoiatech.outofoptions.jobdetail.JobDetailFragment;
import com.unoiatech.outofoptions.model.Job;
import com.unoiatech.outofoptions.model.User;
import com.unoiatech.outofoptions.util.DateTimeConverter;
import com.unoiatech.outofoptions.util.S3Utils;
import com.unoiatech.outofoptions.util.SharedPrefsHelper;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;
import java.util.List;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by unoiaAndroid on 4/27/2017.
 */

public class JobAdapter  extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        private  String TAG="JobAdapter";
        public final int TYPE_JOB = 0;
        public final int TYPE_LOAD = 1;
        private List<JobListResponse> jobs;
        static Activity context;
        private static String emailVerified="completeProfile";
        public OnLoadMoreListener loadMoreListener;
        boolean isLoading = false, isMoreDataAvailable = true;
        public static RecyclerView mRecyclerView;

             // Provide a suitable constructor (depends on the kind of dataset)
        public JobAdapter(FragmentActivity context, RecyclerView mRecyclerView, List<JobListResponse> jobs) {
            Log.e("Adapter","Adapter");
            this.context = context;
            this.jobs = jobs;
            this.mRecyclerView=mRecyclerView;

            //Check Login User is Verified
            User user= SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readUserPref();
            getVerifiedIds(user.getId());
        }

        // Create new views (invoked by the layout manager)
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent,int viewType) {
           //Log.e(TAG,"onCreateViewHolder"+viewType);
                     LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            if(viewType==TYPE_JOB){
                return new JobHolder(inflater.inflate(R.layout.job_list_row_item,parent,false));
            }
            else{
                return new LoadHolder(inflater.inflate(R.layout.progress_bar_layout,parent,false));
            }
        }

        // Replace the contents of a view (invoked by the layout manager)
        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            // - get element from your dataset at this position
            // - replace the contents of the view with that element
           // Log.e(TAG,"onBindViewHolder"+position);

            if(position>=getItemCount()-1 && isMoreDataAvailable && !isLoading && loadMoreListener!=null){
                isLoading = true;
                loadMoreListener.onLoadMore();
             //   Log.e(TAG,"isLoading = true ");
            }

            if(getItemViewType(position)==TYPE_JOB){
                ((JobHolder)holder).bindData(jobs.get(position));
              //  Log.e(TAG,"getItemViewType(position)==TYPE_JOB ");
            }
        }

        // Return the size of your dataset (invoked by the layout manager)
        @Override
        public int getItemCount() {
            return jobs.size();
        }

        /* VIEW HOLDERS */
    static class JobHolder extends RecyclerView.ViewHolder{
            public TextView title,postingTime;
            public ImageView jobImageView;
            public CircleImageView userImageView;
            public Button readMoreButton;
            public AVLoadingIndicatorView progressBar;

            public JobHolder(View view) {
                super(view);
                title=(TextView) view.findViewById(R.id.title);
                postingTime=(TextView) view.findViewById(R.id.posting_time);
                readMoreButton=(Button) view.findViewById(R.id.read_more_button);
                jobImageView=(ImageView) view.findViewById(R.id.job_image_view);
                userImageView=(CircleImageView) view.findViewById(R.id.user_image_view);
                progressBar=(AVLoadingIndicatorView)view.findViewById(R.id.progress);
            }

            void bindData(JobListResponse jobListResponse){
               final Long jobId=jobListResponse.getJob().getId();
                final User user=jobListResponse.getUser();
                Job job=jobListResponse.getJob();

               /* username.setText(user.getFirstName()+"" +
                       user.getLastName());*/
                title.setText(job.getTitle());
                try{
                    //postingTime.setText(TimeConverter.LongMillisecondsToTime(Long.parseLong("1501736300000")));
                    postingTime.setText(DateTimeConverter.timeOnHomeScreen(job.getTimeline()));
                }
                catch (NullPointerException ex){
                    ex.printStackTrace();
                }
                //if job image not null
                if(job.getImages().size()>0)
                    Glide.with(context).load(S3Utils.getDownloadJobImageURL(job.getId(),job.getImages().get(0).getName()))
                           .listener(new RequestListener<String, GlideDrawable>() {
                                @Override
                                public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                    progressBar.setVisibility(View.GONE);
                                    return false;
                                }
                                @Override
                                public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                    progressBar.setVisibility(View.GONE);
                                    return false;
                                }
                            })
                            .into(jobImageView);
                else
                    jobImageView.setImageResource(findImageIdByCategory(job.getCategory(),progressBar));

                     //if user image not null
                    if(user.getImagePath()!=null)
                    Glide.with(context).load(S3Utils.getDownloadUserImageURL(
                            user.getId(),
                           user.getImagePath())).placeholder(R.drawable.user_image).into(userImageView);

                // redirect user to job details screen
                readMoreButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(emailVerified.equals("verified")) {
                            JobDetailFragment fragment = new JobDetailFragment();
                            Bundle bundle = new Bundle();
                            bundle.putLong("jobId", jobId);
                            bundle.putLong("userId", user.getId());
                            fragment.setArguments(bundle);

                            FragmentTransaction transaction = ((HomeScreenActivity) context).getSupportFragmentManager().beginTransaction();
                            // For a little polish, specify a transition animation
                            // transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                            // Set Custom animation
                            transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
                            // To make it fullscreen, use the 'content' root view as the container
                            // for the fragment, which is always the root view for the activity
                            transaction.add(android.R.id.content, fragment, "job_details")
                                    .addToBackStack(null).commit();
                        }
                        else{
                            //if(emailVerified.equals("completeProfile"))
                            if(emailVerified.equals("completeProfile")){
                                Snackbar.make(context.getWindow().getDecorView(), context.getString(R.string.complete_profile_check), Snackbar.LENGTH_LONG)
                                        .setAction(context.getString(R.string.max_ok_btn), new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                Intent intent1 = new Intent(context, RegisterActivity.class);
                                                context.startActivity(intent1);
                                            }
                                        }).setActionTextColor(context.getResources().getColor(R.color.app_color)).show();
                            }else{
                                Snackbar.make(context.getWindow().getDecorView(),context.getString(R.string.verify_email_check),Snackbar.LENGTH_LONG).show();
                            }
                        }
                    }
                });
            }
        }

    static class LoadHolder extends RecyclerView.ViewHolder{
            public LoadHolder(View itemView) {
                super(itemView);
            }
        }

        public void setMoreDataAvailable(boolean moreDataAvailable) {
            isMoreDataAvailable = moreDataAvailable;
        }
    /* notifyDataSetChanged is final method so we can't override it
         call adapter.notifyDataChanged(); after update the list
         */
        public void notifyDataChanged(){
            notifyDataSetChanged();
            isLoading = false;
          //  Log.e(TAG,"notifyDataChanged ");
        }


       public interface OnLoadMoreListener{
            void onLoadMore();
        }

        public void setLoadMoreListener(OnLoadMoreListener loadMoreListener) {
            this.loadMoreListener = loadMoreListener;
            Log.e(TAG,"setLoadMoreListener ");
        }

    @Override
    public int getItemViewType(int position) {
        try {
            if (jobs.get(position).type.equals("job")) {
                return TYPE_JOB;
            } else {
                return TYPE_LOAD;
            }
        }catch (NullPointerException ex){
            ex.printStackTrace();
        }
        return TYPE_JOB;
    }


    static  int findImageIdByCategory(String selectedcategory, AVLoadingIndicatorView progressBar){
        switch (selectedcategory) {
            case "All":
                progressBar.setVisibility(View.GONE);
                return R.drawable.others;

            case "Home & Gardening":
                progressBar.setVisibility(View.GONE);
                return R.drawable.home_n_gardening;

            case "Car & Vehicles":
                progressBar.setVisibility(View.GONE);
                return R.drawable.car_and_vehicles;

            case "Health & Beauty":
                progressBar.setVisibility(View.GONE);
                return R.drawable.health_and_beauty;

            case "Photoshoot":
                progressBar.setVisibility(View.GONE);
                return R.drawable.photoshoot;

            case "Animal sitting & Pet care":
                progressBar.setVisibility(View.GONE);
                return R.drawable.animal_setting_and_pet_care;

            case "Computer & Tech fixes":
                progressBar.setVisibility(View.GONE);
                return R.drawable.computer_and__tech_fixes;

            case "Ground & Construction":
                progressBar.setVisibility(View.GONE);
                return R.drawable.ground_and_construction;

            case "Carrying & Assembling":
                progressBar.setVisibility(View.GONE);
                return R.drawable.carrying_and_assembling;

            case "Others":
                progressBar.setVisibility(View.GONE);
                return R.drawable.others;

            default:
                progressBar.setVisibility(View.GONE);
                return R.drawable.others;
        }
    }

    private void getVerifiedIds(Long id) {
        Log.e("getVerifiedId","GetVerifiedId"+"  "+id);
        ApiInterface apiService= ApiClient.getClient().create(ApiInterface.class);
        Call<ArrayList<User>> call= apiService.verifyIdsOnProfile(id);
        call.enqueue(new Callback<ArrayList<User>>() {
            @Override
            public void onResponse(Call<ArrayList<User>> call, Response<ArrayList<User>> response) {
                Log.d("response","response"+" body" +response.body());
                showAccountVerification(response.body());
            }

            @Override
            public void onFailure(Call<ArrayList<User>> call, Throwable t) {
                Log.d("Failure","Failure"+t.toString());
            }
        });
    }
    public  void showAccountVerification(ArrayList<User> body) {
        for (User item : body) {
            if (item.getVerificationType().equals("email")) {
                emailVerified="profileCompleted";
                if(item.getIsVerified()==1){
                    emailVerified="verified";
                }
            }
            Log.e("Email","Emails"+"  "+emailVerified);
        }
    }
}

