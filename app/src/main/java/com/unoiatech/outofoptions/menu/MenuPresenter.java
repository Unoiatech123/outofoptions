package com.unoiatech.outofoptions.menu;

import android.content.Context;
import android.util.Log;
import com.unoiatech.outofoptions.api.APIError;
import com.unoiatech.outofoptions.api.ErrorHandler;
import com.unoiatech.outofoptions.util.ApiError;
import com.unoiatech.outofoptions.util.BankId.ErrorUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Unoiatech on 1/22/2018.
 */

public class MenuPresenter

{
        private final Context context;
        private final MenuPresenterListener mListener;
        private final MenuService menuService;

public interface MenuPresenterListener{
      void isShopRegistered(Boolean isShopRegistered);
}

    public MenuPresenter(MenuPresenterListener listener, Context context){
        this.mListener = listener;
        this.context = context;
        this.menuService = new MenuService();
    }

    public void isShopRegistered(Long userId){
        menuService
                .getAPI()
                .isShopRegistered(userId)
                .enqueue(new Callback<Boolean>() {
                    @Override
                    public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                        Boolean result = response.body();

                        if(response.isSuccessful())
                            mListener.isShopRegistered(result);
                        else
                        {
                            ApiError error= ErrorUtils.parseError(response);
                        // … and use it to show error information

                        // … or just log the issue like we’re doing :)
                        Log.e("error message", error.getMessage()+"");
                        Log.e("error message", error.getError()+"");
                        Log.e("error message", error.getStatus()+"");

                            mListener.isShopRegistered(false);

                        }

                    }

                    @Override
                    public void onFailure(Call<Boolean> call, Throwable t) {
                        try {
                            throw  new InterruptedException("Failure !");
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }
}
