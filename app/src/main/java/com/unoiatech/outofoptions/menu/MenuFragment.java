package com.unoiatech.outofoptions.menu;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.activity.RegisterActivity;
import com.unoiatech.outofoptions.adminmodule.CompanyPaymentScreen;
import com.unoiatech.outofoptions.companymodule.CompanyProfile;
import com.unoiatech.outofoptions.companymodule.registration.CompanyRegistration;
import com.unoiatech.outofoptions.companymodule.CompanyStaff;
import com.unoiatech.outofoptions.dealsmodule.createdeals.CreateDealScreen;
import com.unoiatech.outofoptions.fragments.menufragments.archivejobs.ArchiveJobs;
import com.unoiatech.outofoptions.fragments.menufragments.paymentfragments.ConnectToSwish;
import com.unoiatech.outofoptions.fragments.menufragments.paymentfragments.PaymentIndividual;
import com.unoiatech.outofoptions.fragments.menufragments.SearchJobsScreen;
import com.unoiatech.outofoptions.fragments.menufragments.paymentfragments.WithdrawFunds;
import com.unoiatech.outofoptions.home.HomeScreenActivity;
import com.unoiatech.outofoptions.api.ApiClient;
import com.unoiatech.outofoptions.api.ApiInterface;
import com.unoiatech.outofoptions.fragments.menufragments.ProfileScreen;
import com.unoiatech.outofoptions.fragments.menufragments.SettingFragment;
import com.unoiatech.outofoptions.home.fragment.CategoryDialogFragment;
import com.unoiatech.outofoptions.model.Company;
import com.unoiatech.outofoptions.model.SwishCustomer;
import com.unoiatech.outofoptions.model.User;
import com.unoiatech.outofoptions.model.ActiveJobsResponse;
import com.unoiatech.outofoptions.registershop.ShopRegistrationScreen;
import com.unoiatech.outofoptions.util.ApiError;
import com.unoiatech.outofoptions.util.BankId.ErrorUtils;
import com.unoiatech.outofoptions.util.SharedPrefsHelper;

import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Unoiatech on 2/24/2017.
 */

public class MenuFragment extends Fragment implements View.OnClickListener, MenuPresenter.MenuPresenterListener  {
    private static final String TAG="MenuFragment";
    private ImageView setting_image,profile_image,extras_image,payments_image,postJobs_image,searchJobs_image,own_business_image,reg_shop_image,imageView1,image1,imageView2,image2,imageView3,image3,job_imageview1,job_imageview2,job_image1,job_image2,registration_img;
    private LinearLayout payment_layout,job_layout,company_register_layout;
    private TextView payment_text1,payment_text2,payment_text3,job_textview1,job_textview2;
    private ArrayList<ActiveJobsResponse> data_list;
    private RelativeLayout menuscreen;
    private String userType;
    private String emailVerified="completeProfile";
    private Company companyData;
    private User user;
    private SwishCustomer swishCustomer;
    private MenuPresenter menuPresenter;
    private Boolean isShopRegistered ;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.main_menu_screen,container,false);

        user = SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readUserPref();

        menuPresenter = new MenuPresenter(this, this.getContext());
        menuPresenter.isShopRegistered(user.getId());

        //Setting Tab click
        LinearLayout settings=(LinearLayout)view.findViewById(R.id.setting);
        setting_image=(ImageView)view.findViewById(R.id.setting_image);
        settings.setOnClickListener(MenuFragment.this);

        //payments TabClick
        LinearLayout payments=(LinearLayout)view.findViewById(R.id.payments);
        payments_image=(ImageView)view.findViewById(R.id.payment_image);
        payments.setOnClickListener(MenuFragment.this);

        //Search JobLayout
        LinearLayout searchjobs=(LinearLayout)view.findViewById(R.id.searchjobs);
        searchJobs_image=(ImageView)view.findViewById(R.id.search_image);
        searchjobs.setOnClickListener(MenuFragment.this);

        //extra Button
        LinearLayout extra=(LinearLayout)view.findViewById(R.id.extra);
        extras_image=(ImageView)view.findViewById(R.id.extra_image);
        //extras_image.setImageResource(R.color.app_color);
        extra.setOnClickListener(MenuFragment.this);

        //Profile Buttons
        LinearLayout profile=(LinearLayout)view.findViewById(R.id.profile);
        profile_image=(ImageView)view.findViewById(R.id.profile_image);
        profile.setOnClickListener(MenuFragment.this);

        //Post Job Button
        LinearLayout post_job=(LinearLayout)view.findViewById(R.id.post_job);
        postJobs_image=(ImageView)view.findViewById(R.id.postjob_image);
        post_job.setOnClickListener(MenuFragment.this);

        // Own Bussiness Button
        LinearLayout own_business=(LinearLayout)view.findViewById(R.id.own_business);
        own_business_image=(ImageView)view.findViewById(R.id.activejob_image);
        own_business.setOnClickListener(MenuFragment.this);

        //Chat Button
        LinearLayout chat=(LinearLayout)view.findViewById(R.id.register_shop);
        reg_shop_image=(ImageView)view.findViewById(R.id.shop_image);
        chat.setOnClickListener(MenuFragment.this);

        //Payment Layout
        payment_layout=(LinearLayout)view.findViewById(R.id.payment_layout);

        //company_layout
        company_register_layout=(LinearLayout)view.findViewById(R.id.compny_layout);
        registration_img=(ImageView)view.findViewById(R.id.cmpny_reg_image);

        //Admin image
        ImageView admin=(ImageView)view.findViewById(R.id.admin_image);
        admin.setOnClickListener(this);

               //textviews
        payment_text1=(TextView)view.findViewById(R.id.payment_text1);
        payment_text2=(TextView)view.findViewById(R.id.payment_text2);
        payment_text3=(TextView)view.findViewById(R.id.payment_text3);

        payment_text1.setOnClickListener(MenuFragment.this);
        payment_text2.setOnClickListener(MenuFragment.this);

        //Images with green background
        imageView1=(ImageView)view.findViewById(R.id.imageview1);
        image1=(ImageView)view.findViewById(R.id.first_image);
        imageView2=(ImageView)view.findViewById(R.id.imageview2);
        image2=(ImageView)view.findViewById(R.id.second_image);
        imageView3=(ImageView)view.findViewById(R.id.imageview3);
        image3=(ImageView)view.findViewById(R.id.third_image);

        //Job layout with two views
        job_layout=(LinearLayout)view.findViewById(R.id.job_layout);
        job_textview1=(TextView)view.findViewById(R.id.job_text1);
        job_textview2=(TextView)view.findViewById(R.id.job_text2);

        //Images with green Background
        job_imageview1=(ImageView)view.findViewById(R.id.jobimageview1);
        job_image1=(ImageView)view.findViewById(R.id.job_image1);
        job_imageview1.setOnClickListener(MenuFragment.this);

        job_imageview2=(ImageView)view.findViewById(R.id.jobimageview2);
        job_image2=(ImageView)view.findViewById(R.id.job_image2);
        job_imageview2.setOnClickListener(MenuFragment.this);

        //Layout to replace with Fragment layout
        menuscreen=(RelativeLayout) view.findViewById(R.id.main_menu) ;


        
        //Back Images
        ImageView backImage=(ImageView)view.findViewById(R.id.back);
        backImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent backIntent= new Intent(getActivity(), HomeScreenActivity.class);
                backIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                getActivity().finish();
                startActivity(backIntent);
            }
        });
        return view;
    }

    private void getSwishCustomer(Long id) {
        swishCustomer= new SwishCustomer();
        ApiInterface apiService= ApiClient.getClient().create(ApiInterface.class);
        Call<SwishCustomer> call= apiService.getSwishCustomer(id);
        call.enqueue(new Callback<SwishCustomer>() {
            @Override
            public void onResponse(Call<SwishCustomer> call, Response<SwishCustomer> response) {
                Log.e(TAG,"Response"+response.isSuccessful()+" "+response.body()+"  "+response.message());
                if(response.isSuccessful()) {
                    swishCustomer=response.body();
                }
                else{
                    ApiError error=ErrorUtils.parseError(response);
                    notifyUser(error.getMessage());
                }
            }

            @Override
            public void onFailure(Call<SwishCustomer> call, Throwable t) {
                Log.e("Failure","Failure"+t.toString());
            }
        });
    }

    @Override
    public void onClick(View view) {
        Fragment fragment=null;
        FragmentManager fragMan = getChildFragmentManager();

        // add Fragment A to the main linear layout
        FragmentTransaction fragTrans = fragMan.beginTransaction();
        switch (view.getId()) {
            case R.id.setting:
                payment_layout.setVisibility(View.GONE);
                job_layout.setVisibility(View.GONE);
                setting_image.setImageResource(R.color.app_color);
                profile_image.setImageResource(R.color.unselected_menu);
                searchJobs_image.setImageResource(R.color.unselected_menu);
                own_business_image.setImageResource(R.color.unselected_menu);
                extras_image.setImageResource(R.color.unselected_menu);
                payments_image.setImageResource(R.color.unselected_menu);
                postJobs_image.setImageResource(R.color.unselected_menu);
                reg_shop_image.setImageResource(R.color.unselected_menu);
                job_layout.setVisibility(View.GONE);
                company_register_layout.setVisibility(View.GONE);
                payment_layout.setVisibility(View.GONE);
                fragment= new SettingFragment();
                break;

            case R.id.register_shop:
                if(isShopRegistered) {
                    reg_shop_image.setImageResource(R.color.app_color);
                    profile_image.setImageResource(R.color.unselected_menu);
                    searchJobs_image.setImageResource(R.color.unselected_menu);
                    own_business_image.setImageResource(R.color.unselected_menu);
                    extras_image.setImageResource(R.color.unselected_menu);
                    payments_image.setImageResource(R.color.unselected_menu);
                    postJobs_image.setImageResource(R.color.unselected_menu);
                    setting_image.setImageResource(R.color.unselected_menu);
                    job_layout.setVisibility(View.GONE);
                    company_register_layout.setVisibility(View.GONE);
                    payment_layout.setVisibility(View.GONE);
                    Intent intent1 = new Intent(getActivity(), ShopRegistrationScreen.class);
                    startActivity(intent1);
                }

                else
                {

                    reg_shop_image.setImageResource(R.color.app_color);
                    profile_image.setImageResource(R.color.unselected_menu);
                    searchJobs_image.setImageResource(R.color.unselected_menu);
                    own_business_image.setImageResource(R.color.unselected_menu);
                    extras_image.setImageResource(R.color.unselected_menu);
                    payments_image.setImageResource(R.color.unselected_menu);
                    postJobs_image.setImageResource(R.color.unselected_menu);
                    setting_image.setImageResource(R.color.unselected_menu);
                    job_layout.setVisibility(View.VISIBLE);
                    company_register_layout.setVisibility(View.GONE);
                    payment_layout.setVisibility(View.GONE);
                   // Intent intent1 = new Intent(getActivity(), ShopRegistrationScreen.class);
                  //  startActivity(intent1);

                }
                break;

            case R.id.extra:
               /* extras_image.setImageResource(R.color.app_color);
                profile_image.setImageResource(R.color.unselected_menu);
                searchJobs_image.setImageResource(R.color.unselected_menu);
                own_business_image.setImageResource(R.color.unselected_menu);
                notification_image.setImageResource(R.color.unselected_menu);
                payments_image.setImageResource(R.color.unselected_menu);
                postJobs_image.setImageResource(R.color.unselected_menu);
                setting_image.setImageResource(R.color.unselected_menu);
                job_layout.setVisibility(View.GONE);
                company_register_layout.setVisibility(View.GONE);

                payment_layout.setVisibility(View.VISIBLE);
                imageView1.setImageResource(R.color.app_color);
                image1.setImageResource(R.drawable.own_business);
                payment_text1.setText(R.string.own_business);
                imageView2.setImageResource(R.color.app_color);
                image2.setImageResource(R.drawable.persnol_security);
                payment_text2.setText("Personal \n Security");
                imageView3.setImageResource(R.color.app_color);
                image3.setImageResource(R.drawable.automatic_acconting);
                payment_text3.setText("Automatic \n Accounting");*/
                Intent intent4= new Intent(getActivity(), CreateDealScreen.class);
                getActivity().startActivity(intent4);
                break;
            case R.id.own_business:
                 if(emailVerified.equals("verified")) {
                    own_business_image.setImageResource(R.color.app_color);
                    profile_image.setImageResource(R.color.unselected_menu);
                    searchJobs_image.setImageResource(R.color.unselected_menu);
                    extras_image.setImageResource(R.color.unselected_menu);
                    reg_shop_image.setImageResource(R.color.unselected_menu);
                    payments_image.setImageResource(R.color.unselected_menu);
                    postJobs_image.setImageResource(R.color.unselected_menu);
                    setting_image.setImageResource(R.color.unselected_menu);
                    job_layout.setVisibility(View.GONE);
                    payment_layout.setVisibility(View.GONE);

                    if (companyData != null) {
                        if (companyData.getCompanyStatus().equals("1")) {
                            job_layout.setVisibility(View.GONE);
                            company_register_layout.setVisibility(View.GONE);

                            payment_layout.setVisibility(View.VISIBLE);
                            imageView1.setImageResource(R.color.app_color);
                            image1.setImageResource(R.drawable.company_profile);
                            payment_text1.setText(R.string.company_profile);
                            imageView1.setOnClickListener(this);

                            imageView2.setImageResource(R.color.app_color);
                            image2.setImageResource(R.drawable.company_staff);
                            payment_text2.setText(R.string.company_staff);
                            imageView2.setOnClickListener(this);

                            imageView3.setImageResource(R.color.app_color);
                            image3.setImageResource(R.drawable.cmpny_economy);
                            payment_text3.setText(R.string.company_economy);
                            imageView3.setOnClickListener(this);
                        } else {
                            company_register_layout.setVisibility(View.VISIBLE);
                            registration_img.setImageResource(R.drawable.company_registration);
                            company_register_layout.setOnClickListener(this);
                        }
                    } else {
                        company_register_layout.setVisibility(View.VISIBLE);
                        registration_img.setImageResource(R.drawable.company_registration);
                        company_register_layout.setOnClickListener(this);
                    }
                }else{
                    notifyUserToCompleteProfile(getString(R.string.complete_profile_check));
                }
                break;
            //Company Module
            case R.id.imageview1:
                if(payment_text1.getText().toString().equals(getString(R.string.company_profile))) {
                    companyProfileFragment();
                }
                else{
                    PaymentIndividual frag=(PaymentIndividual) getChildFragmentManager().findFragmentByTag("company_profile");
                    if(frag==null)
                        frag = PaymentIndividual.getPaymentIndividual();
                    FragmentManager fragManger = getChildFragmentManager();
                    Bundle bundle= new Bundle();
                    if(swishCustomer!=null){
                        bundle.putString("swish_number",swishCustomer.getSwishNumber());
                    }
                    //Pass Bundle to Fragment
                    frag.setArguments(bundle);

                    // add Fragment A to the main linear layout
                    FragmentTransaction fragTransaction = fragMan.beginTransaction();
                    fragTrans.replace(R.id.main_menu, frag,"payment");
                    fragTrans.addToBackStack(null);
                    fragTrans.commit();
                    /*Intent intent= new Intent(getActivity(),PaymentSettingFragment.class);
                    getActivity().startActivity(intent);
                    fragment= new PaymentSettingFragment();
                    PaymentSettingFragment paymentFrag= PaymentSettingFragment.getPaymentSettings();
                    fragTrans.add(R.id.main_menu, paymentFrag,"payment_settings");
                    fragTrans.addToBackStack(null);
                    fragTrans.commit();*/
                }
                break;
            case R.id.imageview2:
                if(payment_text2.getText().toString().equals(getString(R.string.company_staff))){
                    Intent intent= new Intent(getActivity(),CompanyStaff.class);
                    Bundle bundle= new Bundle();
                    bundle.putLong("company_id",companyData.getId());
                    intent.putExtras(bundle);
                    getActivity().startActivity(intent);
                }
                else{
                    //fragment= new WithdrawFunds();
                    Intent intent= new Intent(getActivity(),WithdrawFunds.class);
                    if(swishCustomer!=null){
                        intent.putExtra("swish_number",swishCustomer.getSwishNumber());
                    }
                    else{
                        intent.putExtra("swish_number","Not Connected");
                    }
                    startActivity(intent);
                }
                break;
            case R.id.imageview3:
                if(payment_text3.getText().toString().equals(getString(R.string.company_economy))){
                }
                else {
                    Bundle bundle = null;
                    if(swishCustomer!=null){
                        bundle= new Bundle();
                        bundle.putString("swish_number",swishCustomer.getSwishNumber());
                    }
                    Intent intent= new Intent(getActivity(), ConnectToSwish.class);
                    if(bundle!=null)
                    intent.putExtras(bundle);
                    getActivity().startActivity(intent);
                }
                break;

            case R.id.compny_layout:
                if(companyData!=null) {
                    if(companyData.getCompanyStatus().equals("3")){
                        notifyUser(getString(R.string.company_req_sent));}
                    else if(companyData.getCompanyStatus().equals("0")){
                        Intent intent2 = new Intent(getActivity(), CompanyRegistration.class);
                        getActivity().startActivity(intent2);
                    }
                    else if(companyData.getCompanyStatus().equals("2")){
                        Intent intent2 = new Intent(getActivity(), CompanyRegistration.class);
                        getActivity().startActivity(intent2);
                    }
                }
                else {
                    Intent intent2 = new Intent(getActivity(), CompanyRegistration.class);
                    getActivity().startActivity(intent2);
                }
                break;

            case R.id.payments:
                if(emailVerified.equals("verified")) {
                    payments_image.setImageResource(R.color.app_color);
                    profile_image.setImageResource(R.color.unselected_menu);
                    searchJobs_image.setImageResource(R.color.unselected_menu);
                    own_business_image.setImageResource(R.color.unselected_menu);
                    reg_shop_image.setImageResource(R.color.unselected_menu);
                    extras_image.setImageResource(R.color.unselected_menu);
                    postJobs_image.setImageResource(R.color.unselected_menu);
                    setting_image.setImageResource(R.color.unselected_menu);

                    job_layout.setVisibility(View.GONE);
                    company_register_layout.setVisibility(View.GONE);

                    payment_layout.setVisibility(View.VISIBLE);
                    imageView1.setImageResource(R.color.app_color);
                    imageView1.setOnClickListener(this);
                    image1.setImageResource(R.drawable.earning);
                    payment_text1.setText(getString(R.string.payment_setting));

                    imageView2.setImageResource(R.color.app_color);
                    image2.setImageResource(R.drawable.add_credit_card);
                    payment_text2.setText(getString(R.string.withdraw_funds_settings));
                    imageView2.setOnClickListener(this);

                    imageView3.setImageResource(R.color.app_color);
                    image3.setImageResource(R.drawable.swish_image);
                    payment_text3.setText(getString(R.string.connect_swish_settings));
                    imageView3.setOnClickListener(this);
                }else{
                    notifyUserToCompleteProfile(getString(R.string.complete_profile_check));
                }
                break;
            case R.id.profile:
                profile_image.setImageResource(R.color.app_color);
                payments_image.setImageResource(R.color.unselected_menu);
                searchJobs_image.setImageResource(R.color.unselected_menu);
                own_business_image.setImageResource(R.color.unselected_menu);
                reg_shop_image.setImageResource(R.color.unselected_menu);
                extras_image.setImageResource(R.color.unselected_menu);
                postJobs_image.setImageResource(R.color.unselected_menu);
                setting_image.setImageResource(R.color.unselected_menu);
                job_layout.setVisibility(View.GONE);
                company_register_layout.setVisibility(View.GONE);
                payment_layout.setVisibility(View.GONE);
                openChildFragment();
                break;

            case R.id.searchjobs:
                searchJobs_image.setImageResource(R.color.app_color);
                payments_image.setImageResource(R.color.unselected_menu);
                profile_image.setImageResource(R.color.unselected_menu);
                own_business_image.setImageResource(R.color.unselected_menu);
                reg_shop_image.setImageResource(R.color.unselected_menu);
                extras_image.setImageResource(R.color.unselected_menu);
                postJobs_image.setImageResource(R.color.unselected_menu);
                setting_image.setImageResource(R.color.unselected_menu);
                job_layout.setVisibility(View.GONE);
                company_register_layout.setVisibility(View.GONE);
                payment_layout.setVisibility(View.GONE);
                openSearchFragment();
                break;

            case R.id.post_job:
                if(emailVerified.equals("verified")) {
                    postJobs_image.setImageResource(R.color.app_color);
                    payments_image.setImageResource(R.color.unselected_menu);
                    profile_image.setImageResource(R.color.unselected_menu);
                    own_business_image.setImageResource(R.color.unselected_menu);
                    reg_shop_image.setImageResource(R.color.unselected_menu);
                    extras_image.setImageResource(R.color.unselected_menu);
                    searchJobs_image.setImageResource(R.color.unselected_menu);
                    setting_image.setImageResource(R.color.unselected_menu);
                    payment_layout.setVisibility(View.GONE);
                    company_register_layout.setVisibility(View.GONE);

                    job_layout.setVisibility(View.VISIBLE);
                    job_imageview1.setImageResource(R.color.app_color);
                    job_image1.setImageResource(R.drawable.new_job);
                    job_textview1.setText(getString(R.string.post_job));

                    job_imageview2.setImageResource(R.color.app_color);
                    job_image2.setImageResource(R.drawable.archived_job);
                    job_textview2.setText(getString(R.string.archive_job));
                }else{
                    notifyUserToCompleteProfile(getString(R.string.complete_profile_check));
                }
                break;
            //post new Job on post job click
            case R.id.jobimageview1:
                //Toast.makeText(getActivity(),"This Function is not Available",Toast.LENGTH_SHORT).show();
                if(data_list.size()<4) {
                    openCategoryFragment();
                }
                else{
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
                    View view1 = getActivity().getLayoutInflater().inflate(R.layout.max_three_job_layout, null);
                    dialogBuilder.setView(view1);
                    TextView ok_btn = (TextView) view1.findViewById(R.id.okay_btn);
                    final AlertDialog alertDialog = dialogBuilder.create();
                    alertDialog.setCancelable(false);
                    alertDialog.show();
                    ok_btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertDialog.dismiss();
                        }
                    });
                }
                break;
            //display Archive job on post job
            case R.id.jobimageview2:
               // Toast.makeText(getActivity(),"This Function is not Available",Toast.LENGTH_SHORT).show();
                ArchiveJobs frag=(ArchiveJobs) getChildFragmentManager().findFragmentByTag("archive");
                if(frag==null)
                    frag = new ArchiveJobs();
                fragTrans.replace(R.id.main_menu, frag,"archive");
                fragTrans.commit();
                break;

           /* case R.id.payment_text1:
                Log.e(TAG,"payment_text1 clicked");
                // add Fragment A to the main linear layout
                PaymentSettingFragment paymentSettingsFragemnt= PaymentSettingFragment.getPaymentSettings();
                fragTrans.replace(R.id.main_menu, paymentSettingsFragemnt,"payment_settings");
                fragTrans.addToBackStack(null);
                fragTrans.commit();
                break;

            case R.id.payment_text2:
                Log.e(TAG,"payment_text2 clicked");
                // add Fragment A to the main linear layout
                WithdrawFunds withdrawFundsFragemnt= WithdrawFunds.getWithdrawFunds();
                fragTrans.replace(R.id.main_menu,withdrawFundsFragemnt,"withdraw_funds");
                fragTrans.addToBackStack(null);
                fragTrans.commit();
                break;*/

            case R.id.admin_image:
                //if(userType.equals("Admin")){
                    if(job_layout.getVisibility()==View.VISIBLE||payment_layout.getVisibility()==View.VISIBLE||company_register_layout.getVisibility()==View.VISIBLE){
                        job_layout.setVisibility(View.GONE);
                        payment_layout.setVisibility(View.GONE);
                        company_register_layout.setVisibility(View.GONE);
                    }
                Intent intent3 = new Intent(getActivity(), CompanyPaymentScreen.class);
                getActivity().startActivity(intent3);
                //}
                Log.e("USer_Type","User_Type"+userType);
                break;
        }
        if(fragment!=null){
            FragmentTransaction ft= getFragmentManager().beginTransaction();
            ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
            ft.add(R.id.main_menu,fragment).addToBackStack("").commit();
        }
    }



    private void notifyUserToCompleteProfile(String msg) {
        if(emailVerified.equals("completeProfile")) {
            Snackbar.make(getView(), msg, Snackbar.LENGTH_LONG)
                    .setAction(getString(R.string.max_ok_btn), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(getActivity(), RegisterActivity.class);
                            startActivity(intent);
                        }
                    }).setActionTextColor(getResources().getColor(R.color.app_color)).show();
        }
        else if(emailVerified.equals("profileCompleted")) {
            Snackbar.make(getView(),getString(R.string.verify_email_check),Snackbar.LENGTH_LONG).show();
        }
    }

    private void notifyUser(String msg) {
        Snackbar.make(getView(),msg, Snackbar.LENGTH_LONG)
                .setAction(getString(R.string.max_ok_btn), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                    }
                }).setActionTextColor(getResources().getColor(R.color.app_color)).show();
    }

    private void openSearchFragment() {
        Intent intent= new Intent(getActivity(), SearchJobsScreen.class);
        startActivity(intent);
        /*SearchTabFragment frag=(SearchTabFragment) getChildFragmentManager().findFragmentByTag("search");
        if(frag==null)
            frag = SearchTabFragment.getSearchFrag();
        FragmentManager fragMan = getChildFragmentManager();

        // add Fragment A to the main linear layout
        FragmentTransaction fragTrans = fragMan.beginTransaction();
        fragTrans.replace(R.id.main_menu, frag,"search");
        fragTrans.addToBackStack("search");
        fragTrans.commit();*/
    }

    private void openCategoryFragment() {
        CategoryDialogFragment frag=(CategoryDialogFragment) getChildFragmentManager().findFragmentByTag("category");
        if(frag==null)
            frag = CategoryDialogFragment.getCategoryFrag();
        FragmentManager fragMan = getChildFragmentManager();

        // add Fragment A to the main linear layout
        FragmentTransaction fragTrans = fragMan.beginTransaction();
        fragTrans.replace(R.id.main_menu, frag,"category");
        fragTrans.commit();
    }

    //Method to check Count of posted job
    private void getMaxJobCount(final Long id) {
        data_list=new ArrayList<ActiveJobsResponse>();
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ArrayList<ActiveJobsResponse>> call = apiService.getPostedJobs(id);
        call.enqueue(new Callback<ArrayList<ActiveJobsResponse>>() {
            @Override
            public void onResponse(Call<ArrayList<ActiveJobsResponse>> call, Response<ArrayList<ActiveJobsResponse>> response) {
                Log.d(TAG, "RESPONSE" + response.body().size()+"  "+response+data_list.size());
                if (response.isSuccessful()) {
                    for(int i=0;i<response.body().size();i++){
                        if(response.body().get(i).getJob().getIsExpired()==0){
                            if(response.body().get(i).getJob().getJobStatus().equals("pending")||response.body().get(i).getJob().getJobStatus().equals("active")){
                                Log.e(TAG,"Status"+response.body().get(i).getJob().getJobStatus());
                                data_list.add(response.body().get(i));
                            }
                        }
                    }

                    //Method to getUser Type
                    getUserType(id);
                    Log.e(TAG,"Job_Count"+data_list.size());
                }
                else {
                    ApiError apiError=ErrorUtils.parseError(response);
                    notifyUser(apiError.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ArrayList<ActiveJobsResponse>> call, Throwable t) {
                Log.d(TAG, "Failure" + t.toString());
            }
        });
    }

    // Check User Type to enable Admin Button
    public void getUserType(final Long id) {
        ApiInterface apiService= ApiClient.getClient().create(ApiInterface.class);
        Call<User> call= apiService.getUserProfile(id);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                Log.e(TAG,"UserType+Response"+response);
                if(response.code()==200) {
                    userType=response.body().getUser().getUserType();
                    companyData=response.body().getUser().getCompany();

                    //Check whether Email is Verified or Not
                    getVerifiedIds(id);
                }
                else{
                    ApiError error = ErrorUtils.parseError(response);
                    notifyUser(error.getMessage());
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Log.d("Failure","Failure"+t.toString());
            }
        });
    }

    private void getVerifiedIds(Long id) {
        ApiInterface apiService= ApiClient.getClient().create(ApiInterface.class);
        Call<ArrayList<User>> call= apiService.verifyIdsOnProfile(id);
        call.enqueue(new Callback<ArrayList<User>>() {
            @Override
            public void onResponse(Call<ArrayList<User>> call, Response<ArrayList<User>> response) {
                Log.d("response","response"+" body" +response.body());
                showAccountVerification(response.body());
            }

            @Override
            public void onFailure(Call<ArrayList<User>> call, Throwable t) {
                Log.d("Failure","Failure"+t.toString());
            }
        });
    }
    public  void showAccountVerification(ArrayList<User> body) {
        for (User item : body) {
            if (item.getVerificationType().equals("email")) {
                emailVerified="profileCompleted";
                if(item.getIsVerified()==1){
                    emailVerified="verified";
                }
            }
            Log.e("Email","Emails"+"  "+emailVerified);
        }
    }

    void openChildFragment() {
        ProfileScreen frag=(ProfileScreen) getChildFragmentManager().findFragmentByTag("profile");
        if(frag==null)
            frag = ProfileScreen.getProfileScreen();
        FragmentManager fragMan = getChildFragmentManager();

        // add Fragment A to the main linear layout
        FragmentTransaction fragTrans = fragMan.beginTransaction();
        fragTrans.replace(R.id.main_menu, frag,"profile");
        fragTrans.addToBackStack(null);
        fragTrans.commit();
    }

    void companyProfileFragment() {
        Bundle bundle= new Bundle();
        bundle.putLong("companyId",companyData.getId());
        CompanyProfile frag=(CompanyProfile) getChildFragmentManager().findFragmentByTag("company_profile");
        if(frag==null)
            frag = CompanyProfile.getCompanyProfile();
            FragmentManager fragMan = getChildFragmentManager();
            frag.setArguments(bundle);

        // add Fragment A to the main linear layout
        FragmentTransaction fragTrans = fragMan.beginTransaction();
        fragTrans.replace(R.id.main_menu, frag,"profile");
        fragTrans.addToBackStack(null);
        fragTrans.commit();
    }

    private static MyReceiver r;
    public  void  refresh() {
        Log.e(TAG, "refresh()");
       /* MenuFragment currentFragment = (MenuFragment) getActivity().getSupportFragmentManager().findFragmentByTag("menu");

        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .detach(currentFragment)
                .commitNowAllowingStateLoss();

        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .attach(currentFragment)
                .commitAllowingStateLoss();

        getActivity().getSupportFragmentManager().executePendingTransactions();*/

        /*if(isAdded()&&MenuFragment.this!=null&&getChildFragmentManager()!=null) {
            List<Fragment> fragmentList = getChildFragmentManager().getFragments();
            if (fragmentList != null) {
                for (Fragment f : fragmentList) {
                    Log.e(TAG, "Removing fragment");
                    getChildFragmentManager().beginTransaction().remove(f).commit();
                    //checking
                }
            }
        }*/


        if(isAdded()) {
            List<Fragment> fragmentList = getChildFragmentManager().getFragments();
            if (fragmentList != null) {
                for (Fragment f : fragmentList) {
                    Log.e(TAG, "Removing fragment");
                    getChildFragmentManager().popBackStackImmediate();
                    // getChildFragmentManager().beginTransaction().remove(f).commit();
                    //checking
                }
            }
            MenuFragment frg = (MenuFragment) getActivity().getSupportFragmentManager().findFragmentByTag("menu");
            final FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
            ft.detach(frg);
            ft.attach(frg);
            ft.commit();
            //your code in refresh.
            Log.e("Refresh", "YES");
        }
    }

    public void onPause() {
        super.onPause();
        Log.e(TAG,"onPause");
    }

    public void onResume() {
        super.onResume();
        Log.e(TAG,"onResume");

        HomeScreenActivity.removeTabHost();
        if(r==null)
            r = new MyReceiver();
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(r,
                new IntentFilter("TAG_REFRESH"));

        //Posted Job Count and Company data
        getMaxJobCount(user.getId());

        // Check Swish Connected Status
        getSwishCustomer(user.getId());
    }

    @Override
    public void isShopRegistered(Boolean isShopRegistered) {

        this.isShopRegistered=isShopRegistered;

    }

    private class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            MenuFragment.this.refresh();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.e(TAG,"onStop");
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(r);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e(TAG,"onDestroy");
    }
}

