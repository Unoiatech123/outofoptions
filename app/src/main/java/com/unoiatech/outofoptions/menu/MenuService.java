package com.unoiatech.outofoptions.menu;

import com.unoiatech.outofoptions.api.ApiClient;
import com.unoiatech.outofoptions.api.URL;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Unoiatech on 1/22/2018.
 */

public class MenuService {

    public interface MenuAPI{
        @GET(URL.IS_SHOP_REGISTERED)
        Call<Boolean> isShopRegistered(@Path("userId")Long userId);


    }

    public MenuAPI getAPI(){
        MenuAPI apiService = ApiClient.getClient().create(MenuAPI.class);
        return apiService;
    }

}
