package com.unoiatech.outofoptions.fcmIntegration;


import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.unoiatech.outofoptions.model.User;
import com.unoiatech.outofoptions.util.SharedPrefsHelper;

/**
 * Created by Unoiatech on 4/19/2017.
 */

public class FirebaseInstanceIdService extends com.google.firebase.iid.FirebaseInstanceIdService
{
    private static String TAG="FirebaseInstanceIdService";
    @Override
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        //Log.d(TAG, "Refreshed token: " + refreshedToken);
        Log.e("Firebase","REfreshToken:"+refreshedToken);

        // TODO: Implement this method to send any registration to your app's servers.
        saveRefreshToken(refreshedToken);
    }

    private void saveRefreshToken(String refreshedToken) {
       SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).saveFcm(refreshedToken);
    }
}
