package com.unoiatech.outofoptions.fcmIntegration;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.api.ApiClient;
import com.unoiatech.outofoptions.api.ApiInterface;
import com.unoiatech.outofoptions.fragments.menufragments.notificationscreen.ShowFinishDialog;
import com.unoiatech.outofoptions.model.CompanyData;
import com.unoiatech.outofoptions.model.User;
import com.unoiatech.outofoptions.util.ApiError;
import com.unoiatech.outofoptions.util.BankId.ErrorUtils;
import com.unoiatech.outofoptions.util.S3Utils;
import com.unoiatech.outofoptions.util.SharedPrefsHelper;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Unoiatech on 7/29/2017.
 */
public class CompanyInviteResponse extends Activity implements View.OnClickListener {
    private static String TAG = "AcceptCompanyInvite";
    AVLoadingIndicatorView progressBar,progressBar1;
    Long companyId,notificationId;
    Dialog dialog;
    TextView accept,decline;
    ShowFinishDialog mCallback;

     @Override
     protected void onCreate(Bundle savedInstanceState) {
         super.onCreate(savedInstanceState);
         setContentView(R.layout.accept_company_invite_layout);
         TextView companyName = (TextView) findViewById(R.id.company_name);
         TextView companyLocation = (TextView) findViewById(R.id.company_location);
         ImageView companyLogo = (ImageView) findViewById(R.id.company_logo);

         //getData from intent
         Bundle bundle = getIntent().getExtras();
         companyName.setText(bundle.getString("company_name"));
         companyId = bundle.getLong("company_id");
         notificationId=bundle.getLong("notification_id");

         //Get CompanyDta
         getCompanyProfile(companyId,companyLocation,companyLogo);

         TextView staticText = (TextView) findViewById(R.id.static_text);
         TextView accept = (TextView) findViewById(R.id.accept_btn);
         TextView decline = (TextView) findViewById(R.id.decline_btn);
         ImageView cancelImage = (ImageView) findViewById(R.id.cancel_image);
         progressBar = (AVLoadingIndicatorView) findViewById(R.id.progress_bar);
         progressBar1 = (AVLoadingIndicatorView) findViewById(R.id.progress_bar1);

         //set Text on other terms
         Spannable wordtoSpan = new SpannableString(getString(R.string.staff_member_static_text));
         wordtoSpan.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.app_color)), 0, 5, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
         staticText.setText(wordtoSpan);

         this.setFinishOnTouchOutside(false);

         accept.setOnClickListener(this);
         cancelImage.setOnClickListener(this);
         decline.setOnClickListener(this);
     }

    private void getCompanyProfile(Long companyId, final TextView companyLocation, final ImageView companyLogo) {
        ApiInterface apiService= ApiClient.getClient().create(ApiInterface.class);
        Call<CompanyData> call= apiService.getCompanyProfile(companyId);
        call.enqueue(new Callback<CompanyData>() {
            @Override
            public void onResponse(Call<CompanyData> call, Response<CompanyData> response) {
                Log.d(TAG,"Response"+response.body()+" "+response.message());
                if(response.code()==200) {
                    companyLocation.setText(response.body().getCompany().getLocationName());
                    Glide.with(CompanyInviteResponse.this).load(S3Utils.getCompanylogo(response.body().getCompany().getId(),response.body().getCompany().getLogo())).into(companyLogo);
                }else{
                    ApiError error = ErrorUtils.parseError(response);
                    Snackbar.make(getWindow().getDecorView(),error.getMessage(),Snackbar.LENGTH_SHORT).show();
                    Log.d("Code" + "Code", "" + response);
                }
            }

            @Override
            public void onFailure(Call<CompanyData> call, Throwable t) {
                Log.d("Failure","Failure"+t.toString());
            }
        });
    }

    @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.accept_btn:
                    acceptCompanyInvite("yes",companyId,progressBar1);
                    break;
                case R.id.decline_btn:
                    acceptCompanyInvite("no",companyId,progressBar);
                    break;
                case R.id.cancel_image:
                     finish();
                    break;
            }
        }

        private void acceptCompanyInvite(String response, Long companyId, final AVLoadingIndicatorView progress_Bar) {
            progress_Bar.setVisibility(View.VISIBLE);

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            User user= SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readUserPref();
            Call<Void> call = apiService.inviteUserResponse(response,companyId,user.getId());
            call.enqueue(new Callback<Void>() {
                @Override
                public void onResponse(Call<Void> call, Response<Void> response) {
                    progress_Bar.setVisibility(View.GONE);
                    Log.e(TAG, "Response" + response + " " + response.body()+ " "+response.isSuccessful());
                    if (response.isSuccessful()) {
                        Log.e("Successful","Successful");
                        markNotificationAsRead(notificationId);
                    }
                    else{
                        finish();
                        //dialog.dismiss();
                        final ApiError error = ErrorUtils.parseError(response);
                        mCallback.errorDialog(error.getMessage());
                        Log.e("Response","REsponse"+error.getMessage());
                    }
                }

                @Override
                public void onFailure(Call<Void> call, Throwable t) {
                    Log.e(TAG, "Error" + t.toString());
                }
            });
        }

        private void markNotificationAsRead(Long noti_id) {
            ApiInterface apiService= ApiClient.getClient().create(ApiInterface.class);
            ArrayList<Long> list=new ArrayList<>();
            list.add(noti_id);
            Call<ResponseBody> call=apiService.markNotificationAsRead(list);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    Log.e("response",""+response.body()+" "+response.message());
                    finish();
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                }
            });
        }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
