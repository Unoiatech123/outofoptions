package com.unoiatech.outofoptions.dealsmodule;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.common.api.Api;
import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.api.ApiClient;
import com.unoiatech.outofoptions.api.ApiInterface;
import com.unoiatech.outofoptions.model.ShopData;
import com.unoiatech.outofoptions.util.ApiError;
import com.unoiatech.outofoptions.util.BankId.ErrorUtils;

import java.util.Calendar;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Unoiatech on 12/19/2017.
 */
public class BookingViaCallDialog extends DialogFragment implements View.OnClickListener{
    private Dialog dialog;
    private ImageView shopLogo;
    private TextView shopName,shopLocation,shopPhnNo;
    private static String TAG="BookingViaCall";

    public static DialogFragment newInstance() {
        return new BookingViaCallDialog();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.call_dialog_layout,container,false);

        //Initialize Components
        initViews(view);
        return view;
    }

    private void initViews(View view) {
        shopLogo=(ImageView)view.findViewById(R.id.shop_logo);
        shopName = (TextView) view.findViewById(R.id.shop_name);
        shopLocation=(TextView)view.findViewById(R.id.shop_loc);
        shopPhnNo=(TextView)view.findViewById(R.id.shop_number);
        Button cancel=(Button)view.findViewById(R.id.cancel);
        Button call=(Button)view.findViewById(R.id.call_btn);

        //To get shop details
        getShopDetails(getArguments().getLong("shop_id",0));

        //Set OnClickListeners
        call.setOnClickListener(this);
        cancel.setOnClickListener(this);
    }

    private void getShopDetails(long shop_id) {
        ApiInterface apiService= ApiClient.getClient().create(ApiInterface.class);
        Call<ShopData> call= apiService.getShopDetails(shop_id);
        call.enqueue(new Callback<ShopData>() {
            @Override
            public void onResponse(Call<ShopData> call, Response<ShopData> response) {
                Log.e(TAG,"Response"+"  "+response.body());
                if(response.isSuccessful()){
                    shopName.setText(response.body().getShopDto().getName());
                    shopLocation.setText(response.body().getShopDto().getLocation());
                    shopPhnNo.setText(response.body().getShopDto().getPhoneNumber());
                }else{
                    ApiError error= ErrorUtils.parseError(response);
                    Snackbar.make(getView(),error.getMessage(),Snackbar.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ShopData> call, Throwable t) {
                Log.e(TAG,"Failure"+"  "+t.toString());
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.cancel:
                break;
            case R.id.call_btn:
                break;
        }
    }
}
