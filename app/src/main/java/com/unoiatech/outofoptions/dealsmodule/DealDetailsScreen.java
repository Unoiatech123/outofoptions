package com.unoiatech.outofoptions.dealsmodule;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;
import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.api.ApiClient;
import com.unoiatech.outofoptions.api.ApiInterface;
import com.unoiatech.outofoptions.fragments.instantjob.JobPostedDialog;
import com.unoiatech.outofoptions.model.Deals;
import com.unoiatech.outofoptions.util.ApiError;
import com.unoiatech.outofoptions.util.BankId.ErrorUtils;
import com.unoiatech.outofoptions.util.GPSTracker;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Unoiatech on 12/7/2017.
 */

public class DealDetailsScreen extends AppCompatActivity implements OnMapReadyCallback,View.OnClickListener{
    private MapView mapView;
    private GoogleMap map;
    private LatLng origin;
    private SimpleRatingBar rating_bar;
    private TextView deal_title,deal_price,original_price,location,category,reviews,views;
    private Button call_btn;
    private View view;
    private Double latitude,longitude;
    private static String TAG="DealDetailsScreen";
    private String bookingType;
    private Long shopId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.deals_details_layout);

        initViews();

        //getCurrent Location
        getUserLocation();

        //initialise Map
        mapView=(MapView)findViewById(R.id.map);
        mapView.onCreate(savedInstanceState);
        mapView.onResume();
        try {
            MapsInitializer.initialize(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        mapView.getMapAsync(this);

        //Method To get Deal details
        getDealDetails(getIntent().getLongExtra("deal_id",0));
    }

    private void getUserLocation() {
        GPSTracker gps = new GPSTracker(this);
        if (gps.canGetLocation()) {
            latitude = gps.getLatitude();
            longitude = gps.getLongitude();
        } else {
            gps.showSettingsAlert();
        }
    }

    private void initViews() {
        deal_title=(TextView)findViewById(R.id.deal_title);
        category=(TextView)findViewById(R.id.category);
        location=(TextView)findViewById(R.id.location);
        deal_price=(TextView)findViewById(R.id.deal_price);
        original_price=(TextView)findViewById(R.id.original_price);
        reviews=(TextView)findViewById(R.id.reviews);
        rating_bar=(SimpleRatingBar)findViewById(R.id.rating);
        call_btn=(Button)findViewById(R.id.call_btn);
        view=(View)findViewById(R.id.view);

        //set Click Listeners
        call_btn.setOnClickListener(this);

        //original_price.getMeasuredWidth();  //get width
        final NestedScrollView mainScrollView = (NestedScrollView)findViewById(R.id.nested_scroll_view);
        ImageView transparentImageView = (ImageView)findViewById(R.id.transparent_image);

        transparentImageView.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        mainScrollView.requestDisallowInterceptTouchEvent(true);
                        // Disable touch on transparent view
                        return false;

                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        mainScrollView.requestDisallowInterceptTouchEvent(false);
                        return true;

                    case MotionEvent.ACTION_MOVE:
                        mainScrollView.requestDisallowInterceptTouchEvent(true);
                        return false;

                    default:
                        return true;
                }
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        map.setMyLocationEnabled(true);;

        //map.getUiSettings().setZoomControlsEnabled(false);
        origin = new LatLng(30.6514,76.7355);

        //to disable map zooming from world map.
        map.moveCamera(CameraUpdateFactory.newLatLng(origin));
        map.animateCamera(CameraUpdateFactory.zoomTo(10));
    }

    private void getDealDetails(long deal_id) {
        ApiInterface apiService= ApiClient.getClient().create(ApiInterface.class);
        Call<Deals> call=apiService.getDealDetails(deal_id);
        call.enqueue(new Callback<Deals>() {
            @Override
            public void onResponse(Call<Deals> call, Response<Deals> response) {
                Log.e(TAG,"Response"+response.isSuccessful()+"  "+response.body().getBookingType());
                if(response.isSuccessful()){
                    deal_title.setText(response.body().getTitle());
                    deal_price.setText("SEK "+response.body().getDealPrice());
                    original_price.setText("SEK "+response.body().getOriginalPrice());
                    location.setText(response.body().getShopLocation());
                    category.setText(response.body().getShopCategory());
                    bookingType=response.body().getBookingType();
                    shopId=response.body().getShopId();
                    //To get Original Price Textview width and set custom width to the view accordingly
                    original_price.measure(0,0);       //must call measure!
                    view.getLayoutParams().width = original_price.getMeasuredWidth();
                    view.requestLayout();
                }else{
                    ApiError error= ErrorUtils.parseError(response);
                    notifyUser(error.getMessage());
                }
            }

            @Override
            public void onFailure(Call<Deals> call, Throwable t) {
                Log.e(TAG,"Failure"+"  "+t.toString());
            }
        });
    }

    private void notifyUser(String message) {
        Snackbar.make(getWindow().getDecorView().getRootView(),message,Snackbar.LENGTH_SHORT).show();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.call_btn:
                if(bookingType.equals("CALL")){
                    showBookingDialog();
                }else {
                    showCallingDialog();
                }
                break;
        }
    }

    private void showCallingDialog() {
        FragmentTransaction ft= getSupportFragmentManager().beginTransaction();
        ft.addToBackStack(null);
        DialogFragment frag= BookingViaCallDialog.newInstance();

        //set data to pass
        Bundle bundle = new Bundle();
        bundle.putLong("shop_id",shopId);
        frag.setArguments(bundle);
        frag.show(ft,"");
    }

    private void showBookingDialog() {
        DialogFragment bookingDialog = BookingViaO3Dialog.newInstance();

        FragmentTransaction fragTransaction = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("deal_booking");
        Log.e("ShowDialog","ShowDialog"+"  "+prev);
        // For a little polish, specify a transition animation
        fragTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);

        // To make it fullscreen, use the 'content' root view as the container
        // for the fragment, which is always the root view for the activity
        if (prev == null) {
            fragTransaction.add(android.R.id.content, bookingDialog,"deal_booking")
                    .addToBackStack(null).commit();
        }
    }
}
