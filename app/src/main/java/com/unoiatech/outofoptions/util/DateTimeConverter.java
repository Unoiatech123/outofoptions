package com.unoiatech.outofoptions.util;

import android.util.Log;

import com.google.gson.internal.bind.DateTypeAdapter;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;

/**
 * Created by Unoiatech on 5/3/2017.
 */

public class DateTimeConverter {
    public static String getTime(){
        Calendar now= Calendar.getInstance();
        DateFormat target = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss.SSS'Z'");
        return target.format(now.getTime());
    }
    public static String compareDate(Long date) {
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        cal1.setTimeInMillis(date);
        DateFormat targetFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm a");
        String date1 = targetFormat.format(cal1.getTime());
        String date2 = targetFormat.format(cal2.getTime());
        String format = "MM/dd/yyyy hh:mm a";
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        try {
            Date dateObj1 = sdf.parse(date1);
            Date dateObj2 = sdf.parse(date2);
            DecimalFormat crunchifyFormatter = new DecimalFormat("###,###");
            long diff = dateObj1.getTime()-dateObj2.getTime();
            int diffDays = (int) (diff / (24 * 60 * 60 * 1000));
            int diffhours = (int) (diff / (60 * 60 * 1000));
            int diffmin = (int) (diff / (60 * 1000) % 60);
            System.out.println("difference between days: " + diffDays);
            if(diffDays<0) {
                return  "Not accepting proposals :"+"Expired";
            }
            else if(diffDays>= 0){
                Log.e("Daus","Days"+diffDays+diffhours);
                if (diffhours < 0) {
                    System.out.println("difference between hours: " + crunchifyFormatter.format(diffhours));
                    return "Not accepting proposals :" + "Expired";
                }
                if (diffmin < 0){
                    System.out.println("difference between minutes: " + crunchifyFormatter.format(diffmin));
                    return "Not accepting proposals :" + "Expired";
                }
                else{
                    return "Accepting proposals :" + "Expires in "+ String.valueOf(diffhours) + " h " + String.valueOf(diffmin) + " m";
                }
            }
        }
        catch (ParseException ex){
            ex.printStackTrace();
        }
        return null;
    }
    
    public static String getTimeToComplete(Long offeredTimeline) throws ParseException {
        Calendar offeredTime=Calendar.getInstance();
        //offeredTime.setTimeInMillis(Long.parseLong(offeredTimeline));
        offeredTime.setTimeInMillis(offeredTimeline);
        Calendar today= Calendar.getInstance();
        Calendar tomorrow=Calendar.getInstance();
        tomorrow.add(tomorrow.DATE,+1);
        Calendar yesterday=Calendar.getInstance();
        yesterday.add(Calendar.DATE,-1);
        DateFormat date_format= new SimpleDateFormat("dd MMM yyyy");
        if(offeredTime.get(Calendar.YEAR)==today.get(Calendar.YEAR)&&offeredTime.get(Calendar.MONTH)==today.get(Calendar.MONTH)&&offeredTime.get(Calendar.DAY_OF_YEAR)==today.get(Calendar.DAY_OF_YEAR)) {
            return "Today";
        }
        else if(offeredTime.get(Calendar.YEAR)==yesterday.get(Calendar.YEAR)&&offeredTime.get(Calendar.MONTH)==yesterday.get(Calendar.MONTH)&&offeredTime.get(Calendar.DAY_OF_YEAR)==yesterday.get(Calendar.DAY_OF_YEAR)){
            return "Yesterday";
        }
        else if(offeredTime.get(Calendar.YEAR)==tomorrow.get(Calendar.YEAR)&&offeredTime.get(Calendar.MONTH)==tomorrow.get(Calendar.MONTH)&&offeredTime.get(Calendar.DAY_OF_YEAR)==tomorrow.get(Calendar.DAY_OF_YEAR)) {
            return "Tomorrow";
        }
        else{
            return  date_format.format(offeredTime.getTime());
        }
    }
    public static String getExpiredDateTime(Long offeredTimeline) throws ParseException {
        Calendar offeredTime=Calendar.getInstance();
        //offeredTime.setTimeInMillis(Long.parseLong(offeredTimeline));
        offeredTime.setTimeInMillis(offeredTimeline);
        Calendar today= Calendar.getInstance();
        Calendar tomorrow=Calendar.getInstance();
        tomorrow.add(tomorrow.DATE,+1);
        Calendar yesterday=Calendar.getInstance();
        yesterday.add(Calendar.DATE,-1);
        DateFormat date_format= new SimpleDateFormat("dd MMM,HH:mm");
        if(offeredTime.get(Calendar.YEAR)==today.get(Calendar.YEAR)&&offeredTime.get(Calendar.MONTH)==today.get(Calendar.MONTH)&&offeredTime.get(Calendar.DAY_OF_YEAR)==today.get(Calendar.DAY_OF_YEAR)) {
            return "Deal Ends in"+date_format.format(offeredTime.getTime());
        }
        else{
            return  "Deal Ends on"+date_format.format(offeredTime.getTime());
        }
    }

    public static String getTimeInUtcFormat() {
        TimeZone timeZone = TimeZone.getTimeZone("UTC");
        Calendar calendar = Calendar.getInstance(timeZone);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss.SSS'Z'");
        simpleDateFormat.setTimeZone(timeZone);

        System.out.println("Time zone: " + timeZone.getID());
       /* System.out.println("default time zone: " + TimeZone.getDefault().getID());
        System.out.println();*/
        Log.e("Tme_Format","Time_format"+simpleDateFormat.format(calendar.getTime()));
        return simpleDateFormat.format(calendar.getTime());
    }

    public static String timeOnHomeScreen(long millis) {
        int diffhours = 0;
        String dateFormat = "MM/dd/yyyy hh:mm a";
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);

        Date timeline= new Date(millis);
        Date cal2 = new Date();

        DateFormat targetFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm a");
        String date1 = targetFormat.format(timeline);
        String date2 = targetFormat.format(cal2.getTime());

        try {
           Date dateObj1 = sdf.parse(date1);
           Date dateObj2 = sdf.parse(date2);
           long diff = dateObj1.getTime()-dateObj2.getTime();
           diffhours = (int) (diff / (60 * 60 * 1000));
           Log.e("Hours","hours"+diffhours);
        }
        catch (ParseException ex){
            ex.printStackTrace();
        }

        int day = (int) TimeUnit.HOURS.toDays(diffhours);
        long hours = TimeUnit.HOURS.toHours(diffhours) - (day * 24);
        long minutes = TimeUnit.HOURS.toMinutes(diffhours) - (TimeUnit.HOURS.toHours(millis) * 60);
        //long second = TimeUnit.SECONDS.toSeconds(seconds) - (TimeUnit.SECONDS.toMinutes(seconds) *60)}
        Log.e("Time_OnHomeScreen","Time_onHomeScreen"+day+" "+hours+minutes);
        if(day>3){
            return "Expires in "+String.valueOf(day)+" days "+String.valueOf(hours)+" hr";
        }
        else if(day<=3){
            Log.e("Less","Less"+diffhours+ " "+minutes);
            return "Expires in "+String.valueOf(diffhours)+" hr ";
        }
        else if(hours>=0) {
            return "Expires in "+String.valueOf(hours)+" hr "+String.valueOf(minutes)+" min";
        }
        else{
            return "Expired";
        }
    }
    public static String longMillisecondsToDate(Long millis) {
        Date date = new Date(millis);
        DateFormat parser1 = new SimpleDateFormat("dd-MMM, HH:mm");
        return parser1.format(date);
    }
    public static String getCreatedDate(Long millis) {
        Date date= new Date(millis);
        String dateTime= String.valueOf(date);
        String createdDate=dateTime.substring(0,10);
        return createdDate;
    }

    public static String  getRegYear(Long millis) {
            Date date = new Date(millis);
            DateFormat parser1 = new SimpleDateFormat("yyyy");
            return parser1.format(date);
    }

    public static String getPaymentModifiedDate(Long millis){
        Date date = new Date(millis);
        DateFormat parser1 = new SimpleDateFormat("MM/dd/yyyy");
        return parser1.format(date);
    }

    public static String getDateTime(Long millis){
        Calendar offeredTime=Calendar.getInstance();
        offeredTime.setTimeInMillis(millis);
        Calendar today= Calendar.getInstance();
        DateFormat time= new SimpleDateFormat("hh:mm");
        DateFormat date_format= new SimpleDateFormat("dd MMMM, HH:mm");
        if(offeredTime.get(Calendar.YEAR)==today.get(Calendar.YEAR)&&offeredTime.get(Calendar.MONTH)==today.get(Calendar.MONTH)&&offeredTime.get(Calendar.DAY_OF_YEAR)==today.get(Calendar.DAY_OF_YEAR)) {
            return "Today, "+time.format(offeredTime.getTime());
        }
        else{
            return  date_format.format(offeredTime.getTime());
        }
    }
    public static String getDealExpiryTime(Long millis){
        Calendar offeredTime=Calendar.getInstance();
        offeredTime.setTimeInMillis(millis);
        Calendar today= Calendar.getInstance();
        DateFormat time= new SimpleDateFormat("hh:mm");
        DateFormat date_format= new SimpleDateFormat("dd/ MM");
        if(offeredTime.get(Calendar.YEAR)==today.get(Calendar.YEAR)&&offeredTime.get(Calendar.MONTH)==today.get(Calendar.MONTH)&&offeredTime.get(Calendar.DAY_OF_YEAR)==today.get(Calendar.DAY_OF_YEAR)) {
            return "Deal Ends in "+time.format(offeredTime.getTime());
        }
        else{
            return  "Deal Ends on "+date_format.format(offeredTime.getTime());
        }
    }
    public static String getDealBookingTime(){
        Calendar calender=Calendar.getInstance();
        SimpleDateFormat timeFormat=new SimpleDateFormat("HH:mm");
        return timeFormat.format(calender.getTime());
    }
}
