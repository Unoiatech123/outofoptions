package com.unoiatech.outofoptions.util;

/**
 * Created by unoiaAndroid on 3/15/2017.
 */

public class S3Utils {

    //Testing
    public static String BASE_URL="https://s3.eu-central-1.amazonaws.com/o3buckettest";

    //Online
   // private static String BASE_URL="https://s3.eu-central-1.amazonaws.com/o3bucket/";

    public static String getDownloadUserImageURL(Long userId,String imageName){

        return BASE_URL+"/user/"+userId+"/user_image/"+imageName;
    }


    public static String getDownloadJobImageURL(Long userId,String imageName){

        return BASE_URL+"/job/"+userId+"/job_image/"+imageName;
    }


   public static String getDownloadDraftImageURL(Long userId,String imageName){

        return BASE_URL+"/draft/"+userId+"/draft_image/"+imageName;
    }

    //companyLogo
    public static String getCompanylogo(Long companyId,String companyImage) {
        return BASE_URL+"/company/"+companyId+"/company_image/"+companyImage;
    }
}
