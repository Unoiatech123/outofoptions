package com.unoiatech.outofoptions.util.StripeErrorHandler;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import com.unoiatech.outofoptions.R;

/**
 * Created by Unoiatech on 9/6/2017.
 */

public class ErrorDialogFragment extends DialogFragment
{
    Dialog dialog;

    public static DialogFragment newInstance(int titleId, String errorMessage) {
        ErrorDialogFragment fragment = new ErrorDialogFragment();

        Bundle args = new Bundle();
        args.putInt("titleId", titleId);
        args.putString("message", errorMessage);

        fragment.setArguments(args);

        return fragment;
    }

    public ErrorDialogFragment() {
        // Empty constructor required for DialogFragment
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        int titleId = getArguments().getInt("titleId");
        String message = getArguments().getString("message");

        return new AlertDialog.Builder(getActivity())
                .setTitle(titleId)
                .setMessage(message)
                .setPositiveButton(R.string.max_ok_btn, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
    }
}

