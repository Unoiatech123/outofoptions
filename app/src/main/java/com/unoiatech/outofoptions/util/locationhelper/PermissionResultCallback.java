package com.unoiatech.outofoptions.util.locationhelper;

import java.util.ArrayList;

/**
 * Created by Unoiatech on 1/19/2018.
 */

interface PermissionResultCallback {
    void PermissionGranted(int request_code);

    void PartialPermissionGranted(int request_code, ArrayList<String> granted_permissions);

    void PermissionDenied(int request_code);

    void NeverAskAgain(int request_code);
}
