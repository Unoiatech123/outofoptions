package com.unoiatech.outofoptions.util.StripeErrorHandler;

import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;

import com.unoiatech.outofoptions.R;

/**
 * Created by Unoiatech on 9/6/2017.
 */

public class ErrorDialogHandler {
    FragmentManager mFragmentManager;

    public ErrorDialogHandler(FragmentManager manager){
        this.mFragmentManager=manager;
    }

    public void showError(String errorMessage){
        DialogFragment fragment = ErrorDialogFragment.newInstance(R.string.validationErrors, errorMessage);
        fragment.show(mFragmentManager, "error");
    }
}
