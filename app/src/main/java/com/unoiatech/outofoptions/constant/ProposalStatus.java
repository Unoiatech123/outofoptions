package com.unoiatech.outofoptions.constant;

/**
 * Created by unoiaAndroid on 3/14/2017.
 */

public enum ProposalStatus {

  PENDING("pending"),COMPLETED("completed"),ACCEPTED("accepted"),REJECTED("rejected"),CANCELLED("cancelled"),
    CONFIRMED("confirmed");

    private final String val;

    private ProposalStatus(String val) {
        this.val = val;
    }

    @Override
    public String toString() {
        return val;
    }
}
