package com.unoiatech.outofoptions.jobdetail;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTabHost;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.jobdetail.chat.ChatFragment;
import com.unoiatech.outofoptions.jobdetail.chat.ProfileFragment;

/**
 * Created by unoiaAndroid on 5/8/2017.
 */

public class ChatActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initToolbar();
        initTabHost();

    }

    private void initToolbar() {
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
    }

    private void initTabHost() {
        FragmentTabHost mTabHost = (FragmentTabHost)findViewById(android.R.id.tabhost);

        mTabHost.setup(this, getSupportFragmentManager(), android.R.id.tabcontent);
        mTabHost.getTabWidget().setStripEnabled(false);
        mTabHost.getTabWidget().setDividerDrawable(null);

        mTabHost.addTab(mTabHost.newTabSpec("profile").setIndicator(prepareTabView(R.drawable.chat_selector)),
                ProfileFragment.class, null);
        mTabHost.addTab(mTabHost.newTabSpec("chat").setIndicator(prepareTabView(R.drawable.home_selector)),
                ChatFragment.class, null);
            }

    private View prepareTabView(int messageImage) {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.tab_icon_layout, null);
        ImageView image = (ImageView) view.findViewById(R.id.tab_image);
        image.setBackgroundResource(messageImage);
        return view;
    }
}
