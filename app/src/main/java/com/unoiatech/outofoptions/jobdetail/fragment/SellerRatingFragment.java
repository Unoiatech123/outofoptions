package com.unoiatech.outofoptions.jobdetail.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;
import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.model.SellerReviewWithUserInfo;
import com.unoiatech.outofoptions.model.UserRating;
import com.unoiatech.outofoptions.util.S3Utils;
import com.unoiatech.outofoptions.util.TimeConverter;
import java.util.ArrayList;
import java.util.List;
import de.hdodenhof.circleimageview.CircleImageView;


/**
 * Created by unoiaAndroid on 4/25/2017.
 */

public class SellerRatingFragment extends Fragment implements RatingDialogFragment.DataListener {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private static String TAG = "SellerRatingFragment";
    private UserRating userRating;
    private SimpleRatingBar avReview;
    List<SellerReviewWithUserInfo> sellerReviewWithUserInfos = new ArrayList<>();


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.seller_reviews_layout, container, false);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        TextView numOfReviews=(TextView) view.findViewById(R.id.num_of_reviews);
        avReview=(SimpleRatingBar) view.findViewById(R.id.average_review);
        ImageButton cancelButton= (ImageButton) view.findViewById(R.id.cancel);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        //mRecyclerView.setHasFixedSize(true);

        mAdapter = new SellerRatingFragment.SellerAdapter(sellerReviewWithUserInfos);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mRecyclerView.getContext(),
                DividerItemDecoration.VERTICAL);

        mRecyclerView.addItemDecoration(dividerItemDecoration);
        // specify an adapter (see also next example)
        mRecyclerView.setAdapter(mAdapter);

        userRating=RatingDialogFragment.userRating;
        sellerReviewWithUserInfos.clear();
        sellerReviewWithUserInfos.addAll(userRating.getSeller_reviews());
        mAdapter.notifyDataSetChanged();
        numOfReviews.setText(userRating.getSeller_reviews().size()+" reviews");

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RatingDialogFragment ratingDialogFragment = (RatingDialogFragment) getParentFragment();
                ratingDialogFragment.dismiss();
            }
        });

        return view;
    }

    @Override
    public void methodToPassData(Object data) {
        Log.e(TAG,"methodToPassData");
        UserRating rating = (UserRating) data;
        sellerReviewWithUserInfos.addAll(rating.getSeller_reviews());
        mAdapter.notifyDataSetChanged();
    }

    // Custom adapter for displaying User as Seller Review's .
        public class SellerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        private List<SellerReviewWithUserInfo> mDataset;

        private static final int TYPE_HEADER = 0;
        private static final int TYPE_ITEM = 1;



        // Provide a suitable constructor (depends on the kind of dataset)
        public SellerAdapter(List<SellerReviewWithUserInfo> mDataset) {
            this.mDataset = mDataset;
        }

        // Create new views (invoked by the layout manager)
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent,int viewType) {
            Log.e(TAG, "onCreateViewHolder" + viewType);
            if (viewType == TYPE_ITEM) {
                Log.e(TAG, "instance of TYPE_ITEM");
                LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View rowView = inflater.inflate(R.layout.review_row_item, parent, false);
                VHItem vh = new VHItem(rowView);

                return vh;
            } else if (viewType == TYPE_HEADER) {
                Log.e(TAG, "instance of TYPE_HEADER");
                LayoutInflater inflater = (LayoutInflater) getContext()
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View rowView = inflater.inflate(R.layout.seller_review_header_layout, parent, false);

                VHHeader vh = new VHHeader(rowView);
                return vh;
            }

            throw new RuntimeException("there is no type that matches the type " + viewType + " +" +
                    "  make sure your using types correctly");
        }

        // Replace the contents of a view (invoked by the layout manager)
        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            // - get element from your dataset at this position
            // - replace the contents of the view with that element

            Log.e("onBindVH nothing ", "called for " + position);

            if (holder instanceof VHItem) {

                SellerReviewWithUserInfo data = getItem(position);

                Log.e("onBindVH VHItem", "called for " + position);

                ((VHItem) holder).date.setText(TimeConverter.longMillisecondsToDate(
                        data.getReview().getJob().getCompletedTime()));
                ((VHItem) holder).jobTitle.setText(data.getReview().getJob().getTitle());
                ((VHItem) holder).reviewText.setText(data.getReview().getDescriptin());
                Glide.with(getActivity()).load(S3Utils.getDownloadUserImageURL(data.getUser().getId(),
                        data.getUser().getImagePath())).placeholder(R.drawable.user_image).
                        into(((VHItem) holder).userImageView);


                //cast holder to VHItem and set data
            } else if (holder instanceof VHHeader) {
                Log.e("onBindVH VHHeader", "called for " + position);
                //cast holder to VHHeader and set data for header.
                if(userRating.getSeller_reviews().size()>0)

                {
                    Float communication = 0f, punctuality = 0f,qualityOfWork = 0f;
                    for (int i = 0; i < userRating.getSeller_reviews().size(); i++) {
                        communication = communication + userRating.getSeller_reviews().get(i).getReview().getCommunication();
                        punctuality = punctuality + userRating.getSeller_reviews().get(i).getReview().getPunctuality();
                        qualityOfWork = qualityOfWork + userRating.getSeller_reviews().get(i).getReview().getQualityOfWork();
                    }

                    float commRating = (communication / userRating.getSeller_reviews().size());
                    float punRating = (punctuality / userRating.getSeller_reviews().size());
                    float qowRating = (qualityOfWork / userRating.getSeller_reviews().size());

                    ((VHHeader) holder).bar1.setRating(commRating);
                    ((VHHeader) holder).bar2.setRating(punRating);
                    ((VHHeader) holder).bar3.setRating(qowRating);

                    avReview.setRating((commRating + punRating + qowRating) / 3);
                }

            }

        }

        @Override
        public int getItemCount() {
            Log.e(TAG, "getItemCount" + mDataset.size() + 1);
            return mDataset.size() + 1;
        }

        @Override
        public int getItemViewType(int position) {
            if (isPositionHeader(position)) {

                Log.e("getItemViewType", "TYPE_HEADER");
                return TYPE_HEADER;
            }
            Log.e("getItemViewType", "TYPE_ITEM");
            return TYPE_ITEM;
        }

        private boolean isPositionHeader(int position) {
            return position == 0;
        }

        private SellerReviewWithUserInfo getItem(int position) {
            return mDataset.get(position - 1);

        }

        class VHItem extends RecyclerView.ViewHolder {
            TextView jobTitle;
            TextView date;
            TextView reviewText;
            CircleImageView userImageView;

            public VHItem(View itemView) {
                super(itemView);
                jobTitle = (TextView) itemView.findViewById(R.id.job_title);
                date = (TextView) itemView.findViewById(R.id.date);
                reviewText = (TextView) itemView.findViewById(R.id.review_text);
                userImageView = (CircleImageView) itemView.findViewById(R.id.user_image_view);

            }
        }

        class VHHeader extends RecyclerView.ViewHolder {
            SimpleRatingBar bar1, bar2, bar3;

            public VHHeader(View itemView) {
                super(itemView);
                bar1 = (SimpleRatingBar) itemView.findViewById(R.id.rating1);
                bar2 = (SimpleRatingBar) itemView.findViewById(R.id.rating2);
                bar3 = (SimpleRatingBar) itemView.findViewById(R.id.rating3);

            }
        }
    }

    /*public void getUserRating(Long userId) {

        Log.e(TAG, "getUserRating called");
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<UserRating> call = apiService.userRating(userId);
        call.enqueue(new Callback<UserRating>() {
            @Override
            public void onResponse(Call<UserRating> call, Response<UserRating> response) {

                userRating = response.body();
                if (response.body() != null) {

                    // userRating.setSeller_reviews(response.body().getSeller_reviews());

                    sellerReviewWithUserInfo.addAll(response.body().getSeller_reviews());
                    Log.e(TAG, "response was not null");
                    Log.e(TAG, response.body().toString());
                    Log.e(TAG, response.body().getSeller_reviews().size() + "");
                    mAdapter.notifyDataSetChanged();
                    Log.e(TAG, "notifyDataSetChanged called");
                    Log.e(TAG, userRating.getSeller_reviews().size() + "");
                } else
                    Log.e(TAG, "response was null");


            }

            @Override
            public void onFailure(Call<UserRating> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, "onFailure was called" + t.getMessage());
                Log.e(TAG, "onFailure was called" + t.getLocalizedMessage());
                Log.e(TAG, "onFailure was called" + t.toString());
                Log.e(TAG, "onFailure was called" + t.getCause());

                Snackbar.make(getView(), "Server error", Snackbar.LENGTH_SHORT);
            }
        });


    }*/

}
