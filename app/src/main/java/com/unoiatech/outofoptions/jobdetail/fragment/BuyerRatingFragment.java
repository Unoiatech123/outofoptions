package com.unoiatech.outofoptions.jobdetail.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.bumptech.glide.Glide;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;
import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.model.BuyerReviewWithUserInfo;
import com.unoiatech.outofoptions.model.UserRating;
import com.unoiatech.outofoptions.util.S3Utils;
import com.unoiatech.outofoptions.util.TimeConverter;
import android.util.Log;
import android.widget.ImageButton;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by unoiaAndroid on 4/25/2017.
 */

public class BuyerRatingFragment extends Fragment{
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private static String TAG = "BuyerRatingFragment";
    private UserRating userRating;
    private SimpleRatingBar avReview;
    List<BuyerReviewWithUserInfo> buyerReviewWithUserInfos = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.buyer_reviews_layout, container, false);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        TextView numOfReviews=(TextView) view.findViewById(R.id.num_of_reviews);
        avReview=(SimpleRatingBar) view.findViewById(R.id.average_review);
        ImageButton cancelButton= (ImageButton) view.findViewById(R.id.cancel);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        //mRecyclerView.setHasFixedSize(true);

        mAdapter = new BuyerRatingFragment.BuyerAdapter(buyerReviewWithUserInfos);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mRecyclerView.getContext(),
                DividerItemDecoration.VERTICAL);

        mRecyclerView.addItemDecoration(dividerItemDecoration);
        // specify an adapter (see also next example)
        mRecyclerView.setAdapter(mAdapter);

        userRating=RatingDialogFragment.userRating;


        buyerReviewWithUserInfos.clear();
        buyerReviewWithUserInfos.addAll(userRating.getBuyer_reviews());
        mAdapter.notifyDataSetChanged();
        numOfReviews.setText(userRating.getBuyer_reviews().size()+" reviews");

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RatingDialogFragment ratingDialogFragment = (RatingDialogFragment) getParentFragment();
                ratingDialogFragment.dismiss();

            }
        });

        return view;
    }



    // Custom adapter for displaying User as buyer Review's .
    public class BuyerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        private List<BuyerReviewWithUserInfo> mDataset;

        private static final int TYPE_HEADER = 0;
        private static final int TYPE_ITEM = 1;



        // Provide a suitable constructor (depends on the kind of dataset)
        public BuyerAdapter(List<BuyerReviewWithUserInfo> mDataset) {
            this.mDataset = mDataset;
        }

        // Create new views (invoked by the layout manager)
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent,int viewType) {
            if (viewType == TYPE_ITEM) {
                Log.e(TAG, "instance of TYPE_ITEM");
                LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View rowView = inflater.inflate(R.layout.review_row_item, parent, false);
                BuyerRatingFragment.BuyerAdapter.VHItem vh = new BuyerRatingFragment.BuyerAdapter.VHItem(rowView);

                return vh;
            } else if (viewType == TYPE_HEADER) {
                Log.e(TAG, "instance of TYPE_HEADER");
                LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View rowView = inflater.inflate(R.layout.buyer_review_header_layout, parent, false);

                BuyerRatingFragment.BuyerAdapter.VHHeader vh = new BuyerRatingFragment.BuyerAdapter.VHHeader(rowView);
                return vh;
            }
            throw new RuntimeException("there is no type that matches the type " + viewType + " +" +
                    "  make sure your using types correctly");
        }

        // Replace the contents of a view (invoked by the layout manager)
        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            // - get element from your dataset at this position
            // - replace the contents of the view with that element
            Log.e("onBindVH nothing ", "called for " + position);

            if (holder instanceof BuyerRatingFragment.BuyerAdapter.VHItem) {
                BuyerReviewWithUserInfo data = getItem(position);
                Log.e("onBindVH VHItem", "called for " + position);
                try {
                    ((BuyerRatingFragment.BuyerAdapter.VHItem) holder).date.setText(TimeConverter.longMillisecondsToDate(data.getReview().getJob().getCompletedTime()));
                }
                catch (NullPointerException e) {
                }
                try {
                    ((BuyerRatingFragment.BuyerAdapter.VHItem) holder).jobTitle.setText(data.getReview().getJob().getTitle());
                }catch (NullPointerException e){

                }
               try {
                   ((BuyerRatingFragment.BuyerAdapter.VHItem) holder).reviewText.setText(data.getReview().getDescriptin());
               }
               catch (NullPointerException e){

               }

                try {
                    Glide.with(getActivity()).load(S3Utils.getDownloadUserImageURL(data.getUser().getId(),
                            data.getUser().getImagePath())).placeholder(R.drawable.user_image).
                            into(((BuyerRatingFragment.BuyerAdapter.VHItem) holder).userImageView);
                }
                catch (NullPointerException e){
                }

                //cast holder to VHItem and set data
            } else if (holder instanceof BuyerRatingFragment.BuyerAdapter.VHHeader) {
                Log.e("onBindVH VHHeader", "called for " + position);
                //cast holder to VHHeader and set data for header.

                if(userRating.getBuyer_reviews().size()>0){
                    Float communication = 0f;

                    for (int i = 0; i < userRating.getBuyer_reviews().size(); i++) {
                        communication = communication + userRating.getBuyer_reviews().get(i).getReview().getCommunication();
                    }
                    float commRating = (communication / userRating.getBuyer_reviews().size());
                    ((BuyerRatingFragment.BuyerAdapter.VHHeader) holder).bar1.setRating(commRating);
                    avReview.setRating(commRating);
                }
            }
        }

        @Override
        public int getItemCount() {
           return mDataset.size() + 1;
        }

        @Override
        public int getItemViewType(int position) {
            if (isPositionHeader(position)) {
                return TYPE_HEADER;
            }
            return TYPE_ITEM;
        }

        private boolean isPositionHeader(int position) {
            return position == 0;
        }

        private BuyerReviewWithUserInfo getItem(int position) {
            return mDataset.get(position - 1);
        }

        class VHItem extends RecyclerView.ViewHolder {
            TextView jobTitle;
            TextView date;
            TextView reviewText;
            CircleImageView userImageView;

            public VHItem(View itemView) {
                super(itemView);
                jobTitle = (TextView) itemView.findViewById(R.id.job_title);
                date = (TextView) itemView.findViewById(R.id.date);
                reviewText = (TextView) itemView.findViewById(R.id.review_text);
                userImageView = (CircleImageView) itemView.findViewById(R.id.user_image_view);
            }
        }

        class VHHeader extends RecyclerView.ViewHolder {
            SimpleRatingBar bar1, bar2, bar3;

            public VHHeader(View itemView) {
                super(itemView);
                bar1 = (SimpleRatingBar) itemView.findViewById(R.id.rating1);
                bar2 = (SimpleRatingBar) itemView.findViewById(R.id.rating2);
                bar3 = (SimpleRatingBar) itemView.findViewById(R.id.rating3);
            }
        }
    }
}
