package com.unoiatech.outofoptions.fragments.activejobs.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;

import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.api.ApiClient;
import com.unoiatech.outofoptions.api.ApiInterface;
import com.unoiatech.outofoptions.model.RequestJobData;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Unoiatech on 4/25/2017.
 */

public class JobCancellationDialog extends DialogFragment implements View.OnClickListener {
    private static String TAG="JobCancellationDialog";
    Dialog dialog;

    public static DialogFragment newInstance() {
        JobCancellationDialog frag= new JobCancellationDialog();
        return frag;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.job_cancellation_dialog,container,false);

        Button yesBtn=(Button)view.findViewById(R.id.yes_btn);
        Button noBtn=(Button)view.findViewById(R.id.no_btn);

        yesBtn.setOnClickListener(this);
        noBtn.setOnClickListener(this);
        return view;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        dialog=super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.yes_btn:
                dialog.dismiss();
                RequestJobData jobInfo=getArguments().getParcelable("job_info");
                cancelJob(jobInfo);
                Log.e("View","View"+getArguments().getString("view"));
                if(getArguments().getString("view").equals("Recieved_Proposal")) {
                    getActivity().finish();
                }
                break;
            case R.id.no_btn:
                dialog.dismiss();
                if(getArguments().getString("view").equals("Received_Proposal")) {
                    //getActivity().finish();
                }
                break;
        }
    }
    private void cancelJob(final RequestJobData job) {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiService.markPendingJobCancel(job);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d(TAG, "Response" + response+" "+response.body());
                if(response.isSuccessful()){
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, "Error" + t.toString());
            }
        });
    }
}
