package com.unoiatech.outofoptions.fragments.activejobs;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTabHost;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TabHost;
import android.widget.TextView;

import com.allenliu.badgeview.BadgeView;
import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.api.ApiClient;
import com.unoiatech.outofoptions.api.ApiInterface;
import com.unoiatech.outofoptions.fragments.activejobs.adapter.DataReceivedEvent;
import com.unoiatech.outofoptions.fragments.activeproposals.ProposalFragment;
import com.unoiatech.outofoptions.model.Notification;
import com.unoiatech.outofoptions.model.NotificationResponse;
import com.unoiatech.outofoptions.model.User;
import com.unoiatech.outofoptions.util.ApiError;
import com.unoiatech.outofoptions.util.BankId.ErrorUtils;
import com.unoiatech.outofoptions.util.GetBadgeView;
import com.unoiatech.outofoptions.util.SharedPrefsHelper;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

import de.measite.minidns.record.A;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Unoiatech on 5/8/2017.
 */

public class JobProposalScreen extends AppCompatActivity {
    private FragmentTabHost mTabHost;
    private static String TAG = "JobProposalScreen";
    private ArrayList<Notification> activeJobs;
    private ArrayList<Notification> activeProposals;
    TextView helpReq,helpOffered;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.active_jobs_layout);

        /******Fragment TabHost***********/
        mTabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);
        mTabHost.setup(this, getSupportFragmentManager(), android.R.id.tabcontent);
        mTabHost.getTabWidget().setStripEnabled(false);
        mTabHost.getTabWidget().setDividerDrawable(null);

        /**Adding Fragments in Tab Host**/
        View view1 = customJobView(getLayoutInflater(), getResources().getString(R.string.help_requested));
        TabHost.TabSpec tabSpec1 = mTabHost.newTabSpec("Tab1").setIndicator(view1);
        mTabHost.addTab(tabSpec1, JobsFragment.class, null);

        View view2 = customProposalView(getLayoutInflater(), getResources().getString(R.string.offer_request));
        TabHost.TabSpec tabSpec3 = mTabHost.newTabSpec("Tab2").setIndicator(view2);
        mTabHost.addTab(tabSpec3, ProposalFragment.class, null);

        mTabHost.setCurrentTab(0);

        mTabHost.getTabWidget().getChildAt(0).setBackgroundResource(R.drawable.help_requested_drawable);

        /*******TabClickListener****************/
        mTabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String tabId) {
                int tab = mTabHost.getCurrentTab();
                if (tab == 0) {
                    Log.e("TAB", "TAB");
                    mTabHost.getTabWidget().getChildAt(tab).setBackgroundResource(R.drawable.help_requested_drawable);
                } else {
                    Log.e("ElseTAB", "ElseTAB" + tab);
                    mTabHost.getTabWidget().getChildAt(tab).setBackgroundResource(R.drawable.offer_help_drawable);
                }
            }
        });
    }

    private View customJobView(LayoutInflater inflater, String tabText) {
        View view = inflater.inflate(R.layout.tab_layout_for_tab1, null);
        helpReq = (TextView) view.findViewById(R.id.tab_text);
        helpReq.setTextColor(getResources().getColorStateList(R.color.tab_text_color));
        helpReq.setText(tabText);
        helpReq.setBackgroundResource(android.R.color.transparent);
        return view;
    }

    private View customProposalView(LayoutInflater inflater, String tabText) {
        View view = inflater.inflate(R.layout.tab_layout_for_tab2, null);
        helpOffered = (TextView) view.findViewById(R.id.tab_text2);
        helpOffered.setTextColor(getResources().getColorStateList(R.color.tab_text_color));
        helpOffered.setText(tabText);
        helpOffered.setBackgroundResource(android.R.color.transparent);
        return view;
    }

    @Override
    protected void onResume() {
        super.onResume();
        User user= SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readUserPref();
        getNotificationCount(user.getId());
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void getNotificationCount(Long userId) {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<NotificationResponse> call = apiService.getCategorizedNotification(userId);
        call.enqueue(new Callback<NotificationResponse>() {
            @Override
            public void onResponse(Call<NotificationResponse> call, Response<NotificationResponse> response) {
//                Log.e(TAG, "NotificationResponse" + " " + response.body().getJobNotifications().size() + " " + response.isSuccessful());
                if (response.isSuccessful()) {
                    activeJobs = new ArrayList<Notification>();
                    activeProposals = new ArrayList<Notification>();
                    if (response.body().getJobNotifications().size() >= 0) {
                        for (int i = 0; i < response.body().getJobNotifications().size(); i++) {
                            Log.e(TAG, "For");
                            if (response.body().getJobNotifications().get(i).getActivity().equals("CONFIRMED_PROPOSAL") || response.body().getJobNotifications().get(i).getActivity().equals("REFUSED_PROPOSAL") || response.body().getJobNotifications().get(i).getActivity().equals("COMPLETED_PROPOSAL") || response.body().getJobNotifications().get(i).getActivity().equals("ADDED_PROPOSAL")) {
                                activeJobs.add(response.body().getJobNotifications().get(i));
                                Log.e(TAG, "IF" + activeJobs.size());
                            } else if (response.body().getJobNotifications().get(i).getActivity().equals("CANCELLED_JOB") || response.body().getJobNotifications().get(i).getActivity().equals("ACCEPTED_PROPOSAL") || response.body().getJobNotifications().get(i).getActivity().equals("CONFIRMED_PROPOSAL")) {
                                activeProposals.add(response.body().getJobNotifications().get(i));
                                Log.e("Else", "Else" + " " + activeProposals.size());
                            }
                        }
                    }
                    setNoticationBadge(activeJobs,activeProposals);
                } else {
                    ApiError error = ErrorUtils.parseError(response);
                    Snackbar.make(getWindow().getDecorView(), error.getMessage(), Snackbar.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<NotificationResponse> call, Throwable t) {
                Log.e(TAG, "OnFailure" + "  " + t.toString());
            }
        });
    }

    private void setNoticationBadge(ArrayList<Notification> activeJobs, ArrayList<Notification> activeProposals) {
        final Animation animBounce = AnimationUtils.loadAnimation(JobProposalScreen.this, R.anim.anim_bounce);
        if(activeJobs.size()>0){
            BadgeView badge= GetBadgeView.getBadgeAtRight(getApplicationContext());
            badge.setBadgeCount(activeJobs.size()).bind(helpReq).setAnimation(animBounce);
        }
        if(activeProposals.size()>0){
            /*proposalBadge.setVisibility(View.VISIBLE);
            proposalBadge.setBackgroundResource(R.drawable.badge_count);
            proposalBadge.startAnimation(animBounce);
            proposalBadge.setText("" + activeProposals.size());*/
            BadgeView badge= GetBadgeView.getBadgeAtRight(getApplicationContext());
            badge.setBadgeCount(activeProposals.size()).bind(helpOffered).setAnimation(animBounce);
        }
    }

    public ArrayList<Notification> getData() {
        return activeJobs;
    }
}