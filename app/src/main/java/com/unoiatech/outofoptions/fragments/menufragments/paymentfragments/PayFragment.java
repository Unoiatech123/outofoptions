package com.unoiatech.outofoptions.fragments.menufragments.paymentfragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.unoiatech.outofoptions.R;

/**
 * Created by Unoiatech on 9/22/2017.
 */
public class PayFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.pay_fragment,container,false);
        return view;
    }
}
