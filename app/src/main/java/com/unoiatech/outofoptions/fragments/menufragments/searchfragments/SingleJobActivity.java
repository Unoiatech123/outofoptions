package com.unoiatech.outofoptions.fragments.menufragments.searchfragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.Toolbar;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.akexorcist.googledirection.DirectionCallback;
import com.akexorcist.googledirection.GoogleDirection;
import com.akexorcist.googledirection.constant.TransportMode;
import com.akexorcist.googledirection.model.Direction;
import com.akexorcist.googledirection.model.Route;
import com.akexorcist.googledirection.util.DirectionConverter;
import com.bumptech.glide.Glide;
import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;
import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.api.ApiClient;
import com.unoiatech.outofoptions.api.ApiInterface;
import com.unoiatech.outofoptions.fragments.activejobs.dialogs.JobCancellationDialog;
import com.unoiatech.outofoptions.fragments.menufragments.ProfileScreen;
import com.unoiatech.outofoptions.fragments.offerhelp.OfferHelp;
import com.unoiatech.outofoptions.jobdetail.CancellationTermsFragment;
import com.unoiatech.outofoptions.jobdetail.fragment.RatingDialogFragment;
import com.unoiatech.outofoptions.model.Company;
import com.unoiatech.outofoptions.model.Job;
import com.unoiatech.outofoptions.model.JobWithUser;
import com.unoiatech.outofoptions.model.User;
import com.unoiatech.outofoptions.model.UserRating;
import com.unoiatech.outofoptions.util.DateTimeConverter;
import com.unoiatech.outofoptions.util.S3Utils;
import com.unoiatech.outofoptions.util.SharedPrefsHelper;
import com.unoiatech.outofoptions.util.TimeConverter;
import com.unoiatech.outofoptions.util.common.view.CustomTypefaceSpan;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by unoiatech on 5/4/2016.
 */

public class SingleJobActivity extends FragmentActivity implements OnMapReadyCallback, DirectionCallback,BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener{

    private static final String TAG = "JobDetailFragment";
    private TextView categoryTextView, titleTextView, budgetTextView, nameTextView,
            membershipTextView, descriptionTextView, timelineTextView, timeTextView, distanceTextView,cancellation_term,
            numOfReviews1, numOfReviews2,topReview,shareButton;
    private SimpleRatingBar ratingBarAbove, ratingBarBelow;
    private Button fullReviewButton,cancelJobBtn;
    private CircleImageView userImageView;
    private GoogleMap googleMap;
    private String serverKey = "AIzaSyBoIz2RNdPcgXzXoKFHil8vuOWBzkNnS6s";
    private LatLng camera;
    private LatLng origin;
    private LatLng destination;
    private String[] colors = {"#7fff7272", "#7f31c7c5", "#7fff8a00"};
    private RadioGroup radioGroup;
    private SupportMapFragment mapFragment;
    private SliderLayout mDemoSlider;
    private Button chatButton, nextStepButton;
    private UserRating rating;
    private Toolbar toolbar;
    private Long jobId,userId;
    private RelativeLayout bottomLayout;
    private LinearLayout profileLayout,nextBtnLayout,cancellation_term_layout;
    JobWithUser job;
    String cancellationHour;
    Company companyData;
    Company companyUsers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.single_job_activity);
        categoryTextView = (TextView)findViewById(R.id.category_name);
        titleTextView = (TextView)findViewById(R.id.title);
        budgetTextView = (TextView)findViewById(R.id.job_budget);
        nameTextView = (TextView)findViewById(R.id.user_name);
        membershipTextView = (TextView)findViewById(R.id.membership);
        descriptionTextView = (TextView)findViewById(R.id.description);
        timelineTextView = (TextView)findViewById(R.id.timeline);
        timeTextView = (TextView)findViewById(R.id.distance_time);
        distanceTextView = (TextView)findViewById(R.id.distance);
        radioGroup = (RadioGroup)findViewById(R.id.rbg1);
        ratingBarAbove = (SimpleRatingBar)findViewById(R.id.rating1);
        ratingBarBelow = (SimpleRatingBar)findViewById(R.id.rating2);
        userImageView = (CircleImageView)findViewById(R.id.user_image_view);
        mDemoSlider = (SliderLayout)findViewById(R.id.slider);
        numOfReviews1 = (TextView)findViewById(R.id.num_of_reviews1);
        numOfReviews2 = (TextView)findViewById(R.id.num_of_reviews2);
        chatButton = (Button)findViewById(R.id.chat_button);
        nextStepButton = (Button)findViewById(R.id.next_step_button);
        shareButton = (TextView)findViewById(R.id.share_button);
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        topReview = (TextView)findViewById(R.id.top_review);
        fullReviewButton = (Button)findViewById(R.id.full_review_button);
        bottomLayout=(RelativeLayout)findViewById(R.id.bottom_layout);

        cancellation_term=(TextView)findViewById(R.id.mode);
        cancellation_term_layout=(LinearLayout)findViewById(R.id.cancellation_term_layout);
        profileLayout=(LinearLayout)findViewById(R.id.profile_layout);
        cancelJobBtn=(Button)findViewById(R.id.cancel_job);
        nextBtnLayout=(LinearLayout)findViewById(R.id.next_btn);

        // fetch the job id
        Bundle extras =getIntent().getExtras();
        if(extras != null) {
            jobId = extras.getLong("jobId");
            getJobInfo(jobId);
            Log.d(TAG, "on create not  null");
        } else
            Log.d(TAG, "on create null");

        //To Hide n Display Cancel Job Button
        User user = SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readUserPref();
        if(user.getId().equals(extras.getLong("userId"))){
            if(extras.getString("status").equals("Review")) {
               bottomLayout.setVisibility(View.GONE);
            }
            else{
                checkCompanyMember(user.getId());
                cancelJobBtn.setVisibility(View.VISIBLE);
            }
        }
        else{
            nextBtnLayout.setVisibility(View.VISIBLE);
        }
        nextStepButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction ft=getSupportFragmentManager().beginTransaction();
                ft.addToBackStack(null);
                OfferHelp dialogFrag=OfferHelp.newInstance();
                Bundle bundle= new Bundle();
                if(companyData!=null&&companyUsers==null) {
                    Log.e("Owner","Owner");
                    bundle.putLong("companyId",companyData.getId());
                    bundle.putString("user","Owner");
                }
                else if(companyUsers!=null&&companyData==null){
                    bundle.putLong("companyId",companyUsers.getId());
                    if(companyUsers.getUserRole().equals("3")) {
                        Log.e("Staff","Staff");
                        bundle.putString("user", "Staff");
                    }
                    else if(companyUsers.getUserRole().equals("2")){
                        Log.e("Coordinator","Coordinator");
                        bundle.putString("user","Coodinator");
                    }
                }
                else{
                    Log.e("Not Registered","Not Registered");
                    bundle.putString("user","Not Registered");
                }
                bundle.putParcelable("job_info",job.getJob());
                dialogFrag.setArguments(bundle);
                dialogFrag.show(ft,"offer_help");
               /* Intent intent = new Intent(getActivity(), OfferHelpByIndividual.class);
                intent.putExtra("job_info",job.getJob());
                startActivity(intent);*/
                Log.e(TAG,"Data"+titleTextView.getText().toString()+" "+job.getJob().getTimeline()+" "+job.getJob().getPostingTime());
            }
        });

        fullReviewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //open Location Fragemnt
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                // For a little polish, specify a transition animation
                // To make it fullscreen, use the 'content' root view as the container
                // for the fragment, which is always the root view for the activity

                //check to avoid adding fragments multiple times
                Bundle bundle = new Bundle();
                //   bundle.putLong("userId", );
                //    ratingDialogFragment.setArguments();
                DialogFragment ratingDialogFragment = RatingDialogFragment.newInstance(rating);
                ratingDialogFragment.show(transaction, "rating");
                /*ft.replace(R.id.comments_fragment, fragment);
                ft.commit();*/
            }
        });

        //Cancellation term screen
        cancellation_term_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CancellationTermsFragment fragment= new CancellationTermsFragment();
                Bundle bundle= new Bundle();
                bundle.putString("pre_cancellation_hour",job.getJob().getPreCancellationHours());
                fragment.setArguments(bundle);
                FragmentTransaction transaction =getSupportFragmentManager().beginTransaction();

                // for the fragment, which is always the root view for the activity
                transaction.add(android.R.id.content, fragment,"cancellation_term")
                        .addToBackStack(null).commit();
            }
        });
        // initialize map
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);

        // add listener for shwoing the path
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // This will get the radiobutton that has changed in its check state
                RadioButton checkedRadioButton = (RadioButton) group.findViewById(checkedId);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                if (isChecked) {
                    // Changes the textview's text to "Checked: example radiobutton text"

                    if (checkedId == (R.id.walk)) {
                        requestDirection("walk");
                    }

                    if (checkedId == (R.id.car)) {
                        requestDirection("car");
                    }
                }
            }
        });

        final NestedScrollView mainScrollView = (NestedScrollView)findViewById(R.id.nested_scroll_view);
        ImageView transparentImageView = (ImageView)findViewById(R.id.transparent_image);

        transparentImageView.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        mainScrollView.requestDisallowInterceptTouchEvent(true);
                        // Disable touch on transparent view
                        return false;

                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        mainScrollView.requestDisallowInterceptTouchEvent(false);
                        return true;

                    case MotionEvent.ACTION_MOVE:
                        mainScrollView.requestDisallowInterceptTouchEvent(true);
                        return false;

                    default:
                        return true;
                }
            }
        });

        chatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Intent intent = new Intent(JobDetailFragment.this, ChatActivity.class);
                //  startActivity(intent);
            }
        });

        shareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, "outofoptions://jobdetail?jobId="+jobId);
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
            }
        });

        cancelJobBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showCancellationDialog();
            }
        });
        profileLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               User user= SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readUserPref();
                if(!user.getId().equals(userId)){
                    Fragment fragment= new ProfileScreen();
                    Bundle bundle = new Bundle();
                    bundle.putLong("userId", userId);
                    bundle.putString("view","Other User Profile");
                    fragment.setArguments(bundle);
                    FragmentTransaction ft= getSupportFragmentManager().beginTransaction();
                    // For a little polish, specify a transition animation
                    ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
                    // To make it fullscreen, use the 'content' root view as the container
                    // for the fragment, which is always the root view for the activity
                    ft.add(android.R.id.content, fragment,"user_details")
                            .addToBackStack(null).commit();
                }
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap map) {
        googleMap = map;
        //camera = getDestination();
        //googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(camera, 14));
        origin = new LatLng((Double) SharedPrefsHelper.getInstance(SharedPrefsHelper.FILTER_OPTIONS_PREF).
                readSearchRequestByKey("latitude"), (Double) SharedPrefsHelper.getInstance(SharedPrefsHelper.FILTER_OPTIONS_PREF).
                readSearchRequestByKey("longitude"));

        //to disable map zooming from world map.
        map.moveCamera(CameraUpdateFactory.newLatLng(origin));
        map.animateCamera(CameraUpdateFactory.zoomTo(10));

        destination = getDestination();
        Log.e(TAG, "origin " + origin.latitude + " " + origin.longitude);
        Log.e(TAG, "destination " + destination.latitude + " " + destination.longitude);
        requestDirection("car");
    }

    public void getJobInfo(final Long jobId) {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<JobWithUser> call = apiService.findJobById(jobId);
        call.enqueue(new Callback<JobWithUser>() {
            @Override
            public void onResponse(Call<JobWithUser> call, Response<JobWithUser> response) {
                job = response.body();
                if (job != null) {
                    categoryTextView.setText(job.getJob().getCategory());
                    titleTextView.setText(job.getJob().getTitle());
                    budgetTextView.setText(job.getJob().getBudget() + " SEK");
                    nameTextView.setText(job.getUser().getFirstName() + " " +
                            job.getUser().getLastName());
                    membershipTextView.setText("Member since " + TimeConverter.longMillisecondsToDate(
                            job.getUser().getCreatedAt()));
                    descriptionTextView.setText(job.getJob().getDescription());
                    timelineTextView.setText("expired by " + DateTimeConverter.longMillisecondsToDate
                            (job.getJob().getTimeline()));
                    userId=job.getUser().getId();
                    setDestination(new LatLng(job.getJob().getLat(), job.getJob().getLng()));

                    //set Cancellation term text
                    if (job.getJob().getPreCancellationHours().equals("4")) {
                        cancellationHour = getString(R.string.cancellation_flexible_text);
                    } else if (job.getJob().getPreCancellationHours().equals("12")) {
                        cancellationHour = getString(R.string.cancellation_moderate_text);
                    } else if(job.getJob().getPreCancellationHours().equals("24")){
                        cancellationHour = getString(R.string.cancellation_strict_text);
                    }
                    Typeface font2 = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.ttf");
                    SpannableStringBuilder cancellationTerm = new SpannableStringBuilder(cancellationHour);
                    cancellationTerm.setSpan (new CustomTypefaceSpan("", font2), 0, cancellationTerm.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
                    cancellation_term.setText(cancellationTerm);

                    if (job.getUser().getImagePath() != null)
                        Glide.with(SingleJobActivity.this).load(S3Utils.getDownloadUserImageURL(
                                job.getUser().getId(),
                                job.getUser().getImagePath())).
                                placeholder(R.drawable.user_image).into(userImageView);

                    mapFragment.getMapAsync(SingleJobActivity.this);

                    getUserRating(job.getUser().getId());
                    if (job.getJob().getImages().size() > 0) {
                        mDemoSlider.setVisibility(View.VISIBLE);
                        mDemoSlider.setIndicatorVisibility(PagerIndicator.IndicatorVisibility.Visible);
                    }

                    for (Job.Images name : job.getJob().getImages()) {
                        DefaultSliderView textSliderView = new DefaultSliderView(getApplicationContext());
                        // initialize a SliderLayout
                        textSliderView

                                // .description(name)
                                .image(S3Utils.getDownloadJobImageURL(job.getJob().getId(), name.getName()))
                                .setScaleType(BaseSliderView.ScaleType.CenterCrop)

                                .setOnSliderClickListener(SingleJobActivity.this);

                        //add your extra information
                        // textSliderView.bundle(new Bundle());
                        //  textSliderView.getBundle()
                        //           .putString("extra", name);

                        mDemoSlider.setCustomIndicator((PagerIndicator)findViewById(R.id.custom_indicator));
                        mDemoSlider.stopAutoCycle();
                        mDemoSlider.addSlider(textSliderView);

                    }
                    mDemoSlider.setCustomIndicator((PagerIndicator)findViewById(R.id.custom_indicator));
                    Log.d(TAG, " not null with id" + jobId);
                } else

                    Log.d(TAG, " null with id" + jobId);
            }

            @Override
            public void onFailure(Call<JobWithUser> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
            }
        });
    }


    public void requestDirection(String mode) {
        //Snackbar.make(radioGroup, "Direction Requesting...", Snackbar.LENGTH_SHORT).show();

        if (mode.equals("walk")) {
            GoogleDirection.withServerKey(serverKey)
                    .from(origin)
                    .to(destination)
                    .transportMode(TransportMode.WALKING)
                    .alternativeRoute(true)
                    .execute(this);
        }

        if (mode.equals("car")) {
            GoogleDirection.withServerKey(serverKey)
                    .from(origin)
                    .to(destination)
                    .transportMode(TransportMode.DRIVING)
                    .alternativeRoute(true)
                    .execute(this);
        }
    }

    @Override
    public void onDirectionSuccess(Direction direction, String rawBody) {
        //  Snackbar.make(radioGroup, "Success with status : " + direction.getStatus(), Snackbar.LENGTH_SHORT).show();
       Log.e("OnDirectionSuccess","OnSuccess"+origin+"  "+destination);
        try {
           if (direction.isOK()) {
               googleMap.addMarker(
                       new MarkerOptions().position(origin)
                               .icon(BitmapDescriptorFactory.fromResource(R.drawable.current_location)));
               googleMap.addMarker(new MarkerOptions().position(destination)
                       .icon(BitmapDescriptorFactory.fromResource(R.drawable.current_location)));
               for (int i = 0; i < direction.getRouteList().size(); i++) {
                   Route route = direction.getRouteList().get(i);
                   String color = colors[i % colors.length];
                   ArrayList<LatLng> directionPositionList = route.getLegList().get(0).getDirectionPoint();
                   googleMap.addPolyline(DirectionConverter.createPolyline(getApplicationContext(), directionPositionList, 5, Color.parseColor(color)));
               }

               Log.e(TAG, rawBody);
               // radioGroup.setVisibility(View.GONE);

               try {
                   JSONObject jsonObject = new JSONObject(rawBody);
                   // routesArray contains ALL routes
                   JSONArray routesArray = jsonObject.getJSONArray("routes");
                   // Grab the first route
                   JSONObject route = routesArray.getJSONObject(0);
                   // Take all legs from the route
                   JSONArray legs = route.getJSONArray("legs");
                   // Grab first leg
                   JSONObject leg = legs.getJSONObject(0);

                   JSONObject durationObject = leg.getJSONObject("duration");
                   String duration = durationObject.getString("text");

                   JSONObject distanceObject = leg.getJSONObject("distance");
                   String distance = distanceObject.getString("text");

                   //do anything else
                   distanceTextView.setText(distance);
                   timeTextView.setText("Travel Time " + duration);
               } catch (JSONException e) {
                   Log.e(TAG, e.toString());
               }
           }
       }
       catch(NullPointerException ex){
           ex.printStackTrace();
       }
    }

    @Override
    public void onDirectionFailure(Throwable t) {
        Snackbar.make(radioGroup, t.getMessage(), Snackbar.LENGTH_SHORT).show();
    }

    public LatLng getDestination() {
        return destination;
    }

    public void setDestination(LatLng destination) {
        this.destination = destination;
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }


    public void getUserRating(final Long userId) {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<UserRating> call = apiService.userRating(userId);
        call.enqueue(new Callback<UserRating>() {
            @Override
            public void onResponse(Call<UserRating> call, Response<UserRating> response) {
                rating = response.body();
                Log.e("RAting",""+response.body());
                ratingBarAbove.setRating(rating.getAverage_review());
                ratingBarBelow.setRating(rating.getAverage_review());

                numOfReviews1.setText("(" + (rating.getBuyer_reviews().size() + rating.getSeller_reviews().size()) + ")");
                numOfReviews2.setText("(" + (rating.getBuyer_reviews().size() + rating.getSeller_reviews().size()) + ")");

                if(rating.getBuyer_reviews().size()>0){
                    topReview.setText(rating.getBuyer_reviews().get(0).getReview().getDescriptin());
                    fullReviewButton.setVisibility(View.VISIBLE);
                }
                else if(rating.getSeller_reviews().size()>0) {
                    topReview.setText(rating.getSeller_reviews().get(0).getReview().getDescriptin());
                    fullReviewButton.setVisibility(View.VISIBLE);
                }
                else {
                    topReview.setText("No Reviews are available");
                    fullReviewButton.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<UserRating> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
            }
        });
    }
    private void showCancellationDialog() {
        android.app.FragmentTransaction ft = getFragmentManager().beginTransaction();
        android.app.Fragment prev = getFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        // Create and show the dialog.
        android.app.DialogFragment dialogFragment = JobCancellationDialog.newInstance();
       /* Job job = new Job();
        job.setJobId(jobId);
        job.setTime(System.currentTimeMillis());*/
        Bundle bundle= new Bundle();
       // bundle.putParcelable("job_info",job);
        bundle.putString("view","Job_Detail");
        dialogFragment.setArguments(bundle);
        dialogFragment.show(ft,"dialog");
    }

    private void checkCompanyMember(Long userId) {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<User> call = apiService.getUserProfile(userId);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                Log.d(TAG, "Response" + response.body());
                companyData = response.body().getUser().getCompany();
                companyUsers = response.body().getUser().getCompanyUser();
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Log.e(TAG, "Failure" + t.toString());
            }
        });
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
