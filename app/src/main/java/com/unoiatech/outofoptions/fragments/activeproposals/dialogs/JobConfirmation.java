package com.unoiatech.outofoptions.fragments.activeproposals.dialogs;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.api.ApiClient;
import com.unoiatech.outofoptions.api.ApiInterface;
import com.unoiatech.outofoptions.model.RequestProposalData;
import com.unoiatech.outofoptions.util.DateTimeConverter;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Unoiatech on 5/3/2017.
 */

public class JobConfirmation extends Activity implements View.OnClickListener{
    private static String TAG="JobConfirmation";
    Long proposalId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.job_confirmation_dailog_layout);
        TextView staticText=(TextView)findViewById(R.id.static_text);
        Button accept=(Button)findViewById(R.id.accept_btn);
        Button decline=(Button)findViewById(R.id.decline_btn);

        accept.setOnClickListener(this);
        decline.setOnClickListener(this);

        /******GetIntent Data***********/
        Bundle bundle= getIntent().getExtras();
        proposalId=bundle.getLong("proposal_id");
        staticText.setText(getResources().getString(R.string.message)+" '"+bundle.getString("job_title")+" '."+getResources().getString(R.string.message1_text));
        this.setFinishOnTouchOutside(false);
    }

    @Override
    public void onBackPressed() {
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.accept_btn:
                markProposalAsConfirmed(proposalId,"yes");
                finish();
              //  overridePendingTransition(R.anim.slide_in_down, R.anim.slide_out_down);
                break;

            case R.id.decline_btn:
                markProposalAsConfirmed(proposalId,"no");
                finish();
             //   overridePendingTransition(R.anim.slide_in_down, R.anim.slide_out_down);
                break;
        }
    }

    private void markProposalAsConfirmed(Long proposalId, String response) {
        RequestProposalData proposal= new RequestProposalData();
        proposal.setTime(DateTimeConverter.getTime());
        proposal.setResponse(response);
        Log.d(TAG,"Data"+"  "+ DateTimeConverter.getTime()+"  "+response);
        ApiInterface apiService= ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call= apiService.markProposalConfirmed(proposalId,proposal);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d(TAG,"Response"+response.body());
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG,"OnFailure"+t.toString());
            }
        });
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
