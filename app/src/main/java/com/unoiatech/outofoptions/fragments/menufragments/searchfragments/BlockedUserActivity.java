package com.unoiatech.outofoptions.fragments.menufragments.searchfragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.api.ApiClient;
import com.unoiatech.outofoptions.api.ApiInterface;
import com.unoiatech.outofoptions.model.BlockUser;
import com.unoiatech.outofoptions.model.User;
import com.unoiatech.outofoptions.util.DateTimeConverter;
import com.unoiatech.outofoptions.util.S3Utils;
import com.unoiatech.outofoptions.util.SharedPrefsHelper;
import com.unoiatech.outofoptions.api.URL;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Unoiatech on 6/13/2017.
 */

public class BlockedUserActivity extends AppCompatActivity {
    MyAdapter adapter;
    private static String TAG="BockedUserActivity";
    private ArrayList<User> blockedUsersList;
    LinearLayoutManager mLayoutManager;
    RecyclerView mRecyclerView;
    User user;
    TextView staticText;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.blocked_user);

        TextView title= (TextView)findViewById(R.id.title);
        staticText=(TextView)findViewById(R.id.static_text);
        title.setText(getString(R.string.block_user));
        mRecyclerView=(RecyclerView)findViewById(R.id.blocked_user_list);

        //Set RecyclerView
        setmRecyclerView();

        user=SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readUserPref();
        getBlockedUser(user.getId());
    }

    private void getBlockedUser(Long id) {
        blockedUsersList.clear();
        ApiInterface apiServices= ApiClient.getClient().create(ApiInterface.class);
        Call<BlockUser> call= apiServices.getBlockedUsers(id);
        call.enqueue(new Callback<BlockUser>() {
            @Override
            public void onResponse(Call<BlockUser> call, Response<BlockUser> response) {
                Log.d(TAG, "REsponse" + response.body());
                if (response != null) {
                    if(response.body().getBlockedUsers().size()>0){
                    blockedUsersList.addAll(response.body().getBlockedUsers());

                    // notify adapter about the list update
                    adapter.notifyDataSetChanged();}
                    else {
                        staticText.setVisibility(View.VISIBLE);
                        //set Text on other terms
                        Spannable wordtoSpan=new SpannableString(getString(R.string.static_text_on_blocked_screen));
                        wordtoSpan.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.app_color)), 0, 5, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                        staticText.setText(wordtoSpan);
                    }
                } else
                    Snackbar.make(getWindow().getDecorView(), "Server error", Snackbar.LENGTH_SHORT);
                Log.e(TAG,"Size"+blockedUsersList.size());
            }

            @Override
            public void onFailure(Call<BlockUser> call, Throwable t) {
                Snackbar.make(getWindow().getDecorView(),"Server error",Snackbar.LENGTH_SHORT);
                Log.e(TAG,"failure"+t.toString());
            }
        });
    }

    void setmRecyclerView(){
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        // mRecyclerView.setHasFixedSize(true);
        blockedUsersList = new ArrayList<>();
        adapter = new MyAdapter(this,blockedUsersList);
        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(BlockedUserActivity.this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        // specify an adapter
        mRecyclerView.setAdapter(adapter);
    }

    private class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder>{
        private ArrayList<User> blocked_User;
        Activity activity;
        public MyAdapter(BlockedUserActivity blockedUserActivity, ArrayList<User> blockedUsersList) {
            this.activity=blockedUserActivity;
            this.blocked_User=blockedUsersList;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.blocked_user_item, parent, false);
            ViewHolder vh = new ViewHolder(v);
            return vh;
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            holder.name.setText(blocked_User.get(position).getFirstName()+" "+blocked_User.get(position).getLastName());
            String imagePath=blocked_User.get(position).getImagePath();
            try {
                if (imagePath.contains("\\")) {
                    String lastIndex = imagePath.replace("\\", " ");
                    String[] Stamp = lastIndex.split("\\s");
                    String timeStamp = Stamp[3];
                    String userImageUrl = URL.BASE_URL + "/users/download/" + imagePath + "/image/" + timeStamp;
                    Glide.with(activity).load(userImageUrl).into(holder.profileImage);
                } else {
                    Glide.with(activity).load(S3Utils.getDownloadUserImageURL(blocked_User.get(position).getId(), imagePath)).into(holder.profileImage);
                }
        }
            catch (NullPointerException ex)
            {
                ex.printStackTrace();
            }

            holder.unblock_text_view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    unblockUser(blocked_User.get(position).getId());
                }
            });
        }

        @Override
        public int getItemCount() {
            return blocked_User.size();
        }

        // Provide a reference to the views for each data item
        // Complex data items may need more than one view per item, and
        // you provide access to all the views for a data item in a view holder
        public  class ViewHolder extends RecyclerView.ViewHolder {
            public TextView unblock_text_view,name;
            CircleImageView profileImage;

            public ViewHolder(View v) {
                super(v);
                name = (TextView) v.findViewById(R.id.name);
                unblock_text_view = (TextView) v.findViewById(R.id.unblock);
                profileImage = (CircleImageView) v.findViewById(R.id.profile_image);
            }
        }
    }

    private void unblockUser(Long id){
        BlockUser blockUser= new BlockUser();
        blockUser.setBlockedUserId(id);
        blockUser.setCreatedId(DateTimeConverter.getTime());
        blockUser.setUserId(user.getId());
        ApiInterface apiService=ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call= apiService.unBlockUser(blockUser);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d(TAG,"Response"+response.body());
                getBlockedUser(user.getId());
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG,"Failure"+t.toString());
            }
        });
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
