package com.unoiatech.outofoptions.fragments.offerhelp;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.fragments.activejobs.dialogs.AcceptProposalDialog;

/**
 * Created by Unoiatech on 9/22/2017.
 */

public class NumberPickerDialog extends DialogFragment implements View.OnClickListener {
    Dialog dialog;
    NumberPicker picker;
    int oldvalue=0;
    final String[] selectedValues = new String[1];

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.select_timeline,container,false);

        TextView cancel=(TextView)view.findViewById(R.id.cancel);
        picker=(NumberPicker)view.findViewById(R.id.number_picker);
        TextView done=(TextView)view.findViewById(R.id.done);

        // Customise Number Picker Divider
        changeDividerColor(picker,getResources().getColor(R.color.app_color));

        //Initializing a new string array with elements
       // final String[] values= list.toArray(new String[list.size()]);

        final  String[] values={"1","2","3","4","5","6","7"};
        picker.setMinValue(0); //from array first value
        //Specify the maximum value/number of NumberPicker
        picker.setMaxValue(values.length-1); //to array last value

        picker.setValue(picker.getValue());
        //Specify the NumberPicker data source as array elements
        picker.setDisplayedValues(values);

        //Gets whether the selector wheel wraps when reaching the min/max value.
        picker.setWrapSelectorWheel(true);

        //Set Onclicklistener
        picker.setOnClickListener(this);
        done.setOnClickListener(this);

        picker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int oldval, int newVal) {
                selectedValues[0] =values[newVal];
                oldvalue=oldval;
                Log.e("Selected value : "," "+selectedValues+"  "+String.valueOf(oldval)+ " "+ newVal);
            }
        });
        return view;
    }

    private void changeDividerColor(NumberPicker picker, int color) {
        java.lang.reflect.Field[] pickerFields = NumberPicker.class.getDeclaredFields();
        for (java.lang.reflect.Field pf : pickerFields) {
            if (pf.getName().equals("mSelectionDivider")) {
                pf.setAccessible(true);
                try {
                    ColorDrawable colorDrawable = new ColorDrawable(color);
                    pf.set(picker, colorDrawable);
                }
                catch (IllegalArgumentException e) {
                    e.printStackTrace();
                }
                catch (Resources.NotFoundException e) {
                    e.printStackTrace();
                }
                catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    public static DialogFragment newInstance() {
        NumberPickerDialog dialog= new NumberPickerDialog();
        return dialog;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cancel:
                dialog.dismiss();
                break;
            case R.id.done:
                Log.e("Onclick","Onclick"+selectedValues[0]+" "+picker.getValue());
               /* if(selectedValues[0].equals("-")) {
                    completionTime.setText("");
                    Snackbar.make(getWindow().getDecorView(),"Please choose valid time to complete Job",Snackbar.LENGTH_SHORT).show();
                }else {
                    completionTime.setText(selectedValues[0]);
                }*/
                // setStartingEndingTime();
                dialog.dismiss();
        }
    }
}
