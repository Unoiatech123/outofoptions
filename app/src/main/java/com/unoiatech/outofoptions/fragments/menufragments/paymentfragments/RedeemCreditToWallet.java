package com.unoiatech.outofoptions.fragments.menufragments.paymentfragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.common.api.Api;
import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.api.ApiClient;
import com.unoiatech.outofoptions.api.ApiInterface;
import com.unoiatech.outofoptions.model.Payment;
import com.unoiatech.outofoptions.model.User;
import com.unoiatech.outofoptions.util.SharedPrefsHelper;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Unoiatech on 10/14/2017.
 */

public class RedeemCreditToWallet extends Activity implements View.OnClickListener {
    private int totalCredits;
    private int restCredits;
    private TextView redeemText,creditText;
    private static final String TAG="RedeemCreditToWallet";
    private Button redeemBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.e(TAG,"OnCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.redeem_credit_to_wallet);

        //Action Bar Components
        TextView mTitle=(TextView)findViewById(R.id.title);
        mTitle.setText(getString(R.string.redeem_credit_title));

        creditText=(TextView)findViewById(R.id.credits);
        redeemText=(TextView)findViewById(R.id.redeem_amount);
        redeemBtn=(Button)findViewById(R.id.redeem_btn);

        //Get data from Intent
        creditText.setText(getIntent().getStringExtra("credits")+" SEK");
        getRedeemAmount();
    }

    private void getRedeemAmount() {
        int redeemableAmount = 0;
        totalCredits=Integer.parseInt(getIntent().getStringExtra("credits"));
        //totalCredits=220;
        restCredits=totalCredits%100;
        if(totalCredits>=100) {
            redeemableAmount = totalCredits/100*40;
            redeemBtn.setBackgroundResource(R.drawable.ripple_effect);
            redeemBtn.setOnClickListener(RedeemCreditToWallet.this);
            redeemText.setText(redeemableAmount-3+" SEK");
        }
        else{
            redeemText.setText("0 SEK");
            redeemBtn.setBackgroundResource(R.drawable.grey_ripple_effect);
            redeemBtn.setOnClickListener(null);
        }
        Log.e("GetTotalCredit","getTotalCredit"+"  "+restCredits+"  "+redeemableAmount);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
   }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.redeem_btn:
                redeemCreditToWallet();
                break;
        }
    }

    private void redeemCreditToWallet() {
        User user= SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readUserPref();
        Payment payment= new Payment();
        payment.setAmount(totalCredits-restCredits);
        payment.setUserId(user.getId());
        Log.e("Redeem_Data","Redeem_Data"+"  "+payment.getAmount()+"  "+payment.getUserId());
        ApiInterface apiService= ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call= apiService.redeemCreditToWallet(payment);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.e("Response","Response"+response.body()+"  "+response.isSuccessful());
                if(response.isSuccessful()){
                    creditText.setText(restCredits+" SEK");
                    redeemText.setText("0 SEK");
                    redeemBtn.setBackgroundResource(android.R.color.darker_gray);
                    redeemBtn.setOnClickListener(null);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("Failure","Failure"+t.toString());
            }
        });
    }
}
