package com.unoiatech.outofoptions.fragments.menufragments.notificationscreen;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.api.ApiClient;
import com.unoiatech.outofoptions.api.ApiInterface;
import com.unoiatech.outofoptions.model.User;
import com.unoiatech.outofoptions.model.NotificationStatus;
import com.unoiatech.outofoptions.util.ApiError;
import com.unoiatech.outofoptions.util.BankId.ErrorUtils;
import com.unoiatech.outofoptions.util.SharedPrefsHelper;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Unoiatech on 5/8/2017.
 */
public class NotificationPrefScreen extends Activity implements View.OnClickListener{
    CheckBox receiveProposal,markJobCompleted,receiveResponse,jobCancel,jobCompleted;
    private static String TAG="NotificationPrefScreen";
    boolean isStateChanged;
    long userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notification_preferences);

        receiveProposal=(CheckBox)findViewById(R.id.proposal_recieved_check);
        receiveProposal.setOnClickListener(NotificationPrefScreen.this);

        markJobCompleted=(CheckBox)findViewById(R.id.mark_job_completed);
        markJobCompleted.setOnClickListener(NotificationPrefScreen.this);

        receiveResponse=(CheckBox)findViewById(R.id.receive_response);
        receiveResponse.setOnClickListener(this);

        jobCancel=(CheckBox)findViewById(R.id.job_cancel);
        jobCancel.setOnClickListener(this);

        jobCompleted=(CheckBox)findViewById(R.id.seller_job_completed);
        jobCompleted.setOnClickListener(this);

        User user= SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readUserPref();
        userId=user.getId();
        getNotificationStatus(userId);

        isStateChanged=false;

        /****ActionBarComponents*****/
        TextView mtitle = (TextView) findViewById(R.id.title);
        mtitle.setText(getResources().getString(R.string.notification_pref_text));
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void getNotificationStatus(long id) {
        ApiInterface apiService= ApiClient.getClient().create(ApiInterface.class);
        Call<User> call=apiService.getUserProfile(id);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                Log.e(TAG,"Response"+response.body()+ " "+response.message());
                if(response.isSuccessful()) {
                    NotificationStatus notificationStatus = response.body().getUser().getNotificationStatus();
                    setNotificationData(notificationStatus);
                }
                else{
                    ApiError error= ErrorUtils.parseError(response);
                    Snackbar.make(getWindow().getDecorView(),error.getMessage(),Snackbar.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Log.e(TAG,"Error"+t.toString());
            }
        });
    }

    private void setNotificationData(NotificationStatus notification) {
        if(notification.getDoesProposalReceived().equals("1")){
            receiveProposal.setChecked(true);
        }
        if(notification.getDoesJobCompleteResponse().equals("1")){
            markJobCompleted.setChecked(true);
        }
        if(notification.getDoesReceiveResponse().equals("1")){
            receiveResponse.setChecked(true);
        }

        if(notification.getDoesProposalCompleteResponse().equals("1")){
            jobCompleted.setChecked(true);
        }

        if(notification.getDoesJobCancelledResponse().equals("1")){
            jobCancel.setChecked(true);
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onClick(View v) {
        isStateChanged=true;
        switch (v.getId())
        {
            case R.id.proposal_recieved_check:
                if(receiveProposal.isChecked()) {
                    receiveProposal.setChecked(true);
                }else{
                    receiveProposal.setChecked(false);
                }
                break;
            case R.id.mark_job_completed:
                if(markJobCompleted.isChecked()) {
                    markJobCompleted.setChecked(true);
                }else {
                    markJobCompleted.setChecked(false);
                }
                break;
            case R.id.receive_response:
                if(receiveResponse.isChecked()) {
                    receiveResponse.setChecked(true);
                }else {
                    receiveResponse.setChecked(false);
                }
                break;
            case R.id.job_cancel:
                if(jobCancel.isChecked()) {
                    jobCancel.setChecked(true);
                }else {
                jobCancel.setChecked(false);
                }
                break;
            case R.id.seller_job_completed:
                if(jobCompleted.isChecked()) {
                    jobCompleted.setChecked(true);
                }else {
                    jobCompleted.setChecked(false);
                }
            break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(isStateChanged){
            getUpdatedData();
        }
    }

    private void getUpdatedData() {
        NotificationStatus notification= new NotificationStatus();
        if(receiveProposal.isChecked()){
            notification.setDoesProposalReceived("1");
        }
        else{
            notification.setDoesProposalReceived("0");
        }
        if(markJobCompleted.isChecked()) {
            notification.setDoesJobCompleteResponse("1");
        }
        else{
            notification.setDoesJobCompleteResponse("0");
        }
        if(receiveResponse.isChecked()){
            notification.setDoesReceiveResponse("1");
        }
        else{
            notification.setDoesReceiveResponse("0");
        }
        if(jobCancel.isChecked()){
            notification.setDoesJobCancelledResponse("1");
        }
        else{
            notification.setDoesJobCancelledResponse("0");
        }
        if(jobCompleted.isChecked()){
            notification.setDoesProposalCompleteResponse("1");
        }
        else{
            notification.setDoesProposalCompleteResponse("0");
        }
        updateNotificationStatus(userId,notification);
    }

    private void updateNotificationStatus(long userId, final NotificationStatus notification) {
        ApiInterface apiService=ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call=apiService.updateNotificationStatus(userId,notification);
        Log.i(TAG,"Data_to_update"+notification.getDoesJobCancelledResponse()+ " "+notification.getDoesReceiveResponse()+" "+notification.getDoesProposalCompleteResponse()+" "+notification.getDoesProposalReceived()+" "+notification.getDoesJobCompleteResponse()+" "+userId);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d(TAG,"UpdateNotification"+response.body()+"  "+response.message());
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG,"Error"+t.toString());
            }
        });
    }
}

