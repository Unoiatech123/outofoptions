package com.unoiatech.outofoptions.fragments.activeproposals;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.api.ApiClient;
import com.unoiatech.outofoptions.api.ApiInterface;
import com.unoiatech.outofoptions.fragments.activejobs.adapter.DataReceivedEvent;
import com.unoiatech.outofoptions.model.ActiveProposalResponse;
import com.unoiatech.outofoptions.model.Job;
import com.unoiatech.outofoptions.model.User;
import com.unoiatech.outofoptions.model.ActiveJobsResponse;
import com.unoiatech.outofoptions.util.SharedPrefsHelper;
import com.unoiatech.outofoptions.util.common.view.SlidingTabLayout;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by unoiatech on 10/10/2016.
 */
public class ProposalFragment extends Fragment {
    private static final String TAG="ProposalFragment";
    public static ViewPager mViewPager;
    SlidingTabLayout mSlidingTabLayout;
    ArrayList<ActiveProposalResponse> proposalList = new ArrayList<>();
    ActiveJobsResponse proposal_response;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.seller_job_demo,container,false);
        mViewPager = (ViewPager) view.findViewById(R.id.viewpager);
        mViewPager.setAdapter(new MyAdapter(getChildFragmentManager(),3));
        mSlidingTabLayout = (SlidingTabLayout) view.findViewById(R.id.sliding_tabs);
        mSlidingTabLayout.setCustomTabView(R.layout.custom_tabstrip_for_activejobs, R.id.custom_tabstrip_tv);
        mSlidingTabLayout.setDistributeEvenly(false);
        mSlidingTabLayout.setViewPager(mViewPager);
        return view;
    }
    public class MyAdapter extends FragmentStatePagerAdapter {
        int tabcount;
        public MyAdapter(FragmentManager fm, int childCount) {
            super(fm);
            this.tabcount=childCount;
        }

        @Override
        public Fragment getItem(int i){
            switch (i) {
                case 0:
                    MyProposalListFragment f=new MyProposalListFragment();
                    Bundle b = new Bundle();
                    b.putString("type", "confirmed");
                    f.setArguments(b);
                    return f;

                case 1:
                    MyProposalListFragment f1= new MyProposalListFragment();
                    Bundle b1= new Bundle();
                    b1.putString("type","pending");
                    f1.setArguments(b1);
                    return f1;

                case 2:
                    MyProposalListFragment f2=new MyProposalListFragment();
                    Bundle b2 = new Bundle();
                    b2.putString("type","completed");
                    f2.setArguments(b2);
                    return f2;
            }
            return null;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Active";

                case 1:
                    return "Proposals Sent";

                case 2:
                    return "Completed";
            }
            return super.getPageTitle(position);
        }

        @Override
        public int getCount() {
            return tabcount;
        }
    }
    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
        getMyJobs();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    public void getMyJobs() {
        proposalList.clear();
        User user = SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readUserPref();
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ArrayList<ActiveProposalResponse>> call = apiService.getProposal(user.getId());
        call.enqueue(new Callback<ArrayList<ActiveProposalResponse>>() {
            @Override
            public void onResponse(Call<ArrayList<ActiveProposalResponse>> call, Response<ArrayList<ActiveProposalResponse>> response) {
                Log.d(TAG, "Response" + " Size" + response.body().size()+response.body());
                if (response.isSuccessful()) {
                    proposalList.addAll(response.body());
                    EventBus.getDefault().post(new DataReceivedEvent("Hello EventBus!"));
                }
                else {
                    Log.e("TAG", "Error" + response.body());
                }
            }

            @Override
            public void onFailure(Call<ArrayList<ActiveProposalResponse>> call, Throwable t) {
                Log.d(TAG, "failure" + t.toString());
            }
        });
    }
    public  ArrayList<ActiveProposalResponse> getProposalData(){
        return proposalList;
    }
}
