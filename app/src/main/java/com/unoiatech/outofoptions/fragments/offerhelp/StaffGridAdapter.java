package com.unoiatech.outofoptions.fragments.offerhelp;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.unoiatech.outofoptions.model.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Unoiatech on 8/10/2017.
 */
public class StaffGridAdapter extends BaseAdapter {
    ChooseStaffMembers activity;
    ArrayList<User> data;
    ArrayList<User> sortedData;
    private LayoutInflater layoutInflater;
    Context context;
    List selectedPositions;
    String view;

    public StaffGridAdapter(ChooseStaffMembers chooseStaffMembers, ArrayList<User> companyMembers, String status) {
        activity=chooseStaffMembers;
        data=companyMembers;
        this.sortedData= new ArrayList<>();
        this.sortedData.addAll(data);
        context= activity.getApplicationContext();
        selectedPositions = new ArrayList<>();
        view=status;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        /*Holder holder = new Holder();
        View rowView;
        rowView = layoutInflater.inflate(R.layout.staff_grid_items, null);
        holder.userName = (TextView) rowView.findViewById(R.id.user_name);
        holder.userImage = (ImageView) rowView.findViewById(R.id.user_image);
        holder.userRole=(TextView)rowView.findViewById(R.id.user_role);
        holder.userName.setText(data.get(i).getFirstName()+" "+data.get(i).getLastName());

        //Set User Image
        setMemberImage(holder.userImage,data.get(i).getImagePath(),data.get(i).getId());

        try {
            if (data.get(i).getCompanyUser().getUserRole().equals("2")) {
                Log.e("User_Role","User_Role"+data.get(i).getCompanyUser().getUserRole());
                holder.userRole.setVisibility(View.VISIBLE);
            } else {
                holder.userRole.setVisibility(View.GONE);
            }
        }catch (NullPointerException ex){
            ex.printStackTrace();
        }
        return rowView;*/
        String userRole = null;
        try {
            if (data.get(position).getCompanyUser().getUserRole().equals("2")) {
                userRole="coordinator";
            }
        }catch (NullPointerException ex){
            ex.printStackTrace();
        }
        GridItemView customView = (convertView == null) ? new GridItemView(activity) : (GridItemView) convertView;
        if(view.equals("OfferHelp")) {
            customView.display(data.get(position).getFirstName(), data.get(position).getLastName(), data.get(position).getImagePath(), data.get(position).getId(), userRole, selectedPositions.contains(String.valueOf(data.get(position).getId())));
        }else{
            customView.displayStaffMembers(data.get(position).getFirstName(), data.get(position).getLastName(), data.get(position).getImagePath(), data.get(position).getAssignee());
        }
        return customView;
    }

    public void filter(String charSearch) {
        Log.e("Filter","Filter"+charSearch);
        data.clear();
        if(charSearch.length()==0){
            data.addAll(sortedData);
        }else{
            for (User user:sortedData){
                if(user.getFirstName().contains(charSearch)){
                    data.add(user);
                }
            }
        }
        notifyDataSetChanged();
    }
/*
    private void setMemberImage(ImageView memberImage, String imagePath, Long id) {
        if (imagePath != null) {
            if (imagePath.contains("\\")) {
                String lastIndex = imagePath.replace("\\", " ");
                String[] Stamp = lastIndex.split("\\s");
                String timeStamp = Stamp[3];
                String userImageUrl = URL.BASE_URL + "/users/download/" + id + "/image/" + timeStamp;
                Glide.with(activity).load(userImageUrl).into(memberImage);
            } else {
                Glide.with(activity).load(S3Utils.getDownloadUserImageURL(id, imagePath)).into(memberImage);
            }
        }
    }

    public class Holder{
        ImageView userImage;
        TextView userName,userRole;
    }*/
}
