package com.unoiatech.outofoptions.fragments.main;

/**
 * Created by Unoiatech on 10/27/2017.
 */

public interface OnRegisterViewListener {
    void hideFirstView();
    void hideSecondView();
    void hideThirdView();
    void accountTabSelected();
    void userInfoSelected();
    void verifiedIdSelected();
    void accountUnselected();
    void verifyIdsSelected();
}
