package com.unoiatech.outofoptions.fragments.menufragments.paymentfragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.api.ApiClient;
import com.unoiatech.outofoptions.api.ApiInterface;
import com.unoiatech.outofoptions.model.SwishCustomer;
import com.unoiatech.outofoptions.model.User;
import com.unoiatech.outofoptions.util.ApiError;
import com.unoiatech.outofoptions.util.BankId.ErrorUtils;
import com.unoiatech.outofoptions.util.ConnectionDetection;
import com.unoiatech.outofoptions.util.SharedPrefsHelper;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Unoiatech on 9/3/2017.
 */

public class ConnectToSwish extends Activity implements View.OnTouchListener {
    EditText phoneNo;
    TextView title,removeBtn,connectText;
    Button connectButton;
    private static String TAG="ConnectToSwish";
    User user;

    /*@Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.connect_swish_layout,container,false);
        return view;
    }
    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        return false;
    }*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.connect_swish_layout);

        //Action Bar Components
        title=(TextView)findViewById(R.id.title);
        removeBtn=(TextView)findViewById(R.id.edit);

        phoneNo = (EditText) findViewById(R.id.phone_no);
        connectText=(TextView)findViewById(R.id.already_connected);
        connectButton=(Button)findViewById(R.id.connect_btn);
        user= SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readUserPref();

        // Check Already Connected Or not
        Bundle bundle= getIntent().getExtras();
        Log.e(TAG,"Bundle"+bundle);
        if(bundle!=null){
            phoneNo.setText(bundle.getString("swish_number"));
            connectText.setVisibility(View.VISIBLE);
            removeBtn.setVisibility(View.VISIBLE);

            //SetText On Title and remove Button
            title.setText(getString(R.string.swish_account));
            removeBtn.setText(getString(R.string.remove_text));
            connectButton.setVisibility(View.GONE);
        }
        else{
            title.setText(getString(R.string.connect_swish));
            phoneNo.setText("");
            connectText.setVisibility(View.GONE);
            removeBtn.setVisibility(View.INVISIBLE);
            connectButton.setVisibility(View.VISIBLE);
        }
        connectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("onClick","OnClick"+phoneNo.getText().length());

                //dismiss keyboard
                InputMethodManager inputManager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow((null == getCurrentFocus()) ? null : getCurrentFocus().getWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS);

                if(ConnectionDetection.isInternetAvailable(getApplicationContext())){
                    if(phoneNo.getText().length()==0){
                        notifyUser(getString(R.string.swish_phn_text));
                    }
                    else if(phoneNo.getText().length()<9){
                        notifyUser(getString(R.string.swish_phn_text));
                    }
                    else{
                        methodToConnectWithSwish();
                    }
                }else{
                    Snackbar.make(getWindow().getDecorView(),getString(R.string.internet_connection_error),Snackbar.LENGTH_LONG).show();
                }
            }
        });

        removeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                disconnectSwishCustomer(user.getId());
            }
        });
       /* phoneNo.addTextChangedListener(new AutoAddTextWatcher(phoneNo,"-",3,6));
        phoneNo.setOnTouchListener(this);

        phoneNo.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId== EditorInfo.IME_ACTION_NEXT) {
                    checkEnteredData(phoneNo,getString(R.string.telephn_error_msg));
                }
                return false;
            }
        });
    }

    private void checkEnteredData(EditText edit_text, String message) {
        if(edit_text.equals(phoneNo)) {
            if (edit_text.getText().toString().length() < 13) {
                notifyUser(message);
                phoneNo.setFocusableInTouchMode(true);
                phoneNo.setFocusable(true);
            }
            else {
                phoneNo.setFocusableInTouchMode(false);
                phoneNo.setFocusable(false);
            }
        }
    }*/
    }

    private void disconnectSwishCustomer(Long id) {
        Log.e("Disconnect","Disconnect");
        ApiInterface apiService= ApiClient.getClient().create(ApiInterface.class);
        Call<Void> call=apiService.disconnectSwish(id);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.e(TAG,"Response"+response.isSuccessful());
                if(response.isSuccessful()){
                    finish();
                }
                else{
                    ApiError error= ErrorUtils.parseError(response);
                    notifyUser(error.getMessage());
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.e(TAG,"Failure"+" "+t.toString());
            }
        });
    }

    private void notifyUser(String message) {
        Snackbar.make(getWindow().getDecorView().getRootView(), message, Snackbar.LENGTH_LONG)
                .setAction(getString(R.string.max_ok_btn), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                    }
                }).setActionTextColor(getResources().getColor(R.color.app_color)).show();
    }

    private void methodToConnectWithSwish() {
        SwishCustomer swishCustomer= new SwishCustomer();
        swishCustomer.setUserId(user.getId());
        swishCustomer.setSwishNumber(phoneNo.getText().toString());
        ApiInterface apiService= ApiClient.getClient().create(ApiInterface.class);
        Call<Void> call= apiService.connectSwish(swishCustomer);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.d("Response","Response"+" "+response.body());
                if(response.isSuccessful()){
                    finish();
                    /*removeBtn.setVisibility(View.VISIBLE);
                    removeBtn.setText(getString(R.string.remove_text));
                    connectText.setVisibility(View.VISIBLE);
                    title.setText(getString(R.string.swish_account));
                    connectButton.setVisibility(View.INVISIBLE);*/
                }
                else{
                    ApiError error= ErrorUtils.parseError(response);
                    notifyUser(error.getMessage());
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.e("OnFailure","OnFailure"+"  "+t.toString());
            }
        });
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        return false;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
