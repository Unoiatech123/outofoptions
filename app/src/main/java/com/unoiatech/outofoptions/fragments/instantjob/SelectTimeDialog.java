package com.unoiatech.outofoptions.fragments.instantjob;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.util.SharedPrefsHelper;

import org.w3c.dom.Text;

import java.sql.Time;

import static com.unoiatech.outofoptions.R.id.no;
import static com.unoiatech.outofoptions.R.id.starting_time;

/**
 * Created by Unoiatech on 5/20/2017.
 */
public class SelectTimeDialog extends DialogFragment {
    Dialog dialog;
    private String start_time,end_time;
    TimePicker starting_time,ending_time;
    TextView done,title,start_time_text,end_time_text,closed;

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.custom_timepicker, null, false);
        initViews(view);

        /****Set TimePicker 24 hour format*******/
        starting_time.setIs24HourView(true);
        ending_time.setIs24HourView(true);

        starting_time.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
                start_time= String.format("%02d : %02d", hourOfDay,minute);
            }
        });

        ending_time.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
                end_time=String.format("%02d : %02d",hourOfDay,minute);
            }
        });

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                if(start_time!=null) {
                    i.putExtra("startingtime", start_time);
                }else{
                    notifyUser(getString(R.string.starting_time_validation));
                }
                if(end_time!=null){
                    i.putExtra("endingtime",end_time);
                }
                else{
                    notifyUser(getString(R.string.ending_time_validaion));
                }
                i.putExtra("index",getArguments().getInt("index"));
                getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, i);
                dismiss();
            }
        });
        closed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.putExtra("close","Closed");
                i.putExtra("index",getArguments().getInt("index"));
                getTargetFragment().onActivityResult(112, Activity.RESULT_OK, i);
                dismiss();
            }
        });

        title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent();
                intent.putExtra("decide_via_chat","Decide via chat");
                getTargetFragment().onActivityResult(111, Activity.RESULT_OK, intent);
                dismiss();
            }
        });
        return view;
    }

    private void notifyUser(String msg) {
        Snackbar.make(getView(),msg,Snackbar.LENGTH_SHORT).show();
    }

    private void initViews(View view) {
        starting_time = (TimePicker) view.findViewById(R.id.starting_time_picker);
        ending_time = (TimePicker) view.findViewById(R.id.end_time_picker);
        done = (TextView) view.findViewById(R.id.done_text);
        closed = (TextView) view.findViewById(R.id.closed_text);
        title=(TextView)view.findViewById(R.id.title);
        start_time_text=(TextView)view.findViewById(R.id.start_time);
        end_time_text=(TextView)view.findViewById(R.id.end_time);

        if(getArguments().getString("view").equals("shop")){
            title.setText(getString(R.string.set_business_hour));
            start_time_text.setText(getString(R.string.opening_time));
            end_time_text.setText(getString(R.string.closing_time));
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    public static DialogFragment newInstance() {
        SelectTimeDialog frag= new SelectTimeDialog();
        return frag;
    }
}
