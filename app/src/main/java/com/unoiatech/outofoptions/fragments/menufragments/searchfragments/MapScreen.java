package com.unoiatech.outofoptions.fragments.menufragments.searchfragments;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;
import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.activity.RegisterActivity;
import com.unoiatech.outofoptions.api.ApiClient;
import com.unoiatech.outofoptions.api.ApiInterface;
import com.unoiatech.outofoptions.fragments.menufragments.SearchJobsScreen;
import com.unoiatech.outofoptions.home.HomeScreenActivity;
import com.unoiatech.outofoptions.model.JobOnMap;
import com.unoiatech.outofoptions.model.SearchMapResponse;
import com.unoiatech.outofoptions.model.User;
import com.unoiatech.outofoptions.model.SearchJobsRequest;
import com.unoiatech.outofoptions.util.DateTimeConverter;
import com.unoiatech.outofoptions.util.S3Utils;
import com.unoiatech.outofoptions.util.SharedPrefsHelper;

import org.androidannotations.annotations.ServiceAction;

import java.text.ParseException;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Unoiatech on 5/19/2017.
 */

public class MapScreen extends Fragment implements OnMapReadyCallback, ClusterManager.OnClusterClickListener<JobOnMap>, ClusterManager.OnClusterItemClickListener<JobOnMap> {
    private String TAG = "MapScreen";
    GoogleMap mMap;
    MapView mapView;
    ArrayList<SearchMapResponse> job_data;
    ClusterManager<JobOnMap> mClusterManager;
    View view;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        Log.e("OnViewCreated", "OnViewCreated");
        super.onViewCreated(view, savedInstanceState);
        mapView = (MapView) view.findViewById(R.id.map);
        TextView filterButton=(TextView)view.findViewById(R.id.filter_button);

        mapView.onCreate(savedInstanceState);
        // needed to get the map to display immediately
        mapView.onResume();
        try {
            MapsInitializer.initialize(getActivity());
        } catch (Exception e) {
            e.printStackTrace();
        }
        mapView.getMapAsync(MapScreen.this);
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null) {
                parent.removeView(view);
            }
        }
        try {
            view = inflater.inflate(R.layout.map_fragment_layout, container, false);
        } catch (InflateException ex) {
            ex.printStackTrace();
        }
        return view;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        SearchJobsScreen parentFragment = (SearchJobsScreen) getActivity();
        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);
        this.mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(parentFragment.getLatitude(),parentFragment.getLongitude()), 9.5f));
        mClusterManager = new ClusterManager<JobOnMap>(getActivity(), mMap);
        mClusterManager.setRenderer(new JobRenderer());
        mMap.setOnCameraIdleListener(mClusterManager);
        mMap.setOnMarkerClickListener(mClusterManager);
        mMap.setOnInfoWindowClickListener(mClusterManager);
        mClusterManager.setOnClusterClickListener(this);
        mClusterManager.setOnClusterItemClickListener(this);
        mClusterManager.cluster();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        filterSearch();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mMap != null) {
            mMap.clear();
        }
    }

    @Override
    public boolean onClusterClick(Cluster<JobOnMap> cluster) {
        return false;
    }

    @Override
    public boolean onClusterItemClick(JobOnMap myJobs) {
        showDialog(myJobs.getBudget(), myJobs.getTimeline(), myJobs.getJobTitle(), myJobs.getBuyerId(), myJobs.getJobId(), myJobs.getCategory(), myJobs.getLat(), myJobs.getLng(), myJobs.getPostingTime(), myJobs.getReviews(), myJobs.getRating(), myJobs.getImagePath());
        return false;
    }

   public void filterSearch() {
       if (mMap != null) {
            mMap.clear();
       }
       job_data = new ArrayList<>();
       User user = SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readUserPref();
       Long userId = user.getId();
       //SearchTabFragment parentFragment = (SearchTabFragment) getParentFragment();
       SearchJobsScreen parentFragment=(SearchJobsScreen)getActivity();
       final SearchJobsRequest jobs = new SearchJobsRequest();
       jobs.setCategory(new ArrayList<String>());
       jobs.setMinBudget(0);
       jobs.setMaxBudget(50000);
       jobs.setRating(0.0);
       jobs.setDistance(1000.0);
       jobs.setLatitude(parentFragment.getLatitude());
       jobs.setLongitude(parentFragment.getLongitude());
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ArrayList<SearchMapResponse>> call = apiService.getJobsOnMap(userId, jobs);
        call.enqueue(new Callback<ArrayList<SearchMapResponse>>() {
            @Override
            public void onResponse(Call<ArrayList<SearchMapResponse>> call, Response<ArrayList<SearchMapResponse>> response) {
                Log.d(TAG, "MapResponse" + response.body()+response.body().size());
                SearchMapResponse mapdata;
                try{
                    for (int i = 0; i < response.body().size(); i++) {
                        mapdata = new SearchMapResponse();
                        mapdata.setRating(response.body().get(i).getAverage_review());
                        int user_reviews=response.body().get(i).getBuyer_reviews().size()+response.body().get(i).getSeller_reviews().size();
                        mapdata.setReviews(response.body().get(i).getBuyer_reviews().size()+response.body().get(i).getSeller_reviews().size());
                        mapdata.setJob(response.body().get(i).getJob());
                        mapdata.setUser(response.body().get(i).getUser());
                        job_data.add(mapdata);

                        Log.e(TAG,"Size"+mapdata.getReviews()+"  "+user_reviews);
                    }
                }
                catch (NullPointerException ex){
                    ex.printStackTrace();
                }
                addItems(job_data);
            }

            @Override
            public void onFailure(Call<ArrayList<SearchMapResponse>> call, Throwable t) {
                Log.e(TAG, "FAilure" + t.toString());
            }
        });
    }

    private void addItems(ArrayList<SearchMapResponse> job_data) {
        if (job_data.size() > 0) {
            JobOnMap offsetItem = null;
            for (SearchMapResponse item : job_data) {
                offsetItem = new JobOnMap(position(item.getJob().getLat(),item.getJob().getLng()), item.getJob().getCategory(), item.getJob().getTimeline(), item.getJob().getTitle(), item.getJob().getBudget(), item.getJob().getPostingTime(), item.getReviews(), item.getRating(), item.getUser().getImagePath(),item.getJob().getUserId(),item.getJob().getId());
                mClusterManager.addItem(offsetItem);
            }
        }
    }

    private LatLng position(Double lat, Double lng) {
        return new LatLng(lat, lng);
    }

    //Renderer Class To set Cluster Image and Categories imags on Zooming
    private class JobRenderer extends DefaultClusterRenderer<JobOnMap> {
        private IconGenerator mIconGenerator = new IconGenerator(getActivity());
        private IconGenerator mClusterIconGenerator = new IconGenerator(getActivity());
        private ImageView mImageView;
        int mDimension;

        public JobRenderer() {
            super(getActivity(), mMap, mClusterManager);
            LayoutInflater myInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View activityView = myInflater.inflate(R.layout.cluster_layout,null);
            mClusterIconGenerator.setContentView(activityView);

            mImageView = new ImageView(getActivity());
            mDimension = (int) getResources().getDimension(R.dimen.custom_profile_image);
            mImageView.setLayoutParams(new ViewGroup.LayoutParams(mDimension, mDimension));
            int padding = (int) getResources().getDimension(R.dimen.custom_profile_padding);
            mImageView.setPadding(padding, padding, padding, padding);
            mIconGenerator.setContentView(mImageView);
        }

        @Override
        protected void onBeforeClusterItemRendered(JobOnMap item, MarkerOptions markerOptions) {
            super.onBeforeClusterItemRendered(item, markerOptions);
            mImageView.setImageResource(setImageByCategory(item.getCategory()));

            /***to Remove White rectangualr Background of Category Images **/
            final Drawable clusterIcon =((SearchJobsScreen)getActivity()).getResources().getDrawable(android.R.color.transparent);
            clusterIcon.setColorFilter(getActivity().getResources().getColor(android.R.color.transparent), PorterDuff.Mode.SRC_ATOP);

            mIconGenerator.setBackground(clusterIcon);
            Bitmap icon = mIconGenerator.makeIcon();
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));
        }

        @Override
        protected void onBeforeClusterRendered(Cluster<JobOnMap> cluster, MarkerOptions markerOptions) {
            super.onBeforeClusterRendered(cluster, markerOptions);
           /*final Drawable clusterIcon = getActivity().getResources().getDrawable(R.drawable.white_background);
            clusterIcon.setColorFilter(getActivity().getResources().getColor(R.color.app_color), PorterDuff.Mode.SRC_ATOP);

            *//***to set green round image**//*
            mClusterIconGenerator.setBackground(clusterIcon);
            Bitmap icon = mClusterIconGenerator.makeIcon(String.valueOf(cluster.getSize()));
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));*/
        }

        @Override
        protected void onClusterItemRendered(JobOnMap clusterItem, Marker marker) {
            super.onClusterItemRendered(clusterItem, marker);
        }

        @Override
        protected boolean shouldRenderAsCluster(Cluster<JobOnMap> cluster) {
            return super.shouldRenderAsCluster(cluster);
        }
    }

    private int setImageByCategory(String category) {
        switch (category)
        {
            case "Home & Gardening":
                return R.drawable.house_and_gardning;

            case "Car & Vehicles":
                return R.drawable.car_and_vehicles;

            case "Health & Beauty":
                return R.drawable.health_and_beauty_green;

            case "Photoshoot":
                return R.drawable.photographer_green;

            case "Animal sitting & Pet care":
                return R.drawable.animal_passing;

            case "Computer & Tech fixes":
                return R.drawable.minor_tech;

            case "Ground & Construction":
                return R.drawable.ground_n_construction;

            case "Carrying & Assembling":
                return R.drawable.computer_and_networking;

            case "Others":
                 return R.drawable.other;

            case "All":
                return R.drawable.other;

            default:
               return R.drawable.other;

        }
    }

    //Dialog To show Job Detail At bottom
    private void showDialog(int budget, final Long timeline, String title, final Long BuyerId, final Long jobId, final String category, final Double lat, final Double lng, final Long postingTime, int userReviews, String rating, String userImage) {
        View view = getActivity().getLayoutInflater().inflate(R.layout.search_list_view_item, null);
        LinearLayout layout = (LinearLayout) view.findViewById(R.id.item_background);
        layout.setBackgroundColor(getResources().getColor(R.color.background_color));
        TextView jobTitle = (TextView) view.findViewById(R.id.title);
        View view1 = (View)view.findViewById(R.id.view);
        view1.setVisibility(View.GONE);
        TextView jobBudget = (TextView) view.findViewById(R.id.budget);
        TextView reviews = (TextView) view.findViewById(R.id.reviews);
        TextView distance = (TextView) view.findViewById(R.id.distance);
        TextView jobtimeline = (TextView) view.findViewById(R.id.date_to_complete);
        CircleImageView circleImageView = (CircleImageView) view.findViewById(R.id.profile_image);
        SimpleRatingBar ratingBar = (SimpleRatingBar) view.findViewById(R.id.rating);
        ratingBar.setRating(Float.parseFloat(rating));
        reviews.setText("( "+userReviews+" )");
        String image = S3Utils.getDownloadUserImageURL(BuyerId, userImage);
        Glide.with(getActivity()).load(image).into(circleImageView);

        try {
            jobtimeline.setText(DateTimeConverter.getTimeToComplete(timeline));
        } catch (ParseException e) {
            Log.e("ParseException", e.toString());
        }
        jobBudget.setTextSize(22);
        jobBudget.setText(budget + " SEK");
        jobTitle.setTextSize(15);
        jobTitle.setText(title);
        final Dialog mBottomSheetDialog = new Dialog(getActivity(), R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.BOTTOM);
        mBottomSheetDialog.show();
        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SearchJobsScreen parentFragment= (SearchJobsScreen)getActivity();
                if(parentFragment.getIsEmailVerified().equals("verified")) {
                    Intent intent = new Intent(getActivity(), SingleJobActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("status", "Detail");
                    bundle.putLong("jobId", jobId);
                    bundle.putLong("userId", BuyerId);
                    intent.putExtras(bundle);
                    getActivity().startActivity(intent);
                    mBottomSheetDialog.dismiss();
                }
                else{
                    mBottomSheetDialog.dismiss();
                    if(parentFragment.getIsEmailVerified().equals("completeProfile")) {
                        Snackbar.make(getView(), getString(R.string.complete_profile_check), Snackbar.LENGTH_LONG).setAction(getString(R.string.max_ok_btn), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent1 = new Intent(getActivity(), RegisterActivity.class);
                                startActivity(intent1);
                            }
                        }).setActionTextColor(getResources().getColor(R.color.app_color)).show();
                    }
                    else{
                        Snackbar.make(getView(),getString(R.string.verify_email_check),Snackbar.LENGTH_LONG).show();
                    }
                }
            }
        });
    }
}
