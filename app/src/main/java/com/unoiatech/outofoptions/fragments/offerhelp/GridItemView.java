package com.unoiatech.outofoptions.fragments.offerhelp;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.util.S3Utils;
import com.unoiatech.outofoptions.api.URL;

/**
 * Created by Unoiatech on 8/10/2017.
 */

public class GridItemView extends FrameLayout{
    private ImageView selected,userImage,selected_img;
    TextView userName,userRole,userId;
    Context context;

    public GridItemView(Context context) {
        super(context);
        this.context=context;
        LayoutInflater.from(context).inflate(R.layout.staff_grid_items, this);
        selected = (ImageView) getRootView().findViewById(R.id.selected);
        selected_img=(ImageView)getRootView().findViewById(R.id.selected_img);

        userName = (TextView) getRootView().findViewById(R.id.user_name);
        userImage = (ImageView) getRootView().findViewById(R.id.user_image);
        userRole=(TextView)getRootView().findViewById(R.id.user_role);
        userId=(TextView)getRootView().findViewById(R.id.user_id);
    }

    public void display(String firstName, String lastName, String imagePath, Long id, String text, boolean isSelected) {
        Log.e("Display","Display"+" "+text);
        userName.setText(firstName+" "+lastName);
        userId.setText(String.valueOf(id));
        selected_img.setVisibility(GONE);
        setMemberImage(userImage,imagePath,id);
        if(text!=null) {
            userRole.setVisibility(VISIBLE);
        }
        display(isSelected);
    }

    public void display(boolean isSelected) {
        //selected.setBackgroundResource(isSelected ? android.R.color.holo_red_light : R.drawable.selected_item);

        //Rounded ImageView Background
        selected.setImageResource(isSelected ? R.color.selected_item : android.R.color.transparent);
    }
    private void setMemberImage(ImageView memberImage, String imagePath, Long id) {
        if (imagePath != null) {
            if (imagePath.contains("\\")) {
                String lastIndex = imagePath.replace("\\", " ");
                String[] Stamp = lastIndex.split("\\s");
                String timeStamp = Stamp[3];
                String userImageUrl = URL.BASE_URL + "/users/download/" + id + "/image/" + timeStamp;
                Glide.with(context).load(userImageUrl).into(memberImage);
            } else {
                Glide.with(context).load(S3Utils.getDownloadUserImageURL(id, imagePath)).into(memberImage);
            }
        }
    }

    public void displayStaffMembers(String firstName, String lastName, String imagePath, Long assignee) {
        Log.e("Display_Staff_Member","Display_staff_Member");
        userName.setText(firstName+" "+lastName);
        userId.setText(String.valueOf(assignee));
        setMemberImage(userImage,imagePath,assignee);
        selected_img.setImageResource(R.drawable.selectedimage);
    }
}
