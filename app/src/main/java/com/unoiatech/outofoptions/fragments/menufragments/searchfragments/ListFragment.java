package com.unoiatech.outofoptions.fragments.menufragments.searchfragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.api.ApiClient;
import com.unoiatech.outofoptions.api.ApiInterface;
import com.unoiatech.outofoptions.fragments.menufragments.SearchJobsScreen;
import com.unoiatech.outofoptions.fragments.menufragments.SearchTabFragment;
import com.unoiatech.outofoptions.fragments.menufragments.adapter.SearchJobListAdapter;
import com.unoiatech.outofoptions.home.fragment.adapter.JobListResponse;
import com.unoiatech.outofoptions.home.fragment.filter.FilterDialogFragment;
import com.unoiatech.outofoptions.model.Job;
import com.unoiatech.outofoptions.model.SearchJobsRequest;
import com.unoiatech.outofoptions.model.SearchListResponse;
import com.unoiatech.outofoptions.model.SearchRequest;
import com.unoiatech.outofoptions.model.User;
import com.unoiatech.outofoptions.util.GPSTracker;
import com.unoiatech.outofoptions.util.RecyclerItemClickListener;
import com.unoiatech.outofoptions.util.SharedPrefsHelper;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by unoiatech on 4/30/2016.
 */

public class ListFragment extends Fragment {
    private String TAG = "ListFragment";
    ArrayList<SearchListResponse> arrayList;
    SearchJobListAdapter listAdapter;
    int count;
    RecyclerView recyclerView;

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.list_fragment_layout, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.list_recycler_view);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        //setListenerOnFilterButton(filterButton);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        arrayList = new ArrayList<>();
        count=1;
        final SearchRequest searchRequest = SharedPrefsHelper.getInstance(SharedPrefsHelper.FILTER_OPTIONS_PREF).
                readSearchRequest();

        setDataOnList(searchRequest,arrayList);

        Log.e("OnResume","OnResume"+searchRequest.getDistance()+" "+searchRequest.getCategory().toArray().toString()+"  "+searchRequest.getLatitude()+" "+searchRequest.getRating()+"  "+searchRequest.getLongitude()
        +"  "+searchRequest.getRating()+" "+searchRequest.getMaxBudget()+"  "+searchRequest.getMinBudget());
        getPostedJobs(searchRequest,0);

        /********TouchListener on RecyclerView***********************/
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Job job= arrayList.get(position).getJob();
                Intent intent = new Intent(getActivity(), SingleJobActivity.class);
                Bundle bundle= new Bundle();
                bundle.putString("status","Detail");
                bundle.putLong("jobId",job.getId());
                bundle.putLong("userId",arrayList.get(position).getUser().getId());
                intent.putExtras(bundle);
                startActivity(intent);
            }
        }));
    }

    private void setDataOnList(final SearchRequest searchRequest, ArrayList<SearchListResponse> arrayList) {
       // listAdapter = new SearchJobListAdapter(getActivity(), arrayList);
        listAdapter.setLoadMoreListener(new SearchJobListAdapter.OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                Log.e("LoadMore", "LoadMore" + count);
                recyclerView.post(new Runnable() {
                    @Override
                    public void run() {
                        loadMore(searchRequest,count);
                    }
                });
            }
        });
        recyclerView.setAdapter(listAdapter);
    }

    private void loadMore(SearchRequest searchRequest, int index) {
        Log.e("Load_More","Load_More"+"  "+index);
        arrayList.add(new SearchListResponse("load"));
        listAdapter.notifyItemInserted(arrayList.size() - 1);
        /*SearchJobsScreen parentFragment = (SearchJobsScreen) getActivity();
        SearchJobsRequest jobs = new SearchJobsRequest();
        jobs.setCategory(new ArrayList<String>());
        jobs.setPageNo(index);
        jobs.setMinBudget(0);
        jobs.setMaxBudget(50000);
        jobs.setDistance(1000.0);
        jobs.setRating(0.0);
        jobs.setLatitude(parentFragment.getLatitude());
        jobs.setLongitude(parentFragment.getLongitude());*/

        User user= SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readUserPref();
        ApiInterface api = ApiClient.getClient().create(ApiInterface.class);
        Call<SearchListResponse> call = api.getJobsOnList(user.getId(), searchRequest);
        call.enqueue(new Callback<SearchListResponse>() {
            @Override
            public void onResponse(Call<SearchListResponse> call, Response<SearchListResponse> response) {
                if (response.isSuccessful()) {
                    Log.e(TAG, "Load_Result" + response.body().getContent().size());
                    arrayList.remove(arrayList.size() - 1);
                    List<SearchListResponse> result = response.body().getContent();
                    if (response.body().getContent().size() > 0) {
                        count++;
                        //arrayList.addAll(result);
                        listAdapter.notifyDataSetChanged();
                    }
                    else {
                        listAdapter.setMoreDataAvailable(false);
                    }
                    listAdapter.notifyDataChanged();
                }
                else {
                    Log.e(TAG, " Load More Response Error " + String.valueOf(response.code()));
                }
            }

            @Override
            public void onFailure(Call<SearchListResponse> call, Throwable t) {
                Log.e(TAG, " Load More Response Error " + t.getMessage());
            }
        });
    }

    public String[] getLocation() {
        GPSTracker gps = new GPSTracker(getActivity());
        String[] coordinates = new String[2];
        // check if GPS enabled
        if (gps.canGetLocation()) {
            coordinates[0] = String.valueOf(gps.getLatitude());
            coordinates[1] = String.valueOf(gps.getLongitude());
        } else {
            gps.showSettingsAlert();
        }
        return coordinates;
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.e(TAG, "onStop");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.e(TAG, "onDetach");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e(TAG, "onDestroy");
        //getActivity().unregisterReceiver(list_reciever);

    }

    @Override
    public void onResume() {
        Log.e(TAG,"OnResume");
        super.onResume();
        /*final SearchRequest searchRequest = SharedPrefsHelper.getInstance(SharedPrefsHelper.FILTER_OPTIONS_PREF).
                readSearchRequest();
        count = 1;
        arrayList = new ArrayList<>();
        listAdapter = new SearchJobListAdapter(getActivity(), arrayList);
        listAdapter.setLoadMoreListener(new SearchJobListAdapter.OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                Log.e("LoadMore", "LoadMore" + count);
                recyclerView.post(new Runnable() {
                    @Override
                    public void run() {
                        loadMore(searchRequest,count);
                    }
                });
            }
        });*/

    }

    @Override
    public void onPause() {
        super.onPause();
        Log.e(TAG, "onPause");
    }
    public void getPostedJobs(final SearchRequest searchRequest, int index) {
        Log.e("GetPosted_Jobs","Get_Posted_Job"+index);
        //SearchTabFragment parentFragment = (SearchTabFragment) getParentFragment();
        /*SearchJobsScreen parentFragment=(SearchJobsScreen)getActivity();
        SearchJobsRequest jobs = new SearchJobsRequest();
        jobs.setCategory(new ArrayList<String>());
        jobs.setPageNo(index);
        jobs.setMinBudget(0);
        jobs.setMaxBudget(50000);
        jobs.setDistance(1000.0);
        jobs.setRating(0.0);
        jobs.setLatitude(parentFragment.getLatitude());
        jobs.setLongitude(parentFragment.getLongitude());*/
        arrayList.clear();
        User user= SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readUserPref();
        Long userId=user.getId();
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<SearchListResponse> call = apiService.getJobsOnList(userId, searchRequest);
        call.enqueue(new Callback<SearchListResponse>() {
            @Override
            public void onResponse(Call<SearchListResponse> call, Response<SearchListResponse> response) {
                Log.e(TAG,"RESPONSE"+response.body().getContent().size());
                if(response.code()==200) {
                    //List<SearchListResponse> data = response.body().getContent();
                    //arrayList.addAll(data);
                }
                setDataOnList(searchRequest,arrayList);
            }

            @Override
            public void onFailure(Call<SearchListResponse> call, Throwable t) {
                Log.e(TAG, "Failure" + t.toString());
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.e(TAG, "onAttach");
    }
}
