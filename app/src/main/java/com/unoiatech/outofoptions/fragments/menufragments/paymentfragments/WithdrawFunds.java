package com.unoiatech.outofoptions.fragments.menufragments.paymentfragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.common.api.Api;
import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.api.ApiClient;
import com.unoiatech.outofoptions.api.ApiInterface;
import com.unoiatech.outofoptions.companymodule.InviteStaffDialog;
import com.unoiatech.outofoptions.model.Payment;
import com.unoiatech.outofoptions.model.SellerTransaction;
import com.unoiatech.outofoptions.model.SwishCustomer;
import com.unoiatech.outofoptions.model.User;
import com.unoiatech.outofoptions.util.ApiError;
import com.unoiatech.outofoptions.util.BankId.ErrorUtils;
import com.unoiatech.outofoptions.util.SharedPrefsHelper;

import org.jivesoftware.smackx.xdatalayout.packet.DataLayout;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by unoiaAndroid on 6/23/2017.
 */

public class WithdrawFunds extends Activity implements View.OnClickListener{
    private static WithdrawFunds withdrawFunds;
    private EditText withdrawAmount;
    private ImageView connectSwish;
    private TextView walletBalance,swishNumber;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.withdraw_fund_main);

        swishNumber=(TextView)findViewById(R.id.swish_number);
        connectSwish=(ImageView)findViewById(R.id.connect_swish);
        walletBalance=(TextView)findViewById(R.id.wallet_bal);
        withdrawAmount=(EditText) findViewById(R.id.withdraw_amount);
        Button submitBtn=(Button)findViewById(R.id.submit_btn);

        //Get Swish Number Using Intent from previous Activity
        if(getIntent().getStringExtra("swish_number")!=null)
        if(getIntent().getStringExtra("swish_number").equals("Not Connected")){
            connectSwish.setVisibility(View.VISIBLE);
        }else{
            swishNumber.setVisibility(View.VISIBLE);
            swishNumber.setText(getIntent().getStringExtra("swish_number"));
        }

        //Set onclick Listeners
        submitBtn.setOnClickListener(this);
        connectSwish.setOnClickListener(this);

        //Method to Get Wallet Balance
        user= SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readUserPref();
        getUserWalletBalance(user.getId());
    }

    private void getUserWalletBalance(Long id) {
        Log.e("UserWallet","WalletBalance");
        ApiInterface apiService=ApiClient.getClient().create(ApiInterface.class);
        Call<Payment> call= apiService.getUserWallet(id);
        call.enqueue(new Callback<Payment>() {
            @Override
            public void onResponse(Call<Payment> call, Response<Payment> response) {
                Log.d("Response","Response"+response.body()+" "+response.isSuccessful());
                if(response.isSuccessful()){
                    if(response.body().getBalance()!=null){
                        walletBalance.setText(response.body().getBalance()+" SEK");
                    }else{
                        walletBalance.setText(0+" SEK");
                    }
                }else{
                    ApiError error= ErrorUtils.parseError(response);
                    notifyUser(error.getMessage());
                }
            }

            @Override
            public void onFailure(Call<Payment> call, Throwable t) {
                Log.e("Failure","Failure"+"  "+t.toString());
            }
        });
    }

    /* @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View view= inflater.inflate(R.layout.withdraw_fund_main,container,false);

            Button addAccount=(Button)view.findViewById(R.id.add_account);
            addAccount.setOnClickListener(this);
            return view;
        }
    */
    public static WithdrawFunds getWithdrawFunds(){
        if(withdrawFunds==null)
        withdrawFunds=new WithdrawFunds();
        return withdrawFunds;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.connect_swish:
                Intent intent= new Intent(WithdrawFunds.this,ConnectToSwish.class);
                this.finish();
                startActivity(intent);
                break;
            case R.id.submit_btn:
                //dismiss keyboard
                InputMethodManager inputManager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow((null == getCurrentFocus()) ? null : getCurrentFocus().getWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS);

                if(withdrawAmount.getText().length()==0||withdrawAmount.getText().toString().isEmpty()){
                    notifyUser(getString(R.string.withdraw_amount_text));
                }
                else {
                    submitWithdrawalAmount();
                }
                break;
        }
    }

    private void submitWithdrawalAmount() {
        String[] bal=walletBalance.getText().toString().split("SEK");
        String balance=bal[0].replaceAll("\\s","");
        int withdraw=Integer.parseInt(withdrawAmount.getText().toString());
        int amount=Integer.parseInt(balance);
        if(withdraw>amount){
            notifyUser(getString(R.string.withdrawal_error_msg));
        }
        else if(connectSwish.getVisibility()==View.VISIBLE){
            notifyUser(getString(R.string.connect_swish_error_text));
        }
        else{
            methodToWithdrawFunds();
        }
    }

    private void methodToWithdrawFunds() {
        SellerTransaction sellerTransaction= new SellerTransaction();
        sellerTransaction.setSwishNumber(swishNumber.getText().toString());
        sellerTransaction.setAmountRequested(Long.parseLong(withdrawAmount.getText().toString()));
        sellerTransaction.setUserId(user.getId());
        Log.e("data","Data"+sellerTransaction.getSwishNumber()+"  "+sellerTransaction.getAmountRequested());

        ApiInterface apiService=ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call=apiService.withdrawFundRequest(sellerTransaction);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d("Response","Response"+response.body()+" "+response.toString());
                if(response.isSuccessful()){
                    showWithdrawDialog();
                }else{
                    ApiError error=ErrorUtils.parseError(response);
                    notifyUser(error.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("Failure","Failure"+"  "+t.toString());
            }
        });
    }

    private void showWithdrawDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        View view1 = getLayoutInflater().inflate(R.layout.custom_withdraw_confirmation, null);
        dialogBuilder.setView(view1);
        TextView ok_btn = (TextView) view1.findViewById(R.id.okay_btn);
        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();
        ok_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                finish();
            }
        });
    }

    public void notifyUser(String message) {
        Snackbar.make(getWindow().getDecorView(), message, Snackbar.LENGTH_LONG).setAction(getString(R.string.max_ok_btn), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        }).setActionTextColor(getResources().getColor(R.color.app_color)).show();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
