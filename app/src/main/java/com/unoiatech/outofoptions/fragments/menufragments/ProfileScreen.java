package com.unoiatech.outofoptions.fragments.menufragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;
import com.unoiatech.outofoptions.home.HomeScreenActivity;
import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.api.ApiClient;
import com.unoiatech.outofoptions.api.ApiInterface;
import com.unoiatech.outofoptions.home.fragment.JobsListFragment;
import com.unoiatech.outofoptions.model.BlockUser;
import com.unoiatech.outofoptions.model.User;
import com.unoiatech.outofoptions.util.ApiError;
import com.unoiatech.outofoptions.util.BankId.ErrorUtils;
import com.unoiatech.outofoptions.util.ConnectionDetection;
import com.unoiatech.outofoptions.util.DateTimeConverter;
import com.unoiatech.outofoptions.util.S3Utils;
import com.unoiatech.outofoptions.util.SharedPrefsHelper;
import com.unoiatech.outofoptions.api.URL;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Unoiatech on 3/28/2017.
 */
public class ProfileScreen extends Fragment implements View.OnClickListener {
    private ImageView profile_Image,blockUser;
    private TextView name;
    private TextView emailText,phoneText,bankText,facebookText;
    private TextView title;
    private TextView edit_profile;
    private TextView tagline;
    private TextView reviews;
    private TextView about;
    private ArrayList<String> verified;
    private Long userId;
    private String userImageUrl;
    private static String emailAddress;
    private SimpleRatingBar ratingBar;
    private static  ProfileScreen profileScreen;
    private static String TAG="ProfileScreen";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState){
        View view= inflater.inflate(R.layout.profile_screen,container,false);

        initViews(view);
        return view;
    }

    private void initViews(View view) {
        title=(TextView)view.findViewById(R.id.title);
        ratingBar=(SimpleRatingBar)view.findViewById(R.id.rating_bar);
        reviews=(TextView)view.findViewById(R.id.reviews);
        profile_Image=(ImageView)view.findViewById(R.id.profile_image);
        name=(TextView)view.findViewById(R.id.name);
        tagline=(TextView)view.findViewById(R.id.tagline);
        about=(TextView)view.findViewById(R.id.about);

        emailText=(TextView)view.findViewById(R.id.email);
        bankText=(TextView)view.findViewById(R.id.bank);
        facebookText=(TextView)view.findViewById(R.id.facebook);
        phoneText=(TextView)view.findViewById(R.id.phone);

        edit_profile=(TextView)view.findViewById(R.id.edit);
        blockUser=(ImageView)view.findViewById(R.id.block_user);
    }

    @Override
    public void onResume() {
        super.onResume();

        /****GEtUserProfile***********/
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            userId=bundle.getLong("userId");
            if(bundle.getString("view").equals(getString(R.string.other_user_profile))){
                title.setText(bundle.getString("view"));
                edit_profile.setVisibility(View.GONE);
                blockUser.setVisibility(View.VISIBLE);
                blockUser.setOnClickListener(this);
                if(ConnectionDetection.isInternetAvailable(getActivity())) {
                    getUserProfile(bundle.getLong("userId"));
                }else{
                    Snackbar.make(getView(),getString(R.string.internet_connection_error),Snackbar.LENGTH_SHORT).show();
                }
            }
        }
        else{
            HomeScreenActivity.showTabHost();
            edit_profile.setVisibility(View.VISIBLE);
            title.setText(getString(R.string.profile_title));
            edit_profile.setText(getString(R.string.edit_title));
            edit_profile.setOnClickListener(ProfileScreen.this);
            User user= SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readUserPref();
            userId=user.getId();
            if(ConnectionDetection.isInternetAvailable(getActivity())) {
                getUserProfile(user.getId());
            }else{
                //Toast.makeText(getActivity(),getString(R.string.internet_connection_error),Toast.LENGTH_SHORT).show();
                Snackbar.make(getView(),getString(R.string.internet_connection_error),Snackbar.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.edit:
                //if(emailAddress!=null)
                editUserDetails();
                break;

            case R.id.block_user:
                AlertDialog.Builder dialogBuilder= new AlertDialog.Builder(getActivity());
                View view1= getActivity().getLayoutInflater().inflate(R.layout.block_user_dialog,null);
                dialogBuilder.setView(view1);

                //Dialog Components
                TextView cancelText=(TextView)view1.findViewById(R.id.cancel);
                final TextView blockUser=(TextView)view1.findViewById(R.id.block_user);

                final AlertDialog dialog=dialogBuilder.create();
                dialog.setCancelable(false);
                dialog.show();

                cancelText.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                blockUser.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                        blockUser();
                    }
                });
        }
    }

    private void editUserDetails() {
        Intent intent= new Intent(getActivity(),EditProfileScreen.class);
        Bundle bundle = new Bundle();
        bundle.putString("name",name.getText().toString());
        bundle.putString("email",emailAddress);
        bundle.putString("image",userImageUrl);
        bundle.putString("about",about.getText().toString());
        bundle.putString("tagline",tagline.getText().toString());
        bundle.putStringArrayList("verifyid",verified);
        bundle.putLong("user_id",userId);
        intent.putExtras(bundle);
        startActivity(intent);
        Log.e("data","Data"+emailAddress+"  "+userImageUrl);
    }

    private void blockUser() {
        User user=SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readUserPref();
        BlockUser blockUser= new BlockUser();
        blockUser.setUserId(user.getId());
        blockUser.setBlockedUserId(userId);
        blockUser.setCreatedId(DateTimeConverter.getTime());
        ApiInterface apiService=ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call=apiService.blockUser(blockUser);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d(TAG,"Response"+response.body()+"  "+ ((HomeScreenActivity)getActivity()).getSupportFragmentManager().getBackStackEntryCount());
                if(response.body()!=null){
                    getActivity().getSupportFragmentManager().popBackStack();
                    JobsListFragment jobsListFragment=(JobsListFragment) getActivity().getSupportFragmentManager().findFragmentByTag("jobs_list");
                    jobsListFragment.onResume();
                }
            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG,"FAilure"+t.toString());
            }
        });
    }

    public void getUserProfile(long userId) {
        Log.e("Get_Profile","GEt_Profile");
        ApiInterface apiService= ApiClient.getClient().create(ApiInterface.class);
        Call<User> call= apiService.getUserProfile(userId);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                Log.d(TAG,"Response"+response.body()+" "+response.message());
                if(response.code()==200) {
                    showRetroResult(response.body().getUser());
                    getAccountVerificationInfo(response.body().getUser().getId());
                    getUserReviews(response.body().getUser().getId());
                }else{
                    ApiError error = ErrorUtils.parseError(response);
                    Snackbar.make(getView(),error.getMessage(),Snackbar.LENGTH_SHORT).show();
                    Log.d("Code" + "Code", "" + response);
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Log.d("Failure","Failure"+t.toString());
            }
        });
    }
    private void getUserReviews(Long userId){
        ApiInterface apiService= ApiClient.getClient().create(ApiInterface.class);
        Call<User> call= apiService.getUserReviews(userId);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                String rating = response.body().getAverage_review();
                ratingBar.setRating(Float.parseFloat(rating));
                int userReviews = (response.body().getBuyer_reviews().size() + response.body().getSeller_reviews().size());
                reviews.setText("( " + userReviews + " reviews )");
            }
            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Log.e("Failure","Failure"+t.toString());
            }
        });
    }

    private void getAccountVerificationInfo(Long userId) {
        ApiInterface apiService= ApiClient.getClient().create(ApiInterface.class);
        Call<ArrayList<User>> call= apiService.verifyIdsOnProfile(userId);
        call.enqueue(new Callback<ArrayList<User>>() {
            @Override
            public void onResponse(Call<ArrayList<User>> call, Response<ArrayList<User>> response) {
                Log.d("response","response"+" body" +response.body());
                showAccountVerification(response.body());
            }

            @Override
            public void onFailure(Call<ArrayList<User>> call, Throwable t) {
                Log.d("Failure","Failure"+t.toString());
            }
        });
    }
    public  void showRetroResult(User body) {
        try {
            String username = body.getFirstName()+" "+body.getLastName();
            Long userId=body.getId();
            String imagePath=body.getImagePath();
            emailAddress=body.getEmail();
            Log.e("ImagePath","ImagePath"+ "  "+imagePath);
            if(imagePath!=null){
                if(imagePath.contains("\\")){
                    Log.e("ImagePath","ImagePath"+ "  "+imagePath);

                    String lastIndex= imagePath.replace("\\", " ");
                    String[] Stamp=lastIndex.split("\\s");
                    String timeStamp=Stamp[3];
                    userImageUrl = URL.BASE_URL+"/users/download/"+userId+"/image/"+timeStamp;
                    Glide.with(getActivity()).load(userImageUrl).into(profile_Image);
                }
                else{
                    userImageUrl=imagePath;
                    Glide.with(getActivity()).load(S3Utils.getDownloadUserImageURL(userId,imagePath)).into(profile_Image);
                }
            }
            name.setText(username);
            if(body.getAbout().isEmpty()||body.getAbout().equals(null)){
               about.setText("-");
            }
            else{
                about.setText(body.getAbout());
            }

            if(body.getTagline().isEmpty()||body.getTagline().equals(null)){
                tagline.setText("-");
            }else{
                tagline.setText(body.getTagline());
            }
        }
        catch (NullPointerException ex) {
            ex.printStackTrace();
        }
    }

    public  void showAccountVerification(ArrayList<User> body) {
        verified = new ArrayList<>();
        ArrayList<User> verifyIdsList = new ArrayList<User>(body);
        int size = verifyIdsList.size();
        for (int i = 0; i < size; i++) {
            String verificationType = verifyIdsList.get(i).getVerificationType();
            verified.add(verificationType);
        }
        for (String item : verified) {
            if (item.equals("bankId")) {
                phoneText.setText(" BankID");
            }
            if (item.equals("facebook")) {
                facebookText.setText("Facebook");
            }
            if (item.equals("email")) {
                emailText.setText("Email ");
            }
            if (item.equals("phone")) {
                phoneText.setText("  Phone");
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if(title.getText().toString().equals(getString(R.string.profile_title))){
        HomeScreenActivity.removeTabHost();}
    }

    public static ProfileScreen getProfileScreen() {
        if(profileScreen==null) {
            profileScreen=new ProfileScreen();
        }
        return profileScreen;
    }
}
