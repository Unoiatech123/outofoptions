package com.unoiatech.outofoptions.fragments.menufragments.archivejobs;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.api.ApiClient;
import com.unoiatech.outofoptions.api.ApiInterface;
import com.unoiatech.outofoptions.fragments.activejobs.JobsFragment;
import com.unoiatech.outofoptions.fragments.activejobs.MyJobsListFragment;
import com.unoiatech.outofoptions.fragments.activejobs.adapter.DataReceivedEvent;
import com.unoiatech.outofoptions.home.HomeScreenActivity;
import com.unoiatech.outofoptions.model.ActiveJobsResponse;
import com.unoiatech.outofoptions.model.DraftResponse;
import com.unoiatech.outofoptions.model.User;
import com.unoiatech.outofoptions.model.ActiveJobsResponse;
import com.unoiatech.outofoptions.util.ApiError;
import com.unoiatech.outofoptions.util.BankId.ErrorUtils;
import com.unoiatech.outofoptions.util.SharedPrefsHelper;
import com.unoiatech.outofoptions.util.common.view.SlidingTabLayout;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Unoiatech on 5/25/2017.
 */

public class ArchiveJobs extends Fragment {
    private static final String TAG="ArchiveFragment";
    ArrayList<ActiveJobsResponse> data_list;
    ViewPager mViewPager;
    SlidingTabLayout mSlidingTabLayout;
    ArrayList<DraftResponse> draftJob;

    @Override
    public void onAttachFragment(Fragment childFragment) {
        super.onAttachFragment(childFragment);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.archived_job_layout,container,false);
        mViewPager = (ViewPager) view.findViewById(R.id.viewpager);
        mSlidingTabLayout =(SlidingTabLayout) view.findViewById(R.id.sliding_tabs);
        mSlidingTabLayout.setCustomTabView(R.layout.custom_tabstrip_for_activejobs, R.id.custom_tabstrip_tv);
        mSlidingTabLayout.setDistributeEvenly(false);
        mViewPager.setAdapter(new MyAdapter(getChildFragmentManager(),3));
        mSlidingTabLayout.setViewPager(mViewPager);

        draftJob= new ArrayList<>();
        return view;
    }

    public class MyAdapter extends FragmentStatePagerAdapter {
        int tabcount;

        public MyAdapter(FragmentManager fm, int childCount) {
            super(fm);
            this.tabcount=childCount;
        }

        @Override
        public Fragment getItem(int i){
            switch (i) {
                case 0:
                    ArchiveJobList f=new ArchiveJobList();
                    Bundle b = new Bundle();
                    b.putString("type", "expired");
                    f.setArguments(b);
                    return f;

                case 1:
                    ArchiveJobList f1= new ArchiveJobList();
                    Bundle b1= new Bundle();
                    b1.putString("type","cancelled");
                    f1.setArguments(b1);
                    return f1;

                case 2:
                    DraftJobFragment f2=new DraftJobFragment();
                    Bundle b2 = new Bundle();
                    b2.putString("type","draft");
                    f2.setArguments(b2);
                    return f2;
            }
            return null;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getResources().getString(R.string.expire_job);
                case 1:
                    return getResources().getString(R.string.archive_cancel_job);
                case 2:
                    return getResources().getString(R.string.archive_drafts);
            }
            return super.getPageTitle(position);
        }

        @Override
        public int getCount() {
            return tabcount;
        }
    }
    @Override
    public void onStop() {
        super.onStop();
        Log.e(TAG, "onStop");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.e(TAG, "onDetach");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e(TAG, "onDestroy");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e(TAG, "onResume");
        //HomeScreenActivity.showTabHost();
        data_list= new ArrayList<>();
        getPostedJobList();

        getDraftJobList();

    }

    @Override
    public void onPause() {
        super.onPause();
        Log.e(TAG, "onPause");
       // HomeScreenActivity.removeTabHost();
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.e(TAG, "onStart");
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.e(TAG, "onAttach");
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.e(TAG, "onActivityCreated");
    }
    public void getPostedJobList() {
        User user = SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readUserPref();
        data_list.clear();
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ArrayList<ActiveJobsResponse>> call = apiService.getPostedJobs(user.getId());
        call.enqueue(new Callback<ArrayList<ActiveJobsResponse>>() {
            @Override
            public void onResponse(Call<ArrayList<ActiveJobsResponse>> call, Response<ArrayList<ActiveJobsResponse>> response) {
                Log.d(TAG, "RESPONSE" + response.body().size()+"  "+response);
                if (response.isSuccessful()) {
                    data_list.addAll(response.body());
                    EventBus.getDefault().post(new DataReceivedEvent("Hello EventBus!"));
                }
                else{
                    Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                }
                Log.e("ArchiveJob","ArchieveJobs"+data_list.size());
            }

            @Override
            public void onFailure(Call<ArrayList<ActiveJobsResponse>> call, Throwable t) {
                Log.d(TAG, "Failure" + t.toString());
            }
        });
    }
    private void getDraftJobList() {
        User user= SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readUserPref();
        draftJob.clear();
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ArrayList<DraftResponse>> call = apiService.getDraftedJobData(user.getId());
        call.enqueue(new Callback<ArrayList<DraftResponse>>(){
            @Override
            public void onResponse(Call<ArrayList<DraftResponse>> call, Response<ArrayList<DraftResponse>> response) {
                if (response.code() == 200) {
                    draftJob.addAll(response.body());
                    Log.e("Drafted_DAta","Drafted_data"+draftJob.size());
                    EventBus.getDefault().post(new DataReceivedEvent("Hello EventBus!"));
                } else {
                    ApiError error = ErrorUtils.parseError(response);
                    Snackbar.make(getView(), error.getMessage(), Snackbar.LENGTH_SHORT).show();
                    Log.d("Code" + "Code", "" + response);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<DraftResponse>> call, Throwable t) {
                Log.d("Failure", "Failure" + t.toString());
            }
        });
    }

    public ArrayList<ActiveJobsResponse> getData(){
        return data_list;
    }
    public ArrayList<DraftResponse> getDraftedData(){
        return draftJob;
    }
}