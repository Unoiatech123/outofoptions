package com.unoiatech.outofoptions.fragments.activejobs.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;
import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.fragments.activejobs.proposalsfragment.RecievedProposals;
import com.unoiatech.outofoptions.model.PropsalResponse;
import com.unoiatech.outofoptions.util.S3Utils;
import java.util.ArrayList;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by unoiatech on 9/8/2016.
 */
public class RecievedProposalAdapter extends RecyclerView.Adapter<RecievedProposalAdapter.ViewHolder>
{
    private ArrayList<PropsalResponse> dataList;
    Activity activity;

    public RecievedProposalAdapter(RecievedProposals recievedProposals, ArrayList<PropsalResponse> proposals) {
        this.activity=recievedProposals;
        this.dataList=proposals;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView title,budget,description,dateToComplete,dueDate,name,reviews;
        CircleImageView circleImageView;
        SimpleRatingBar rating;
        public ViewHolder(View v) {
            super(v);
            name=(TextView) v.findViewById(R.id.name);
            budget=(TextView) v.findViewById(R.id.quoted_price);
            description=(TextView) v.findViewById(R.id.description);
            reviews=(TextView)v.findViewById(R.id.review_count);
            dateToComplete=(TextView) v.findViewById(R.id.date_to_complete);
            dueDate=(TextView) v.findViewById(R.id.due_date);
            circleImageView=(CircleImageView) v.findViewById(R.id.profile_image);
            rating=(SimpleRatingBar)v.findViewById(R.id.rating);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.proposal_list_view_item, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
            holder.description.setText(dataList.get(position).getProposal().getProposalDescription());
            holder.budget.setText(dataList.get(position).getProposal().getOfferedQuote() + " SEK");
            holder.name.setText(dataList.get(position).getUser().getFirstName() + " " + dataList.get(position).getUser().getLastName());
            holder.rating.setRating(Float.parseFloat(dataList.get(position).getAverage_review()));
            int review_count= dataList.get(position).getBuyer_reviews().size()+dataList.get(position).getSeller_reviews().size();
            holder.reviews.setText("("+review_count+")");
            String imageURL = S3Utils.getDownloadUserImageURL(dataList.get(position).getUser().getId(), dataList.get(position).getUser().getImagePath());
            Glide.with(holder.circleImageView.getContext())
                    .load(imageURL)
                    .asBitmap()
                    .centerCrop()
                    .into(holder.circleImageView);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        Log.e("Item count", "Item count"+dataList.size());
        return dataList.size();
    }
}
