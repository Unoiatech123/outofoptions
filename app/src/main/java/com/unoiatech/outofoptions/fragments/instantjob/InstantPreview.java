package com.unoiatech.outofoptions.fragments.instantjob;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DialogFragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;
import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.api.ApiClient;
import com.unoiatech.outofoptions.api.ApiInterface;
import com.unoiatech.outofoptions.fragments.instantjob.adapter.CustomImageAdapter;
import com.unoiatech.outofoptions.model.AddInstantJob;
import com.unoiatech.outofoptions.model.JobPostingDataHolder;
import com.unoiatech.outofoptions.model.User;
import com.unoiatech.outofoptions.util.ApiError;
import com.unoiatech.outofoptions.util.BankId.ErrorUtils;
import com.unoiatech.outofoptions.util.DateTimeConverter;
import com.unoiatech.outofoptions.util.GPSTracker;
import com.unoiatech.outofoptions.util.GetDirectionPath;
import com.unoiatech.outofoptions.util.SharedPrefsHelper;
import com.viewpagerindicator.CirclePageIndicator;
import com.viewpagerindicator.PageIndicator;
import com.wang.avi.AVLoadingIndicatorView;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Unoiatech on 3/6/2017.
 */

public class InstantPreview extends Fragment implements OnMapReadyCallback,AsynResponse {
    private final String TAG = "InstantPreview";
    private CustomImageAdapter mAdapter;
    private ViewPager mPager;
    private Double latitude, longitude;
    private TextView timeline, location, reviews;
    private OnViewChangeListener mlistener;
    private String formattedDate;
    private String startingTime, endingTime;
    private MapView mapView;
    private GoogleMap map;
    private SimpleRatingBar ratingBar;
    private AVLoadingIndicatorView progressBar;
    private PageIndicator mIndicator;
    private User user;
    RelativeLayout imagesLayout;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mlistener = (OnViewChangeListener) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.instant_preview, container, false);
        TextView title = (TextView) view.findViewById(R.id.job_title);
        TextView budget = (TextView) view.findViewById(R.id.budget);
        location = (TextView) view.findViewById(R.id.location);
        imagesLayout=(RelativeLayout)view.findViewById(R.id.display_images_layout);
        timeline = (TextView) view.findViewById(R.id.timeline);
        reviews = (TextView) view.findViewById(R.id.reviews);
        ratingBar = (SimpleRatingBar)view.findViewById(R.id.rating);
        //ratingBar.setScaleX((float) 0.8);
        TextView description = (TextView) view.findViewById(R.id.description);
        final ScrollView mainScrollView = (ScrollView) view.findViewById(R.id.main_scroll);
        final LinearLayout cancel_image = (LinearLayout) getActivity().findViewById(R.id.cancel_image);
        Button postButton =(Button)view.findViewById(R.id.post_btn);
        progressBar = (AVLoadingIndicatorView) view.findViewById(R.id.progress_bar);
        mapView = (MapView) view.findViewById(R.id.map);
        mlistener.showFourthView();
        getLocation();
        //get user Id
        user = SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readUserPref();

        changeDateFormat();
        mapView.onCreate(savedInstanceState);
        mapView.onResume();
        try {
            MapsInitializer.initialize(getActivity());
        } catch (Exception e) {
            e.printStackTrace();
        }
        mapView.getMapAsync(InstantPreview.this);

        /**********************Listener on Post Button*************************/
        postButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("ONClick","OnClick"+mlistener.getRepostData());
                if(mlistener.getRepostData()!=null) {
                    if(mlistener.getRepostData().getJobStatus().equals("Expired")||mlistener.getRepostData().getJobStatus().equals("Cancelled")||mlistener.getRepostData().getJobStatus().equals("Draft")) {
                        deleteJobPermanently(mlistener.getRepostData().getJobStatus(),mlistener.getRepostData().getUserId(),mlistener.getRepostData().getId());
                    }
                }
                else{
                setDataToPost("post");}
            }
        });

        /**********Listener on Back_Button*********/
        FrameLayout back_Button = (FrameLayout) view.findViewById(R.id.back_btn);
        back_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTabHost mhost = (FragmentTabHost) getActivity().findViewById(R.id.fragment_tabhost);
                mhost.setCurrentTab(2);
                mlistener.showFourthView();
            }
        });

        cancel_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveAsDraftAlertDialog();
                //setDataToPost("draft");
            }
        });

        /**************Scroll Map View inside Scroll view***********/
        final ImageView transparentImage = (ImageView) view.findViewById(R.id.transparent_image);
        transparentImage.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        mainScrollView.requestDisallowInterceptTouchEvent(true);
                        return false;
                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        mainScrollView.requestDisallowInterceptTouchEvent(false);
                        return true;
                    case MotionEvent.ACTION_MOVE:
                        mainScrollView.requestDisallowInterceptTouchEvent(true);
                        return false;
                    default:
                        return true;
                }
            }
        });

        /********************Get Data from previous Fragments***************/
        JobPostingDataHolder dataHolderClass = JobPostingDataHolder.getInstance();
        title.setText(dataHolderClass.getTitle());
        budget.setText(dataHolderClass.getBudget() + " " + "SEK");
        description.setText(dataHolderClass.getDescription());
        startingTime = dataHolderClass.getStartingDate();
        endingTime = dataHolderClass.getEndingDate();

        /*****************Get Images********************/
        if (JobPostingDataHolder.getInstance().getImageUri().length>0) {
            imagesLayout.setVisibility(View.VISIBLE);
            mAdapter = new CustomImageAdapter(getActivity());
            mPager = (ViewPager) view.findViewById(R.id.pager);
            mPager.setAdapter(mAdapter);
            mIndicator = (CirclePageIndicator) view.findViewById(R.id.indicator);
            mIndicator.setViewPager(mPager);
            ((CirclePageIndicator) mIndicator).setSnap(false);
            mIndicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageSelected(int position) {
                }

                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                }

                @Override
                public void onPageScrollStateChanged(int state) {
                }
            });
        }
        return view;
    }

    private void deleteJobPermanently(String jobStatus, Long userId, Long id) {
        ApiInterface apiService=ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call;
        if(jobStatus.equals("Draft")) {
            call=apiService.removeDraft(id);
        }
        else{
            call= apiService.deleteJobPermanently(id,userId);
        }
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d(TAG,"Response"+response.body());
                setDataToPost("post");
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG,"FAilure"+t.toString());
            }
        });
    }

    private void saveAsDraftAlertDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setMessage("Do you want to save job as Draft");
                alertDialogBuilder.setPositiveButton("Save As Draft",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {
                                setDataToPost("draft");}
                        });

        alertDialogBuilder.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                getActivity().finish();}
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
    public void getLocation() {
        GPSTracker gps = new GPSTracker(getActivity());
        if (gps.canGetLocation()) {
            latitude = gps.getLatitude();
            longitude = gps.getLongitude();
        } else {
            gps.showSettingsAlert();
        }
    }
    private void setDataToPost(String viewType) {
        JobPostingDataHolder dataHolder = JobPostingDataHolder.getInstance();
        AddInstantJob data = new AddInstantJob();
        data.setBudget(dataHolder.getBudget());
        data.setCategory(dataHolder.getCategory());
        data.setDescription(dataHolder.getDescription());
        data.setTitle(dataHolder.getTitle());
        data.setPlace(dataHolder.getLocation());
        data.setLat(dataHolder.getLat());
        data.setLng(dataHolder.getLng());
        data.setPreCancellationHours(dataHolder.getCancellation_hours());
        data.setWhoCanSendProposal(dataHolder.getIndividual());
        if (startingTime != null) {
            data.setStartingTime(startingTime);
            data.setEndingTime(endingTime);
        }
        data.setUserId(user.getId());
        data.setTimeline(formattedDate);
        if (viewType.equals("draft")) {
            data.setCreatedAt(DateTimeConverter.getTime());
            saveJobAsDraft(data);
        }
        else if(viewType.equals("post")) {
            data.setPostingTime(DateTimeConverter.getTimeInUtcFormat());
            data.setJobStatus("pending");
            postInstantJob(data);
        }
        Log.e("setDataToPost","SetData"+dataHolder.getLng()+"  "+dataHolder.getLat()+" "+viewType+"  " + DateTimeConverter.getTime());
    }

    private void saveJobAsDraft(AddInstantJob data) {
        progressBar.setVisibility(View.VISIBLE);
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<Long> call = apiInterface.saveAsDraft(user.getId(),data);
        call.enqueue(new Callback<Long>() {
            @Override
            public void onResponse(Call<Long> call, Response<Long> response) {
                Log.d(TAG, "SaveJobResponse" + response.toString());
                if(response.isSuccessful()) {
                    Long jobId= response.body();
                    Uri[] images=JobPostingDataHolder.getInstance().getImageUri();
                    if(images.length>0) {
                        for (Uri uri : images) {
                            addJobImages(getRealPathFromURI(uri), jobId, user.getId());
                        }
                    }
                    else {
                        showJobCompletedDialog();
                    }
                }
                else{
                    ApiError error = ErrorUtils.parseError(response);
                    Log.d(TAG + "Code", "" + response);
                    Snackbar.make(getView(),error.getMessage(), Snackbar.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Long> call, Throwable t) {
                Log.e("OnFailure", "OnFailure" + t.toString());
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    public void changeDateFormat() {
        JobPostingDataHolder dataHolderClass = JobPostingDataHolder.getInstance();
        TimeZone timeZone = TimeZone.getTimeZone("UTC");
        Date tomorrow = null;
        if (dataHolderClass.getDate().contains("8")) {
            Date today = new Date();
            tomorrow = new Date(today.getTime() + (1000 * 60 * 60 * 8));
        } else if (dataHolderClass.getDate().contains("24")) {
            Date today = new Date();
            tomorrow = new Date(today.getTime() + (1000 * 60 * 60 * 24));
        } else if (dataHolderClass.getDate().contains("03")) {
            Date today = new Date();
            tomorrow = new Date(today.getTime() + (1000 * 60 * 60 * 72));
        } else if (dataHolderClass.getDate().contains("30")) {
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.MONTH, 1);
            tomorrow=cal.getTime();
           /* Date today = new Date();
            tomorrow = new Date(today.getTime() + (1000 * 60 * 60 * 7200));*/
        }
        DateFormat targetFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        targetFormat.setTimeZone(timeZone);
        formattedDate = targetFormat.format(tomorrow);
        Log.e("formatedDate","formattedDate"+" "+formattedDate+"  "+dataHolderClass.getDate());
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        map.setMyLocationEnabled(true);
        JobPostingDataHolder dataholder = new JobPostingDataHolder().getInstance();
        String category = dataholder.getCategory();
        LatLng start = new LatLng(dataholder.getLat(),dataholder.getLng());


        /***Add Marker to corresponding Category***/
        LatLng end = new LatLng(dataholder.getLat(),dataholder.getLng());
        map.moveCamera(CameraUpdateFactory.newLatLng(start));
        map.animateCamera(CameraUpdateFactory.zoomTo(12));
        map.addMarker(new MarkerOptions().position(start).icon(BitmapDescriptorFactory.fromResource(R.drawable.current_location)));
        if (category.equals("Animals \n Sitting")) {
            map.addMarker(new MarkerOptions().position(end).icon(BitmapDescriptorFactory.fromResource(R.drawable.animal_passing)));
        } else if (category.equals("Computer & \n Network")) {
            map.addMarker(new MarkerOptions().position(end).icon(BitmapDescriptorFactory.fromResource(R.drawable.computer_and_networking)));
        } else if (category.equals("Ground & \n Construction")) {
            map.addMarker(new MarkerOptions().position(end).icon(BitmapDescriptorFactory.fromResource(R.drawable.ground_n_construction)));
        } else if (category.equals("Carrying & \n Assemble")) {
            map.addMarker(new MarkerOptions().position(end).icon(BitmapDescriptorFactory.fromResource(R.drawable.carring_assemble)));
        } else if (category.equals("Minor \n tech-fixes")) {
            map.addMarker(new MarkerOptions().position(end).icon(BitmapDescriptorFactory.fromResource(R.drawable.minor_tech)));
        } else if (category.equals("Photographer")) {
            map.addMarker(new MarkerOptions().position(end).icon(BitmapDescriptorFactory.fromResource(R.drawable.photographer_green)));
        } else if (category.equals("House & \n Gardening")) {
            map.addMarker(new MarkerOptions().position(end).icon(BitmapDescriptorFactory.fromResource(R.drawable.house_and_gardning)));
        } else if (category.equals("Home & \n Cleaning")) {
            map.addMarker(new MarkerOptions().position(end).icon(BitmapDescriptorFactory.fromResource(R.drawable.cleaning_green)));
        }

        /**************To Draw path on Map***************/
        if (dataholder.getLat() != null & dataholder.getLng() != null) {
            String sensor = "sensor=false";
            String source = "origin=" + latitude + "," + longitude;
            String destination = "destination=" + dataholder.getLat() + "," + dataholder.getLng();
            String parameters = source + "&" + destination + "&" + sensor;
            String output = "json";
            String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;
            Log.e("URl","URL"+url+"  "+latitude+" "+longitude);
            GetDirectionPath downloadtask = new GetDirectionPath();
            downloadtask.response = this;
            downloadtask.execute(url);
        }
    }

    private void postInstantJob(AddInstantJob data) {
        progressBar.setVisibility(View.VISIBLE);
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<Long> call = apiInterface.addInstantJob(data);
        call.enqueue(new Callback<Long>() {
            @Override
            public void onResponse(Call<Long> call, Response<Long> response) {
                Log.d(TAG, "Response" + response.toString());
                if(response.isSuccessful()) {
                    Long jobId= response.body();
                    Uri[] images=JobPostingDataHolder.getInstance().getImageUri();
                    showJobCompletedDialog();
                    if(images.length>0) {
                        for (Uri uri : images) {
                            addJobImages(getRealPathFromURI(uri), jobId, user.getId());
                        }
                    }
                    else {
                        progressBar.setVisibility(View.GONE);
                        showJobCompletedDialog();
                    }
                }
                else{
                    ApiError error = ErrorUtils.parseError(response);
                    Log.d(TAG + "Code", "" + response);
                    Snackbar.make(getView(),
                            error.getMessage(), Snackbar.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Long> call, Throwable t) {
                Log.e("OnFailure", "OnFailure" + t.toString());
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    private void showJobCompletedDialog() {
        FragmentTransaction ft= getActivity().getFragmentManager().beginTransaction();
        ft.addToBackStack(null);
        DialogFragment dialog= JobPostedDialog.newInstance();
        dialog.show(ft,"");
    }

    private void addJobImages(String imagePath, Long jobId, Long userId) {
        Log.e("Add_Job_Image","Add_Job_Image"+"  "+imagePath);

        //Create Upload Server Client
        ApiInterface service =   ApiClient.getClient().create(ApiInterface.class);

        //File creating from selected URL
        File file = new File(imagePath);

        // create RequestBody instance from file
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("file", file.getName(), requestFile);
        //User user= SharedPrefsHelper.getInstance().readUserPref();
        Log.e(TAG,"UploadJobImage"+body+" "+jobId+" "+userId);

        Call<Void> resultCall = service.uploadJobImage(body,jobId,userId);
        resultCall.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.d(TAG,"UploadImagesResponse"+response.body()+response);
                progressBar.setVisibility(View.GONE);
                if(response.isSuccessful()) {
                    showJobCompletedDialog();
                }
                else {
                    ApiError error = ErrorUtils.parseError(response);
                    Log.d(TAG + "Code", "" + response);
                    Snackbar.make(getView(),
                            error.getMessage(), Snackbar.LENGTH_SHORT).show();
                    progressBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.d(TAG,"Error"+t.toString());
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void processFinish(List<List<HashMap<String, String>>> result) {
        ArrayList<LatLng> points = null;
        PolylineOptions lineOptions = null;

        // Traversing through all the routes
        for (int i = 0; i < result.size(); i++) {
            points = new ArrayList<LatLng>();
            lineOptions = new PolylineOptions();
            List<HashMap<String, String>> path = result.get(i);
            for (int j = 0; j < path.size(); j++) {
                HashMap<String, String> point = path.get(j);
                if(j==0){
                    String distance =point.get("distance");
                    String[] dist=distance.split("km|m");
                    String totalDistance=dist[0];
                    //double distance_in_miles=Double.parseDouble(totalDistance)*0.621;
                    //double roundOff = Math.round(distance_in_miles * 100.0) / 100.0;
                    //location.setText(String.valueOf(roundOff)+" miles away");
                    location.setText(distance);
                    continue;}
                else if(j==1) {
                    //duration = (String)point.get("duration");
                    //timeText.setText(duration);
                    //Log.e("Time","Time"+duration);
                    continue;}
                double lat = Double.parseDouble(point.get("lat"));
                double lng = Double.parseDouble(point.get("lng"));
                LatLng position = new LatLng(lat, lng);
                points.add(position);}
            //Adding all the points in the route to LineOptions
            lineOptions.addAll(points);
            lineOptions.width(6);
            lineOptions.color(Color.rgb(65,199,88));
        }
        // Drawing polyline in the Google Map for the i-th route
        try{
        map.addPolyline(lineOptions);}
        catch (NullPointerException ex)
        {
            ex.printStackTrace();
        }
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }
}