package com.unoiatech.outofoptions.fragments.activeproposals;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.fragments.activejobs.proposalsfragment.ProposalDetailsChat;
import com.unoiatech.outofoptions.fragments.activejobs.proposalsfragment.UserInfoOnProposalDetails;
import com.unoiatech.outofoptions.model.Job;
import com.unoiatech.outofoptions.model.JobProposalData;
import com.unoiatech.outofoptions.model.Proposal;
import com.unoiatech.outofoptions.model.SearchListResponse;
import com.unoiatech.outofoptions.util.GPSTracker;

import java.util.ArrayList;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Unoiatech on 4/28/2017.
 */

public class JobDetails extends AppCompatActivity implements View.OnClickListener {
    ImageView proposal_info,user_info,chat_info;
    ArrayList<SearchListResponse> proposalDeatils;
    String latitude, longitude;
    JobProposalData proposalData;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.proposal_details_layout);
        TextView userName=(TextView)findViewById(R.id.name);
        proposal_info=(ImageView)findViewById(R.id.proposal_info);
        user_info=(ImageView)findViewById(R.id.user_info);
        chat_info=(ImageView)findViewById(R.id.chat_info);

        /****ToolBar Components********/
        TextView toolBarTitle=(TextView)findViewById(R.id.title);
        LinearLayout help_button=(LinearLayout) findViewById(R.id.help_btn);
        proposalDeatils= new ArrayList<>();

        /*******To Hide Help Button****************/
       proposalData=(JobProposalData)getIntent().getExtras().getParcelable("proposal_data");
       if(proposalData.getViewId().equals("pending")) {
            toolBarTitle.setText(R.string.received_proposal_toolbar_title);
            help_button.setVisibility(View.GONE);
        }
        else {
            toolBarTitle.setText(R.string.active_text);
            help_button.setVisibility(View.VISIBLE);
            help_button.setOnClickListener(this);
        }
        userName.setText(proposalData.getSellerName());
        proposal_info.setBackgroundResource(R.drawable.info_green);
        user_info.setBackgroundResource(R.drawable.user_grey);
        chat_info.setBackgroundResource(R.drawable.chat_grey);

        proposal_info.setOnClickListener(this);
        user_info.setOnClickListener(this);
        chat_info.setOnClickListener(this);

        initialiseFirstFragment();
        getCurrentLocation();
    }

    private void getCurrentLocation() {
        GPSTracker gps = new GPSTracker(this);
        // check if GPS enabled
        if (gps.canGetLocation()) {
            latitude = String.valueOf(gps.getLatitude());
            longitude = String.valueOf(gps.getLongitude());
        } else {
            gps.showSettingsAlert();
        }
    }
    public String getLocation() {
        return latitude+":"+longitude;
    }

    private void initialiseFirstFragment() {
        Fragment f1 = new ProposalInfoFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, f1).commit();
        Bundle b1= new Bundle();
        b1.putParcelable("proposal_info",proposalData);
        f1.setArguments(b1);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.proposal_info:
                proposal_info.setBackgroundResource(R.drawable.info_green);
                user_info.setBackgroundResource(R.drawable.user_grey);
                chat_info.setBackgroundResource(R.drawable.chat_grey);

                InputMethodManager im1 =(InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
                im1.hideSoftInputFromInputMethod(v.getWindowToken(),0);
                initialiseFirstFragment();
                break;

            case R.id.user_info:
                proposal_info.setBackgroundResource(R.drawable.info_grey);
                user_info.setBackgroundResource(R.drawable.user_green);
                chat_info.setBackgroundResource(R.drawable.chat_grey);

                InputMethodManager im2 =(InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
                im2.hideSoftInputFromInputMethod(v.getWindowToken(),0);
                Fragment f2 = new UserInfoOnProposalDetails();
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.content_frame, f2)
                        .commit();
                Bundle b2= new Bundle();
                b2.putLong("userId",proposalData.getSellerId());
                f2.setArguments(b2);
                break;

            case R.id.chat_info:
                proposal_info.setBackgroundResource(R.drawable.info_grey);
                user_info.setBackgroundResource(R.drawable.user_grey);
                chat_info.setBackgroundResource(R.drawable.chat_green);

                InputMethodManager im3 =(InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
                im3.hideSoftInputFromInputMethod(v.getWindowToken(),0);
                Fragment f3 = new ProposalDetailsChat();
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.content_frame, f3)
                        .commit();
                break;
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}





