package com.unoiatech.outofoptions.fragments.menufragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.nfc.Tag;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.common.api.Api;
import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.api.ApiClient;
import com.unoiatech.outofoptions.api.ApiInterface;
import com.unoiatech.outofoptions.companymodule.registration.CompanyLocation;
import com.unoiatech.outofoptions.fragments.instantjob.SetJobLocation;
import com.unoiatech.outofoptions.model.User;
import com.unoiatech.outofoptions.util.GlobalVariables;
import com.unoiatech.outofoptions.util.SharedPrefsHelper;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Unoiatech on 2/21/2017.
 */
public class AddLocation extends Activity implements View.OnClickListener {
    private static String TAG="AddLocation";
    TextView home_address,workplace,workplace1;
    SharedPreferences preferences;
    String addressType;
    TextView title;
    private Long userId;
    RelativeLayout parentView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_location);
        parentView=(RelativeLayout)findViewById(R.id.parent_view);
        preferences= getSharedPreferences(GlobalVariables.USER_PREFS,MODE_PRIVATE);
        home_address=(TextView)findViewById(R.id.home_address);
        workplace=(TextView)findViewById(R.id.work_address1);
        workplace1=(TextView)findViewById(R.id.work_address2);
        Button submit=(Button)findViewById(R.id.submit);
        title=(TextView)findViewById(R.id.title);

        title.setText(getString(R.string.add_loc_text));
        home_address.setOnClickListener(this);
        workplace.setOnClickListener(this);
        workplace1.setOnClickListener(this);
        submit.setOnClickListener(this);

        //Get UserId From SharedPreference
        User users = SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readUserPref();
        userId = users.getId();
        //Fetch Address List from Server
        getListofAddress(userId);
    }

    private void getListofAddress(Long userId) {
        ApiInterface apiService= ApiClient.getClient().create(ApiInterface.class);
        Call<ArrayList<User>> call= apiService.getAddressList(userId);
        call.enqueue(new Callback<ArrayList<User>>() {
            @Override
            public void onResponse(Call<ArrayList<User>> call, Response<ArrayList<User>> response) {
                Log.d(TAG,"Response"+response.body());
                for(int i=0;i<response.body().size();i++){
                    if(response.body().get(i).getAddressType().equals("home address")){
                        home_address.setText(response.body().get(i).getName());
                    }else if(response.body().get(i).getAddressType().equals("work address 1")){
                       workplace.setText(response.body().get(i).getName());
                    }else if(response.body().get(i).getAddressType().equals("work address 2")){
                        workplace1.setText(response.body().get(i).getName());
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<User>> call, Throwable t) {
                Log.e(TAG,"Failure"+t.toString());
            }
        });
    }

    private void removePreferenceData() {
        SharedPreferences.Editor editer=preferences.edit();
        editer.remove("home");
        editer.remove("workplace");
        editer.remove("workplace1");
        editer.commit();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.home_address:
                Intent intent= new Intent(AddLocation.this,SetLocation.class);
                intent.putExtra("place","Home");
                startActivity(intent);
                break;
            case R.id.work_address1:
                Intent intent1= new Intent(AddLocation.this,SetLocation.class);
                intent1.putExtra("place","Workplace");
                startActivity(intent1);
                break;
            case R.id.work_address2:
                Intent intent2= new Intent(AddLocation.this,SetLocation.class);
                intent2.putExtra("place","Workplace1");
                startActivity(intent2);
                break;

            case R.id.submit:
                if(preferences.contains("home")||preferences.contains("workplace")||preferences.contains("workplace1")) {
                    if(preferences.contains("home")) {
                        if(home_address.getText().toString() != null || !home_address.getText().toString().equals("")) {
                            addressType = "home address";
                            String[] addparts = preferences.getString("home", "").split(":");
                            String lat = addparts[1];
                            String lng = addparts[2];
                            sendAddressToServer(addressType, home_address.getText().toString(), lat, lng);
                        }
                    }
                    else if(preferences.contains("workplace")){
                        if(workplace.getText().toString()!=null||!workplace.getText().toString().equals("")) {
                            addressType="work address 1";
                            String[] addparts=preferences.getString("workplace", "").split(":");
                            String lat=addparts[1];
                            String lng=addparts[2];
                            sendAddressToServer(addressType,workplace.getText().toString(),lat,lng);
                        }
                    }
                    else if(preferences.contains("workplace1")) {
                        if (workplace1.getText().toString() != null || !workplace1.getText().toString().equals("")) {
                            addressType = "work address 2";
                            String[] addparts = preferences.getString("workplace1", "").split(":");
                            String lat = addparts[1];
                            String lng = addparts[2];
                            sendAddressToServer(addressType, workplace1.getText().toString(), lat, lng);
                        }
                    }
                }
                else{
                    Snackbar.make(parentView,"Choose atleast one Address", Snackbar.LENGTH_SHORT)
                    .setAction("OK", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                        }
                    }).setActionTextColor(getResources().getColor(R.color.app_color)).show();
                }
                break;
        }
    }

    private void sendAddressToServer(final String addressType, final String address, String lat, String lng) {
        Log.e("SendAddressTo Server","SendAddressToServer"+" "+addressType);
        String addType;
        /*if(addressType.equals("work1")) {
            addType = "work address 2";
        }
        else {
            addType = addressType;
        }*/
        User user = new User();
        user.setAddressType(addressType);
        user.setLat(Double.valueOf(lat));
        user.setLng(Double.valueOf(lng));
        user.setName(address);

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<Void> call = apiService.addAddresses(userId, user);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.d("Data","Data"+response.toString());
                if (addressType.equals("home address")) {
                    if(preferences.contains("workplace")) {
                        String[] addparts = preferences.getString("workplace", "").split(":");
                        String lat = addparts[1];
                        String lng = addparts[2];
                        String type = "work address 1";
                        sendAddressToServer(type, workplace.getText().toString(), lat, lng);
                    } else if (preferences.contains("workplace1")) {
                        String[] addparts = preferences.getString("workplace1", "").split(":");
                        String lat = addparts[1];
                        String lng = addparts[2];
                        String type = "work address 2";
                        sendAddressToServer(type, workplace1.getText().toString(), lat, lng);
                    } else {
                        finish();
                    }
                } else if (addressType.equals("work address 1")) {
                    if (preferences.contains("workplace1")) {
                        String[] addparts = preferences.getString("workplace1", "").split(":");
                        String lat = addparts[1];
                        String lng = addparts[2];
                        String type = "work address 2";
                        sendAddressToServer(type, workplace1.getText().toString(), lat, lng);
                    }
                    else {
                        finish();
                    }
                }
                else {
                    finish();
                }
            }
            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.d("Failure","Failure"+t.toString());
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("OnResume","OnResume"+preferences.getString("workplace", ""));
        if(preferences.contains("home")) {
            if(preferences.getString("home", "").contains(":")){
                String[] addparts=preferences.getString("home", "").split(":");
                String placeName=addparts[0];
                home_address.setText(placeName);
            }
        }
       /*else{
            home_address.setText(getString(R.string.home_address_text));
       }*/
       if(preferences.contains("workplace")) {
            if(preferences.getString("workplace", "").contains(":")) {
                String[] addparts=preferences.getString("workplace", "").split(":");
                String placeName=addparts[0];
                workplace.setText(placeName);
            }
        }
       /* else{
            workplace.setText(getString(R.string.work_adress1_text));
        }*/
        if(preferences.contains("workplace1")) {
            if(preferences.getString("workplace1", "").contains(":")) {
                String[] addparts=preferences.getString("workplace1", "").split(":");
                String placeName=addparts[0];
                workplace1.setText(placeName);
            }
        }
       /*else{
            workplace1.setText(getString(R.string.work_adress2_text));
        }*/
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        removePreferenceData();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
