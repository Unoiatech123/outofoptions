package com.unoiatech.outofoptions.fragments.menufragments.paymentfragments;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;

import com.unoiatech.outofoptions.R;

/**
 * Created by Unoiatech on 9/25/2017.
 */
public class AddNewAccount extends DialogFragment implements View.OnClickListener{
    Dialog dialog;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.add_new_account,container,false);

        //Dialog components
        ImageView cancelImage=(ImageView)view.findViewById(R.id.cancel_img);
        Button addAccount=(Button)view.findViewById(R.id.add_account);
        cancelImage.setOnClickListener(this);
        return view;
    }

    public static DialogFragment newInstance() {
        AddNewAccount dialogFrag= new AddNewAccount();
        return dialogFrag;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.cancel_img:
                dialog.dismiss();
                break;
        }
    }
}
