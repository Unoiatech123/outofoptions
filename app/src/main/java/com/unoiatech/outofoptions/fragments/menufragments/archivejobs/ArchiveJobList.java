package com.unoiatech.outofoptions.fragments.menufragments.archivejobs;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.api.ApiClient;
import com.unoiatech.outofoptions.api.ApiInterface;
import com.unoiatech.outofoptions.fragments.activejobs.adapter.DataReceivedEvent;
import com.unoiatech.outofoptions.model.ActiveJobsResponse;
import com.unoiatech.outofoptions.model.RepostData;
import com.unoiatech.outofoptions.model.User;
import com.unoiatech.outofoptions.util.ApiError;
import com.unoiatech.outofoptions.util.BankId.ErrorUtils;
import com.unoiatech.outofoptions.util.DateTimeConverter;
import com.unoiatech.outofoptions.util.RecyclerItemClickListener;
import com.unoiatech.outofoptions.util.S3Utils;
import com.unoiatech.outofoptions.util.SharedPrefsHelper;
import com.unoiatech.outofoptions.api.URL;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.text.ParseException;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Unoiatech on 5/25/2017.
 */

public class ArchiveJobList extends Fragment {
    ArrayList<ActiveJobsResponse> data;
    ArrayList<ActiveJobsResponse> jobsList;
    MyAdapter listAdapter;
    String type="",username,imagePath;
    Long userId;
    RecyclerView recyclerView;
    TextView staticText;
    LinearLayout emptyView;

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.my_proposal_list_fragment,container,false);
        recyclerView=(RecyclerView) view.findViewById(R.id.my_jobs_recycler_view);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getParentFragment().getActivity());
        recyclerView.setLayoutManager(layoutManager);
        emptyView=(LinearLayout)view.findViewById(R.id.empty_view);
        staticText=(TextView)view.findViewById(R.id.static_text);
        data= new ArrayList<>();
        type=getArguments().getString("type");
        ArchiveJobs parentFragment=(ArchiveJobs) getParentFragment();
        data= parentFragment.getData();
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(),new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (type.equals("expired")) {
                    Intent intent = new Intent(getActivity(), ArchiveJobDetail.class);
                    RepostData data= new RepostData();
                    data.setTitle(jobsList.get(position).getJob().getTitle());
                    data.setDescription(jobsList.get(position).getJob().getDescription());
                    data.setBudget(jobsList.get(position).getJob().getBudget());
                    data.setTimeline(jobsList.get(position).getJob().getTimeline());
                    data.setLat(jobsList.get(position).getJob().getLat());
                    data.setLng(jobsList.get(position).getJob().getLng());
                    data.setPlace(jobsList.get(position).getJob().getPlace());
                    data.setPreCancellationHours(jobsList.get(position).getJob().getPreCancellationHours());
                    data.setWhoCanSendProposal(jobsList.get(position).getJob().getWhoCanSendProposal());
                    data.setUserId(jobsList.get(position).getJob().getUserId());
                    data.setId(jobsList.get(position).getJob().getId());
                    data.setJobStatus("Expired");
                    data.setImages(jobsList.get(position).getJob().getImages());
                    intent.putExtra("job_info",data);
                    startActivity(intent);
                }
                else if (type.equals("cancelled")) {
                    Intent intent = new Intent(getActivity(),ArchiveJobDetail.class);
                    RepostData data= new RepostData();
                    data.setTitle(jobsList.get(position).getJob().getTitle());
                    data.setDescription(jobsList.get(position).getJob().getDescription());
                    data.setBudget(jobsList.get(position).getJob().getBudget());
                    data.setTimeline(jobsList.get(position).getJob().getCancellationTime());
                    data.setLat(jobsList.get(position).getJob().getLat());
                    data.setLng(jobsList.get(position).getJob().getLng());
                    data.setPlace(jobsList.get(position).getJob().getPlace());
                    data.setPreCancellationHours(jobsList.get(position).getJob().getPreCancellationHours());
                    data.setWhoCanSendProposal(jobsList.get(position).getJob().getWhoCanSendProposal());
                    data.setUserId(jobsList.get(position).getJob().getUserId());
                    data.setId(jobsList.get(position).getJob().getId());
                    data.setJobStatus("Cancelled");
                    data.setImages(jobsList.get(position).getJob().getImages());
                    intent.putExtra("job_info",data);
                    startActivity(intent);
                }
            }
        }));
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setDataOnList();
        getUserProfile();
    }

    private void setDataOnList() {
        jobsList= new ArrayList<>();
        for(int i=0;i<data.size();i++) {
            /*if (data.get(i).getJob().getJobStatus().equals(type)||data.get(i).getJob().getIsExpired()==1) {
                Log.e(TAG,"IF"+data.get(i).getJob().getIsExpired()+" "+data.get(i).getJob().getJobStatus()+jobsList.size());
                jobsList.add(data.get(i));
            }*/
            if(type.equals("expired")) {
                if (data.get(i).getJob().getIsExpired()== 1) {
                    jobsList.add(data.get(i));
                }
            }
            else if(type.equals("cancelled")) {
                if(data.get(i).getJob().getJobStatus().equals("cancelled")) {
                    jobsList.add(data.get(i));
                }
            }
        }
        if(type.equals("expired")&&jobsList.size()==0){
            emptyView.setVisibility(View.VISIBLE);
            staticText.setVisibility(View.VISIBLE);
            staticText.setText(R.string.archive_expired_text);
        }
        else if(type.equals("cancelled")&&jobsList.size()==0){
            emptyView.setVisibility(View.VISIBLE);
            staticText.setVisibility(View.VISIBLE);
            staticText.setText(R.string.archive_cancelled_text);
        }
        else{
            emptyView.setVisibility(View.GONE);
            listAdapter = new MyAdapter(getActivity(),jobsList,type);
            recyclerView.setAdapter(listAdapter);}
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(ArchiveJobList.this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Subscribe
    public void onEvent(DataReceivedEvent event){
        setDataOnList();
    }

    private  class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
        private ArrayList<ActiveJobsResponse> archiveData;
        Activity activity;
        String type;

        // Provide a reference to the views for each data item
        // Complex data items may need more than one view per item, and
        // you provide access to all the views for a data item in a view holder
        public  class ViewHolder extends RecyclerView.ViewHolder {
            public TextView title, budget, proposals, dateToComplete, dueDate, time, name_text;
            ImageView status_image, time_image;
            CircleImageView profileImage;
            RelativeLayout profileLayout;

            public ViewHolder(View v) {
                super(v);
                title = (TextView) v.findViewById(R.id.title);
                budget = (TextView) v.findViewById(R.id.budget);
                dueDate = (TextView) v.findViewById(R.id.due_date);
                status_image = (ImageView) v.findViewById(R.id.status_image);
                profileLayout = (RelativeLayout) v.findViewById(R.id.profileLayout);
                name_text = (TextView) v.findViewById(R.id.name);
                profileImage = (CircleImageView) v.findViewById(R.id.profile_image);
            }
        }

        public MyAdapter(FragmentActivity activity, ArrayList<ActiveJobsResponse> dataList, String type) {
            this.activity = activity;
            archiveData = dataList;
            this.type = type;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.archived_list_items, parent, false);
            ViewHolder vh = new ViewHolder(v);
            return vh;
        }

        //Replace the contents of a view (invoked by the layout manager)
        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            if (type.equals("expired")) {
                if(archiveData.get(position).getJob().getIsExpired()==1) {
                    holder.title.setText(archiveData.get(position).getJob().getTitle());
                    holder.budget.setText("Quoted " + archiveData.get(position).getJob().getBudget() + " SEK");
                    holder.name_text.setText(username);
                    holder.status_image.setBackgroundResource(R.drawable.flag_new);
                    try {
                        if (imagePath.contains("\\")) {
                            String lastIndex = imagePath.replace("\\", " ");
                            String[] Stamp = lastIndex.split("\\s");
                            String timeStamp = Stamp[3];
                            String userImageUrl = URL.BASE_URL + "/users/download/" + userId + "/image/" + timeStamp;
                            Glide.with(getActivity()).load(userImageUrl).into(holder.profileImage);
                        } else {
                            Glide.with(getActivity()).load(S3Utils.getDownloadUserImageURL(userId, imagePath)).into(holder.profileImage);
                        }
                    }catch (NullPointerException ex)
                    {
                        ex.printStackTrace();
                    }
                    try {
                        holder.dueDate.setText("Expired On " + DateTimeConverter.getExpiredDateTime(archiveData.get(position).getJob().getTimeline()));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            }
            else if (type.equals("cancelled")) {
                if(archiveData.get(position).getJob().getJobStatus().equals("cancelled")) {
                    holder.title.setText(archiveData.get(position).getJob().getTitle());
                    holder.budget.setText("Quoted " + archiveData.get(position).getJob().getBudget() + " SEK");
                    holder.name_text.setText(username);
                    holder.status_image.setBackgroundResource(R.drawable.flag_new);
                    try {
                        if (imagePath.contains("\\")) {
                            String lastIndex = imagePath.replace("\\", " ");
                            String[] Stamp = lastIndex.split("\\s");
                            String timeStamp = Stamp[3];
                            String userImageUrl = URL.BASE_URL + "/users/download/" + userId + "/image/" + timeStamp;
                            Glide.with(getActivity()).load(userImageUrl).into(holder.profileImage);
                        } else {
                            Glide.with(getActivity()).load(S3Utils.getDownloadUserImageURL(userId, imagePath)).into(holder.profileImage);
                        }
                    }catch (NullPointerException  ex){
                        ex.printStackTrace();
                    }
                    try {
                        holder.dueDate.setText("Cancelled On " + DateTimeConverter.getExpiredDateTime(archiveData.get(position).getJob().getCancellationTime()));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        // Return the size of your dataset (invoked by the layout manager)
        @Override
        public int getItemCount() {
            return archiveData.size();
        }
    }
        public void getUserProfile() {
            User user= SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readUserPref();
            ApiInterface apiService= ApiClient.getClient().create(ApiInterface.class);
            Call<User> call= apiService.getUserProfile(user.getId());
            call.enqueue(new Callback<User>() {
                @Override
                public void onResponse(Call<User> call, Response<User> response) {
                    if(response.code()==200){
                        username = response.body().getUser().getFirstName()+" "+response.body().getUser().getLastName();
                        userId=response.body().getUser().getId();
                        imagePath=response.body().getUser().getImagePath();
                    }else{
                        ApiError error = ErrorUtils.parseError(response);
                        Snackbar.make(getView(),error.getMessage(),Snackbar.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<User> call, Throwable t) {
                    Log.d("Failure","Failure"+t.toString());
                }
            });
        }
}