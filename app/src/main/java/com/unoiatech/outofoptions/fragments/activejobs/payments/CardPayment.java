package com.unoiatech.outofoptions.fragments.activejobs.payments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.android.view.CardMultilineWidget;
import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.api.ApiClient;
import com.unoiatech.outofoptions.api.ApiInterface;
import com.unoiatech.outofoptions.model.AcceptProposal;
import com.unoiatech.outofoptions.model.Customer;
import com.unoiatech.outofoptions.model.Payment;
import com.unoiatech.outofoptions.util.ApiError;
import com.unoiatech.outofoptions.util.BankId.ErrorUtils;
import com.unoiatech.outofoptions.util.DateTimeConverter;
import com.wang.avi.AVLoadingIndicatorView;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Unoiatech on 9/1/2017.
 */

public class CardPayment extends AppCompatActivity implements View.OnClickListener {
    AlertDialog alertDialog ;
    CardMultilineWidget cardMultilineWidget;
    private static String TAG="CardPayment";
    private AVLoadingIndicatorView progressBar,progress;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.card_payment_layout);

        Button paymentBtn=(Button)findViewById(R.id.payment);
        progressBar=(AVLoadingIndicatorView)findViewById(R.id.progress_bar);

        //CardMultilineWidget for collecting Card Details
        cardMultilineWidget=(CardMultilineWidget)findViewById(R.id.multi_line_card);
        paymentBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.payment:
                getCardDetails(cardMultilineWidget);
                break;
          /*  case R.id.cancel_img:
                dialog.dismiss();
                break;*/
        }
    }

    private void getCardDetails(CardMultilineWidget cardMultilineWidget) {
        progressBar.setVisibility(View.VISIBLE);
        Card cardToSave = cardMultilineWidget.getCard();
        if (cardToSave == null) {
            //mErrorDialogHandler.showError("Invalid Card Data");
            //Handle later
            return;
        }
        else{
            Stripe stripe= new Stripe(getApplicationContext(),"pk_test_BOCER5yuGtUofbX0BMOZ0aHI");
            //Create Token from Card Object
            stripe.createToken(cardToSave, new TokenCallback() {
                @Override
                public void onError(Exception error) {
                    Log.e("OnError","OnError"+"  "+error.toString());
                    //Toast To Show Error
                    Toast.makeText(getApplicationContext(),error.toString(),Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onSuccess(Token token) {
                    Log.e("OnSuccess","OnSuccess"+"  "+token);
                    creatingCharges(token);
                    //Toast.makeText(getApplicationContext(),"Token Successfully Generated"+token.toString(),Toast.LENGTH_SHORT).show();
                }
            });
        }
        Log.e("GetCardDetails","GetCardDetail"+"  "+cardToSave+ " "+cardToSave.getNumber()+"  "+cardToSave.getCVC()+" "+cardToSave.getExpMonth()+" "+cardToSave.getExpYear());
    }

    private void creatingCharges(Token token) {
        //convert Amount into paise
        Bundle bundle= getIntent().getExtras();
        int amount= bundle.getInt("budget")*100;
        Log.e("Amount","Amount"+amount);

        //Set Customer Model Source value
        Customer customer= new Customer();
        customer.setSource(token.toString());

        //Model Class Payment
        Payment payment= new Payment();
        payment.setAmount(amount);
        payment.setUserId(bundle.getLong("userId"));
        payment.setTransactionId(bundle.getLong("transactionId"));
        payment.setCustomer(customer);

        Log.e("PaymentJson","PaymentJson"+"  "+token.toString()+"  "+payment.getCustomer().getSource()+ "  "+payment.getAmount());
        ApiInterface apiServices= ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call= apiServices.creatingCharges(payment);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.e(TAG,"Response"+ response.body()+" "+response.isSuccessful());
                if(response.isSuccessful()) {
                    transactionSuccessful();
                }
                else{
                    ApiError error = ErrorUtils.parseError(response);
                    Log.d(TAG+"Error Body",error.toString());
                    if(error.getMessage().equals("Already Paid")){
                        transactionSuccessful();
                    }else {
                        Snackbar.make(getWindow().getDecorView().getRootView(),
                                error.getMessage(), Snackbar.LENGTH_SHORT).show();
                    }
                }
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG,"Failure"+t.toString());
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    private void transactionSuccessful() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        View view1 = getLayoutInflater().inflate(R.layout.payment_success_layout, null);
        final TextView okayButton=(TextView)view1.findViewById(R.id.ok_btn);
        okayButton.setText(getString(R.string.ok_btn));
        progress=(AVLoadingIndicatorView)view1.findViewById(R.id.progress_bar);
        dialogBuilder.setView(view1);

        //set click listener on Okay Button
        okayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //alertDialog.dismiss();
                acceptProposal("accepted",getIntent().getExtras().getLong("proposalId"),okayButton);
            }
        });
        alertDialog = dialogBuilder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();
    }

    private void acceptProposal(String decision, Long proposalId, final TextView okayButton) {
        progress.setVisibility(View.VISIBLE);
        okayButton.setText("");
        final AcceptProposal proposal = new AcceptProposal();
        proposal.setProposalId(proposalId);
        proposal.setResponse(decision);
        proposal.setTime(DateTimeConverter.getTime());
        Log.e("AcceptProposal", "AcceptProposal");
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiService.acceptProposal(proposal);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d("Accept_Proposal", "Response" + "  " + response.body() + response);
                alertDialog.dismiss();
                if(response.isSuccessful()){
                    finish();
                }
                else{
                    ApiError error = ErrorUtils.parseError(response);
                    Log.d(TAG+"Error Body",error.toString());
                    Snackbar.make(getWindow().getDecorView().getRootView(),error.getMessage(), Snackbar.LENGTH_SHORT).show();
                }
                progress.setVisibility(View.GONE);
                okayButton.setText(getString(R.string.ok_btn));
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("Failure", "Failure" + t.toString());
                progress.setVisibility(View.GONE);
                okayButton.setText(getString(R.string.ok_btn));
                alertDialog.dismiss();
            }
        });
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
