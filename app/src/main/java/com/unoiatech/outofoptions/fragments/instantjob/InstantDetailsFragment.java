package com.unoiatech.outofoptions.fragments.instantjob;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.home.HomeScreenActivity;
import com.unoiatech.outofoptions.model.JobPostingDataHolder;
import com.unoiatech.outofoptions.util.SharedPrefsHelper;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Unoiatech on 3/6/2017.
 */

public class InstantDetailsFragment extends Fragment implements View.OnClickListener {
    private static final int REP_DELAY = 80 ;
    private OnViewChangeListener mlistener;
    private boolean mAutoIncrement = false;
    private boolean mAutoDecrement = false;
    private Handler repeatUpdateHandler = new Handler();
    int mValue=25;
    int oldvalue=0;
    private EditText budget,title,description;
    private RelativeLayout timeline;
    private String location_text;
    private Double lat,lng;
    private TextView duration,location,start_time,end_time,addBudget,decreaseBudget;
    DialogFragment dialog;
    static final int TIME_DIALOG_REQUEST=999;
    final String[] selectedValues = new String[1];

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mlistener = (OnViewChangeListener) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.instant_video_details_layout, container, false);

        initViews(view);

        /*****SlidingTAblayoutStrip***********/
        mlistener.showSecondView();

        /*****Set StartTime and EndTime***********/
        setStartingEndingTime();
        title.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId== EditorInfo.IME_ACTION_NEXT) {
                    //checkEnteredData(emailEditText,"Email address field cannot be empty","Please enter a valid email address");
                }
                return false;
            }
        });

        addBudget.setOnLongClickListener(new View.OnLongClickListener() {
               @Override
               public boolean onLongClick(View view) {
                    mAutoIncrement = true;
                    repeatUpdateHandler.post(new RptUpdater());
                    return false;
               }
        });
        addBudget.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if( (motionEvent.getAction()== MotionEvent.ACTION_UP || motionEvent.getAction()== MotionEvent.ACTION_CANCEL)
                        && mAutoIncrement ){
                    mAutoIncrement = false;
                }
                else {
                    mAutoIncrement = true;
                    repeatUpdateHandler.post(new RptUpdater());
                }
                return false;
            }
        });
        decreaseBudget.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                mAutoDecrement = true;
                repeatUpdateHandler.post(new RptUpdater());
                return false;
            }
        });
        decreaseBudget.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if( (motionEvent.getAction()== MotionEvent.ACTION_UP || motionEvent.getAction()== MotionEvent.ACTION_CANCEL)
                        && mAutoDecrement ){
                    mAutoDecrement = false;
                }
                else{
                    mAutoDecrement = true;
                    repeatUpdateHandler.post(new RptUpdater());
                }
                return false;
            }
        });

        //Set limit on Budget Edittext
        budget.addTextChangedListener(new TextWatcher(){
            public void afterTextChanged(Editable s) {
                try{
                    if (Integer.parseInt(budget.getText().toString())>50000) {
                        //budget.setEnabled(false);
                        String str=budget.getText().toString().substring(0,budget.getText().length()-1);
                        Log.e("Budget","Budget"+s+" "+budget.getText().toString()+"  "+budget.getText().length());
                        budget.setText(str);
                        Toast.makeText(getActivity(),"Maximum budget value 50000",Toast.LENGTH_SHORT).show();
                    }
                }catch (NumberFormatException ex){
                    ex.printStackTrace();
                }
            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after){}
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });

            /***********Scroll EditTExt Inside Scroll View***************/
        description.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (v.getId() == R.id.description) {
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });
        checkForDataRepost(title,budget,description);
        return view;
    }

    private void initViews(View view) {
        LinearLayout cancelImage = (LinearLayout) getActivity().findViewById(R.id.cancel_image);
        budget = (EditText) view.findViewById(R.id.budget);
        addBudget = (TextView) view.findViewById(R.id.add_budget);
        duration=(TextView)view.findViewById(R.id.duration);
        timeline=(RelativeLayout)view.findViewById(R.id.timeline);
        start_time=(TextView)view.findViewById(R.id.start_time);
        end_time=(TextView)view.findViewById(R.id.end_time);
        description=(EditText)view.findViewById(R.id.description);
        location=(TextView)view.findViewById(R.id.location);
        decreaseBudget = (TextView) view.findViewById(R.id.decrease_budget);
        title = (EditText) view.findViewById(R.id.title);
        //RelativeLayout main_layout=(RelativeLayout)view.findViewById(R.id.main_layout);
        FrameLayout nextButton = (FrameLayout) view.findViewById(R.id.next_button);
        FrameLayout backButton = (FrameLayout) view.findViewById(R.id.back_button);

        //Click Listeners
        cancelImage.setOnClickListener(this);
        backButton.setOnClickListener(this);
        nextButton.setOnClickListener(this);
        duration.setOnClickListener(this);
        timeline.setOnClickListener(this);
        location.setOnClickListener(this);
    }
/*
    private void disableEditText(EditText title) {
        title.setFocusable(false);
        title.setEnabled(true);
        title.setCursorVisible(true);
    }*/

    private void checkForDataRepost(EditText title, EditText budget, EditText description) {
            if (mlistener.getRepostData()!=null) {
                title.setText(mlistener.getRepostData().getTitle());
                description.setText(mlistener.getRepostData().getDescription());
                budget.setText(String.valueOf(mlistener.getRepostData().getBudget()));
                location.setText(mlistener.getRepostData().getPlace());
            }
    }

    private void notifyUser(String message){
        Snackbar snackbar = Snackbar.make(getView(), message, Snackbar.LENGTH_LONG);
        snackbar.setAction(getString(R.string.max_ok_btn), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
        snackbar.show();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onResume() {
        super.onResume();

        //get Location Frag data
        getUpdatedLocation();
    }

    private void getUpdatedLocation() {
        String place=(String) SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readJobLocationData("place");
        if(!place.isEmpty()) {
            location.setText(place);
            lat=(Double) SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readJobLocationData("lat");
            lng=(Double)SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readJobLocationData("lng");
        }
        else{
            InstantJobActivity activity;
            activity=(InstantJobActivity)getActivity();
            String[] parts =activity.showAddress().split(":");
            Log.e("GetAddress","GEtAddress"+"  "+parts.length);
            if(parts.length>0) {
                location_text = parts[0];
                Log.e("GetAddress", "GEtAddress" + "  " + parts + " " + parts[0]);
                lat = Double.valueOf(parts[1]);
                lng = Double.valueOf(parts[2]);
                location.setText(location_text);
            }
            else{
                notifyUser("Unable to get Location");
            }
        }
        Log.e("Updated_Location","Updated_Location"+place+" "+lat+" "+lng);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.cancel_image:
                Intent  intent=new Intent(getActivity(),HomeScreenActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
            case R.id.back_button:
                FragmentTabHost mhost = (FragmentTabHost) getActivity().findViewById(R.id.fragment_tabhost);
                mhost.setCurrentTab(0);
                mlistener.showSecondView();
                break;
            case R.id.next_button:
                checkJobDetails();
                break;
            case R.id.duration:
                showDurationDialog(getActivity());
                break;
            case R.id.timeline:
                if(duration.getText().toString().equals("visible for 8 hours")) {
                }
                else{
                    showTimeDialog();
                }
                break;
            case R.id.location:
                Intent intent1= new Intent(getActivity(),SetJobLocation.class);
                Bundle bundle= new Bundle();
                bundle.putDouble("lat",lat);
                bundle.putDouble("lng",lng);
                bundle.putString("location_name",location_text);
                intent1.putExtras(bundle);
                startActivity(intent1);
                break;
        }
    }

    private void checkJobDetails() {
        if (!budget.getText().toString().equals("") && !title.getText().toString().equals("") && !description.getText().toString().equals("")&& !location.getText().toString().equals("")) {
            JobPostingDataHolder dataHolderClass = JobPostingDataHolder.getInstance();
            dataHolderClass.setBudget(Integer.parseInt(budget.getText().toString()));
            dataHolderClass.setTitle(title.getText().toString());
            dataHolderClass.setDescription(description.getText().toString());
            if(duration.getText().toString().equals("visible for 3 days")){
                dataHolderClass.setDate("visible for 03 days");
            }else{
                dataHolderClass.setDate(duration.getText().toString());
            }
            dataHolderClass.setLocation(location.getText().toString());
            Double latnLongi= (Double) SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readJobLocationData("lat");
            if(!latnLongi.equals(0.0)){
                dataHolderClass.setLat((Double) SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readJobLocationData("lat"));
                dataHolderClass.setLng((Double) SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readJobLocationData("lng"));
            }
            else {
                dataHolderClass.setLat(lat);
                dataHolderClass.setLng(lng);
                Log.e("Else","LAtnLong"+lat+ " "+lng);
            }
            if(start_time.getVisibility()==View.VISIBLE){
                dataHolderClass.setStartingDate(start_time.getText().toString());
                dataHolderClass.setEndingDate(end_time.getText().toString());
            }
            FragmentTabHost mhost = (FragmentTabHost) getActivity().findViewById(R.id.fragment_tabhost);
            mhost.setCurrentTab(2);
            mlistener.hideSecondView();
            InputMethodManager inputManager = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow((null == getActivity().getCurrentFocus()) ? null : getActivity().getCurrentFocus().getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }
        else {
            notifyUser("Please enter the above Fields");
        }
    }

    class RptUpdater implements Runnable{
        public void run() {
            if (mAutoIncrement) {
                increment();
                repeatUpdateHandler.postDelayed(new RptUpdater(), REP_DELAY);
            } else if (mAutoDecrement) {
                decrement();
                repeatUpdateHandler.postDelayed(new RptUpdater(), REP_DELAY);}
        }
    }
    private void increment() {
        int n = 0;
        try{
        if(Integer.parseInt(budget.getText().toString())<50000) {
            n= Integer.valueOf(budget.getText().toString()) +25;
            mValue++;
            budget.setText( ""+n );
        }
        else {
            budget.setText("50000");
        }}
        catch (NumberFormatException ex){
            ex.printStackTrace();
        }
    }
    private void decrement() {
        try {
            if (Integer.valueOf(budget.getText().toString()) > 25) {
                int n = Integer.valueOf(budget.getText().toString()) - 25;
                mValue++;
                budget.setText("" + n);
            } else {
                budget.setText("25");
            }
        }
        catch (NumberFormatException ex){
            ex.printStackTrace();
        }
    }
 @RequiresApi(api = Build.VERSION_CODES.KITKAT)
 private void showDurationDialog(FragmentActivity activity) {
     View view= activity.getLayoutInflater().inflate(R.layout.select_timeline,null);
     final Dialog timelineDialog = new Dialog(getActivity(), R.style.Theme_Dialog);
     timelineDialog.setContentView(view);
     timelineDialog.setCancelable(false);
     timelineDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
     timelineDialog.getWindow().setGravity(Gravity.BOTTOM);
     timelineDialog.show();

     //Get Views from Timeline Dialog
     TextView cancel=(TextView) timelineDialog.findViewById(R.id.cancel);
     final NumberPicker picker=(NumberPicker)timelineDialog.findViewById(R.id.number_picker);
     TextView done=(TextView)timelineDialog.findViewById(R.id.done);

     // Customize Number picker Divider
     changeDividerColor(picker,getResources().getColor(R.color.background_color_of_line));

     //Initializing a new string array with elements
     final String[] values= getResources().getStringArray(R.array.timeline_selection_array);

     //timelineSelector.setMinValue(0); //from array first value
     Log.e("Data","Number_data"+oldvalue+"  "+picker.getValue()+" "+values.length);
        //Specify the maximum value/number of NumberPicker
     picker.setMaxValue(values.length-1); //to array last value

     picker.setValue(picker.getValue());
     //Specify the NumberPicker data source as array elements
     picker.setDisplayedValues(values);

     //Gets whether the selector wheel wraps when reaching the min/max value.
     picker.setWrapSelectorWheel(true);

     picker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
         @Override
         public void onValueChange(NumberPicker numberPicker, int oldval, int newVal) {
             selectedValues[0] =values[newVal];
             oldvalue=oldval;
             Log.e("Selected value : "," "+selectedValues+"  "+String.valueOf(oldval)+ " "+ newVal);
         }
     });
     done.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View view) {
             Log.e("Onclick","Onclick"+selectedValues[0]+" "+picker.getValue());
             try {
                 if (selectedValues[0].equals("visible for 8 hours")) {
                     showTimelineDialog();
                 }
             }catch (NullPointerException ex)
             {
                 ex.printStackTrace();
             }
             duration.setText(selectedValues[0]);
             setStartingEndingTime();
             timelineDialog.dismiss();
         }
     });
    cancel.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
           timelineDialog.dismiss();
        }
    });
  }

    private void changeDividerColor(NumberPicker picker, int color) {
        java.lang.reflect.Field[] pickerFields = NumberPicker.class.getDeclaredFields();
        for (java.lang.reflect.Field pf : pickerFields) {
            if (pf.getName().equals("mSelectionDivider")) {
                pf.setAccessible(true);
                try {
                    ColorDrawable colorDrawable = new ColorDrawable(color);
                    pf.set(picker, colorDrawable);
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (Resources.NotFoundException e) {
                    e.printStackTrace();
                }
                catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
    }

    private void setStartingEndingTime() {
        String currDate = new SimpleDateFormat("HH:mm").format(new Date());
        start_time.setText(currDate);
        Calendar cal = Calendar.getInstance(); // creates calendar
        cal.setTime(new Date()); // sets calendar time/date
        cal.add(Calendar.HOUR_OF_DAY, 8); // adds one hour
        String ending_time= new SimpleDateFormat("HH:mm").format(cal.getTime());
        end_time.setText(ending_time);
    }

    /****Notify user for the job visibility******/
    private void showTimelineDialog() {
        AlertDialog alertDialogBuilder = new AlertDialog.Builder(getActivity()).create();
        alertDialogBuilder.setMessage(getResources().getString(R.string.job_timeline_text));
        alertDialogBuilder.setButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alertDialogBuilder.show();
    }
    public void showTimeDialog() {
        dialog = SelectTimeDialog.newInstance();
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();

        //Add Data To sent
        Bundle bundle= new Bundle();
        bundle.putString("view","Job");
        dialog.setArguments(bundle);
        // specify a transition animation
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        // To make it fullscreen, use the 'content' root view as the container
        // for the fragment, which is always the root view for the activity
        // To bind data from DialogFragment
        dialog.setTargetFragment(this,TIME_DIALOG_REQUEST);
        transaction.add(R.id.main_layout, dialog, "time_picker")
                .addToBackStack(null).commit();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case TIME_DIALOG_REQUEST:
                if (resultCode == Activity.RESULT_OK) {
                    // here the part where I get my selected date from the saved variable in the intent of Select Time Dialog and the displaying it on Detail Fragment.
                    Bundle bundle = data.getExtras();
                    if(bundle.getString("startingtime")!=null) {
                        start_time.setText(bundle.getString("startingtime"));
                    }else{
                        start_time.setText(start_time.getText().toString());
                    }
                    if(bundle.getString("endingtime")!=null){
                        end_time.setText(bundle.getString("endingtime"));
                    }
                    else{
                        end_time.setText(end_time.getText().toString());
                    }
                }
                break;
            case 111:
                if (resultCode == Activity.RESULT_OK) {
                    start_time.setVisibility(View.GONE);
                    View time_view=(View)getView().findViewById(R.id.time_view);
                    time_view.setVisibility(View.GONE);
                    end_time.setText(data.getExtras().getString("decide_via_chat"));
                }
        }
    }
}
