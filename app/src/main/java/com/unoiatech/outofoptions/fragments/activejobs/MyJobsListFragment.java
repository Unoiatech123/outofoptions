package com.unoiatech.outofoptions.fragments.activejobs;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.fragments.activejobs.adapter.DataReceivedEvent;
import com.unoiatech.outofoptions.fragments.activejobs.adapter.MyJobsListAdapter;
import com.unoiatech.outofoptions.fragments.activejobs.proposalsfragment.ProposalDetails;
import com.unoiatech.outofoptions.fragments.activejobs.proposalsfragment.RecievedProposals;
import com.unoiatech.outofoptions.model.Job;
import com.unoiatech.outofoptions.model.ActiveJobsResponse;
import com.unoiatech.outofoptions.model.JobProposalData;
import com.unoiatech.outofoptions.model.Proposal;
import com.unoiatech.outofoptions.util.RecyclerItemClickListener;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import java.util.ArrayList;

/**
 * Created by unoiatech on 5/10/2016.
 */
public class MyJobsListFragment extends Fragment {
    ArrayList<ActiveJobsResponse> data;
    ArrayList<ActiveJobsResponse> jobsList;
    MyJobsListAdapter listAdapter;
    String type="";
    RecyclerView recyclerView;
    TextView staticText;
    LinearLayout emptyView;
    
    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.my_proposal_list_fragment,container,false);
        recyclerView=(RecyclerView) view.findViewById(R.id.my_jobs_recycler_view);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getParentFragment().getActivity());
        recyclerView.setLayoutManager(layoutManager);
        emptyView=(LinearLayout)view.findViewById(R.id.empty_view);
        staticText=(TextView)view.findViewById(R.id.static_text);

        data= new ArrayList<>();

        type=getArguments().getString("type");
        JobsFragment parentFragment=(JobsFragment)getParentFragment();
        data= parentFragment.getData();
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(),new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if(type.equals("pending")) {
                    Intent intent = new Intent(getActivity(), RecievedProposals.class);
                    intent.putExtra("view_id","pending");
                    Job job= jobsList.get(position).getJob();
                    intent.putExtra("jobId",job.getId());
                    intent.putExtra("jobTitle",job.getTitle());
                    startActivity(intent);
                }
                else if (type.equals("active")) {
                    Job job= jobsList.get(position).getJob();
                    Intent intent = new Intent(getActivity(),ProposalDetails.class);
                    JobProposalData data=new JobProposalData();
                    data.setViewId("Active");
                    data.setJobTitle(job.getTitle());
                    data.setJobId(job.getId());
                    data.setLat(job.getLat());
                    data.setLng(job.getLng());
                    data.setCategory(job.getCategory());
                    data.setUserId(job.getUserId());

                    //proposals data
                    ArrayList<Proposal> proposals=jobsList.get(position).getJob().getProposals();
                    for (Proposal items : proposals) {
                        if (items.getProposalStatus().equals("confirmed")||items.getProposalStatus().equals("completed")) {
                            data.setSellerName(items.getUser().getFirstName() + " " + items.getUser().getLastName());
                            data.setSellerId(items.getUser().getId());
                            data.setProposalId(items.getId());
                            data.setProposalStartingTime(items.getStartingTime());
                            data.setProposalDescription(items.getProposalDescription());
                            data.setOfferedQuote(items.getOfferedQuote());
                            Log.e("Active","Active"+items.getUser().getFirstName()+" "+items.getUser().getLastName());
                        }
                    }
                    intent.putExtra("job_data",data);
                    startActivity(intent);
                }
                /*else if (type.equals("completed")) {
                    MyJobsDataHolderClass obj = (new ArrayList<MyJobsDataHolderClass>(myJobsDataHolderClasses.keySet()).get(position));
                    List<ProposalDataHolder> proposals = (new ArrayList<List<ProposalDataHolder>>(myJobsDataHolderClasses.values()).get(position));
                    *//*Intent intent = new Intent(getActivity(), SingleProposal.class);
                    for (ProposalDataHolder items : proposals) {
                        if (items.getProposalStatus().equals("accepted")) {
                            intent.putExtra("price", items.getProposedQuote());
                            intent.putExtra("user_id", items.getProposedUserId());
                            intent.putExtra("job_id", obj.getJobId());
                            intent.putExtra("proposal_id", items.getProposalId());
                            intent.putExtra("offeredTime", obj.getCompletedDate());
                            intent.putExtra("View_Id", "Completed");
                        }
                    }
                    intent.putExtra("job_id",obj.getJobId());
                    intent.putExtra("Key","Proposal");
                    intent.putExtra("lat",obj.getLatitude());
                    intent.putExtra("lng",obj.getLongitude());
                    intent.putExtra("category",obj.getCategory());
                    intent.putExtra("title", obj.getTitle());
                    startActivity(intent);*//*
                }*/
            }
        }
        ));
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setDataOnList();
    }

    private void setDataOnList() {
        jobsList= new ArrayList<>();
        for(int i=0;i<data.size();i++) {
            if(data.get(i).getJob().getJobStatus().equals(type)&& data.get(i).getJob().getIsExpired()==0){
                jobsList.add(data.get(i));
            }
        }
        //(data.get(i).getJob().getJobStatus().equals(type)&& data.get(i).getJob().getIsExpired()== 0)
        if(type.equals("active")&&jobsList.size()==0){
            emptyView.setVisibility(View.VISIBLE);
            staticText.setVisibility(View.VISIBLE);
            staticText.setText(R.string.active_job_empty_text);
        }
        else if(type.equals("pending")&&jobsList.size()==0){
            emptyView.setVisibility(View.VISIBLE);
            staticText.setVisibility(View.VISIBLE);
            staticText.setText(R.string.pending_job_empty_text);
        }
        else if(type.equals("completed")&&jobsList.size()==0) {
            emptyView.setVisibility(View.VISIBLE);
            staticText.setVisibility(View.VISIBLE);
            staticText.setText(R.string.completed_job_empty_text);
        }
        else{
            emptyView.setVisibility(View.GONE);
            listAdapter = new MyJobsListAdapter(getActivity(),jobsList,type);
            recyclerView.setAdapter(listAdapter);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        //EventBus.getDefault().register(MyJobsListFragment.this);
    }

    @Override
    public void onStop() {
        super.onStop();
        //EventBus.getDefault().unregister(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(MyJobsListFragment.this);
    }

    @Subscribe
    public void onEvent(DataReceivedEvent event){
       setDataOnList();
    }
}
