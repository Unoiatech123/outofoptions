package com.unoiatech.outofoptions.fragments.instantjob.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.model.JobPostingDataHolder;

import java.util.ArrayList;

/**
 * Created by Unoiatech on 3/15/2017.
 */
public class CustomImageAdapter extends PagerAdapter
{
        Context mContext;
        LayoutInflater mLayoutInflater;
        private Uri[] images = JobPostingDataHolder.getInstance().getImageUri();
        ArrayList<String> list= new ArrayList<>();
        public CustomImageAdapter(FragmentActivity context) {
            mContext = context;
            mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            for(Uri uri:images) {
                String image=uri.getPath();
                list.add("content://media/"+image);
                Log.e("OnAdapter","OnAdpter"+" "+images.length+" "+list.size()+" "+image);}
        }

        @Override
        public int getCount() {
            return images.length;
        }
        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((LinearLayout) object);
        }
        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View itemView = mLayoutInflater.inflate(R.layout.preview_viewpager_item, container, false);
            ImageView imageView = (ImageView) itemView.findViewById(R.id.imageView);
           // imageView.setImageResource(list.indexOf(position));
            container.addView(itemView);
            Glide.with(imageView.getContext())
                    .load(list.get(position))
                   // .fitCenter()
                    .into(imageView);
            return itemView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((LinearLayout) object);
        }
}
