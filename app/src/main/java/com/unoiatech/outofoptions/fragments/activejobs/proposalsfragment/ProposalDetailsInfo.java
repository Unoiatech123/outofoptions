package com.unoiatech.outofoptions.fragments.activejobs.proposalsfragment;

import android.app.AlertDialog;
import android.app.DialogFragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.api.ApiClient;
import com.unoiatech.outofoptions.api.ApiInterface;
import com.unoiatech.outofoptions.fragments.activejobs.dialogs.Submit_Payment_Dialog;
import com.unoiatech.outofoptions.fragments.instantjob.AsynResponse;
import com.unoiatech.outofoptions.fragments.menufragments.searchfragments.SingleJobActivity;
import com.unoiatech.outofoptions.model.AcceptProposal;
import com.unoiatech.outofoptions.model.JobProposalData;
import com.unoiatech.outofoptions.util.DateTimeConverter;
import com.unoiatech.outofoptions.util.GetDirectionPath;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Unoiatech on 4/14/2017.
 */

public class ProposalDetailsInfo extends Fragment implements OnMapReadyCallback,AsynResponse,View.OnClickListener{
    private MapView mapView;
    private GoogleMap map;
    private TextView distance_text;
    private JobProposalData data;
    private AlertDialog alertDialog;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.proposal_detail_info_layout,container,false);
        TextView job_title=(TextView)view.findViewById(R.id.job_title);
        TextView price_quote=(TextView)view.findViewById(R.id.budget);
        TextView time_to_start=(TextView)view.findViewById(R.id.time_to_start);
        TextView proposal_description=(TextView)view.findViewById(R.id.proposal_description);
        mapView=(MapView)view.findViewById(R.id.map);
        Button response_button =(Button)view.findViewById(R.id.response_button);
        LinearLayout job_info=(LinearLayout) view.findViewById(R.id.info);
        LinearLayout proposal_view=(LinearLayout)view.findViewById(R.id.proposal_line_view);
        distance_text=(TextView)view.findViewById(R.id.distance);

        LinearLayout proposal_layout= (LinearLayout)view.findViewById(R.id.proposal_layout);
        TextView time_text=(TextView)view.findViewById(R.id.time_text_field);
        TextView budget_text=(TextView)view.findViewById(R.id.budget_text_field);
        ScrollView main_scrollview=(ScrollView)view.findViewById(R.id.main_scroll);
        ImageView transparentImage=(ImageView)view.findViewById(R.id.transparent_image);

        /***GetIntent Data*******/
        data= getArguments().getParcelable("job_info");

        job_title.setText(data.getJobTitle());
        price_quote.setText(data.getOfferedQuote()+ " SEK");
        try {
            time_to_start.setText(DateTimeConverter.getTimeToComplete(data.getProposalStartingTime()));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        /*****OnClick Listener********/
        response_button.setOnClickListener(this);
        job_info.setOnClickListener(this);

        initialiseMap(savedInstanceState);

        /******Changes at Runtime************/
        checkViewId(proposal_layout,proposal_view,time_text,budget_text,response_button,proposal_description,data);
        scrollMapView(main_scrollview,transparentImage);
        return view;
    }

    /*********Scroll MapView inside ScrollView************/
    private void scrollMapView(final ScrollView main_scrollview, ImageView transparentImage) {
        transparentImage.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        main_scrollview.requestDisallowInterceptTouchEvent(true);
                        // Disable touch on transparent view
                        return false;

                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        main_scrollview.requestDisallowInterceptTouchEvent(false);
                        return true;

                    case MotionEvent.ACTION_MOVE:
                        main_scrollview.requestDisallowInterceptTouchEvent(true);
                        return false;
                    default:
                        return true;
                }
            }
        });
    }

    private void checkViewId(LinearLayout proposal_layout, LinearLayout view, TextView time_text, TextView budget_text, Button response_button, TextView proposal_description, JobProposalData job) {
       if(job.getViewId().equals("Active")) {
            proposal_layout.setVisibility(View.GONE);
            time_text.setText(R.string.active_job_time);
            budget_text.setText(R.string.active_job_price);
            response_button.setText(R.string.active_job_response_btn);
        }
        else if(job.getViewId().equals("Completed")) {
            proposal_layout.setVisibility(View.GONE);
            time_text.setText(R.string.completed_job_time);
            budget_text.setText(R.string.completed_job_price);
            response_button.setVisibility(View.GONE);
        }
        else {
            proposal_layout.setVisibility(View.VISIBLE);
            view.setVisibility(View.VISIBLE);
            proposal_description.setText(job.getProposalDescription());
            time_text.setText(R.string.pending_job_time);
            budget_text.setText(R.string.pending_job_price);
            response_button.setText(R.string.accept_decline_btn);
        }
    }

    private void initialiseMap(Bundle savedInstanceState) {
        mapView.onCreate(savedInstanceState);
        mapView.onResume();
        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        mapView.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map=googleMap;

        /******Get Data************/
        String currentLocation=((ProposalDetails)getActivity()).getLocation();
        String[] location=currentLocation.split(":");
        String category=data.getCategory();
        Double lat,lng;
        lat=data.getLat();
        lng=data.getLng();

        LatLng start = new LatLng(Double.parseDouble(location[0]), Double.parseDouble(location[1]));
        LatLng end = new LatLng(lat,lng);
        map.moveCamera(CameraUpdateFactory.newLatLng(start));
        map.animateCamera(CameraUpdateFactory.zoomTo(12));
        map.addMarker(new MarkerOptions().position(start).icon(BitmapDescriptorFactory.fromResource(R.drawable.current_location)));
        if(category.equals("Animals sitting & Pet care"))
        {
            map.addMarker(new MarkerOptions().position(end).icon(BitmapDescriptorFactory.fromResource(R.drawable.animal_passing)));
        }
        else if(category.equals("Computer & tech fixes"))
        {
            map.addMarker(new MarkerOptions().position(end).icon(BitmapDescriptorFactory.fromResource(R.drawable.computer_and_networking)));
        }
        else if(category.equals("Ground & Construction"))
        {
            map.addMarker(new MarkerOptions().position(end).icon(BitmapDescriptorFactory.fromResource(R.drawable.ground_and_construction_gray)));
        }
        else if(category.equals("Carrying & Assembling"))
        {
            map.addMarker(new MarkerOptions().position(end).icon(BitmapDescriptorFactory.fromResource(R.drawable.carring_assemble)));
        }
        else if(category.equals("Health & Beauty"))
        {
            map.addMarker(new MarkerOptions().position(end).icon(BitmapDescriptorFactory.fromResource(R.drawable.minor_tech)));
        }
        else if(category.equals("Photoshoot"))
        {
            map.addMarker(new MarkerOptions().position(end).icon(BitmapDescriptorFactory.fromResource(R.drawable.photographer_green)));
        }
        else if(category.equals("Home & Gardening"))
        {
            map.addMarker(new MarkerOptions().position(end).icon(BitmapDescriptorFactory.fromResource(R.drawable.house_and_gardning)));
        }
        else if(category.equals("Car & Vehicles"))
        {
            map.addMarker(new MarkerOptions().position(end).icon(BitmapDescriptorFactory.fromResource(R.drawable.cleaning_green)));
        }
        else if(category.equals("All")){
            map.addMarker(new MarkerOptions().position(end).icon(BitmapDescriptorFactory.fromResource(R.drawable.cleaning_green)));
        }

        /*****Display Path********/
        if (lat!= null&lng!=null) {
            String sensor = "sensor=false";
            String source = "origin="+location[0]+ ","+ location[1];
            String destination = "destination="+lat +","+ lng;
            String parameters = source + "&" + destination + "&" + sensor;
            String output = "json";
            String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;
            GetDirectionPath downloadtask = new GetDirectionPath();
            downloadtask.response=this;
            downloadtask.execute(url);
        }
    }

    @Override
    public void processFinish(List<List<HashMap<String, String>>> result) {
        ArrayList<LatLng> points = null;
        PolylineOptions lineOptions = null;
        // Traversing through all the routes
        for (int i = 0; i < result.size(); i++) {
            points = new ArrayList<LatLng>();
            lineOptions = new PolylineOptions();
            List<HashMap<String, String>> path = result.get(i);
            for (int j = 0; j < path.size(); j++) {
                HashMap<String, String> point = path.get(j);
                if(j==0){
                    String distance =point.get("distance");
                    //String[] dist=distance.split("km|m");
                    //String totalDistance=dist[0];
                    Log.e("Distance","Distance"+distance);
                    distance_text.setText(distance);
                   /* double distance_in_miles=Double.parseDouble(totalDistance)*0.621;
                    double roundOff = Math.round(distance_in_miles * 100.0) / 100.0;
                    distanceText.setText(String.valueOf(roundOff)+" miles away");*/
                    continue;}
                else if(j==1) {
                    /*duration = (String)point.get("duration");
                    timeText.setText(duration);*/
                    continue;
                }
                double lat = Double.parseDouble(point.get("lat"));
                double lng = Double.parseDouble(point.get("lng"));
                LatLng position = new LatLng(lat, lng);
                points.add(position);}
            //Adding all the points in the route to LineOptions
            lineOptions.addAll(points);
            lineOptions.width(6);
            lineOptions.color(Color.rgb(65,199,88));
        }
        // Drawing polyline in the Google Map for the i-th route
        map.addPolyline(lineOptions);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.info:
                Intent intent= new Intent(getActivity(),SingleJobActivity.class);
                intent.putExtra("status","Detail");
                intent.putExtra("jobId",data.getJobId());
                intent.putExtra("userId",data.getUserId());
                startActivity(intent);
                Log.e("UserId","UserId"+data.getUserId());
                break;
            case R.id.response_button:
                 CheckViewStatus();
                break;
            case R.id.message:
                alertDialog.dismiss();
                ProposalDetails activity=(ProposalDetails)getActivity();
                activity.openChatFragment();
                break;
            //Accept Decline Proposal Dialog Components
            case R.id.cancel_img:
                alertDialog.dismiss();
                break;
            case R.id.accept:
                alertDialog.dismiss();
                submitPaymentDialog();
                break;
            case R.id.decline:
                alertDialog.dismiss();
                acceptProposal("rejected",data.getProposalId());
                Log.e("Proposal_Id","Proposal_Id"+" "+data.getProposalId());
                break;
        }
    }

    private void submitPaymentDialog() {
        FragmentTransaction ft= getActivity().getFragmentManager().beginTransaction();
        ft.addToBackStack(null);

        //Create and show the dialog.
        DialogFragment newFragment = Submit_Payment_Dialog.newInstance();
        Bundle bundle= new Bundle();
        bundle.putLong("userId",data.getUserId());
        bundle.putLong("jobId",data.getJobId());
        bundle.putInt("amount",data.getOfferedQuote());
        bundle.putLong("proposalId",data.getProposalId());
        newFragment.setArguments(bundle);
        newFragment.show(ft, "dialog");
        Log.e("Data_To_Sent","Data_to_sent"+data.getUserId()+"  "+data.getJobId()+"  "+data.getOfferedQuote());
    }

    private void CheckViewStatus() {
        if(data.getViewId().equals("Active")) {
            Intent intent = new Intent(getActivity(), FeedbackActivity.class);
            intent.putExtra("job_id", data.getJobId());
            intent.putExtra("view_id", "Job");
            intent.putExtra("proposal_id",data.getProposalId());
            intent.putExtra("seller_id",data.getSellerId());
            startActivity(intent);
        }
        else {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
            View view1 = getActivity().getLayoutInflater().inflate(R.layout.accept_decline_dialog, null);
            dialogBuilder.setView(view1);
            RelativeLayout cancel = (RelativeLayout) view1.findViewById(R.id.cancel_img);
            TextView accept = (TextView)view1.findViewById(R.id.accept);
            TextView decline = (TextView)view1.findViewById(R.id.decline);
            TextView message = (TextView)view1.findViewById(R.id.message);
            cancel.setOnClickListener(this);
            accept.setOnClickListener(this);
            decline.setOnClickListener(this);
            message.setOnClickListener(this);
            alertDialog = dialogBuilder.create();
            alertDialog.setCancelable(false);
            alertDialog.show();
        }
    }

    public void acceptProposal(String decision, long proposal_id) {
        AcceptProposal proposal= new AcceptProposal();
        proposal.setProposalId(proposal_id);
        proposal.setResponse(decision);
        proposal.setTime(DateTimeConverter.getTime());
        Log.e("AcceptProposal","AcceptProposal"+proposal.getProposalId()+" "+proposal.getResponse()+proposal.getTime());
        ApiInterface apiService= ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call= apiService.acceptProposal(proposal);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d("Accept_Proposal","Response"+"  "+response.body()+response);
                alertDialog.dismiss();
                getActivity().finish();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("Failure","Failure"+ t.toString());
            }
        });
    }
}

