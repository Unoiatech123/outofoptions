package com.unoiatech.outofoptions.fragments.activejobs.proposalsfragment;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.api.ApiClient;
import com.unoiatech.outofoptions.api.ApiInterface;
import com.unoiatech.outofoptions.model.Job;
import com.unoiatech.outofoptions.model.JobProposalData;
import com.unoiatech.outofoptions.model.Proposal;
import com.unoiatech.outofoptions.util.GPSTracker;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Unoiatech on 4/14/2017.
 */
public class ProposalDetails extends AppCompatActivity implements View.OnClickListener
{
    ImageView proposal_info,user_info,chat_info;
    String latitude, longitude;
    JobProposalData job;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.proposal_details_layout);
        TextView userName=(TextView)findViewById(R.id.name);
        proposal_info=(ImageView)findViewById(R.id.proposal_info);
        user_info=(ImageView)findViewById(R.id.user_info);
        chat_info=(ImageView)findViewById(R.id.chat_info);

        /****ToolBar Components********/
        TextView toolBarTitle=(TextView)findViewById(R.id.title);
        LinearLayout help_button=(LinearLayout) findViewById(R.id.help_btn);

        /*******Check To Hide Help Button****************/
        job=(JobProposalData)getIntent().getExtras().getParcelable("job_data");

        Log.e("Job_data","Job_data"+job.getViewId()+" "+job.getCategory());
        if(job.getViewId().equals("pending")) {
            toolBarTitle.setText(R.string.received_proposal_toolbar_title);
            help_button.setVisibility(View.GONE);
        }
        else {
            toolBarTitle.setText(R.string.active_text);
            help_button.setVisibility(View.VISIBLE);
            help_button.setOnClickListener(this);
        }

        userName.setText(job.getSellerName());
        proposal_info.setBackgroundResource(R.drawable.info_green);
        user_info.setBackgroundResource(R.drawable.user_grey);
        chat_info.setBackgroundResource(R.drawable.chat_grey);

        proposal_info.setOnClickListener(this);
        user_info.setOnClickListener(this);
        chat_info.setOnClickListener(this);

        initialiseFirstFragment();
        getCurrentLocation();
    }

    private void getCurrentLocation() {
        GPSTracker gps = new GPSTracker(this);
            // check if GPS enabled
        if (gps.canGetLocation()) {
            latitude = String.valueOf(gps.getLatitude());
            longitude = String.valueOf(gps.getLongitude());
        } else {
            gps.showSettingsAlert();
        }
    }
    public String getLocation() {
         return latitude+":"+longitude;
     }

    private void initialiseFirstFragment() {
        Fragment f1 = new ProposalDetailsInfo();
        getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, f1).commit();
        Bundle b1= new Bundle();
        b1.putParcelable("job_info",job);
        Log.e("Frag","Frag"+job);
        f1.setArguments(b1);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.proposal_info:
                proposal_info.setBackgroundResource(R.drawable.info_green);
                user_info.setBackgroundResource(R.drawable.user_grey);
                chat_info.setBackgroundResource(R.drawable.chat_grey);

                InputMethodManager im1 =(InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
                im1.hideSoftInputFromInputMethod(v.getWindowToken(),0);
                initialiseFirstFragment();
                break;

            case R.id.user_info:
                proposal_info.setBackgroundResource(R.drawable.info_grey);
                user_info.setBackgroundResource(R.drawable.user_green);
                chat_info.setBackgroundResource(R.drawable.chat_grey);

                InputMethodManager im2 =(InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
                im2.hideSoftInputFromInputMethod(v.getWindowToken(),0);
                Fragment f2 = new UserInfoOnProposalDetails();
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.content_frame, f2)
                        .commit();
                Bundle b2= new Bundle();
                b2.putLong("userId",job.getSellerId());
                f2.setArguments(b2);
                break;

            case R.id.chat_info:
                InputMethodManager im3 =(InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
                im3.hideSoftInputFromInputMethod(v.getWindowToken(),0);
                openChatFragment();
                break;
            
            case R.id.help_btn:
                showDialog();
                break;
        }
    }

    public void openChatFragment() {
        proposal_info.setBackgroundResource(R.drawable.info_grey);
        user_info.setBackgroundResource(R.drawable.user_grey);
        chat_info.setBackgroundResource(R.drawable.chat_green);

        Fragment f3 = new ProposalDetailsChat();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_frame, f3)
                .commit();

    }

    private void showDialog(){
        final Dialog helpDialog= new Dialog(this,R.style.DialogTheme);
        helpDialog.setContentView(R.layout.help_dialog);
        helpDialog.show();

        Button cancel_job=(Button)helpDialog.findViewById(R.id.cancel);
        ImageView cancelDialog=(ImageView)helpDialog.findViewById(R.id.cancel_img);
        cancel_job.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                helpDialog.dismiss();
                //markJobCancel(bundle.getLong("job_id"),bundle.getLong("proposal_id"));
            }
        });
        cancelDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                helpDialog.dismiss();
            }
        });
    }

    /*private void markJobCancel(Long id, Long proposalId){
        Job job= new Job();
        job.setTime(System.currentTimeMillis());
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiService.cancelActiveJob(id,proposalId,job);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d("Details", "Response" + response);
                finish();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("Details", "Error" + t.toString());
            }
        });
    }*/

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
