package com.unoiatech.outofoptions.fragments.offerhelp;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TextView;

import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.api.ApiClient;
import com.unoiatech.outofoptions.api.ApiInterface;
import com.unoiatech.outofoptions.model.CompanyData;
import com.unoiatech.outofoptions.model.User;
import com.unoiatech.outofoptions.util.SharedPrefsHelper;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Unoiatech on 8/9/2017.
 */

public class ChooseStaffMembers extends AppCompatActivity {
    GridView gridView;
    private static String TAG="ChooseStaffMember";
    ArrayList<User> companyMembers;
    TextView noteText;
    EditText searchView;
    StaffGridAdapter adapter;
    ArrayList<String> selectedIds;
    ArrayList<String> selectedName;
    ArrayList<User> coordinator;
    ArrayList<String> ownerIds;
    ArrayList<String> coordinatorIds;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.choose_staff_member);

        //Action Bar components
        TextView mTitle= (TextView)findViewById(R.id.title);
        mTitle.setText(R.string.choose_member_title);

        selectedIds= new ArrayList<>();
        selectedName=new ArrayList<>();
        coordinator = new ArrayList<User>();
        companyMembers= new ArrayList<>();
        ownerIds=new ArrayList<>();
        coordinatorIds=new ArrayList<>();

        gridView=(GridView)findViewById(R.id.staff_members);
        noteText=(TextView)findViewById(R.id.note);
        searchView=(EditText)findViewById(R.id.search_view);

        //Set OnClick Listener on GridView
        if(getIntent().getStringExtra("status").equals("OfferHelp"))
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long l) {
                TextView tv =(TextView) view.findViewById(R.id.user_id);
                TextView name=(TextView)view.findViewById(R.id.user_name);
                int selectedIndex = adapter.selectedPositions.indexOf(position);
                if (selectedIndex > -1) {
                    adapter.selectedPositions.remove(selectedIndex);
                    ((GridItemView) view).display(false);
                    selectedIds.remove((String)tv.getText().toString());
                    selectedName.remove((String)name.getText().toString());
                }
                else {
                    adapter.selectedPositions.add(position);
                    ((GridItemView) view).display(true);
                    selectedIds.add((String)tv.getText().toString());
                    selectedName.add((String)name.getText().toString());
                }
            }
        });

        //Add TextWatcher on SearchView EditText
        searchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                Log.e("AfterTextChanged","AfterTextChanged");
                String charSearch= searchView.getText().toString();
                adapter.filter(charSearch);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e(TAG,"onResume"+getIntent().getLongExtra("companyId",0));

        //To Get Company Members
        if(getIntent().getStringExtra("status").equals("OfferHelp")){
            getCompanyMembers(getIntent().getLongExtra("companyId",0));
        }else{
            companyMembers=getIntent().getParcelableArrayListExtra("data");
            setGridItems(companyMembers);
            Log.e("Else","ELSe"+companyMembers.size());
        }
    }

    private void getCompanyMembers(final long companyId) {
        companyMembers.clear();
        ApiInterface apiService= ApiClient.getClient().create(ApiInterface.class);

        Call<CompanyData> call= apiService.getCompanyProfile(companyId);
        call.enqueue(new Callback<CompanyData>() {
            @Override
            public void onResponse(Call<CompanyData> call, Response<CompanyData> response) {
                Log.d(TAG,"Response"+response.body()+"Coordinator"+response.body().getCoordinator());
                if(response.isSuccessful()) {
                    User user = new User();
                    ArrayList<User> owner = new ArrayList<User>();
                    if (response.body().getOwner() != null) {
                        owner.add(response.body().getOwner());
                        ownerIds.add(String.valueOf(response.body().getOwner().getId()));
                    }
                    if (response.body().getCoordinator()!= null) {
                        coordinator.add(response.body().getCoordinator());
                        coordinatorIds.add(String.valueOf(response.body().getCoordinator().getId()));
                    }
                    if (owner.size() > 0)
                        companyMembers.addAll(owner);
                    if (coordinator.size() > 0)
                        companyMembers.addAll(coordinator);
                    if (response.body().getMembers().size() > 0)
                        companyMembers.addAll(response.body().getMembers());

                    if (companyMembers.size() > 0) {
                        setGridItems(companyMembers);
                    } else {
                        noteText.setVisibility(View.VISIBLE);
                        SpannableStringBuilder staticText = new SpannableStringBuilder(getString(R.string.static_text_on_staff_screen));
                        staticText.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.app_color)), 0, 5, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                        noteText.setText(staticText);
                    }
                    Log.e("Data_Size", "Data_Size" + companyMembers.size());
                }
            }
            @Override
            public void onFailure(Call<CompanyData> call, Throwable t) {
                Log.e(TAG,"OnFailure"+t.toString());
            }
        });
    }

    private void setGridItems(ArrayList<User> companyMembers) {
        adapter= new StaffGridAdapter(this,companyMembers,getIntent().getStringExtra("status"));
        gridView.setAdapter(adapter);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        Log.e("Back_press","Back_press"+selectedIds.containsAll(ownerIds)+ "  "+selectedIds.containsAll(coordinatorIds)+"  "+selectedIds.size()+coordinatorIds.size());
        if(selectedIds.size()>0) {
            if(selectedIds.size() > 1) {
                if (selectedIds.containsAll(ownerIds)) {
                    Log.e("Owner","Owner");
                    SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).saveStaffMember("member_ids", selectedIds);
                    SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).saveStaffMember("members_name",android.text.TextUtils.join(" , ", selectedName));
                    finish();
                }
                else if (coordinatorIds.size()>0) {
                    if(selectedIds.containsAll(coordinatorIds)) {
                        Log.e("Coordinator", "Coordinator");
                        SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).saveStaffMember("member_ids", selectedIds);
                        SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).saveStaffMember("members_name", android.text.TextUtils.join(" , ", selectedName));
                        finish();
                    }
                    else{
                        showAlert();
                    }
                }
                else {
                    showAlert();
                }
            }
            else {
                finish();
                SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).saveStaffMember("member_ids", selectedIds);
                SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).saveStaffMember("members_name",android.text.TextUtils.join(" , ", selectedName));
            }
        }
        else {
            finish();
        }
        Log.e("Back_Press","Back_Press"+selectedIds.size()+" "+selectedName.size());
    }

    private void showAlert() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        View view1 = getLayoutInflater().inflate(R.layout.custom_toast_layout, null);
        dialogBuilder.setView(view1);
        TextView ok_btn = (TextView) view1.findViewById(R.id.okay_btn);
        TextView msgText=(TextView)view1.findViewById(R.id.message);
        msgText.setText(getString(R.string.selected_member_msg));
        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();
        ok_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                adapter.notifyDataSetChanged();
                selectedIds.clear();
                selectedName.clear();
                SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).removePostedJobLocationData();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
