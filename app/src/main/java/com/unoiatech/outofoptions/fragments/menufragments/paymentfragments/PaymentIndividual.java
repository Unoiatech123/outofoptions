package com.unoiatech.outofoptions.fragments.menufragments.paymentfragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.common.api.Api;
import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.api.ApiClient;
import com.unoiatech.outofoptions.api.ApiInterface;
import com.unoiatech.outofoptions.fragments.menufragments.ProfileScreen;
import com.unoiatech.outofoptions.home.HomeScreenActivity;
import com.unoiatech.outofoptions.model.Payment;
import com.unoiatech.outofoptions.model.SwishCustomer;
import com.unoiatech.outofoptions.model.User;
import com.unoiatech.outofoptions.util.ApiError;
import com.unoiatech.outofoptions.util.BankId.ErrorUtils;
import com.unoiatech.outofoptions.util.SharedPrefsHelper;

import org.w3c.dom.Text;

import de.measite.minidns.record.A;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by unoiaAndroid on 6/22/2017.
 */

public class PaymentIndividual extends Fragment implements View.OnClickListener{
    private static PaymentIndividual paymentIndividual;
    private SwishCustomer swishCustomer;
    User user;
    TextView balance,credit;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.payment_screen,container,false);

        //Action bar Title
        TextView title= (TextView)view.findViewById(R.id.title);
        title.setText(getString(R.string.payments));

        //payment individual components
        TextView connectSwish=(TextView)view.findViewById(R.id.connect_swish);
        TextView withdrawFunds=(TextView)view.findViewById(R.id.withdraw_funds);
        TextView earnings=(TextView)view.findViewById(R.id.earning);
        TextView redeemText=(TextView)view.findViewById(R.id.redeem_text);
        LinearLayout mainLayout=(LinearLayout)view.findViewById(R.id.main_layout);
        balance=(TextView)view.findViewById(R.id.balance);
        credit=(TextView)view.findViewById(R.id.credit);

        //Set Listeners
        connectSwish.setOnClickListener(this);
        withdrawFunds.setOnClickListener(this);
        earnings.setOnClickListener(this);
        redeemText.setOnClickListener(this);
        
        user= SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readUserPref();

        //Get user Wallet
        getUserWallet(user.getId());
        return view;
    }

    private void getUserWallet(Long id) {
        ApiInterface apiService=ApiClient.getClient().create(ApiInterface.class);
        Call<Payment> call=apiService.getUserWallet(id);
        call.enqueue(new Callback<Payment>() {
            @Override
            public void onResponse(Call<Payment> call, Response<Payment> response) {
                Log.e("Response","Response"+" "+response.toString()+"  "+response.body()+"  "+response.isSuccessful());
                if(response.isSuccessful()){
                    Payment payment=response.body();
                    if(response.body()!=null) {
                        balance.setText(payment.getBalance().toString() + " SEK");
                    }else{
                        balance.setText("0 SEK");
                    }
                }
                else{
                    ApiError error=ErrorUtils.parseError(response);
                    notifyUser(error.getMessage());
                }
                //Method to get User Wallet
                getUserCredit(user.getId());
            }

            @Override
            public void onFailure(Call<Payment> call, Throwable t) {
                Log.e("Failure","Failure"+" "+t.toString());
            }
        });
    }

    private void getUserCredit(Long id) {
        ApiInterface apiService= ApiClient.getClient().create(ApiInterface.class);
        Call<Payment> call= apiService.getUserCredit(id);
        call.enqueue(new Callback<Payment>() {
            @Override
            public void onResponse(Call<Payment> call, Response<Payment> response) {
                Log.d("GetCredit","Response"+" "+response.body()+"   "+response.isSuccessful());
                if(response.isSuccessful()&&response.body()!=null){
                    credit.setText(response.body().getCreditBalance()+"");
                }else{
                    credit.setText("0");
                }
            }

            @Override
            public void onFailure(Call<Payment> call, Throwable t) {
                Log.e("Failure","Failure"+t.toString());
            }
        });
    }

    private void notifyUser(String message) {
        Snackbar.make(getView(), message, Snackbar.LENGTH_LONG).setAction(getString(R.string.max_ok_btn), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        }).setActionTextColor(getResources().getColor(R.color.app_color)).show();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.e("OnActivity","OnActivity");
    }

    @Override
    public void onResume() {
        super.onResume();
        HomeScreenActivity.showTabHost();

        //Method to Get Swish Customer
        getSwishCustomer(user.getId());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.connect_swish:
                Intent intent= new Intent(getActivity(),ConnectToSwish.class);
                Bundle bundle= new Bundle();
                if(swishCustomer!=null) {
                    bundle.putString("swish_number",swishCustomer.getSwishNumber());
                    intent.putExtras(bundle);
                }
                startActivity(intent);
                break;
            case R.id.withdraw_funds:
                Intent intent1= new Intent(getActivity(),WithdrawFunds.class);
                if(swishCustomer!=null){
                    intent1.putExtra("swish_number",swishCustomer.getSwishNumber());}
                else {
                    intent1.putExtra("swish_number","Not Connected");
                }
                intent1.putExtra("balance",balance.getText().toString());
                startActivity(intent1);
                break;
            case R.id.redeem_text:
                Intent intent2= new Intent(getActivity(),RedeemCreditToWallet.class);
                intent2.putExtra("credits",credit.getText().toString());
                getActivity().startActivity(intent2);
                break;
        }
    }

    public static PaymentIndividual getPaymentIndividual() {
        if(paymentIndividual==null){
            paymentIndividual= new PaymentIndividual();
        }
        return paymentIndividual;
    }

    @Override
    public void onPause() {
        super.onPause();
        HomeScreenActivity.removeTabHost();
    }

    private void getSwishCustomer(Long id) {
        Log.e("GetSwishCustomer","GetSwishCustomer");
        swishCustomer= new SwishCustomer();
        ApiInterface apiService= ApiClient.getClient().create(ApiInterface.class);
        Call<SwishCustomer> call= apiService.getSwishCustomer(id);
        call.enqueue(new Callback<SwishCustomer>() {
            @Override
            public void onResponse(Call<SwishCustomer> call, Response<SwishCustomer> response) {
                Log.e("Response","Response"+response.isSuccessful()+" "+response.body());
                if(response.isSuccessful()) {
                    swishCustomer=response.body();
                }
            }

            @Override
            public void onFailure(Call<SwishCustomer> call, Throwable t) {
                Log.e("Failure","Failure"+t.toString());
            }
        });
    }
}
