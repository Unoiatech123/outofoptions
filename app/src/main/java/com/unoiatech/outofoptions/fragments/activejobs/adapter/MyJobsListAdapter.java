package com.unoiatech.outofoptions.fragments.activejobs.adapter;

import android.app.Activity;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.iarcuschin.simpleratingbar.SimpleRatingBar;
import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.model.ActiveJobsResponse;
import com.unoiatech.outofoptions.model.Proposal;
import com.unoiatech.outofoptions.util.DateTimeConverter;

import java.text.ParseException;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Unoiatech on 3/4/2016.
 */

public class MyJobsListAdapter extends RecyclerView.Adapter<MyJobsListAdapter.ViewHolder> {
    private ArrayList<ActiveJobsResponse> data;
    Activity activity;
    String type;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView title,budget,proposals,dateToComplete,dueDate,time,name_text;
        ImageView status_image,time_image;
        CircleImageView profileImage;
        SimpleRatingBar rating_bar;
        RelativeLayout profileLayout;

        public ViewHolder(View v) {
            super(v);
            title=(TextView) v.findViewById(R.id.title);
            budget=(TextView) v.findViewById(R.id.budget);
            proposals=(TextView) v.findViewById(R.id.proposals);
            dateToComplete=(TextView) v.findViewById(R.id.date_to_complete);
            dueDate=(TextView) v.findViewById(R.id.due_date);
            time=(TextView) v.findViewById(R.id.time);
            status_image=(ImageView)v.findViewById(R.id.status_image);
            time_image=(ImageView)v.findViewById(R.id.time_image);
            rating_bar=(SimpleRatingBar)v.findViewById(R.id.rating);
            profileLayout=(RelativeLayout)v.findViewById(R.id.profileLayout);
            name_text=(TextView)v.findViewById(R.id.name);
            profileImage=(CircleImageView)v.findViewById(R.id.profile_image);}
    }

    public MyJobsListAdapter(FragmentActivity activity, ArrayList<ActiveJobsResponse> dataList, String type) {
        this.activity=activity;
        data = dataList;
        this.type=type;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.applied_jobs_recycler_view_item, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    //Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Log.e("OnBInd","OnBind"+" "+ data.get(position).getJob().getJobStatus());
        if(type.equals("active")) {
            holder.profileLayout.setVisibility(View.VISIBLE);
            holder.dateToComplete.setVisibility(View.VISIBLE);
            holder.time.setVisibility(View.VISIBLE);
            holder.time.setText(activity.getString(R.string.active_job_payment));
            holder.time_image.setBackgroundResource(R.drawable.sek_new);
            holder.title.setText(data.get(position).getJob().getTitle());
            holder.budget.setText(data.get(position).getJob().getBudget() + " SEK");
            for (Proposal value : data.get(position).getJob().getProposals()) {
                if (value.getProposalStatus().equals(activity.getString(R.string.active_confirm_text))){
                    holder.proposals.setText(value.getOfferedQuote() + " SEK");
                    try {
                        holder.dateToComplete.setText(activity.getString(R.string.active_by_text)+" " + DateTimeConverter.getTimeToComplete(value.getCompletionTime()));
                    } catch (ParseException e) {
                        Log.e("ParseException", e.toString());
                    }
                }
            }
            /*try {
                holder.dueDate.setText("Hired on " + DateTimeConverter.getTimeToComplete(data.get(position).getJob().getHiringTime()));
            } catch (ParseException e) {
                Log.e("ParseException", e.toString());
            }*/
            holder.status_image.setBackgroundResource(R.drawable.flag_new);
        }
        else if(type.equals("pending")){
            Log.e("Pending","Pending");
            holder.profileLayout.setVisibility(View.VISIBLE);
            holder.dateToComplete.setVisibility(View.VISIBLE);
            holder.time.setVisibility(View.VISIBLE);
            holder.title.setText(data.get(position).getJob().getTitle());
            holder.budget.setText(data.get(position).getJob().getBudget() + " SEK");
            holder.dateToComplete.setText(activity.getString(R.string.pending_expires_text)+" "+ DateTimeConverter.longMillisecondsToDate(data.get(position).getJob().getTimeline()));
            String[] parts = DateTimeConverter.compareDate(data.get(position).getJob().getTimeline()).split(":");
            String status = parts[0];
            String part2 = parts[1];
            holder.dueDate.setText(status);
            holder.time.setText(part2);
           // Log.e("part String", "" + status + proposal.size());
            setImages(holder, status);
            holder.time_image.setBackgroundResource(R.drawable.about);
            ArrayList<Proposal> proposals= new ArrayList<>();
                for(Proposal value:data.get(position).getJob().getProposals()){
                if(value.getProposalStatus().equals("pending")){
                    proposals.add(value);
                }
            }
            holder.proposals.setText(activity.getString(R.string.recieved_proposal_text)+" "+"( "+proposals.size()+" )");
            holder.time.setTag(holder);
        }
        else {
            Log.e("Else","Else");
            holder.profileLayout.setVisibility(View.VISIBLE);
            holder.name_text.setVisibility(View.VISIBLE);
            holder.profileImage.setVisibility(View.VISIBLE);
            holder.rating_bar.setVisibility(View.VISIBLE);
            holder.title.setText(data.get(position).getJob().getTitle());
            holder.budget.setText(data.get(position).getJob().getBudget() + " SEK");
           /* for (ProposalDataHolder value : proposal) {
                if (value.getProposalStatus().equals("accepted")) {
                    getProfile(value.getProposedUserId(),holder);
                    holder.proposals.setText("Paid " + value.getProposedQuote() + " SEK");
                }
            }*/
            try {
               holder.dueDate.setText(activity.getString(R.string.completed_proposal_text)+" " + DateTimeConverter.getTimeToComplete(data.get(position).getJob().getCompletedTime()));
            } catch (ParseException ex) {
                ex.printStackTrace();
            }
            try{
            //holder.rating_bar.setRating(Float.parseFloat(jobs.getStars()));
            holder.time_image.setBackgroundResource(R.drawable.about);
            holder.status_image.setBackgroundResource(R.drawable.flag);}
            catch (NullPointerException ex){
                holder.rating_bar.setVisibility(View.INVISIBLE);
                holder.time.setVisibility(View.VISIBLE);
                holder.time_image.setBackgroundResource(R.drawable.about);
                holder.status_image.setBackgroundResource(R.drawable.flag);
                holder.time.setText(activity.getString(R.string.waiting_for_feedback));
            }
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return data.size();
    }

    void  setImages(ViewHolder holder, String proposals_status) {
        if(proposals_status.equals(activity.getString(R.string.accepting_proposals))){
            holder.status_image.setBackgroundResource(R.drawable.accepted);
        }
        else {
            holder.status_image.setBackgroundResource(R.drawable.expired);
        }
    }
}