package com.unoiatech.outofoptions.fragments.menufragments;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TabHost;
import android.widget.TextView;

import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.api.ApiClient;
import com.unoiatech.outofoptions.api.ApiInterface;
import com.unoiatech.outofoptions.fragments.menufragments.searchfragments.ListFragment;
import com.unoiatech.outofoptions.fragments.menufragments.searchfragments.MapScreen;
import com.unoiatech.outofoptions.fragments.menufragments.searchfragments.SearchListFragment;
import com.unoiatech.outofoptions.home.fragment.filter.FilterDialogFragment;
import com.unoiatech.outofoptions.model.User;
import com.unoiatech.outofoptions.util.GPSTracker;
import com.unoiatech.outofoptions.util.SharedPrefsHelper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Unoiatech on 8/28/2017.
 */

public class SearchJobsScreen extends AppCompatActivity implements View.OnClickListener {
    private FragmentTabHost mTabHost;
    private static final String TAG = "SearchJobsScreen";
    Double latitude, longitude;
    private static SearchTabFragment searchFrag;
    FilterDialogFragment filterDialogFragment;
    public String emailVerified="completeProfile";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        Log.e(TAG, "OnCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_job_layout);
        final TextView filterButton = (TextView) findViewById(R.id.filter_button);

        mTabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);
        mTabHost.setup(this, getSupportFragmentManager(), android.R.id.tabcontent);
        mTabHost.getTabWidget().setDividerDrawable(null);

        /**Adding Fragments in Tab Host**/
        View view1 = customTabView(getLayoutInflater(), getString(R.string.map_text));
        TabHost.TabSpec tabSpec1 = mTabHost.newTabSpec("Tab1").setIndicator(view1);
        mTabHost.addTab(tabSpec1, MapScreen.class, null);

        View view2 = customTabView(getLayoutInflater(),getString(R.string.list_text));
        TabHost.TabSpec tabSpec3 = mTabHost.newTabSpec("Tab2").setIndicator(view2);
        mTabHost.addTab(tabSpec3, SearchListFragment.class, null);

        getSupportFragmentManager().addOnBackStackChangedListener(getListener());

        mTabHost.setCurrentTab(0);
        mTabHost.getTabWidget().getChildAt(0).setBackgroundResource(R.drawable.map_selected_drawable);

        /*******TabClickListener****************/
        mTabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String tabId) {
                int tab = mTabHost.getCurrentTab();
                if (tab == 0) {
                    if (filterDialogFragment != null) {
                        getSupportFragmentManager().popBackStack();
                    }
                    Log.e(TAG, "IF" + "  " + filterDialogFragment);
                    mTabHost.getTabWidget().getChildAt(tab).setBackgroundResource(R.drawable.map_selected_drawable);
                } else {
                    Log.e(TAG, "ELSE" + " " + filterDialogFragment);
                    if (filterDialogFragment != null)
                        getSupportFragmentManager().popBackStack();
                    mTabHost.getTabWidget().getChildAt(tab).setBackgroundResource(R.drawable.list_selected_drawable);
                }
            }
        });

        //Filter Button Cli8ck Listener
        filterButton.setOnClickListener(this);
        getLocation();

        // Check login user Account is Verified or Not
        User user= SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readUserPref();
        getVerifiedIds(user.getId());
    }

    private FragmentManager.OnBackStackChangedListener getListener() {
        FragmentManager.OnBackStackChangedListener result = new FragmentManager.OnBackStackChangedListener() {
            public void onBackStackChanged() {
                FragmentManager manager = getSupportFragmentManager();
                if (manager != null) {
                    Log.e("Manager_is_not_Null", "Manager_is_not_null");
                    filterDialogFragment = (FilterDialogFragment) manager.findFragmentByTag("filter");
                    if (filterDialogFragment != null)
                        filterDialogFragment.onResume();
                }
            }
        };
        return result;
    }

    private View customTabView(LayoutInflater inflater, String tabText) {
        View view = inflater.inflate(R.layout.tab_layout_for_search, null);
        TextView text = (TextView) view.findViewById(R.id.tab_text);
        text.setTextColor(getResources().getColorStateList(R.color.tab_text_color_on_search));
        text.setText(tabText);
        text.setBackgroundResource(android.R.color.transparent);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    public FragmentTabHost getFragmentTabHost() {
        return mTabHost;
    }

    public void getLocation() {
        GPSTracker gps = new GPSTracker(SearchJobsScreen.this);

        // check if GPS enabled
        if (gps.canGetLocation()) {
            latitude = gps.getLatitude();
            longitude = gps.getLongitude();
            Geocoder geocoder;
            List<Address> addresses = null;
            geocoder = new Geocoder(this, Locale.getDefault());
            try {
                addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            gps.showSettingsAlert();
        }
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public  String getIsEmailVerified(){
        return emailVerified;
    }

    public static SearchTabFragment getSearchFrag() {
        if (searchFrag == null) {
            searchFrag = new SearchTabFragment();
        }
        return searchFrag;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.filter_button:
                addFilterFragment();
                break;
        }
    }

    private void addFilterFragment() {
        FilterDialogFragment frag = (FilterDialogFragment) getSupportFragmentManager().findFragmentByTag("filter");
        try {
            if (frag == null)
                frag = FilterDialogFragment.newInstance();
            FragmentManager fragMan = getSupportFragmentManager();

            //add Data to Sent
            Bundle bundle = new Bundle();
            if (mTabHost.getCurrentTab() == 0) {
                bundle.putString("selected_tab", "Map");
            } else {
                bundle.putString("selected_tab", "List");
            }
            bundle.putString("view", "SearchJobs");
            frag.setArguments(bundle);

            // add Fragment A to the main linear layout
            FragmentTransaction fragTrans = fragMan.beginTransaction();
            fragTrans.add(android.R.id.tabcontent, frag, "filter");
            fragTrans.addToBackStack("filter");
            fragTrans.commit();
        }
        catch (IllegalStateException ex){
            ex.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        if(getSupportFragmentManager().getBackStackEntryCount()==1) {
        }
        else{
            super.onBackPressed();
        }
    }
    private void getVerifiedIds(Long id) {
        ApiInterface apiService= ApiClient.getClient().create(ApiInterface.class);
        Call<ArrayList<User>> call= apiService.verifyIdsOnProfile(id);
        call.enqueue(new Callback<ArrayList<User>>() {
            @Override
            public void onResponse(Call<ArrayList<User>> call, Response<ArrayList<User>> response) {
                Log.d("response","response"+" body" +response.body());
                showAccountVerification(response.body());
            }

            @Override
            public void onFailure(Call<ArrayList<User>> call, Throwable t) {
                Log.d("Failure","Failure"+t.toString());
            }
        });
    }
    public  void showAccountVerification(ArrayList<User> body) {
        for (User item : body) {
            if (item.getVerificationType().equals("email")) {
                emailVerified="profileCompleted";
                if(item.getIsVerified()==1){
                    emailVerified="verified";
                }
            }
            Log.e("Email","Emails"+"  "+emailVerified);
        }
    }
}