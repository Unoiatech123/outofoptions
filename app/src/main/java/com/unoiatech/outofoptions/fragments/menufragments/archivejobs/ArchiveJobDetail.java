package com.unoiatech.outofoptions.fragments.menufragments.archivejobs;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.home.fragment.CategoryDialogFragment;
import com.unoiatech.outofoptions.model.RepostData;
import com.unoiatech.outofoptions.util.DateTimeConverter;

import java.text.ParseException;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Unoiatech on 6/1/2017.
 */
public class ArchiveJobDetail extends AppCompatActivity {
    RepostData data;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.expired_job_detail);

        //Action bar tittle
        TextView title=(TextView)findViewById(R.id.title);

        TextView job_title=(TextView)findViewById(R.id.job_title);
        TextView job_budget=(TextView)findViewById(R.id.budget);
        TextView timeline=(TextView)findViewById(R.id.timeline);
        TextView description=(TextView)findViewById(R.id.job_description);
        Button repost_Button=(Button)findViewById(R.id.repost_button);

        //Set data from getIntent
        data=(RepostData) getIntent().getExtras().getParcelable("job_info");

        //actionbar title
        title.setText(data.getJobStatus());

        job_title.setText(data.getTitle());
        job_budget.setText(Integer.toString(data.getBudget()));
        description.setText(data.getDescription());
        try {
            timeline.setText(DateTimeConverter.getExpiredDateTime(data.getTimeline()));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        //Listener to Repost Job
        repost_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragment= new CategoryDialogFragment();
                Bundle bundle= new Bundle();
                bundle.putString("View_id","Repost");
                bundle.putParcelable("job_info",data);
                fragment.setArguments(bundle);
                FragmentTransaction ft=getSupportFragmentManager().beginTransaction();
                ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
                ft.replace(android.R.id.content,fragment).addToBackStack(null).commit();
                Log.e("Data_to_repost","Data_to_repost"+data.getImages()+" "+data.getTitle());
            }
        });
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
