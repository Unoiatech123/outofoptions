package com.unoiatech.outofoptions.fragments.menufragments.notificationscreen;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.api.ApiClient;
import com.unoiatech.outofoptions.api.ApiInterface;
import com.unoiatech.outofoptions.companymodule.CompanyStaff;
import com.unoiatech.outofoptions.model.User;
import com.unoiatech.outofoptions.model.NotificationResponse;
import com.unoiatech.outofoptions.util.ApiError;
import com.unoiatech.outofoptions.util.BankId.ErrorUtils;
import com.unoiatech.outofoptions.util.DateTimeConverter;
import com.unoiatech.outofoptions.util.RecyclerItemClickListener;
import com.unoiatech.outofoptions.util.S3Utils;
import com.unoiatech.outofoptions.util.SharedPrefsHelper;
import com.unoiatech.outofoptions.api.URL;

import java.text.ParseException;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Unoiatech on 5/8/2017.
 */

public class NotificationScreen extends Fragment implements View.OnClickListener,ShowFinishDialog {
    private static String TAG="NotificationScreen";
    Button notification_btn;
    static boolean isEnable;
    RecyclerView list_of_notification;
    ArrayList<NotificationResponse> data;
    int count;
    MyAdapter adapter;
    private LinearLayout staticText;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.notification_screen,container,false);

        /****Actionbar Components****/
        TextView mtitle=(TextView)view.findViewById(R.id.title);
        mtitle.setText(getResources().getString(R.string.notification_title));

        TextView notification_pref=(TextView)view.findViewById(R.id.notification_btn);
        notification_btn=(Button)view.findViewById(R.id.enable_disable_btn);
        staticText=(LinearLayout)view.findViewById(R.id.static_text);
        list_of_notification=(RecyclerView)view.findViewById(R.id.notification_data);

        LinearLayoutManager layoutManager= new LinearLayoutManager(getActivity());
        list_of_notification.setLayoutManager(layoutManager);

        notification_btn.setOnClickListener(this);
        notification_pref.setOnClickListener(this);
        checkNotificationStatus();
        setOnClickOnRecyclerView(list_of_notification);
        return view;
    }

    /*@Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notification_screen);

        *//****Actionbar Components****//*
        TextView mtitle=(TextView)findViewById(R.id.title);
        mtitle.setText(getResources().getString(R.string.notification_title));

        TextView notification_pref=(TextView)findViewById(R.id.notification_btn);
        notification_btn=(Button)findViewById(R.id.enable_disable_btn);

        list_of_notification=(RecyclerView)findViewById(R.id.notification_data);
        LinearLayoutManager layoutManager= new LinearLayoutManager(this);
        list_of_notification.setLayoutManager(layoutManager);

        notification_btn.setOnClickListener(this);
        notification_pref.setOnClickListener(this);
        checkNotificationStatus();
        setOnClickOnRecyclerView(list_of_notification);
    }*/

    private void setOnClickOnRecyclerView(RecyclerView list_of_notification) {
        list_of_notification.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Log.e("Click","Click"+data.get(position).getNotification().getActivity());
                if(data.get(position).getNotification().getActivity().equals("INVITE_USER")){
                    FragmentTransaction ft= getActivity().getFragmentManager().beginTransaction();
                    ft.addToBackStack(null);

                    // Show Company Invitation dialog.
                    DialogFragment acceptCompanyInvite = AcceptCompanyInvite.newInstance();
                    Bundle bundle = new Bundle();
                    bundle.putString("company_name",data.get(position).getUser().getCompany().getBusinessName() );
                    bundle.putString("company_location",data.get(position).getUser().getCompany().getLocationName());
                    bundle.putLong("company_id",data.get(position).getUser().getCompany().getId());
                    bundle.putLong("noti_id",data.get(position).getNotification().getId());
                    bundle.putString("logo",data.get(position).getUser().getCompany().getLogo());
                    acceptCompanyInvite.setArguments(bundle);
                    acceptCompanyInvite.show(ft, "dialog");
                }
                else if(data.get(position).getNotification().getActivity().equals("REVIEW_PROPOSAL")){
                    Intent intent = new Intent(getActivity(),ReviewProposal.class);
                    intent.putExtra("companyProposalId",data.get(position).getNotification().getCompanyProposalId());
                    intent.putExtra("jobTitle",data.get(position).getNotification().getJobTitle());
                    startActivity(intent);
                }
                else if(data.get(position).getNotification().getActivity().equals("USER_INVITE_ACCEPTED")){
                    Intent intent = new Intent(getActivity(),CompanyStaff.class);
                    Bundle bundle= new Bundle();
                    bundle.putLong("company_id",data.get(position).getNotification().getCompanyId());
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
                else if(data.get(position).getNotification().getActivity().equals("ACCEPTED_PROPOSAL")){
                    Intent intent = new Intent(getActivity(), JobDetailsOnNotification.class);
                    Bundle bundle= new Bundle();
                    bundle.putString("name",data.get(position).getUser().getFirstName()+" "+data.get(position).getUser().getLastName());
                    bundle.putLong("proposal_id",data.get(position).getNotification().getProposalId());
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
                else if(data.get(position).getNotification().getActivity().equals("ADDED_PROPOSAL")){
                    Intent intent = new Intent(getActivity(), ProposalDetailOnNotification.class);
                    Bundle bundle= new Bundle();
                    bundle.putString("name",data.get(position).getUser().getFirstName()+" "+data.get(position).getUser().getLastName());
                    bundle.putLong("proposal_id",data.get(position).getNotification().getProposalId());
                    intent.putExtras(bundle);
                    startActivity(intent);
                    Log.e("proposal_Id","ProposalId"+data.get(position).getNotification().getProposalId());
                }
                else if(data.get(position).getNotification().getActivity().equals("WITHDRAW_REQUEST_COMPLETED")){
                    Intent intent= new Intent(getActivity(),Payment_Screen.class);
                    startActivity(intent);
                }
            }
        }));
    }

    private void checkNotificationStatus() {
        if(SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).isNotificationEnable()){
            notification_btn.setBackgroundResource(R.drawable.dark_grey_ripple);
            notification_btn.setText(getResources().getString(R.string.notification_disable_text));
        }
        else{
            notification_btn.setBackgroundResource(R.drawable.rounded_ripple_effect);
            notification_btn.setText(getResources().getString(R.string.notification_enable_text));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        count=1;
        data= new ArrayList<>();
        //setDataOnList(data);
        getNotificationData(0);
    }

    private void getNotificationData(int index) {
        User user= SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readUserPref();
        ApiInterface apiService= ApiClient.getClient().create(ApiInterface.class);
        Call<NotificationResponse> call= apiService.getNotificationData(user.getId(),String.valueOf(index));
        call.enqueue(new Callback<NotificationResponse>()
        {
            @Override
            public void onResponse(Call<NotificationResponse> call, Response<NotificationResponse> response) {
                Log.d(TAG,"Response"+response+" "+response.message());
                if(response.isSuccessful()){
                for(int i=0;i<response.body().getContent().size();i++) {
                    NotificationResponse notificationResponse = new NotificationResponse();
                    if (response.body().getContent().get(i).getNotification().getIsRead() == "0") {
                        try {
                            notificationResponse.setUser(response.body().getContent().get(i).getUser());
                        } catch (NullPointerException ex) {
                            ex.printStackTrace();
                        }
                        Log.e("Is_Read", "Is_Read" + response.body().getContent().get(i).getNotification().getIsRead());
                        notificationResponse.setNotification(response.body().getContent().get(i).getNotification());
                        data.add(notificationResponse);
                    }
                }
                }
                else{
                    ApiError error= ErrorUtils.parseError(response);
                    Snackbar.make(getView(),error.getMessage(),Snackbar.LENGTH_SHORT).show();
                }
                setDataOnList(data);
            }
            @Override
            public void onFailure(Call<NotificationResponse> call, Throwable t) {
                Log.e(TAG,"Error"+t.toString());
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.enable_disable_btn:
                if(SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).isNotificationEnable()){
                    notification_btn.setBackgroundResource(R.drawable.rounded_ripple_effect);
                    notification_btn.setText(getResources().getString(R.string.notification_enable_text));
                    isEnable=false;
                    SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).saveNotificationStatus(isEnable);
                }
                else{
                    notification_btn.setBackgroundResource(R.drawable.dark_grey_ripple);
                    notification_btn.setText(getResources().getString(R.string.notification_disable_text));
                    isEnable=true;
                    SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).saveNotificationStatus(isEnable);
                }
                break;
            case R.id.notification_btn:
                Intent intent=new Intent(getActivity(),NotificationPrefScreen.class);
                startActivity(intent);
                break;
        }
    }

    public void setDataOnList(ArrayList<NotificationResponse> dataOnList) {
        Log.e("Notification_list_size","Notification_list_size"+" "+dataOnList.size());
        if(dataOnList.size()>0) {
            adapter = new MyAdapter(getActivity(), dataOnList);
            adapter.setLoadMoreListener(new MyAdapter.OnLoadMoreListener() {
                @Override
                public void onLoadMore() {
                    list_of_notification.post(new Runnable() {
                        @Override
                        public void run() {
                            loadMore(count);
                        }
                    });
                }
            });
            list_of_notification.setAdapter(adapter);
        }
        else{
            staticText.setVisibility(View.VISIBLE);
            list_of_notification.setVisibility(View.GONE);
        }
    }

    private void loadMore(final int index) {
        Log.e("Load_more","Load_more"+" "+index);
        data.add(new NotificationResponse("load"));
        adapter.notifyItemInserted(data.size() - 1);
        final User user = SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readUserPref();
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<NotificationResponse> call = apiService.getNotificationData(user.getId(), String.valueOf(index));
        call.enqueue(new Callback<NotificationResponse>() {
            @Override
            public void onResponse(Call<NotificationResponse> call, Response<NotificationResponse> response) {
                if (response.isSuccessful()) {
                    Log.e("load", "Response" + response.body().getContent().size()+"  "+user.getId());
                    data.remove(data.size() - 1);
                    if(response.body().getContent().size()>0){
                        count++;
                        for (int i = 0; i < response.body().getContent().size(); i++) {
                        NotificationResponse notificationResponse = new NotificationResponse();

                        try {
                            notificationResponse.setUser(response.body().getContent().get(i).getUser());
                            notificationResponse.setNotification(response.body().getContent().get(i).getNotification());
                        } catch (NullPointerException ex) {
                            ex.printStackTrace();
                        }
                        data.add(notificationResponse);
                        adapter.notifyDataSetChanged();
                    }
                    }
                    else{
                       adapter.setMoreDataAvailable(false);
                    }
                    adapter.notifyDataChanged();
                }
            }

            @Override
            public void onFailure(Call<NotificationResponse> call, Throwable t) {
                Log.e(TAG,"error"+t.toString());
            }
        });
    }

    @Override
    public void errorDialog(String errorMsg) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        View view1 = getActivity().getLayoutInflater().inflate(R.layout.custom_toast_layout, null);
        dialogBuilder.setView(view1);
        TextView ok_btn = (TextView) view1.findViewById(R.id.okay_btn);
        TextView msgText=(TextView)view1.findViewById(R.id.message);
        msgText.setText(errorMsg);
        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();
        ok_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
    }


    /******Notification Screen Adapter*****************/
    public static class MyAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
        public final int TYPE_CONTENT = 0;
        public final int TYPE_LOAD = 1;
        Activity activity;
        ArrayList<NotificationResponse> notificationData;
        OnLoadMoreListener loadMoreListener;
        boolean isLoading = false, isMoreDataAvailable = true;

        public MyAdapter(FragmentActivity activity, ArrayList<NotificationResponse> dataOnList) {
            this.notificationData=dataOnList;
            this.activity=activity;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater= LayoutInflater.from(activity);
            if(viewType==TYPE_CONTENT){
                return new ViewHolder(inflater.inflate(R.layout.notification_list_items,parent,false));
            }
            else{
                return new LoadHolder(inflater.inflate(R.layout.row_load,parent,false));
            }
        }
        @Override
        public int getItemViewType(int position) {
            try{
                if(notificationData.get(position).type.equals("movies")){
                    return TYPE_CONTENT;
                }
                else{
                    return TYPE_LOAD;
                }
            }
            catch (NullPointerException ex) {
                ex.printStackTrace();
            }
            return TYPE_CONTENT;
        }
        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            if (position >= getItemCount() - 1 && isMoreDataAvailable && !isLoading && loadMoreListener != null) {
                isLoading = true;
                loadMoreListener.onLoadMore();
            }
            if (getItemViewType(position) == TYPE_CONTENT) {
                ((ViewHolder) holder).bindData(notificationData.get(position));
            }
        }

        @Override
        public int getItemCount() {
            return notificationData.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder{
            public TextView name,notificationTitle,date;
            CircleImageView userImage;

            public ViewHolder(View v) {
                super(v);
                name=(TextView)v.findViewById(R.id.name);
                date=(TextView)v.findViewById(R.id.date);
                notificationTitle=(TextView)v.findViewById(R.id.notification_title);
                userImage=(CircleImageView)v.findViewById(R.id.profile_image);
            }

            public void bindData(NotificationResponse notificationResponse) {
                try {
                    date.setText(DateTimeConverter.getTimeToComplete(Long.valueOf(notificationResponse.getNotification().getTimeSent())));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                notificationTitle.setText(notificationResponse.getNotification().getMessage());
                try {
                    name.setText(notificationResponse.getUser().getFirstName() + " " + notificationResponse.getUser().getLastName());
                    if(notificationResponse.getUser().getImagePath().contains("\\")){
                        String lastIndex= notificationResponse.getUser().getImagePath().replace("\\", " ");
                        String[] Stamp=lastIndex.split("\\s");
                        String timeStamp=Stamp[3];
                        String userImageUrl = URL.BASE_URL+"/users/download/"+notificationResponse.getUser().getId()+"/image/"+timeStamp;
                        Glide.with(activity).load(userImageUrl).into(userImage);
                    }
                    else{
                        String image = S3Utils.getDownloadUserImageURL(notificationResponse.getUser().getId(), notificationResponse.getUser().getImagePath());
                        Glide.with(activity).load(image).into(userImage);}
                }
                catch (NullPointerException ex){
                    ex.printStackTrace();
                }
            }
        }
        class LoadHolder extends RecyclerView.ViewHolder{
            public LoadHolder(View itemView) {
                super(itemView);
            }
        }

        public void setMoreDataAvailable(boolean moreDataAvailable) {
            isMoreDataAvailable = moreDataAvailable;
        }
        public void notifyDataChanged(){
            notifyDataSetChanged();
            isLoading = false;
        }

        public interface OnLoadMoreListener{
            void onLoadMore();
        }

        public void setLoadMoreListener(OnLoadMoreListener loadMoreListener) {
            this.loadMoreListener = loadMoreListener;
        }
    }
}
