package com.unoiatech.outofoptions.fragments.menufragments.archivejobs;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.api.ApiClient;
import com.unoiatech.outofoptions.api.ApiInterface;
import com.unoiatech.outofoptions.model.DraftResponse;
import com.unoiatech.outofoptions.model.RepostData;
import com.unoiatech.outofoptions.model.User;
import com.unoiatech.outofoptions.util.ApiError;
import com.unoiatech.outofoptions.util.BankId.ErrorUtils;
import com.unoiatech.outofoptions.util.DateTimeConverter;
import com.unoiatech.outofoptions.util.RecyclerItemClickListener;
import com.unoiatech.outofoptions.util.S3Utils;
import com.unoiatech.outofoptions.util.SharedPrefsHelper;
import com.unoiatech.outofoptions.api.URL;

import java.text.ParseException;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Unoiatech on 6/8/2017.
 */

public class DraftJobFragment extends Fragment {
    RecyclerView recyclerView;
    LinearLayout emptyView;
    TextView staticText;
    ArrayList<DraftResponse> draftJob;
    MyAdapter listAdapter;
    String username, imagePath;
    Long userId;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.my_proposal_list_fragment, container, false);
        // recyclerView = (RecyclerView) view.findViewById(R.id.list_recycler_view);
        recyclerView = (RecyclerView) view.findViewById(R.id.my_jobs_recycler_view);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getParentFragment().getActivity());
        recyclerView.setLayoutManager(layoutManager);
        emptyView = (LinearLayout) view.findViewById(R.id.empty_view);
        staticText = (TextView) view.findViewById(R.id.static_text);
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent intent = new Intent(getActivity(), ArchiveJobDetail.class);
                RepostData data = new RepostData();
                data.setTitle(draftJob.get(position).getTitle());
                data.setDescription(draftJob.get(position).getDescription());
                data.setBudget(draftJob.get(position).getBudget());
                data.setTimeline(draftJob.get(position).getCreatedAt());
                data.setLat(draftJob.get(position).getLat());
                data.setLng(draftJob.get(position).getLng());
                data.setPlace(draftJob.get(position).getPlace());
                data.setUserId(draftJob.get(position).getUserId());
                data.setId(draftJob.get(position).getId());
                data.setJobStatus("Draft");
                data.setImages(draftJob.get(position).getDraftsImages());
                intent.putExtra("job_info", data);
                startActivity(intent);
                Log.e("JOb_Data", "Job_data" + data.getTimeline() + " ");
            }
        }));
        return view;
    }


    private void setAdapter(ArrayList<DraftResponse> draftJob) {
        if(draftJob.size()==0) {
            emptyView.setVisibility(View.VISIBLE);
            staticText.setVisibility(View.VISIBLE);
            staticText.setText(R.string.archived_draft_text);
        }
        else{
        listAdapter = new MyAdapter(getActivity(),draftJob);
        recyclerView.setAdapter(listAdapter);}
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getUserProfile();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
        private ArrayList<DraftResponse> archiveData;
        Activity activity;

        // Provide a reference to the views for each data item
        // Complex data items may need more than one view per item, and
        // you provide access to all the views for a data item in a view holder
        public class ViewHolder extends RecyclerView.ViewHolder {
            public TextView title, budget, proposals,dueDate, time, name_text;
            ImageView status_image;
            CircleImageView profileImage;
            RelativeLayout profileLayout;

            public ViewHolder(View v) {
                super(v);
                title = (TextView) v.findViewById(R.id.title);
                budget = (TextView) v.findViewById(R.id.budget);
                dueDate = (TextView) v.findViewById(R.id.due_date);
                status_image = (ImageView) v.findViewById(R.id.status_image);
                profileLayout = (RelativeLayout) v.findViewById(R.id.profileLayout);
                name_text = (TextView) v.findViewById(R.id.name);
                profileImage = (CircleImageView) v.findViewById(R.id.profile_image);
            }
        }

        public MyAdapter(FragmentActivity activity, ArrayList<DraftResponse> dataList) {
            this.activity = activity;
            archiveData = dataList;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.archived_list_items, parent, false);
            ViewHolder vh = new ViewHolder(v);
            return vh;
        }

        //Replace the contents of a view (invoked by the layout manager)
        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            holder.title.setText(archiveData.get(position).getTitle());
            holder.budget.setText("Quoted " + archiveData.get(position).getBudget() + " SEK");
            holder.name_text.setText(username);
            holder.status_image.setBackgroundResource(R.drawable.flag_new);
            if (imagePath.contains("\\")) {
                String lastIndex = imagePath.replace("\\", " ");
                String[] Stamp = lastIndex.split("\\s");
                String timeStamp = Stamp[3];
                String userImageUrl = URL.BASE_URL + "/users/download/" + userId + "/image/" + timeStamp;
                Glide.with(getActivity()).load(userImageUrl).into(holder.profileImage);
            } else {
                Glide.with(getActivity()).load(S3Utils.getDownloadUserImageURL(userId, imagePath)).into(holder.profileImage);
            }
            try {
                holder.dueDate.setText("Created On " + DateTimeConverter.getExpiredDateTime(archiveData.get(position).getCreatedAt()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        // Return the size of your dataset (invoked by the layout manager)
        @Override
        public int getItemCount() {
            return archiveData.size();
        }
    }

    public void getUserProfile() {
        User user= SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readUserPref();
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<User> call = apiService.getUserProfile(user.getId());
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.code() == 200) {
                    Log.e("Profile_Response","Profile_Response"+response.body());
                    username = response.body().getUser().getFirstName() + " " + response.body().getUser().getLastName();
                    userId = response.body().getUser().getId();
                    imagePath = response.body().getUser().getImagePath();

                    //Set Data on Recyclerview
                    displayDraftData();
                } else {
                    ApiError error = ErrorUtils.parseError(response);
                    Snackbar.make(getView(), error.getMessage(), Snackbar.LENGTH_SHORT).show();
                    Log.d("Code" + "Code", "" + response);
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Log.d("Failure", "Failure" + t.toString());
            }
        });
    }

    private void displayDraftData() {
        ArchiveJobs parentFragment=(ArchiveJobs) getParentFragment();
        draftJob= new ArrayList<>();
        draftJob= parentFragment.getDraftedData();
        setAdapter(draftJob);
    }
}
