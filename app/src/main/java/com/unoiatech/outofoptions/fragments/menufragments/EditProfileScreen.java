package com.unoiatech.outofoptions.fragments.menufragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.api.ApiClient;
import com.unoiatech.outofoptions.api.ApiInterface;
import com.unoiatech.outofoptions.model.Login;
import com.unoiatech.outofoptions.model.User;
import com.unoiatech.outofoptions.util.ApiError;
import com.unoiatech.outofoptions.util.BankId.ErrorUtils;
import com.unoiatech.outofoptions.util.GPSTracker;
import com.unoiatech.outofoptions.util.S3Utils;
import com.unoiatech.outofoptions.util.SharedPrefsHelper;
import com.unoiatech.outofoptions.util.common.activities.MyFileContentProvider;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Unoiatech on 10/19/2016.
 */
public class EditProfileScreen extends Activity implements View.OnClickListener {
    public static final int GET_FROM_GALLERY = 3;
    public static final int GET_FROM_CAMERA=2;
    String firstName,lastName,email;
    Long userId;
    Double latitude,longitude;
    Uri selectedImage;
    ImageView profile_Image;
    boolean isEdited;
    Button verifyEmail,verifyFacebook,verifyBankId;
    ImageView emailImage,fbImage,bankImage;
    private static String TAG="EditProfileScreen";
    EditText name,tagline,about;
    private CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_profile_screen);
        profile_Image=(ImageView)findViewById(R.id.profile_image);
        name=(EditText)findViewById(R.id.name);
        tagline=(EditText)findViewById(R.id.tagline);
        about=(EditText)findViewById(R.id.about);

        /*****Account Verification components******/
        verifyEmail=(Button)findViewById(R.id.verify_email_button);
        verifyFacebook=(Button)findViewById(R.id.verify_fb_button);
        verifyBankId=(Button)findViewById(R.id.verify_bank_id_button);

        emailImage=(ImageView)findViewById(R.id.email_image);
        fbImage=(ImageView)findViewById(R.id.facebook_image);
        bankImage=(ImageView)findViewById(R.id.bank_image);

        verifyEmail.setOnClickListener(this);
        verifyFacebook.setOnClickListener(this);
        verifyBankId.setOnClickListener(this);

        bankImage.setBackgroundResource(R.drawable.verified_bankid);
        setVerifiedImage(verifyBankId);

        /*********Initialize Facebook SDk*************/
        FacebookSdk.sdkInitialize(this);

        TextView save=(TextView)findViewById(R.id.edit);
        save.setVisibility(View.VISIBLE);

        TextView edit=(TextView)findViewById(R.id.edit_image);
        TextView title=(TextView)findViewById(R.id.title);
        title.setText(R.string.edit_profile_text);
        name.setCursorVisible(false);
        save.setText(R.string.edit_profile_save);

        /********Get data from Intent******************/
        Bundle bundle= getIntent().getExtras();

        userId=bundle.getLong("user_id");
        name.setText(bundle.getString("name"));
        tagline.setText(bundle.getString("tagline"));
        email=bundle.getString("emailAddress");
        if(bundle.getString("about").equals("null")){
            about.setHint(R.string.add_about_text);
        }
        else {
            about.setText(getIntent().getStringExtra("about"));
        }
        Log.e("Image","Image"+bundle.getString("image"));
        if(bundle.getString("image")!=null) {
            if (bundle.getString("image").contains("//")) {
                Glide.with(this).load(bundle.getString("image")).into(profile_Image);
            } else {
                Glide.with(this).load(S3Utils.getDownloadUserImageURL(userId, bundle.getString("image"))).into(profile_Image);
            }
        }

        for(String item:bundle.getStringArrayList("verifyid")){
            if (item.equals("email")){
                emailImage.setBackgroundResource(R.drawable.email);
                setVerifiedImage(verifyEmail);
            }
            if (item.equals("facebook")) {
                fbImage.setBackgroundResource(R.drawable.fb);
                setVerifiedImage(verifyFacebook);
            }
           /* if (item.equals("bankId")) {
                bankImage.setBackgroundResource(R.drawable.verified_bankid);
                setVerifiedImage(verifyBankId);
            }
            if (item.equals("phone")) {
                phoneImage.setBackgroundResource(R.drawable.verified_phone);
                setVerifiedImage(verifyPhone);
            }*/
        }
        getLocation();
        isEdited=false;

        /*******OnClickListeners************/
        save.setOnClickListener(EditProfileScreen.this);
        about.setOnClickListener(EditProfileScreen.this);
        edit.setOnClickListener(EditProfileScreen.this);

        name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
              Log.e(TAG,TAG+" "+"before_text_change");
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Log.e(TAG,TAG+" "+"onTextChanged");
                isEdited=true;
                name.setCursorVisible(true);
            }

            @Override
            public void afterTextChanged(Editable editable) {
                Log.e(TAG,TAG+" "+"afterTextChanged");
            }
        });
        tagline.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                tagline.setCursorVisible(true);
                isEdited=true;
                return false;
            }
        });
        about.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                about.setCursorVisible(true);
                isEdited=true;
                return false;
            }
        });
    }

    private void setVerifiedImage(Button verifiedBtn) {
        verifiedBtn.setBackgroundColor(getResources().getColor(android.R.color.transparent));
        verifiedBtn.setText(getString(R.string.verified_text));
        verifiedBtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.tick, 0, 0, 0);
        verifiedBtn.setTextColor(getResources().getColor(R.color.unselected_menu));
        verifiedBtn.setAllCaps(true);
        verifiedBtn.setTextColor(getResources().getColor(R.color.text_color));
    }

    private void getData() {
        String updatedname =name.getText().toString();
        if(name.getText().toString()!=null) {
            firstName = updatedname.substring(0, updatedname.indexOf(' '));
            lastName = updatedname.substring(updatedname.indexOf(' ') + 1);
        }
        else {
            Snackbar.make(getWindow().getDecorView(),getString(R.string.edit_name_text),Snackbar.LENGTH_LONG)
                .setAction(getString(R.string.max_ok_btn), new View.OnClickListener() {
                @Override
                public void onClick(View v) {
            }
        }).setActionTextColor(getResources().getColor(R.color.app_color)).show();
        }
    }

    private void getLocation() {
            GPSTracker gps = new GPSTracker(this);
            // check if GPS enabled
            if (gps.canGetLocation()) {
                latitude=gps.getLatitude();
                longitude= gps.getLongitude();
            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gps.showSettingsAlert();
            }
        }

    private void saveProfileData() {
        User user = new User();
        user.setDeviceId(SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).getFcmId());
        user.setDeviceType("android");
        user.setEmail(getIntent().getStringExtra("email"));
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setLat(latitude);
        user.setLng(longitude);
        user.setTagline(tagline.getText().toString());
        user.setAbout(about.getText().toString());
        ApiInterface apiService= ApiClient.getClient().create(ApiInterface.class);
        Call<Void> call= apiService.editUserProfile(userId,user);
        Log.e("posted Data","Posted Data"+"  "+userId+"  "+user.getFirstName()+" "+user.getLastName()+user.getDeviceType()+" "+user.getDeviceId()+"" +
                " "+user.getEmail()+ " "+user.getLat()+" "+user.getLng()+" "+user.getTagline()+ " "+user.getAbout());
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.d(TAG,"Response"+response.isSuccessful()+response.message());
                if(selectedImage!=null) {
                    saveUserProfileImage(userId,getRealPathFromURI(selectedImage));
                }else{
                    finish();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {

            }
        });
    }


    private void saveUserProfileImage(Long userId, String realPathFromURI) {
        File file= new File(realPathFromURI);

        // create RequestBody instance from file
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("uploaded_file", file.getName(), requestFile);
        Log.e("SaveImage",""+file.getName()+"  "+requestFile+"  "+body+" "+userId);
        ApiInterface apiService= ApiClient.getClient().create(ApiInterface.class);
        Call<Void> resultCall = apiService.uploadImage(body,userId);
        resultCall.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
               Log.e("Response","Image_REsponse"+response.toString());
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.e(TAG,"Failure"+" "+t.toString());
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.edit:
                InputMethodManager inputManager=(InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromInputMethod(v.getApplicationWindowToken(),0);
                getData();
                saveProfileData();
                break;
            case R.id.about:
                Intent intent= new Intent(EditProfileScreen.this,EditAboutScreen.class);
                intent.putExtra("about",about.getText().toString());
                startActivity(intent);
                break;
            case R.id.edit_image:
                InputMethodManager inputmanager=(InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
                inputmanager.hideSoftInputFromInputMethod(v.getApplicationWindowToken(),0);
                showDialog();
                break;
            case R.id.verify_email_button:
                Log.e("OnClick","OnClick"+" "+verifyEmail.getText().toString());
                if(verifyEmail.getText().toString().equals("Verify"))
                verifyEmail(email);
                break;
            case R.id.verify_bank_id_button:
                break;
            case R.id.verify_fb_button:
                if(verifyFacebook.getText().toString().equals("Verify"))
                AuthenticateUserViaFB();
                break;
        }
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getApplicationContext().getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    private void showDialog() {
        View view = getLayoutInflater().inflate (R.layout.add_images_dialog_layout, null);
        TextView fromGallery = (TextView)view.findViewById( R.id.gallery);
        TextView fromCamera = (TextView)view.findViewById( R.id.camera);
        TextView cancel=(TextView)view.findViewById(R.id.cancel);
        final Dialog mBottomSheetDialog = new Dialog(EditProfileScreen.this, R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView (view);
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.getWindow ().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow ().setGravity(Gravity.BOTTOM);
        mBottomSheetDialog.show();
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetDialog.dismiss();
            }
        });
        fromGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetDialog.dismiss();
                startActivityForResult(new Intent(Intent.ACTION_PICK,
                                MediaStore.Images.Media.INTERNAL_CONTENT_URI),
                        GET_FROM_GALLERY);
            }
        });
        fromCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PackageManager pm = getApplicationContext().getPackageManager();
                mBottomSheetDialog.dismiss();
                if (pm.hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
                    Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    i.putExtra(MediaStore.EXTRA_OUTPUT, MyFileContentProvider.CONTENT_URI);
                    startActivityForResult(i, GET_FROM_CAMERA);
                } else {
                    Toast.makeText(EditProfileScreen.this, "Camera is not available", Toast.LENGTH_LONG).show();}
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //Authenticate By Facebook
        try {
            if (callbackManager.onActivityResult(requestCode, resultCode, data)) {
                return;
            }
        }
        catch (NullPointerException ex){
            ex.printStackTrace();
        }
        //Detects request codes to Add Profile Image
        if (requestCode == GET_FROM_GALLERY && resultCode == Activity.RESULT_OK) {
            selectedImage = data.getData();
            Bitmap bitmap;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(EditProfileScreen.this.getContentResolver(), data.getData());
                profile_Image.setImageBitmap(bitmap);
            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        if (requestCode == GET_FROM_CAMERA && resultCode == Activity.RESULT_OK) {
            File out = new File(getApplicationContext().getFilesDir(), "newImage.jpg");
            if (!out.exists()) {
                Toast.makeText(EditProfileScreen.this,
                        "Error while capturing image", Toast.LENGTH_LONG)
                        .show();
                return;
            }
            Bitmap mBitmap = BitmapFactory.decodeFile(out.getAbsolutePath());
            selectedImage = getImageUri(getApplicationContext(), mBitmap);
            profile_Image.setImageBitmap(mBitmap);
        }
    }
    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(TAG, "OnResume");
        String aboutText=(String)SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).readJobLocationData("about_data");
        if (!aboutText.isEmpty()) {
            about.setText(aboutText);
        }else{
            about.setText(about.getText().toString());
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        SharedPrefsHelper.getInstance(SharedPrefsHelper.USER_INFO_PREF).removePostedJobLocationData();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void AuthenticateUserViaFB() {
        Log.e("Authenticate_User","Authenticate_User");
        callbackManager= CallbackManager.Factory.create();

        /***set Permissions***/
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile"));
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        Log.d("Success","Success");
                        GraphRequest.newMeRequest(
                                loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject json, GraphResponse response) {
                                        if (response.getError() != null) {
                                            // handle error
                                            System.out.println("ERROR");
                                        } else {
                                            System.out.println("Success");
                                            try {
                                                String jsonresult = String.valueOf(json);
                                                System.out.println("JSON Result"+jsonresult);
                                                String fb_id = json.getString("id");
                                                if(fb_id!= null){
                                                    verifyIdsViaFb(fb_id);
                                                }
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }
                                }).executeAsync();
                    }

                    @Override
                    public void onCancel() {
                        Log.d("On_cancel","On cancel");
                    }
                    @Override
                    public void onError(FacebookException error) {
                        Log.d("on_Error",error.toString());
                    }
                });
    }

    private void verifyIdsViaFb(String facebook_id) {
        final Login login= new Login();
        login.setIdValue(facebook_id);
        login.setIsVerified("0");
        login.setUserId(userId);
        login.setVerificationType(getString(R.string.fb_verification));

        ApiInterface apiInterface= ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiInterface.verifyIdViaFacebook(login);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody>call, Response<ResponseBody> response) {
                Log.d(TAG, "VerifiedFacebook" + response+" "+response.message()+login.getUserId());
                if(response.code()==200){
                    fbImage.setImageResource(R.drawable.fb);
                    setVerifiedImage(verifyFacebook);
                }
                else{
                    ApiError error=ErrorUtils.parseError(response);
                    Snackbar.make(getWindow().getDecorView(),error.getMessage(),Snackbar.LENGTH_LONG).show();
                }
            }
            @Override
            public void onFailure(Call<ResponseBody>call, Throwable t) {
                // Log error here since request failed
                Log.d(TAG, t.toString());}
        });
    }


    public void verifyEmail(String email) {
        Login login= new Login();
        login.setIdValue(email);
        login.setUserId(userId);
        login.setIsVerified("0");
        login.setVerificationType(getString(R.string.email_text));
        Log.e("VerIfy_EMAIL",""+login.getUserId()+" "+email);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<Void> call = apiService.verifyEmail(login);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void>call, Response<Void> response) {
                Log.d(TAG, "Verified" + response);
                if(response.isSuccessful()) {
                    emailImage.setImageResource(R.drawable.email);
                    setVerifiedImage(verifyEmail);
                }
                else{
                    ApiError error= ErrorUtils.parseError(response);
                    Snackbar.make(getWindow().getDecorView(),error.getMessage(),Snackbar.LENGTH_LONG).show();
                }
            }
            @Override
            public void onFailure(Call<Void>call, Throwable t) {
                Log.e(TAG, t.toString());
            }
        });
    }
}
