package com.unoiatech.outofoptions.fragments.activejobs.dialogs;


import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;

import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.api.ApiClient;
import com.unoiatech.outofoptions.api.ApiInterface;
import com.unoiatech.outofoptions.fragments.activejobs.payments.CardPayment;
import com.unoiatech.outofoptions.model.Payment;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Unoiatech on 9/7/2017.
 */

public class Submit_Payment_Dialog extends DialogFragment implements View.OnClickListener {
    Dialog dialog;
    private static final String TAG="SubmitPymentDialog";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.submit_payment_dialog_layout,container,false);

        ImageView cancel=(ImageView) view.findViewById(R.id.cancel);
        Button submitPayment=(Button)view.findViewById(R.id.submit_payment);

        //OnClick Listener
        cancel.setOnClickListener(this);
        submitPayment.setOnClickListener(this);
        return view;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    public static DialogFragment newInstance() {
        Submit_Payment_Dialog dialog= new Submit_Payment_Dialog();
        return dialog;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cancel:
                dialog.dismiss();
                break;
            case R.id.submit_payment:
                submitPaymentToEscrow();
                break;
        }
    }

    private void submitPaymentToEscrow() {
        Payment payment= new Payment();
        payment.setAmount(getArguments().getInt("amount"));
        payment.setJobId(getArguments().getLong("jobId"));
        payment.setUserId(getArguments().getLong("userId"));
        ApiInterface apiService= ApiClient.getClient().create(ApiInterface.class);
        Call<Payment> call= apiService.submitPaymentToO3Escrow(payment);
        call.enqueue(new Callback<Payment>() {
            @Override
            public void onResponse(Call<Payment> call, Response<Payment> response) {
                Log.e(TAG,"Response"+" "+response.isSuccessful()+" "+response.body().getTransactionId());
                if(response.isSuccessful()){
                    /*Fragment f1 = new ProposalDetailsInfo();
                    getChildFragmentManager().beginTransaction().replace(R.id.content_frame, f1).commit();
                    Bundle b1= new Bundle();
                    b1.putParcelable("job_info",job);
                    Log.e("Frag","Frag"+job);
                    f1.setArguments(b1);*/
                    dialog.dismiss();
                    /*FragmentTransaction ft= getActivity().getFragmentManager().beginTransaction();
                    ft.addToBackStack(null);

                    //Create and show the dialog.
                    DialogFragment newFragment = CardPayment.newInstance();
                    Bundle bundle= new Bundle();
                    bundle.putLong("transactionId",response.body().getTransactionId());
                    newFragment.setArguments(bundle);
                    newFragment.show(ft, "dialog");*/
                    Intent intent=  new Intent(getActivity(),CardPayment.class);
                    getActivity().finish();
                    Bundle bundle= new Bundle();
                    bundle.putLong("transactionId",response.body().getTransactionId());
                    bundle.putInt("budget",getArguments().getInt("amount"));
                    bundle.putLong("userId",getArguments().getLong("userId"));
                    bundle.putLong("proposalId",getArguments().getLong("proposalId"));
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
            }

            @Override
            public void onFailure(Call<Payment> call, Throwable t) {
                Log.e(TAG,"Failure"+t.toString());
                dialog.dismiss();
            }
        });
    }
}
