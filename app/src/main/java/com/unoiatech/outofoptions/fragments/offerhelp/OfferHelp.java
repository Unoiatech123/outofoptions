package com.unoiatech.outofoptions.fragments.offerhelp;


import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.unoiatech.outofoptions.R;
import com.unoiatech.outofoptions.model.Job;

/**
 * A simple {@link Fragment} subclass.
 */
public class OfferHelp extends DialogFragment implements View.OnClickListener {
    private Dialog dialog;
    private boolean individual=false;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.offer_help_dialog,container,false);
        Button individual_btn=(Button)view.findViewById(R.id.individual);
        Button company_btn=(Button) view.findViewById(R.id.company);
        TextView note_text=(TextView)view.findViewById(R.id.static_text);
        ImageView cancel_image=(ImageView)view.findViewById(R.id.cancel_image);

        SpannableString note=new SpannableString(getString(R.string.offer_help_dialog_static_text));
        note.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.app_color)), 0, 5, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        note_text.setText(note, TextView.BufferType.SPANNABLE);

        //Get Job Data
        Job job= getArguments().getParcelable("job_info");

        if(job.getWhoCanSendProposal().equals("1")){
            individual=true;
            note_text.setVisibility(View.GONE);
            individual_btn.setTextColor(getResources().getColor(R.color.app_color));
            individual_btn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.individual_proposal_selected, 0, 0, 0);
        }
        else{
            note_text.setVisibility(View.VISIBLE);
            individual_btn.setTextColor(getResources().getColor(R.color.unselected_menu));
            individual_btn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.individual_proposal,0,0,0);
        }

        if(individual){
            individual_btn.setOnClickListener(this);
        }else{
            individual_btn.setOnClickListener(null);
        }
        company_btn.setOnClickListener(this);
        cancel_image.setOnClickListener(this);
        return view;
    }

    public static OfferHelp newInstance() {
        OfferHelp frag=new OfferHelp();
        return frag;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.individual:
                dismiss();
                Intent intent=new Intent(getActivity(),OfferHelpScreen.class);
                intent.putExtra("job_info",getArguments().getParcelable("job_info"));
                intent.putExtra("user_type","Individual");
                startActivity(intent);
                break;
            case R.id.company:
                dismiss();
                if(getArguments().getString("user").equals("Not Registered")) {
                    showAlert(getString(R.string.register_user_msg));
                }
                else{
                    Intent intent1=new Intent(getActivity(),OfferHelpScreen.class);
                    intent1.putExtra("job_info",getArguments().getParcelable("job_info"));
                    intent1.putExtra("user_type",getArguments().getString("user"));
                    intent1.putExtra("companyId",getArguments().getLong("companyId"));
                    startActivity(intent1);
                }
                break;
            case R.id.cancel_image:
                dismiss();
                break;
        }
    }

    private void showAlert(String errorMsg) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        View view1 = getActivity().getLayoutInflater().inflate(R.layout.custom_toast_layout, null);
        dialogBuilder.setView(view1);
        TextView ok_btn = (TextView) view1.findViewById(R.id.okay_btn);
        TextView msgText=(TextView)view1.findViewById(R.id.message);
        msgText.setText(errorMsg);
        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();
        ok_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
    }
}
