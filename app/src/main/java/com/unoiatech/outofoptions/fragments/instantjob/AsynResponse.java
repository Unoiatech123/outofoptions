package com.unoiatech.outofoptions.fragments.instantjob;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Unoiatech on 3/27/2017.
 */

public interface AsynResponse
{
    void processFinish(List<List<HashMap<String, String>>> result);

}
