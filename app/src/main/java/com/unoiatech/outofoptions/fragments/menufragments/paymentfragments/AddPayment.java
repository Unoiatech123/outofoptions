package com.unoiatech.outofoptions.fragments.menufragments.paymentfragments;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.unoiatech.outofoptions.R;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Unoiatech on 9/25/2017.
 */

public class AddPayment extends Activity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_payment_account);

        //action bar components
        TextView title=(TextView)findViewById(R.id.title);
        title.setText(getString(R.string.payments));

        TextView addAccount=(TextView)findViewById(R.id.add_account);
        addAccount.setOnClickListener(this);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.add_account:
                showAddAccountDialog();
                break;
        }
    }

    private void showAddAccountDialog() {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        Fragment prev = getFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        // Create and show the dialog.
        DialogFragment dialogFragment = AddNewAccount.newInstance();
        dialogFragment.show(ft,"dialog");
    }
}
